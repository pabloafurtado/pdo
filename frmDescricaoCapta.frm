VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmDescricaoCapta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o da descri��o de produto usada pelo Capta/CAN"
   ClientHeight    =   4200
   ClientLeft      =   930
   ClientTop       =   2535
   ClientWidth     =   11280
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   11280
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Height          =   1965
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   11025
      Begin VB.TextBox txtDescrCom 
         Height          =   675
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1140
         Width           =   10725
      End
      Begin VB.TextBox txtDescrCAN 
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   420
         Width           =   6345
      End
      Begin VB.TextBox txtCodVenda 
         Height          =   315
         Left            =   180
         TabIndex        =   1
         Top             =   420
         Width           =   1245
      End
      Begin VB.Label lblDescrCom 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o Comercial"
         Height          =   195
         Left            =   150
         TabIndex        =   10
         Top             =   900
         Width           =   1455
      End
      Begin VB.Label lblDescrCan 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o CAN"
         Height          =   195
         Left            =   1590
         TabIndex        =   6
         Top             =   150
         Width           =   1095
      End
      Begin VB.Label lblCodVenda 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Venda"
         Height          =   195
         Left            =   150
         TabIndex        =   5
         Top             =   150
         Width           =   840
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   765
      Left            =   5880
      Picture         =   "frmDescricaoCapta.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3300
      Width           =   1005
   End
   Begin VB.CommandButton cmdSalvar 
      Caption         =   "Salvar"
      Height          =   765
      Left            =   4320
      Picture         =   "frmDescricaoCapta.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3300
      Width           =   1065
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   420
      Top             =   3600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":0614
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":092E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":0C48
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":0F62
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblManutDescCan 
      AutoSize        =   -1  'True
      Caption         =   "MANUTEN��O DA DESCRI��O DE PRODUTO USADA PELO CAPTA/CAN"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   2580
      TabIndex        =   8
      Top             =   840
      Width           =   6390
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   9720
      Top             =   3540
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":127C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":1596
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":18B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":1BCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":1EE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":21FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":2518
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCapta.frx":2832
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Salvar"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuCancelar 
         Caption         =   "&Cancelar"
         Shortcut        =   ^Q
      End
      Begin VB.Menu MNUBAR1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "Sai&r"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmDescricaoCapta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ProtegeTela()
        
    txtCodVenda.Text = ""
    txtDescrCAN.Text = ""
    txtDescrCom.Text = ""

    txtCodVenda.Enabled = True
    txtDescrCAN.Enabled = False
    txtCodVenda.SetFocus

End Sub
Private Sub DesprotegeTela()
        
    txtCodVenda.Enabled = False
    txtDescrCAN.Enabled = True
    txtDescrCAN.SetFocus

End Sub

Private Sub cmdCancelar_Click()
    
    If MsgBox("Confirma o cancelamento da altera��o?", vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
        ProtegeTela
    Else
        txtDescrCAN.SetFocus
    End If
    
End Sub

Private Sub cmdSalvar_Click()

    If txtDescrCAN.Text <> txtDescrCAN.Tag Then
       SalvaAlteracao txtCodVenda, txtDescrCAN
       MsgBox "Descri��o alterada com sucesso!", vbInformation, ""
       txtDescrCAN.Text = txtDescrCAN.Tag
    Else
       MsgBox "Altere a descricao para Salvar posteriormente!", vbInformation, ""
    End If
    
    ProtegeTela
    
End Sub

Private Sub Form_Activate()

    txtCodVenda.SetFocus
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    mdiPIO001.DesbloqueiaMenu

End Sub

Private Sub mnuSair_Click()
    Unload Me
End Sub

Private Sub mnuSalvar_Click()
    cmdSalvar_Click
End Sub

Private Sub txtCodVenda_KeyPress(KeyAscii As Integer)

    If KeyAscii <> 13 Then
        Exit Sub
    End If
    
        If txtCodVenda <> "" Then
            DesprotegeTela
            sCarregaDados txtCodVenda.Text, txtCodVenda, txtDescrCAN, txtDescrCom
        Else
            MsgBox "Digite o c�digo de venda do Produto que deseja pesquisar", vbCritical
        End If
        
    'End If
        
    txtDescrCAN.Tag = txtDescrCAN.Text
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
        Case 1
        Unload Me
    End Select
End Sub


Public Sub SalvaAlteracao(codvenda As Long, descrProd As String)

    Dim cSQL  As String
    
    On Error GoTo Err
    
    Dim sql_cd_ret As Long
    Dim sql_cd_opr As String
    Dim sql_nm_tab As String
    Dim sql_qt_lnh As Long
        
    cSQL = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSQL = cSQL & " exec stp_pdr_descricao_can_u " & codvenda & ",'" & descrProd & "', @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
     
    If gObjeto.natPrepareQuery(hsql, cSQL) Then
       Do While gObjeto.natFetchNext(hsql)
       Loop
    End If
     
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If
     
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar a stp_pdr_descricao_can_u " & "RETURN STATUS = " & sql_cd_ret & " "
       Exit Sub
    End If
    
Exit Sub

Err:
        MsgBox "Erro ao Salvar Altera��o.", vbCritical, "Aten��o"
        
End Sub


Public Sub sCarregaDados(codvenda As Long, txtCodVenda As Object, txtDescrCAN As Object, txtDescrCom As Object)

    'Executar a QUERY
    'Declara��o de Vari�veis
    
    Dim avData() As Variant
    Dim cSQL     As String
    Dim bResp    As Boolean
    
    On Error GoTo Err
    ReDim avData(2, 1)
    Dim sql_cd_ret As Long
    Dim sql_cd_opr As String
    Dim sql_nm_tab As String
    Dim sql_qt_lnh As Long
        
     cSQL = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
     cSQL = cSQL & " exec stp_pdr_descricao_can_s " & codvenda & ", @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
     
     If gObjeto.natPrepareQuery(hsql, cSQL) Then
        Do While gObjeto.natFetchNext(hsql)
        Loop
     End If
     bResp = gObjeto.natExecuteQuery(hsql, cSQL, avData())

     If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
     End If

     sql_cd_ret = CInt(gObjeto.varOutputParam(0))
     sql_cd_opr = gObjeto.varOutputParam(1)
     sql_nm_tab = gObjeto.varOutputParam(2)
     sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
     
     If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_descricao_can_s " & "RETURN STATUS = " & sql_cd_ret & " "
        Exit Sub
     End If
     
     txtDescrCAN = avData(0, 0)
       
     txtDescrCom = avData(1, 0)
     
Exit Sub

Err:
    MsgBox "ERRO", vbCritical
End Sub
