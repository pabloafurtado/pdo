VERSION 5.00
Object = "{CDF3B183-D408-11CE-AE2C-0080C786E37D}#2.0#0"; "EDT32X20.OCX"
Begin VB.Form frmCadLinhaProd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Linha de Produtos"
   ClientHeight    =   2880
   ClientLeft      =   645
   ClientTop       =   2025
   ClientWidth     =   7635
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmCadLinhaProd.frx":0000
   ScaleHeight     =   2880
   ScaleWidth      =   7635
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Height          =   1035
      Left            =   1770
      TabIndex        =   6
      Top             =   1800
      Width           =   4575
      Begin VB.CommandButton cmdSalvar 
         Caption         =   "Salvar"
         Enabled         =   0   'False
         Height          =   645
         Left            =   690
         Picture         =   "frmCadLinhaProd.frx":0542
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   270
         Width           =   1425
      End
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   645
         Left            =   2460
         Picture         =   "frmCadLinhaProd.frx":084C
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   270
         Width           =   1425
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1755
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7575
      Begin EditLib.fpLongInteger txtOrdem 
         Height          =   405
         Left            =   2145
         TabIndex        =   3
         Top             =   1155
         Width           =   915
         _Version        =   131072
         _ExtentX        =   1614
         _ExtentY        =   714
         _StockProps     =   68
         Enabled         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ButtonDisable   =   0   'False
         ButtonHide      =   0   'False
         ButtonIncrement =   1
         ButtonMin       =   0
         ButtonMax       =   100
         ButtonStyle     =   0
         ButtonWidth     =   0
         ButtonWrap      =   -1  'True
         ButtonDefaultAction=   -1  'True
         ThreeDText      =   0
         ThreeDTextHighlightColor=   -2147483633
         ThreeDTextShadowColor=   -2147483632
         ThreeDTextOffset=   1
         AlignTextH      =   2
         AlignTextV      =   1
         AllowNull       =   0   'False
         NoSpecialKeys   =   0
         AutoAdvance     =   0   'False
         AutoBeep        =   0   'False
         CaretInsert     =   0
         CaretOverWrite  =   3
         UserEntry       =   0
         HideSelection   =   -1  'True
         InvalidColor    =   -2147483637
         InvalidOption   =   0
         MarginLeft      =   3
         MarginTop       =   3
         MarginRight     =   3
         MarginBottom    =   3
         NullColor       =   -2147483637
         OnFocusAlignH   =   0
         OnFocusAlignV   =   0
         OnFocusNoSelect =   0   'False
         OnFocusPosition =   0
         ControlType     =   0
         Text            =   "0"
         MaxValue        =   "1000"
         MinValue        =   "0"
         NegFormat       =   1
         NegToggle       =   0   'False
         Separator       =   ""
         UseSeparator    =   0   'False
         IncInt          =   1
         BorderGrayAreaColor=   -2147483637
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         MouseIcon       =   "frmCadLinhaProd.frx":0B56
      End
      Begin VB.TextBox txtDescricao 
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   2
         Top             =   660
         Width           =   4935
      End
      Begin VB.TextBox txtCodigo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   1
         Top             =   180
         Width           =   1545
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ordem no Relat�rio"
         Height          =   240
         Left            =   150
         TabIndex        =   9
         Top             =   1230
         Width           =   1755
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o "
         Height          =   240
         Left            =   150
         TabIndex        =   5
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   240
         Left            =   150
         TabIndex        =   4
         Top             =   270
         Width           =   660
      End
   End
End
Attribute VB_Name = "frmCadLinhaProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vAlterar As Boolean
Dim vOper    As String * 1
Dim vCodigo  As String
Dim vDescricao As String
Dim vOrdem     As String
Dim vOK        As Integer
Dim cSql          As String

Public Sub Incluir(maxCodigo As Long)
    txtCodigo.Text = maxCodigo + 1
    txtCodigo.Locked = True
    txtdescricao.Enabled = True
    txtOrdem.Enabled = True
    vAlterar = True
    vOper = "I"
    Me.Show 1
End Sub

Public Sub Alterar(pCodigo As Long, pDescricao As String, pOrdem As String)
    txtdescricao.Enabled = True
    txtOrdem.Enabled = True
    txtCodigo.Text = pCodigo
    txtdescricao.Text = pDescricao
    txtOrdem.Text = pOrdem
    vAlterar = True
    vOper = "A"
    Me.Show 1
End Sub

Private Sub SelCodigo()
    txtCodigo.SelStart = 0
    txtCodigo.SelLength = Len(txtCodigo.Text)
End Sub

Private Sub Gravar()

    vCodigo = txtCodigo.Text
    vDescricao = txtdescricao.Text
    vOrdem = txtOrdem.Text
    
    'chama a procedure de cancelamento da Abrangencia Vigencia
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    
    If vOper = "A" Then
        cSql = cSql & " exec stp_pdr_cad_linha_u " & Val(vCodigo) & ",'" & vDescricao & "', " & vOrdem & ", "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    Else
        cSql = cSql & " exec stp_pdr_cad_linha_i " & vCodigo & ",'" & vDescricao & "', " & vOrdem & ", "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    End If
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_cad_linha_i " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Sub
    End If
           
    vOK = vbOK
    
    MsgBox "Grava��o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    vOK = vbCancel
    Unload Me
End Sub

Private Sub cmdSalvar_Click()
    Call Gravar
End Sub

Private Sub Form_Load()
    vOK = vbCancel
End Sub

Private Sub txtDescricao_Change()
    If vAlterar Then
        cmdSalvar.Enabled = True
    End If
End Sub

Private Sub LimpaCampos()
    txtCodigo.Text = ""
    txtCodigo.Locked = False
    txtdescricao.Text = ""
    txtdescricao.Enabled = False
    txtOrdem.Text = 0
    txtOrdem.Enabled = False
    cmdSalvar.Enabled = False
End Sub

Public Function codigoCad() As String
    codigoCad = vCodigo
End Function

Public Function descricaoCad() As String
    descricaoCad = vDescricao
End Function

Public Function ordemCad() As String
    ordemCad = vOrdem
End Function

Public Function OK() As Integer
    OK = vOK
End Function

Private Sub txtOrdem_Change()
    If vAlterar Then
        cmdSalvar.Enabled = True
    End If
End Sub
