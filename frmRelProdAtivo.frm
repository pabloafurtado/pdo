VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmRelProdAtivo 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relat�rio de Produtos Ativos por Ciclo"
   ClientHeight    =   6570
   ClientLeft      =   2430
   ClientTop       =   1575
   ClientWidth     =   7695
   Icon            =   "frmRelProdAtivo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   7695
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdGeraRelatorio 
      Caption         =   "Gerar Relat�rio"
      Default         =   -1  'True
      Height          =   615
      Left            =   3120
      TabIndex        =   12
      Top             =   5880
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   "Par�metros para Impress�o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4935
      Left            =   0
      TabIndex        =   15
      Top             =   780
      Width           =   7695
      Begin VB.ComboBox cmbCicloFin 
         Height          =   360
         ItemData        =   "frmRelProdAtivo.frx":030A
         Left            =   4500
         List            =   "frmRelProdAtivo.frx":030C
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   420
         Width           =   1605
      End
      Begin VB.ComboBox cmbCicloIni 
         Height          =   360
         ItemData        =   "frmRelProdAtivo.frx":030E
         Left            =   2520
         List            =   "frmRelProdAtivo.frx":0310
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   420
         Width           =   1545
      End
      Begin VB.OptionButton optFiltro 
         Caption         =   "C�d. de material"
         Height          =   240
         Index           =   1
         Left            =   3060
         TabIndex        =   2
         Top             =   960
         Width           =   1875
      End
      Begin VB.OptionButton optFiltro 
         Caption         =   "C�d. de venda"
         Height          =   240
         Index           =   0
         Left            =   960
         TabIndex        =   1
         Top             =   960
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.TextBox txtCodVenda 
         Height          =   735
         Left            =   240
         TabIndex        =   3
         Top             =   1260
         Width           =   7095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Op��es do Relat�rio"
         Height          =   2655
         Left            =   240
         TabIndex        =   13
         Top             =   2160
         Width           =   3555
         Begin VB.CheckBox chkDescrCAN 
            Caption         =   "Mostra descri��o CAN"
            Height          =   435
            Left            =   180
            TabIndex        =   8
            Top             =   1980
            Width           =   2235
         End
         Begin VB.CheckBox chkQuebraCat 
            Caption         =   "Quebra por Categoria"
            Height          =   435
            Left            =   180
            TabIndex        =   7
            Top             =   1560
            Value           =   1  'Checked
            Width           =   2235
         End
         Begin VB.CheckBox chkQuebraLin 
            Caption         =   "Quebra por Linha"
            Height          =   435
            Left            =   180
            TabIndex        =   6
            Top             =   1140
            Value           =   1  'Checked
            Width           =   2235
         End
         Begin VB.CheckBox chkListaMaterial 
            Caption         =   "Lista materiais"
            Height          =   315
            Left            =   180
            TabIndex        =   4
            Top             =   360
            Width           =   1455
         End
         Begin VB.CheckBox chkLancam 
            Caption         =   "Destaca lan�amentos/Descontinuados"
            Enabled         =   0   'False
            Height          =   435
            Left            =   180
            TabIndex        =   5
            Top             =   720
            Width           =   3135
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Ordem de Impress�o"
         Height          =   2655
         Left            =   4140
         TabIndex        =   14
         Top             =   2160
         Width           =   3255
         Begin VB.OptionButton optOrdem 
            Caption         =   "Descri��o"
            Height          =   255
            Index           =   1
            Left            =   420
            TabIndex        =   10
            Top             =   1260
            Width           =   1095
         End
         Begin VB.OptionButton optOrdem 
            Caption         =   "C�digo de venda"
            Height          =   315
            Index           =   0
            Left            =   420
            TabIndex        =   9
            Top             =   840
            Value           =   -1  'True
            Width           =   1575
         End
         Begin VB.OptionButton optOrdem 
            Caption         =   "C�d. de material"
            Enabled         =   0   'False
            Height          =   255
            Index           =   2
            Left            =   420
            TabIndex        =   11
            Top             =   1680
            Width           =   1515
         End
      End
      Begin VB.Label Label3 
         Caption         =   "Filtro:"
         Height          =   195
         Left            =   240
         TabIndex        =   18
         Top             =   960
         Width           =   555
      End
      Begin VB.Label Label2 
         Caption         =   "�"
         Height          =   255
         Left            =   4200
         TabIndex        =   17
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Ciclo:"
         Height          =   255
         Left            =   1980
         TabIndex        =   0
         Top             =   480
         Width           =   555
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   6960
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelProdAtivo.frx":0312
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelProdAtivo.frx":062C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelProdAtivo.frx":0946
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelProdAtivo.frx":0C60
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmRelProdAtivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vCicloIni As Long
Dim vCicloFin As Long
Dim cSql          As String

Private Sub chkListaMaterial_Click()

    If chkListaMaterial.Value = 0 Then
        optOrdem(2).Enabled = False
    Else
        optOrdem(2).Enabled = True
    End If

End Sub

Private Sub cmbCicloFin_Click()
    
    If cmbCicloIni.Text = cmbCicloFin.Text Then
        If Trim(cmbCicloIni.Text) <> "" And Trim(cmbCicloFin.Text) <> "" Then
            chkLancam.Value = 1
            chkLancam.Enabled = True
        End If
    Else
        chkLancam.Value = 0
        chkLancam.Enabled = False
    End If
    
End Sub

Private Sub cmbCicloIni_Click()
    
    If cmbCicloIni.Text = cmbCicloFin.Text Then
        If Trim(cmbCicloIni.Text) <> "" And Trim(cmbCicloFin.Text) <> "" Then
            chkLancam.Value = 1
            chkLancam.Enabled = True
        End If
    Else
        chkLancam.Value = 0
        chkLancam.Enabled = False
    End If

End Sub

Private Sub cmdGeraRelatorio_Click()
    
    If Not fConsisteCiclo(cmbCicloIni, cmbCicloFin) Then
        Exit Sub
    End If
    
    Load frmRelAtivo
    
    'titulo/ciclos
    frmRelAtivo.lblInicio.Caption = cmbCicloIni.Text
    frmRelAtivo.lblFim.Caption = cmbCicloFin.Text
    
    'ciclo inicial
    vCicloIni = cmbCicloIni.ItemData(cmbCicloIni.ListIndex)
        
    'ciclo final
    If cmbCicloFin <> "" Then
        vCicloFin = cmbCicloFin.ItemData(cmbCicloFin.ListIndex)
    Else
        vCicloFin = 0
    End If
        
    'filtro
    If optFiltro(0).Value = True Then
        vfiltro = txtcodvenda.Text
    Else
        Call converte_Filtro(txtcodvenda.Text)
    End If
    
    'exibir quebra por lan�amento/ativos/descontinuados
    If chkLancam.Value = 1 Then
        vQuebraLanc = "S"
    Else
        vQuebraLanc = "N"
    End If
    
    'exibir quebra por linha
    If chkQuebraLin.Value = 1 Then
        vQuebraLin = "S"
    Else
        vQuebraLin = "N"
    End If
    
    'exibir quebra por categoria
    If chkQuebraCat.Value = 1 Then
        vQuebraCat = "S"
    Else
        vQuebraCat = "N"
    End If
    
    'Ordena��o
    If optOrdem(0) Then
        vOrdena = 1
    ElseIf optOrdem(1) Then
        vOrdena = 2
    ElseIf optOrdem(2) Then
        vOrdena = 3
    End If
    
    'exibir lista de material
    If chkListaMaterial.Value = 1 Then
        vListaMaterial = "S"
        redimGrid
    Else
        vListaMaterial = "N"
    End If
    
    frmRelAtivo.Grid.Visible = True
    frmRelAtivo.Grid.MaxRows = 0
    DoEvents
    Me.MousePointer = vbHourglass
    DoEvents
    Call Preenche_Grid(vCicloIni, vCicloFin, chkDescrCAN.Value)
    Me.MousePointer = 1


End Sub

Private Sub Form_Activate()

    Call Carrega_Ciclo(cmbCicloIni)
    Call Carrega_Ciclo(cmbCicloFin)
    chkListaMaterial.Value = 0

End Sub

Private Sub Form_Load()
    chkListaMaterial.Value = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)

    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
   
    Unload Me

End Sub

Public Sub redimGrid()

        'Adiciona coluna
        frmRelAtivo.Grid.MaxCols = frmRelAtivo.Grid.MaxCols + 7
        '
        'nomeia os cabe�alhos
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 13
        frmRelAtivo.Grid.ColWidth(13) = 11
        frmRelAtivo.Grid.Text = "C�d.Material"
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 14
        frmRelAtivo.Grid.ColWidth(14) = 33
        frmRelAtivo.Grid.Text = "Descri��o Material"
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 15
        frmRelAtivo.Grid.ColWidth(15) = 4.5
        frmRelAtivo.Grid.Text = "Prior."
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 16
        frmRelAtivo.Grid.ColWidth(16) = 8.75
        frmRelAtivo.Grid.Text = "Ciclo Inicio"
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 17
        frmRelAtivo.Grid.ColWidth(17) = 9.25
        frmRelAtivo.Grid.Text = "Data Inicio"
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 18
        frmRelAtivo.Grid.ColWidth(18) = 8.75
        frmRelAtivo.Grid.Text = "Ciclo Final"
        '
        frmRelAtivo.Grid.Row = 0
        frmRelAtivo.Grid.Col = 19
        frmRelAtivo.Grid.ColWidth(19) = 9.25
        frmRelAtivo.Grid.Text = "Data Final"
        '
End Sub
