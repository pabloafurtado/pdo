VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form frmExcecaoPreco 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reajuste Individual do Pre�o de Refer�ncia"
   ClientHeight    =   4155
   ClientLeft      =   4110
   ClientTop       =   2865
   ClientWidth     =   9675
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   9675
   Begin ComctlLib.Toolbar st_Toolbar 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Description     =   "Novo"
            Object.ToolTipText     =   "Novo"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Excluir"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Gravar"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Nova pesquisa"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Imprimir"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Pesquisar"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   4
            Object.Width           =   2000
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Height          =   1725
      Left            =   120
      TabIndex        =   10
      Top             =   660
      Width           =   7245
      Begin VB.ComboBox cb_Segmento 
         Height          =   360
         ItemData        =   "frmExcecaoPreco.frx":0000
         Left            =   6210
         List            =   "frmExcecaoPreco.frx":0007
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.TextBox txt_Codigo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   360
         Left            =   4170
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   225
         Width           =   1275
      End
      Begin VB.TextBox txt_Ciclo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   360
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   210
         Width           =   1365
      End
      Begin VB.ComboBox cb_Listas 
         Height          =   360
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   1140
         Width           =   975
      End
      Begin VB.TextBox txt_Dc_Produto 
         Enabled         =   0   'False
         Height          =   360
         Left            =   1080
         TabIndex        =   9
         Top             =   660
         Width           =   6045
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Segmento"
         Height          =   240
         Left            =   5430
         TabIndex        =   12
         Top             =   270
         Visible         =   0   'False
         Width           =   990
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo de Venda"
         Height          =   240
         Left            =   2520
         TabIndex        =   5
         Top             =   270
         Width           =   1590
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo/Ano"
         Height          =   240
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   870
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Lista"
         Height          =   240
         Left            =   585
         TabIndex        =   7
         Top             =   1230
         Width           =   420
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         Height          =   240
         Left            =   120
         TabIndex        =   11
         Top             =   705
         Width           =   930
      End
   End
   Begin FPSpread.vaSpread grdPercentual 
      Height          =   2535
      Left            =   7410
      TabIndex        =   15
      Top             =   720
      Width           =   2205
      _Version        =   131077
      _ExtentX        =   3889
      _ExtentY        =   4471
      _StockProps     =   64
      ButtonDrawMode  =   4
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   2
      MaxRows         =   0
      OperationMode   =   3
      ProcessTab      =   -1  'True
      ScrollBarExtMode=   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmExcecaoPreco.frx":000F
      UserResize      =   2
      VisibleCols     =   5
      VisibleRows     =   20
   End
   Begin VB.Frame Frame3 
      Height          =   915
      Left            =   120
      TabIndex        =   16
      Top             =   2340
      Width           =   7245
      Begin VB.TextBox txt_pc_Reajuste_Produto 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   3150
         MaxLength       =   9
         TabIndex        =   17
         Top             =   270
         Width           =   1095
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Percentual de Reajuste na Lista:"
         Height          =   240
         Left            =   150
         TabIndex        =   18
         Top             =   315
         Width           =   2880
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   2790
      TabIndex        =   14
      Top             =   3150
      Width           =   4335
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "Fechar"
         Height          =   675
         Left            =   2430
         Picture         =   "frmExcecaoPreco.frx":028A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   210
         Width           =   1665
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   675
         Left            =   330
         Picture         =   "frmExcecaoPreco.frx":0594
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   210
         Width           =   1605
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   2490
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":089E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":0BB8
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":0ED2
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":11EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":1506
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":1820
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":1B3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":1E54
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":216E
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExcecaoPreco.frx":2488
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmExcecaoPreco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Z As Integer

Private Sub cb_Listas_Click()
'    If cb_Listas.ListIndex > -1 Then
'        If Not Acha_Percentual(Right$(txt_Ciclo.Tag, 4) & Left$(txt_Ciclo.Tag, 2), cb_Listas.Text, cb_Segmento.ItemData(cb_Segmento.ListIndex), txt_Codigo.Text) Then
'           SetStatus "Erro na leitura..."
'           MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
'           Exit Sub
'        Else
'            st_Toolbar.Buttons(5).Enabled = True
'        End If
'    End If

    If cb_Listas.ListIndex > -1 Then
        txt_pc_Reajuste_Produto = obtem_percentual(cb_Listas.Text)
        txt_pc_Reajuste_Produto.SetFocus
    End If

End Sub


Private Sub cb_Listas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txt_Codigo.SetFocus
    End If
End Sub

Private Sub cb_Segmento_KeyPress(KeyAscii As Integer)

   If KeyAscii = 13 Then
      If txt_Ciclo.Tag = "" And cb_Segmento.ListIndex = -1 Then Exit Sub
      If Not Combo_Lista(cb_Listas, Right$(txt_Ciclo.Tag, 4) & Left$(txt_Ciclo.Tag, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      cb_Listas.SetFocus
   End If
   
End Sub

Private Sub cb_Segmento_LostFocus()

    If txt_Ciclo.Tag = "" And cb_Segmento.ListIndex = -1 Then Exit Sub
    If Not Combo_Lista(cb_Listas, Right$(txt_Ciclo.Tag, 4) & Left$(txt_Ciclo.Tag, 2)) Then
       SetStatus "Erro na leitura..."
       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
    End If

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
    Set frmExcecaoPreco = Nothing
End Sub

Private Sub cmdConfirmar_Click()

    Dim ne_conta_seg    As Integer
    
    If Val(cb_Listas.Text) <= 0 Then
        Exit Sub
    End If
    
    'SE 6346 - Corrige problema com controle do cb_segmento
    If cb_Segmento.Visible = False Then

        For ne_conta_seg = 1 To 4
            If Not Grava_Percentual(Right$(txt_Ciclo.Tag, 4) + Left$(txt_Ciclo.Tag, 2), _
                                    cb_Listas.Text, _
                                    ne_conta_seg, _
                                    txt_Codigo.Text, _
                                    txt_pc_Reajuste_Produto.Text) Then
                SetStatus "Erro na leitura..."
                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                Exit Sub
            End If
        Next
    ElseIf cb_Segmento.ItemData(cb_Segmento.ListIndex) = 1 Then
        For ne_conta_seg = 1 To 4
            If Not Grava_Percentual(Right$(txt_Ciclo.Tag, 4) + Left$(txt_Ciclo.Tag, 2), _
                                    cb_Listas.Text, _
                                    ne_conta_seg, _
                                    txt_Codigo.Text, _
                                    txt_pc_Reajuste_Produto.Text) Then
                SetStatus "Erro na leitura..."
                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                Exit Sub
            End If
        Next
    Else
        If Not Grava_Percentual(Right$(txt_Ciclo.Tag, 4) + Left$(txt_Ciclo.Tag, 2), _
                                cb_Listas.Text, _
                                cb_Segmento.ItemData(cb_Segmento.ListIndex), _
                                txt_Codigo.Text, _
                                txt_pc_Reajuste_Produto.Text) Then
            SetStatus "Erro na leitura..."
            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
            Exit Sub
        End If
    End If
    
    Call CarregaListaPercentual
    txt_pc_Reajuste_Produto.Text = ""
    cb_Listas.ListIndex = -1
    cb_Listas.SetFocus
    
End Sub

Private Sub Form_Activate()
    cb_Segmento.ListIndex = 0
End Sub

Private Sub Form_Load()
    Call CarregaListaPercentual
End Sub

Private Sub Form_Resize()
'    Me.st_Toolbar.Buttons(10).Width = Me.Width - (Me.st_Toolbar.Buttons(1).Width * 8)
End Sub

Private Sub grdPercentual_Click(ByVal Col As Long, ByVal Row As Long)
    
    Dim vPercent As String
    Dim vLista As String
    Dim i As Integer
    
    grdPercentual.Row = Row
    
    grdPercentual.Col = 1
    vLista = grdPercentual.Text
    
    grdPercentual.Col = 2
    vPercent = grdPercentual.Text
    
    For i = 0 To cb_Listas.ListCount - 1
        cb_Listas.ListIndex = i
        If cb_Listas.Text = vLista Then
            Exit For
        End If
    Next i
  
    txt_pc_Reajuste_Produto.Text = vPercent
   ' txt_pc_Reajuste_Produto.SetFocus
    
End Sub

Private Sub st_Toolbar_ButtonClick(ByVal Button As ComctlLib.Button)
Dim ne_conta_seg    As Integer

    Select Case Button.Index
        Case 3
'            If cb_Segmento.ItemData(cb_Segmento.ListIndex) = 1 Then
'                For ne_conta_seg = 1 To 4
'                    If Not Grava_Percentual(Right$(txt_ciclo.tag, 4) + Left$(txt_ciclo.tag, 2), _
'                                            cb_Listas.Text, _
'                                            ne_conta_seg, _
'                                            txt_Codigo.Text, _
'                                            txt_pc_Reajuste_Produto.Text) Then
'                        SetStatus "Erro na leitura..."
'                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
'                    End If
'                Next
'                MsgBox "Grava��o efetuada com sucesso", vbInformation, "A T E N � � O"
'                Limpa_Controles
'            Else
'                If Not Grava_Percentual(Right$(txt_ciclo.tag, 4) + Left$(txt_ciclo.tag, 2), _
'                                        cb_Listas.Text, _
'                                        cb_Segmento.ItemData(cb_Segmento.ListIndex), _
'                                        txt_Codigo.Text, _
'                                        txt_pc_Reajuste_Produto.Text) Then
'                    SetStatus "Erro na leitura..."
'                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
'                End If
'                MsgBox "Grava��o efetuada com sucesso", vbInformation, "A T E N � � O"
'                Limpa_Controles
'            End If

            If cb_Segmento.ItemData(cb_Segmento.ListIndex) = 1 Then
                For ne_conta_seg = 1 To 4
                    If Not Grava_Percentual(Right$(txt_Ciclo.Tag, 4) + Left$(txt_Ciclo.Tag, 2), _
                                            cb_Listas.Text, _
                                            ne_conta_seg, _
                                            txt_Codigo.Text, _
                                            txt_pc_Reajuste_Produto.Text) Then
                        SetStatus "Erro na leitura..."
                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                        Exit Sub
                    End If
                Next
                Unload Me
            Else
                If Not Grava_Percentual(Right$(txt_Ciclo.Tag, 4) + Left$(txt_Ciclo.Tag, 2), _
                                        cb_Listas.Text, _
                                        cb_Segmento.ItemData(cb_Segmento.ListIndex), _
                                        txt_Codigo.Text, _
                                        txt_pc_Reajuste_Produto.Text) Then
                    SetStatus "Erro na leitura..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                    Exit Sub
                End If
                Unload Me
            End If
            
       Case 2
         If Val(cb_Listas.Text) > 0 Then
            If MsgBox("Confirma a exclus�o do percentual da lista " & cb_Listas.Text & " ? ", vbQuestion + vbYesNo, Me.Caption) = vbYes Then
                Call Excluir(cb_Listas.Text)
                Call CarregaListaPercentual
                Call Limpa_Controles
            End If
         End If
          
       ' Limpar
       Case 5
            Limpa_Controles
        ' Sair
        Case 10
            Unload Me
            Set frmExcecaoPreco = Nothing
    End Select

End Sub

Private Sub txt_Ciclo_KeyPress(KeyAscii As Integer)
    Dim Ano_ciclo As String

    If KeyAscii = 13 Then
'        cb_Segmento.SetFocus
        cb_Listas.SetFocus
        KeyAscii = 0
        Exit Sub
    End If
   
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
        Beep
    End If
'    bm_Alterado = True
End Sub

Private Sub Excluir(cd_lista As Integer)

    For Z = 1 To UBound(ReajusteLista)
        If ReajusteLista(Z).cd_lista = cd_lista Then
            ReajusteLista(Z).Excluir = "S"
        End If
    Next Z

End Sub

Private Function obtem_percentual(cd_lista As Integer) As String

    Dim Occur As Integer
    Dim NovaLista As Boolean
    Dim OcorrenciaLista As Integer
    Dim vPercentual As String

    vPercentual = 0
    
    On Error GoTo Grava_Percentual
   
    PonteiroMouse vbHourglass
    
    For Z = 1 To UBound(ReajusteLista)
        If ReajusteLista(Z).cd_lista = cd_lista And ReajusteLista(Z).Excluir = "N" Then
            vPercentual = ReajusteLista(Z).pc_reajuste_lista
            Exit For
        End If
    Next Z
    
    obtem_percentual = vPercentual
    
'    Call CarregaListaPercentual
    
    PonteiroMouse vbNormal
    Exit Function
   
Grava_Percentual:
    MsgBox " Erro (" & Err & ") - " & Error, vbCritical, Me.Caption
    PonteiroMouse vbNormal
    Exit Function

End Function

Public Function Grava_Percentual(nm_ciclo_operacional As String, _
                                 cd_lista As Integer, _
                                 cd_Segmento_canal As Integer, _
                                 cd_venda_produto As Integer, _
                                 pc_reajuste_lista As String) As Boolean

    Dim Occur As Integer
    Dim NovaLista As Boolean
    Dim OcorrenciaLista As Integer

    Grava_Percentual = False
    On Error GoTo Grava_Percentual
   
    PonteiroMouse vbHourglass
    
    NovaLista = True
    
    For Z = 1 To UBound(ReajusteLista)
        If (ReajusteLista(Z).cd_lista = cd_lista) And (ReajusteLista(Z).cd_Segmento_canal = cd_Segmento_canal) Then
            NovaLista = False
            OcorrenciaLista = Z
            Exit For
        End If
    Next Z

    If NovaLista Then
        Occur = UBound(ReajusteLista) + 1
        ReDim Preserve ReajusteLista(Occur)
    Else
        Occur = OcorrenciaLista
    End If
    
    ReajusteLista(Occur).nm_ciclo_operacional = nm_ciclo_operacional
    ReajusteLista(Occur).cd_lista = cd_lista
    ReajusteLista(Occur).cd_Segmento_canal = cd_Segmento_canal
    ReajusteLista(Occur).cd_venda_produto = cd_venda_produto
    ReajusteLista(Occur).pc_reajuste_lista = ConvertePontoDecimal(pc_reajuste_lista, 0)
    ReajusteLista(Occur).Excluir = "N"
    
    Grava_Percentual = True
    
        
    PonteiroMouse vbNormal
    Exit Function
   
Grava_Percentual:
    MSG$ = "Grava_Percentual  - Erro (" & Err & ") - " & Error
    PonteiroMouse vbNormal
    Exit Function
    
End Function

Private Sub txt_pc_Reajuste_Produto_Change()
    If txt_pc_Reajuste_Produto.Text <> "" Then
        st_Toolbar.Buttons(2).Enabled = True
        cmdConfirmar.Enabled = True
    End If
End Sub

Private Sub txt_pc_Reajuste_Produto_GotFocus()
    SetaSelecao txt_pc_Reajuste_Produto
End Sub

Private Sub txt_pc_Reajuste_Produto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txt_pc_Reajuste_Produto.Text = Format(txt_pc_Reajuste_Produto.Text, "#0.00")
    End If
End Sub

Private Sub txt_pc_Reajuste_Produto_LostFocus()

    txt_pc_Reajuste_Produto.Text = Format(txt_pc_Reajuste_Produto.Text, "#0.00")
    
End Sub

Public Sub Limpa_Controles()

    st_Toolbar.Buttons(1).Enabled = False
    st_Toolbar.Buttons(2).Enabled = False
    st_Toolbar.Buttons(3).Enabled = False
    st_Toolbar.Buttons(5).Enabled = False
    cmdConfirmar.Enabled = False
    
    'txt_codigo.Enabled = True
    cb_Listas.Enabled = True
    'cb_Segmento.Enabled = True
    'txt_Ciclo.Enabled = True
    'txt_Codigo.Text = ""
    'txt_Dc_Produto.Text = ""
    txt_pc_Reajuste_Produto.Text = ""
    cb_Listas.ListIndex = -1
    'cb_Segmento.ListIndex = 0
    'Unload Me
    
End Sub

Private Sub SelCodigo(pcampo As Object)
    pcampo.SelStart = 0
    pcampo.SelLength = Len(pcampo.Text)
End Sub

Private Sub CarregaListaPercentual()

    Dim nRow      As Long
    Dim vListaAnt As String
    
    grdPercentual.MaxRows = 0
    vListaAnt = ""
    
    For Z = 1 To UBound(ReajusteLista)
    
        If (vListaAnt <> ReajusteLista(Z).cd_lista) And ReajusteLista(Z).Excluir = "N" Then
            nRow = grdPercentual.MaxRows + 1
            
            grdPercentual.MaxRows = nRow
            grdPercentual.Row = nRow
    
            grdPercentual.Col = 1
            grdPercentual.Text = ReajusteLista(Z).cd_lista
            
            grdPercentual.Col = 2
            grdPercentual.Text = ReajusteLista(Z).pc_reajuste_lista
            
            vListaAnt = ReajusteLista(Z).cd_lista
        End If
        
    Next Z

End Sub
