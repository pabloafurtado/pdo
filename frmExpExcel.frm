VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmExpExcel 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Exportar Lista de Pre�os para Excel"
   ClientHeight    =   2895
   ClientLeft      =   1590
   ClientTop       =   1530
   ClientWidth     =   8730
   ClipControls    =   0   'False
   Icon            =   "frmExpExcel.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2895
   ScaleWidth      =   8730
   Begin ComctlLib.Toolbar barFerramenta 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   8730
      _ExtentX        =   15399
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "sair"
            Object.ToolTipText     =   "Sair "
            Object.Tag             =   "Sair"
            ImageIndex      =   5
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   2610
      TabIndex        =   4
      Top             =   1710
      Width           =   3525
      Begin VB.CommandButton cmdGerar 
         Caption         =   "Gerar Excel"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   795
         Left            =   1050
         Picture         =   "frmExpExcel.frx":0CCA
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   210
         Width           =   1605
      End
   End
   Begin MSComDlg.CommonDialog dlgSelect 
      Left            =   4260
      Top             =   30
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   1170
      TabIndex        =   0
      Top             =   720
      Width           =   6285
      Begin VB.ComboBox cmbCiclo 
         Height          =   360
         ItemData        =   "frmExpExcel.frx":0FD4
         Left            =   2535
         List            =   "frmExpExcel.frx":0FFF
         TabIndex        =   1
         Top             =   360
         Width           =   1545
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo / Ano"
         Height          =   240
         Left            =   1320
         TabIndex        =   2
         Top             =   420
         Width           =   1110
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   120
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":1078
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":1392
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":16AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":19C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":1CE0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":1FFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":2314
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmExpExcel.frx":262E
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmExpExcel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit
Dim strNomeArquivo As String

Private Sub barFerramenta_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
        Case 1:
            Unload Me
    End Select
End Sub

Private Sub cmbCiclo_Click()
    If cmbCiclo.ListIndex >= 0 Then
        cmdGerar.Enabled = True
    End If
End Sub

Private Sub cmdGerar_Click()
    Call Exportar
End Sub

Private Sub Exportar()

    Dim lngLinha        As Long
    Dim lngColuna       As Long
    Dim X               As Long
    Dim Y               As Double
    Dim QtdColunas      As Integer
    Dim fim             As Boolean
    Dim contador        As Integer

    Dim vConteudoCelula As String
    Dim vLinha          As Double
    Dim vCicloIni       As Long
    Dim cdVendaAnt      As Long
    Dim nmOrdAnt        As Integer
    Dim Col             As Long
    Dim avData()        As Variant
    
    'On Error GoTo trata_erro
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    If ExcelSheet Is Nothing Then
        Set ExcelSheet = CreateObject("Excel.Sheet")
    Else
        Set ExcelSheet = Nothing
        Set ExcelSheet = CreateObject("Excel.Sheet")
    End If
    If ExcelSheet.Application.Visible = False Then
        ExcelSheet.Application.Visible = True
    End If
    
    vLinha = 0
        
    vCicloIni = Val(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2))
    
    ReDim avData(0, 0)
    
    cSql = "exec stp_pdr_exp_pr_refer_s " & vCicloIni
    
    vCicloIni = Val(Left(cmbCiclo.Text, 2) & Right(cmbCiclo.Text, 4))
    
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())
     
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.nServerNum & " " & gObjeto.sServerMsg, vbOKOnly + vbCritical, "Erro"
        Exit Sub
    End If
    
    vLinha = vLinha + 1
    ExcelSheet.ActiveSheet.Cells(vLinha, 1).Value = "Ciclo"
    ExcelSheet.ActiveSheet.Cells(vLinha, 2).Value = "C�d.Prod"
    ExcelSheet.ActiveSheet.Cells(vLinha, 3).Value = "Descri��o do Produto"
    ExcelSheet.ActiveSheet.Cells(vLinha, 4).Value = "C�d.Pai"
    ExcelSheet.ActiveSheet.Cells(vLinha, 5).Value = "Redutor Ptos (%)"
    ExcelSheet.ActiveSheet.Cells(vLinha, 5).WrapText = True
    ExcelSheet.ActiveSheet.Cells(vLinha, 6).Value = "Pontos"
    ExcelSheet.ActiveSheet.Cells(vLinha, 7).Value = "Status"
    ExcelSheet.ActiveSheet.Cells(vLinha, 8).Value = "Pre�o Brasil"
    ExcelSheet.ActiveSheet.Cells(vLinha, 8).WrapText = True
    ExcelSheet.ActiveSheet.Columns(8).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 9).Value = "Pre�o Refer�ncia"
    ExcelSheet.ActiveSheet.Cells(vLinha, 9).WrapText = True
    ExcelSheet.ActiveSheet.Columns(9).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 10).Value = "Pre�o Lista 1"
    ExcelSheet.ActiveSheet.Cells(vLinha, 10).WrapText = True
    ExcelSheet.ActiveSheet.Columns(10).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 11).Value = "Pre�o Lista 2"
    ExcelSheet.ActiveSheet.Cells(vLinha, 11).WrapText = True
    ExcelSheet.ActiveSheet.Columns(11).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 12).Value = "Pre�o Lista 3"
    ExcelSheet.ActiveSheet.Cells(vLinha, 12).WrapText = True
    ExcelSheet.ActiveSheet.Columns(12).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 13).Value = "Pre�o Lista 4"
    ExcelSheet.ActiveSheet.Cells(vLinha, 13).WrapText = True
    ExcelSheet.ActiveSheet.Columns(13).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 14).Value = "Pre�o Lista 5"
    ExcelSheet.ActiveSheet.Cells(vLinha, 14).WrapText = True
    ExcelSheet.ActiveSheet.Columns(14).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 15).Value = "Pre�o Lista 6"
    ExcelSheet.ActiveSheet.Cells(vLinha, 15).WrapText = True
    ExcelSheet.ActiveSheet.Columns(15).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 16).Value = "Pre�o Lista 7"
    ExcelSheet.ActiveSheet.Cells(vLinha, 16).WrapText = True
    ExcelSheet.ActiveSheet.Columns(16).NumberFormat = "#,##0.00"
    ExcelSheet.ActiveSheet.Cells(vLinha, 17).Value = "Pre�o Lista 8"
    ExcelSheet.ActiveSheet.Cells(vLinha, 17).WrapText = True
    ExcelSheet.ActiveSheet.Columns(17).NumberFormat = "#,##0.00"
    
    For X = 0 To UBound(avData, 2)
        If Len(Trim(avData(0, X))) > 0 Then
            vLinha = vLinha + 1
            ExcelSheet.ActiveSheet.Cells(vLinha, 1).Value = vCicloIni
            ExcelSheet.ActiveSheet.Cells(vLinha, 2).Value = avData(0, X)
            ExcelSheet.ActiveSheet.Cells(vLinha, 3).Value = avData(1, X)
            ExcelSheet.ActiveSheet.Cells(vLinha, 4).Value = IIf(avData(2, X) = avData(0, X), " ", avData(2, X))
'            ExcelSheet.ActiveSheet.Cells(vLinha, 4).Value = IIf(avData(2, X) = "0", " ", _
'                                                            IIf(avData(2, X) = avData(0, X), " ", avData(2, X)))
            ExcelSheet.ActiveSheet.Cells(vLinha, 6).Value = avData(4, X)
            If avData(11, X) = 3 Then
                ExcelSheet.ActiveSheet.Cells(vLinha, 5).Value = ""
                ExcelSheet.ActiveSheet.Cells(vLinha, 7).Value = ""
                ExcelSheet.ActiveSheet.Cells(vLinha, 8).Value = ""
            Else
                ExcelSheet.ActiveSheet.Cells(vLinha, 5).Value = IIf(avData(3, X) = "0", " ", avData(3, X))
                ExcelSheet.ActiveSheet.Cells(vLinha, 7).Value = fSetaStatus(avData(5, X))
                ExcelSheet.ActiveSheet.Cells(vLinha, 8).Value = IIf(avData(6, X) = "0", "N", "S")
            End If
            ExcelSheet.ActiveSheet.Cells(vLinha, 9).Value = avData(7, X)
            cdVendaAnt = avData(0, X)
            nmOrdAnt = avData(6, X)
            Do While X <= UBound(avData, 2) 'And cdVendaAnt = Val(avData(0, X))
                If cdVendaAnt <> avData(0, X) Or avData(6, X) <> nmOrdAnt Then
                    Exit Do
                End If
                If Val(avData(8, X)) <> 0 Then
                    Col = 9 + Val(avData(8, X))
                    ExcelSheet.ActiveSheet.Cells(vLinha, Col).Value = avData(9, X)
                End If
                X = X + 1
            Loop
            X = X - 1
        End If
    Next X
    
    Screen.MousePointer = vbNormal
            
    MsgBox "Arquivo Exportado com sucesso.", vbInformation, App.Title
        
    Screen.MousePointer = vbNormal
    cmbCiclo.ListIndex = -1
    cmdGerar.Enabled = False
    cmbCiclo.SetFocus
    Exit Sub
    
'trata_erro:

    Screen.MousePointer = vbNormal
    If Err.Number = 55 Then
        MsgBox "O arquivo " & strNomeArquivo & " j� est� sendo utilizado. Favor gerar os dados em um arquivo com outro nome." & Err.Description
    Else
        MsgBox "Ocorreu o seguinte erro ao exportar " & Err.Number & " " & Err.Description
    End If
    
End Sub
Private Function fSetaStatus(status As Variant) As String

    Select Case Val(status)
        Case 0
            fSetaStatus = "Regular Venda"
        Case 2
            fSetaStatus = "Material de apoio"
        Case 3
            fSetaStatus = "Sald�o"
        Case 4
            fSetaStatus = "Produto sem pre�o"
        Case 5
            fSetaStatus = "Lan�amento Futuro"
        Case 9
            fSetaStatus = "Descontinuado"
        Case Else
            fSetaStatus = "  "
    End Select

End Function

Private Sub Form_Load()
    
'    Me.Show
'    Refresh
'    Me.MousePointer = vbHourglass
'    Refresh
    
    Call CarrCicloComPreco(cmbCiclo)
    
    Me.MousePointer = vbNormal
    Refresh
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    MDIpdo001.DesbloqueiaMenu

End Sub
