Attribute VB_Name = "SAP"
Option Explicit
Option Compare Text

'Public SapObjeto_G As Object
'Public SapSysNumber_G As Integer, SapServer_G As String
'Public SapClient_G As String, SapUser_G As String, SapPass_G As String

'--23-11-10
Public SapObjeto_CRM As Object
Public SapSysNumber_CRM As Integer, SapServer_CRM As String
Public SapClient_CRM As String, SapUser_CRM As String, SapPass_CRM As String
Public gcNomeUsuario As String

'---



'Sub InitSap(SapSysNumber As Integer, SapServer As String, SapClient As String, SapUser As String, SapPass As String)
'
'    On Error Resume Next
'
'   'Inicializa as variaveis para conexao com o SAP
'
'   Set SapObjeto_G = CreateObject("SAP.Functions")
'
'   SapSysNumber_G = SapSysNumber
'   SapServer_G = SapServer
'   SapClient_G = SapClient
'   SapUser_G = SapUser
'   SapPass_G = SapPass
'
'   ReDim SapCc_G(0 To 0)
'   ReDim SapPep_G(0 To 0)
'   ReDim SapOi_G(0 To 0)
'
'
'End Sub

Sub InitSapCRM(SapSysNumber As Integer, SapServer As String, SapClient As String, SapUser As String, SapPass As String)

    On Error Resume Next
    
   'Inicializa as variaveis para conexao com o SAP CRM

   Set SapObjeto_CRM = CreateObject("SAP.Functions")

   SapSysNumber_CRM = SapSysNumber
   SapServer_CRM = SapServer
   SapClient_CRM = SapClient
   SapUser_CRM = SapUser
   SapPass_CRM = SapPass

   ReDim SapCc_CRM(0 To 0)
   ReDim SapPep_CRM(0 To 0)
   ReDim SapOi_CRM(0 To 0)

End Sub


Function SAPStart()

' Declara��o de Vari�veis ---------------------------------------------------
Dim cAppPath       As String
Dim cSapIni        As String
Dim nTam           As Integer
Dim cAux           As String * 255
Dim cCommand       As Variant
Dim nPos           As Integer
'----------------------------------------------------------------------------

Dim nNumber As Integer
Dim cServer As String
Dim cClient As String
Dim cUsuario As String
Dim cSenha As String

Dim nNumberCRM As Integer
Dim cServerCRM As String
Dim cClientCRM As String
Dim cUsuarioCRM As String
Dim cSenhaCRM As String

' Determina o diret�rio do programa execut�vel
 cCommand = Trim(Command())
 ' Inibe a passagem de parametros ( tirar para testes com robo )
 cCommand = ""

 nPos = InStr(cCommand, " ")
 If nPos = 0 Then
   cAppPath = cCommand
   gcNomeUsuario = UCase(Environ("_U"))
 Else
   cAppPath = Left$(cCommand, nPos - 1)
   gcNomeUsuario = Right$(cCommand, Len(cCommand) - nPos)
 End If
 If cAppPath = "" Or cAppPath = "." Then cAppPath = App.Path
 If Right$(cAppPath, 1) <> "\" Then
   cAppPath = cAppPath + "\"
 End If
'----------------------------------------------------------------------------

' Verifica a exist�ncia do arquivo INI -------------------------------------
 cSapIni = cAppPath + "SAP.INI"
 If Dir$(cSapIni) = "" Then
   MsgBox "Arquivo " & cSapIni & " n�o encontrado." & Chr$(KEY_RETURN) & "Contate o suporte.", MB_ICONSTOP
   ErroFatal
 End If
'-----------------------------------------------------------------------------------------------

 'Number
 nTam = GetPrivateProfileString("Number", "SapNumber", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 nNumber = Trim$(Left$(cAux, nTam))
 'Serve
 nTam = GetPrivateProfileString("Server", "SapServer", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 cServer = Trim$(Left$(cAux, nTam))
 ' Client
 nTam = GetPrivateProfileString("Client", "SapClient", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 cClient = Trim$(Left$(cAux, nTam))

' Usuario
 nTam = GetPrivateProfileString("Usu", "SapClient", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 cUsuario = Trim$(Left$(cAux, nTam))
 ' Password
 nTam = GetPrivateProfileString("Pass", "SapClient", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 cSenha = Trim$(Left$(cAux, nTam))
 
'--------------------------------------------------------------------------------------------------
'--SAP CRM - 23-11-10
'--------------------------------------------------------------------------------------------------
 'Number
 nTam = GetPrivateProfileString("Number", "SapNumberCRM", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 nNumberCRM = Trim$(Left$(cAux, nTam))
 'Serve
 nTam = GetPrivateProfileString("Server", "SapServerCRM", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 cServerCRM = Trim$(Left$(cAux, nTam))
 ' Client
 nTam = GetPrivateProfileString("Client", "SapClientCRM", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
 cClientCRM = Trim$(Left$(cAux, nTam))

' Usuario
nTam = GetPrivateProfileString("Usu", "SapClientCRM", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
cUsuarioCRM = Trim$(Left$(cAux, nTam))
' Password
nTam = GetPrivateProfileString("Pass", "SapClientCRM", "M�dulo de Cadastro", cAux, Len(cAux), cSapIni)
cSenhaCRM = Trim$(Left$(cAux, nTam))

'-- FIM SAP CRM
'--------------------------------------------------------------------------------------------------

'InitSap nNumber, cServer, cClient, cUsuario, cSenha
'InitSap nNumber, cServer, cClient, "SDINTERF", "NATURA"
'InitSap nNumber, cServer, cClient, gcUsername, "ELITE"

'InitSap nNumber, cServer, cClient, "USERFC", "SISTEMAS" ' NAO VAI UTILIZAR AQUI

'-- PARA SAP CRM - VERIFICAR QUAL USUARIO E SENHA SERA UTILIZADO PARA LOGAR !!!!!!!!
InitSapCRM nNumberCRM, cServerCRM, cClientCRM, "USERFC", "rfc_natura"

'-- INIT SAP CRM


End Function

'Function LogonSap(SapSysNumber As Integer, SapServer As String, SapClient As String, SapUser As String, SapPass As String) As Boolean
'
'
'  On Error Resume Next
'
'  '01,"172.16.16.231","015","TESTERFC","SILVANO"
'   SapObjeto_G.Connection.ApplicationServer = SapServer
'  'SapConnection.System = "Desenvolvimento"
'   SapObjeto_G.Connection.SystemNumber = SapSysNumber
'   SapObjeto_G.Connection.Client = SapClient
'   SapObjeto_G.Connection.USER = SapUser
'   SapObjeto_G.Connection.Password = SapPass
'   SapObjeto_G.Connection.Language = "P"
'   If SapObjeto_G.Connection.Logon(0, True) Then
'      LogonSap = True
'   Else
'      LogonSap = False
'   End If
'
'End Function

Function LogonSapCRM(SapSysNumber As Integer, SapServer As String, SapClient As String, SapUser As String, SapPass As String) As Boolean

   'Loga com o Sap
   '
   'Exemplo: LogonSap(5, "CPD04006", "050", "UF012345", "xxxyyy") = True
   '
   'Parametros:
   '   1. System Number
   '   2. Application Server
   '   3. Client
   '   4. User
   '   5. Password
   '
   'Retorna True se conseguiu e False se n?o

    On Error Resume Next

   SapObjeto_CRM.Connection.ApplicationServer = SapServer
   SapObjeto_CRM.Connection.SystemNumber = SapSysNumber
   SapObjeto_CRM.Connection.Client = SapClient
   SapObjeto_CRM.Connection.USER = SapUser
   SapObjeto_CRM.Connection.Password = SapPass
   SapObjeto_CRM.Connection.Language = "P"
   If SapObjeto_CRM.Connection.Logon(0, True) Then
      LogonSapCRM = True
   Else
      LogonSapCRM = False
   End If

End Function

Function ValidaViaSap(ByVal nProduto As String, ByVal cDescricao As String) As Variant

   On Error Resume Next
   SAPStart
   Dim SapFuncCRM As Object
   Dim nMaterial As Long
   'Testa a conexao CRM - 23-11-10
    If SapObjeto_CRM.Connection Is Nothing Then
        If Not LogonSapCRM(SapSysNumber_CRM, SapServer_CRM, SapClient_CRM, SapUser_CRM, SapPass_CRM) Then
            MsgBox "Falha na conex�o com o SAP CRM ", vbCritical
            Exit Function
        End If
    End If
    If SapObjeto_CRM.Connection.IsConnected = 0 Then
        If Not LogonSapCRM(SapSysNumber_CRM, SapServer_CRM, SapClient_CRM, SapUser_CRM, SapPass_CRM) Then
            ValidaViaSap = "N"
            Exit Function
        End If
    End If
    
'--------------------------------------------------------------------------------
' Field name      Description                    Datatype    Tam  Is PK?  Default
'--------------------------------------------------------------------------------
' CD_VENDAS  C�digo do produto de vendas        CHAR     20        Y
' DS_VENDAS  Descri��o do c�d.prod.de vendas    CHAR     50        N
' CD_PRODUTO C�digo do produto dentro do SAP    CHAR     18        Y
'--------------------------------------------------------------------------------
    
    
    'Adiciona a funcao RFC SAP CRM - 23-11-10
    'Set SapFuncCRM = SapObjeto_CRM.Add("Z_NTFCR_DOC_VENDA")
    Set SapFuncCRM = SapObjeto_CRM.Add("Z_NTFCR_DOC_VENDA_SINGLE")
    
    SapFuncCRM.Exports("CD_VENDAS") = Right("000000000000000000" & nProduto, 20)
    SapFuncCRM.Exports("DS_VENDAS") = IIf(Len(Trim(cDescricao)) > 50, Left(Trim(cDescricao), 50), Trim(cDescricao))
    SapFuncCRM.Exports("CD_PRODUTO") = "" 'Right("000000000000000000" & nProduto, 18)
    
    'Executa a Funcao
    'Err.Clear
    
    If Not SapFuncCRM.Call Then
        MsgBox SapFuncCRM.Exception & "  " & Err.Description
        ValidaViaSap = SapFuncCRM.Exception
        SapObjeto_CRM.Connection.Logoff
        Exit Function
    End If

    ValidaViaSap = "OK"
    
    SapObjeto_CRM.Connection.Logoff
    
    'SapObjeto_G.Connection.Logoff ' VERIFICAR SE SERA UTILIZADO
  
    
End Function

Sub ErroFatal()

'----------------------------------------------------------------------------
' Exibir mensagem ao usu�rio indicando que a aplica��o ser� finalizada
'
' Sintaxe:  ErroFatal
' onde:
'
'
' Autor:
' Data:     14/10/94
' Obs.:
'
' (Declarar seis �ltimas)
' �ltima Altera��o:
' Por:
' Motivo:
'----------------------------------------------------------------------------
Dim DbOra As New DbConnect

 Screen.MousePointer = DEFAULT
 MsgBox "Opera��o Interrompida." & Chr(KEY_RETURN) & "Aplicativo ser� finalizado.", MB_ICONSTOP
 'DbOra.Disconnect
 End

End Sub

