VERSION 5.00
Object = "{CDF3B183-D408-11CE-AE2C-0080C786E37D}#2.0#0"; "EDT32X20.OCX"
Object = "{BE4F3AC8-AEC9-101A-947B-00DD010F7B46}#1.0#0"; "MSOUTL32.OCX"
Begin VB.Form frmVigenciaComercialAux 
   Caption         =   "Manuten��o de Abrang�ncia e Vig�ncia"
   ClientHeight    =   4440
   ClientLeft      =   1590
   ClientTop       =   4035
   ClientWidth     =   8685
   ControlBox      =   0   'False
   Icon            =   "frmVigenciaComercialAux.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4440
   ScaleWidth      =   8685
   Begin VB.Frame Frame4 
      Caption         =   "Vig�ncia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3105
      Left            =   4680
      TabIndex        =   3
      Top             =   120
      Width           =   3975
      Begin EditLib.fpMask txtDtFinal 
         Height          =   315
         Left            =   2220
         TabIndex        =   7
         Top             =   2355
         Width           =   1335
         _Version        =   131072
         _ExtentX        =   2355
         _ExtentY        =   556
         _StockProps     =   68
         BackColor       =   -2147483643
         ForeColor       =   0
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ButtonDisable   =   0   'False
         ButtonHide      =   0   'False
         ButtonIncrement =   1
         ButtonMin       =   0
         ButtonMax       =   100
         ButtonStyle     =   0
         ButtonWidth     =   0
         ButtonWrap      =   -1  'True
         ThreeDText      =   0
         ThreeDTextHighlightColor=   -2147483633
         ThreeDTextShadowColor=   -2147483632
         ThreeDTextOffset=   1
         AlignTextH      =   1
         AlignTextV      =   1
         AllowNull       =   0   'False
         NoSpecialKeys   =   0
         AutoAdvance     =   0   'False
         AutoBeep        =   0   'False
         CaretInsert     =   0
         CaretOverWrite  =   3
         UserEntry       =   0
         HideSelection   =   -1  'True
         InvalidColor    =   -2147483637
         InvalidOption   =   0
         MarginLeft      =   3
         MarginTop       =   3
         MarginRight     =   3
         MarginBottom    =   3
         NullColor       =   -2147483637
         OnFocusAlignH   =   0
         OnFocusAlignV   =   0
         OnFocusNoSelect =   0   'False
         OnFocusPosition =   0
         ControlType     =   0
         AllowOverflow   =   0   'False
         BestFit         =   0   'False
         ClipMode        =   0
         DataFormat      =   -10968
         Mask            =   "##/##/####"
         PromptChar      =   " "
         PromptInclude   =   0   'False
         RequireFill     =   0   'False
         BorderGrayAreaColor=   -2147483637
         NoPrefix        =   0   'False
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         AutoTab         =   0   'False
         MouseIcon       =   "frmVigenciaComercialAux.frx":2072
      End
      Begin EditLib.fpMask txtDtInicio 
         Height          =   315
         Left            =   2280
         TabIndex        =   5
         Top             =   915
         Width           =   1335
         _Version        =   131072
         _ExtentX        =   2355
         _ExtentY        =   556
         _StockProps     =   68
         BackColor       =   -2147483643
         ForeColor       =   0
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ButtonDisable   =   0   'False
         ButtonHide      =   0   'False
         ButtonIncrement =   1
         ButtonMin       =   0
         ButtonMax       =   100
         ButtonStyle     =   0
         ButtonWidth     =   0
         ButtonWrap      =   -1  'True
         ThreeDText      =   0
         ThreeDTextHighlightColor=   -2147483633
         ThreeDTextShadowColor=   -2147483632
         ThreeDTextOffset=   1
         AlignTextH      =   1
         AlignTextV      =   1
         AllowNull       =   0   'False
         NoSpecialKeys   =   0
         AutoAdvance     =   0   'False
         AutoBeep        =   0   'False
         CaretInsert     =   0
         CaretOverWrite  =   3
         UserEntry       =   0
         HideSelection   =   -1  'True
         InvalidColor    =   -2147483637
         InvalidOption   =   0
         MarginLeft      =   3
         MarginTop       =   3
         MarginRight     =   3
         MarginBottom    =   3
         NullColor       =   -2147483637
         OnFocusAlignH   =   0
         OnFocusAlignV   =   0
         OnFocusNoSelect =   0   'False
         OnFocusPosition =   0
         ControlType     =   0
         AllowOverflow   =   0   'False
         BestFit         =   0   'False
         ClipMode        =   0
         DataFormat      =   -10968
         Mask            =   "##/##/####"
         PromptChar      =   " "
         PromptInclude   =   0   'False
         RequireFill     =   0   'False
         BorderGrayAreaColor=   -2147483637
         NoPrefix        =   0   'False
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         AutoTab         =   0   'False
         MouseIcon       =   "frmVigenciaComercialAux.frx":208E
      End
      Begin VB.ComboBox cmbCicloFinal 
         Height          =   360
         ItemData        =   "frmVigenciaComercialAux.frx":20AA
         Left            =   330
         List            =   "frmVigenciaComercialAux.frx":20AC
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   2310
         Width           =   1605
      End
      Begin VB.ComboBox cmbCicloInicial 
         Height          =   360
         ItemData        =   "frmVigenciaComercialAux.frx":20AE
         Left            =   330
         List            =   "frmVigenciaComercialAux.frx":20B0
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   900
         Width           =   1545
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Data Final:"
         ForeColor       =   &H80000007&
         Height          =   240
         Left            =   2220
         TabIndex        =   13
         Top             =   1980
         Width           =   960
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Data Inicial:"
         ForeColor       =   &H80000007&
         Height          =   240
         Left            =   2280
         TabIndex        =   12
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo Final"
         Height          =   240
         Left            =   360
         TabIndex        =   10
         Top             =   1950
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo Inicial"
         Height          =   240
         Left            =   420
         TabIndex        =   8
         Top             =   600
         Width           =   1005
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   2310
      TabIndex        =   2
      Top             =   3270
      Width           =   3885
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Default         =   -1  'True
         Height          =   765
         Left            =   390
         Picture         =   "frmVigenciaComercialAux.frx":20B2
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   180
         Width           =   1065
      End
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   765
         Left            =   2340
         Picture         =   "frmVigenciaComercialAux.frx":23BC
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   180
         Width           =   1125
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Abrang�ncia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3105
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin MSOutl.Outline outEstrutura 
         Height          =   2280
         Left            =   180
         TabIndex        =   1
         ToolTipText     =   "Estrutura Comercial"
         Top             =   600
         Width           =   4095
         _Version        =   65536
         _ExtentX        =   7223
         _ExtentY        =   4022
         _StockProps     =   77
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   4
         PicturePlus     =   "frmVigenciaComercialAux.frx":26C6
         PictureMinus    =   "frmVigenciaComercialAux.frx":2838
         PictureLeaf     =   "frmVigenciaComercialAux.frx":29AA
         PictureOpen     =   "frmVigenciaComercialAux.frx":2B1C
         PictureClosed   =   "frmVigenciaComercialAux.frx":2C8E
      End
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Confirmar"
      End
      Begin VB.Menu mnuCancelar 
         Caption         =   "C&ancelar"
      End
   End
End
Attribute VB_Name = "frmVigenciaComercialAux"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim varRE     As Integer
Dim varGV     As Integer
Dim varSET    As Integer

Dim vCiclIni  As Variant
Dim vDataIni  As Variant
Dim vCiclFin  As Variant
Dim vDataFin  As Variant
Dim cSql      As String
    
Private Sub cmdConfirmar_Click()

    Dim vTip  As String
    Dim vCod  As String
    Dim vOper As Variant
    
    If Trim(outEstrutura.Text) = "" Then
       MsgBox "Aten��o...Abrang�ncia � Obrigat�rio.", vbCritical, "Aviso"
       Exit Sub
    End If
    
    'codigo da estrutura comercial
    vCod = outEstrutura.ItemData(outEstrutura.ListIndex)
    'tipo da estrutura comercial
    vTip = "0"
    If Val(varRE) > 0 Then
       vTip = "2"
    End If
    If Val(varGV) > 0 Then
       vTip = "3"
    End If
    If Val(varSET) > 0 Then
       vTip = "4"
    End If

    If cmbCicloInicial.Text = "" And txtDtInicio.UnFmtText = "" Then
       MsgBox "Aten��o...O In�cio da Vig�ncia � Obrigat�rio.", vbCritical, "Aviso"
       Exit Sub
    End If
    If cmbCicloInicial.Text <> "" And txtDtInicio.UnFmtText <> "" Then
       MsgBox "Aten��o...Escolha In�cio da Vig�ncia pelo Ciclo ou pela Data.", vbCritical, "Aviso"
       cmbCicloInicial.SetFocus
       Exit Sub
    End If
    If cmbCicloFinal.Text <> "" And txtDtFinal.UnFmtText <> "" Then
       MsgBox "Aten��o...Escolha Fim da Vig�ncia pelo Ciclo ou pela Data.", vbCritical, "Aviso"
       cmbCicloFinal.SetFocus
       Exit Sub
    End If
    
    If txtDtInicio.Text <> "" Then
        If Not IsDate(txtDtInicio.Text) Then
            MsgBox "Aten��o...A data inicial informada est� inv�lida.", vbCritical, "Aviso"
            txtDtInicio.SetFocus
            Exit Sub
        End If
    End If
    
    If txtDtFinal.Text <> "" Then
        If Not IsDate(txtDtFinal.Text) Then
            MsgBox "Aten��o...A data final informada est� inv�lida.", vbCritical, "Aviso"
            txtDtFinal.SetFocus
            Exit Sub
        End If
    End If
    
    'validar vigencia
    If Not ValidarVigencia(vTip, vCod) Then Exit Sub

    If indicaAltera = 1 Then
       frmVigenciaComercial.spdAVIG.Row = frmVigenciaComercial.spdAVIG.ActiveRow
    Else
       frmVigenciaComercial.spdAVIG.MaxRows = frmVigenciaComercial.spdAVIG.MaxRows + 1
       frmVigenciaComercial.spdAVIG.Row = frmVigenciaComercial.spdAVIG.MaxRows
       'estrutura comercial
       If Val(vTip) = 0 Then
          frmVigenciaComercial.spdAVIG.SetText 1, frmVigenciaComercial.spdAVIG.Row, vCod & " - " & Trim(outEstrutura.Text)
       Else
          frmVigenciaComercial.spdAVIG.SetText 1, frmVigenciaComercial.spdAVIG.Row, Trim(outEstrutura.Text)
       End If
    End If
    
    'ciclo inicial
    frmVigenciaComercial.spdAVIG.SetText 2, frmVigenciaComercial.spdAVIG.Row, cmbCicloInicial.Text
    'data inicial
    'Cleiton - Cadmus 12/09/2007
    'Colocado o format na data para grava��o na base, pois ocorria erro na procedure de grava��o.
    frmVigenciaComercial.spdAVIG.SetText 3, frmVigenciaComercial.spdAVIG.Row, Format(txtDtInicio.Text, "dd/mm/yyyy")
    'ciclo final
    frmVigenciaComercial.spdAVIG.SetText 4, frmVigenciaComercial.spdAVIG.Row, cmbCicloFinal.Text
    'data final
    'Cleiton - Cadmus 12/09/2007
    'Colocado o format na data para grava��o na base, pois ocorria erro na procedure de grava��o.
    frmVigenciaComercial.spdAVIG.SetText 5, frmVigenciaComercial.spdAVIG.Row, Format(txtDtFinal.Text, "dd/mm/yyyy")
    'status
    frmVigenciaComercial.spdAVIG.SetText 6, frmVigenciaComercial.spdAVIG.Row, "ATIVA"
    'estrutura comercial
    If indicaAltera = 0 Then
       frmVigenciaComercial.spdAVIG.SetText 8, frmVigenciaComercial.spdAVIG.Row, vTip
    End If
    'cod estrutura comercial
    frmVigenciaComercial.spdAVIG.SetText 9, frmVigenciaComercial.spdAVIG.Row, vCod
    If indicaAltera = 0 Then
        'regiao estrategia
        frmVigenciaComercial.spdAVIG.SetText 10, frmVigenciaComercial.spdAVIG.Row, varRE
        'gerencia de vendas
        frmVigenciaComercial.spdAVIG.SetText 11, frmVigenciaComercial.spdAVIG.Row, varGV
        'setor
        frmVigenciaComercial.spdAVIG.SetText 12, frmVigenciaComercial.spdAVIG.Row, varSET
    End If
    'inclusao ou alteracao
    frmVigenciaComercial.spdAVIG.GetText 17, frmVigenciaComercial.spdAVIG.Row, vOper
    If vOper <> "I" Then
       If indicaAltera = 1 Then
          frmVigenciaComercial.spdAVIG.SetText 17, frmVigenciaComercial.spdAVIG.Row, "A"
       Else
          frmVigenciaComercial.spdAVIG.SetText 17, frmVigenciaComercial.spdAVIG.Row, "I"
       End If
    End If
    
    frmVigenciaComercial.sLiberaSalvar

    Unload Me

End Sub

Private Sub cmdCancelar_Click()
   
    Dim X As Integer

    indicaAltera = 0
       
    For X = 1 To outEstrutura.ListCount - 1
        outEstrutura.Expand(X) = False
    Next
    
    Unload Me
    
End Sub

Private Sub Form_Activate()

    Dim X    As Integer
    Dim vLin As Integer
    Dim vInt As Integer
    
    Dim vSeq As Variant
    Dim vTip As Variant
    Dim vRE  As Variant
    Dim vGV  As Variant
    Dim vSet As Variant
    Dim vCod As Variant
    
    vLin = frmVigenciaComercial.spdAVIG.ActiveRow
    
    frmVigenciaComercial.spdAVIG.GetText 2, vLin, vCiclIni
    frmVigenciaComercial.spdAVIG.GetText 3, vLin, vDataIni
    frmVigenciaComercial.spdAVIG.GetText 4, vLin, vCiclFin
    frmVigenciaComercial.spdAVIG.GetText 5, vLin, vDataFin
    
    frmVigenciaComercial.spdAVIG.GetText 7, vLin, vSeq
    frmVigenciaComercial.spdAVIG.GetText 8, vLin, vTip
    frmVigenciaComercial.spdAVIG.GetText 9, vLin, vCod
    frmVigenciaComercial.spdAVIG.GetText 10, vLin, vRE
    frmVigenciaComercial.spdAVIG.GetText 11, vLin, vGV
    frmVigenciaComercial.spdAVIG.GetText 12, vLin, vSet
    
    outEstrutura.Enabled = True
    
    'limpar as variaveis
    cmbCicloInicial.ListIndex = -1
    cmbCicloFinal.ListIndex = -1
    txtDtInicio.Text = ""
    txtDtFinal.Text = ""
    
    varRE = 0
    varGV = 0
    varSET = 0
    
    If indicaAltera = 1 Then
    
       varRE = Val(vRE)
       varGV = Val(vGV)
       varSET = Val(vSet)
    
       'data de vigencia
       txtDtInicio.Text = vDataIni
       txtDtFinal.Text = vDataFin
       
       'ciclo
       If vCiclIni <> "" Then
          vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
          For X = 0 To cmbCicloInicial.ListCount - 1
              If cmbCicloInicial.ItemData(X) = vCiclIni Then
                 cmbCicloInicial.ListIndex = X
                 Exit For
              End If
          Next
       End If
       If vCiclFin <> "" Then
          vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
          For X = 0 To cmbCicloFinal.ListCount - 1
              If cmbCicloFinal.ItemData(X) = vCiclFin Then
                 cmbCicloFinal.ListIndex = X
                 Exit For
              End If
          Next
       End If
       
       'brasil
       If vTip = 0 Then
          outEstrutura.ListIndex = 0
       End If
       
       'regiao estrategica
       For X = 0 To outEstrutura.ListCount - 1
           If Val(vRE) > 0 Then
              If outEstrutura.ItemData(X) = vRE And outEstrutura.Indent(X) = 1 Then
                 outEstrutura.ListIndex = X
                 If Val(vGV) > 0 Then
                    outEstrutura.Expand(X) = True
                    vInt = outEstrutura.Indent(X)
                 End If
                 Exit For
              End If
           End If
       Next
       
       'gerencia de vendas
       If Val(vGV) > 0 Then
          For X = outEstrutura.ListIndex + 1 To outEstrutura.ListCount - 1
              If outEstrutura.ItemData(X) = vGV And outEstrutura.Indent(X) = 2 Then
                 outEstrutura.ListIndex = X
                 If Val(vSet) > 0 Then
                    outEstrutura.Expand(X) = True
                    vInt = outEstrutura.Indent(X)
                 End If
                 Exit For
              End If
          Next
       End If
        
       'setor
       If Val(vSet) > 0 Then
          For X = outEstrutura.ListIndex + 1 To outEstrutura.ListCount - 1
              If outEstrutura.ItemData(X) = vSet And outEstrutura.Indent(X) = 3 Then
                 outEstrutura.ListIndex = X
                 Exit For
              End If
          Next
       End If
       
       outEstrutura.Enabled = False
       
    End If

End Sub

Private Sub Form_Load()

    Dim s As String
    
    s = UCase(Environ("_U"))
    
    Screen.MousePointer = 11
    
    'Carrega listbox de ciclo
    cmbCicloInicial.AddItem " "
    cmbCicloFinal.AddItem " "
    
    Call Carrega_Ciclo(cmbCicloInicial)
    Call Carrega_Ciclo(cmbCicloFinal)
         
    'Insere item na outline
    outEstrutura.Clear
   'outEstrutura.AddItem "N�VEL BRASIL", 0
   'outEstrutura.Indent(0) = 0
   'outEstrutura.ItemData(0) = 1
       
    'Carrega Estrutura Comercial
    Call Carrega_Estrutura_Comercial(outEstrutura)
    
    Screen.MousePointer = 1

End Sub

Private Sub mnuCancelar_Click()
    indicaAltera = 0
    Unload Me
End Sub

Private Sub mnuSalvar_Click()
    cmdConfirmar_Click
End Sub

Private Sub outEstrutura_Click()

    If outEstrutura.ListIndex = 0 Then
       varRE = 0
       varGV = 0
       varSET = 0
    End If

    If outEstrutura.Indent(outEstrutura.ListIndex) = 1 Then
       varRE = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
       varGV = 0
       varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 2 Then
           varGV = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
           varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 3 Then
           varSET = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
    End If
    
End Sub

Private Sub outEstrutura_DblClick()

    If outEstrutura.ListIndex = 0 Then
       varRE = 0
       varGV = 0
       varSET = 0
    End If
    
    If outEstrutura.Expand(outEstrutura.ListIndex) Then
       outEstrutura.Expand(outEstrutura.ListIndex) = False
    Else
       outEstrutura.Expand(outEstrutura.ListIndex) = True
    End If

    If outEstrutura.Indent(outEstrutura.ListIndex) = 1 Then
       varRE = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
       varGV = 0
       varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 2 Then
           varGV = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
           varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 3 Then
           varSET = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
    End If
  
End Sub

Private Sub outEstrutura_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        If outEstrutura.Expand(outEstrutura.ListIndex) Then
           outEstrutura.Expand(outEstrutura.ListIndex) = False
        Else
           outEstrutura.Expand(outEstrutura.ListIndex) = True
        End If
    End If

End Sub

Private Function ValidarVigencia(vTipo, vCod) As Boolean

    Dim vInicio As Long
    Dim vFinal  As Long
 
    vCiclIni = cmbCicloInicial.Text
    vDataIni = txtDtInicio.Text
    vCiclFin = cmbCicloFinal.Text
    vDataFin = txtDtFinal.Text
    
    ValidarVigencia = False

    If Trim(vCiclIni) <> "" And Trim(vCiclFin) <> "" Then
       '
       vInicio = Right(vCiclIni, 4) & Left(vCiclIni, 2)
       vFinal = Right(vCiclFin, 4) & Left(vCiclFin, 2)
       '
       If vFinal < vInicio Then
          MsgBox "A Vig�ncia Final deve ser Igual ou Superior a Vig�ncia Inicial.", vbCritical, "Aten��o"
          Exit Function
       End If
       '
    Else
       'seleciona a vigencia do ciclo
       'data inicial
       If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
          vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
          'vigencia
          If Not SelecionarVigenciaCiclo(vCiclIni, vTipo, vCod) Then Exit Function
          'determinou a data de inicio
          vDataIni = vDataInicioCiclo
       End If
       'data final
       If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
          vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
          'vigencia
          If Not SelecionarVigenciaCiclo(vCiclFin, vTipo, vCod) Then Exit Function
          'determinou a data de inicio
          vDataFin = vDataFinalCiclo
       End If
       'caso nao tenha final
       If Trim(vDataFin) = "" Then
          vDataFin = "31/12/9999"
       End If
        
      'If Trim(vDataIni) = "" Then
      '   MsgBox "N�o Possui Ciclo Cadastrado Para a Estrutura Comercial Selecionada.", vbCritical, "Aten��o"
      '   Exit Function
      'End If
        
       If DateValue(vDataFin) < DateValue(vDataIni) Then
          MsgBox "A Vig�ncia Final deve ser Igual ou Superior a Vig�ncia Inicial.", vbCritical, "Aten��o"
          Exit Function
       End If
    End If
    
    ValidarVigencia = True

End Function



