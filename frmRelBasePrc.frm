VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmRelBasePrc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relat�rio de Base de Pre�os"
   ClientHeight    =   8160
   ClientLeft      =   285
   ClientTop       =   1410
   ClientWidth     =   14295
   Icon            =   "frmRelBasePrc.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   14295
   Begin ComctlLib.Toolbar st_Toolbar 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   14295
      _ExtentX        =   25215
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   3
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Gravar"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.TextBox txtVersao 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7890
      TabIndex        =   5
      TabStop         =   0   'False
      Text            =   "Text1"
      Top             =   810
      Visible         =   0   'False
      Width           =   2205
   End
   Begin VB.CommandButton cmdExporta 
      Caption         =   "Exportar para Excel"
      Height          =   495
      Left            =   11760
      TabIndex        =   2
      Top             =   720
      Width           =   2115
   End
   Begin FPSpread.vaSpread grdBasePreco 
      Height          =   6735
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   14055
      _Version        =   131077
      _ExtentX        =   24791
      _ExtentY        =   11880
      _StockProps     =   64
      ColHeaderDisplay=   0
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   8
      MaxRows         =   1
      SpreadDesigner  =   "frmRelBasePrc.frx":0E42
      UserResize      =   1
      VisibleCols     =   5
      VisibleRows     =   20
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   11
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":121C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":1536
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":1850
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":1B6A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":1E84
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":219E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":24B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":27D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":2AEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":2E06
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelBasePrc.frx":3120
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblMeioComunic 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   180
      TabIndex        =   3
      Top             =   810
      Width           =   4125
   End
   Begin VB.Label lblCiclo 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5970
      TabIndex        =   1
      Top             =   840
      Width           =   1185
   End
   Begin VB.Label Label1 
      Caption         =   "Ciclo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5280
      TabIndex        =   0
      Top             =   840
      Width           =   645
   End
End
Attribute VB_Name = "frmRelBasePrc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim iVersao As Integer
Dim ExcelApp As Object

Private Sub cmdExporta_Click()

'On Error GoTo fim

'    cD.DialogTitle = "Selecione o local e o nome do arquivo para grava��o"
'    cD.Filter = "Planilha Excel  (*.xls)|*.XLS"
'    cD.DefaultExt = ""
'    cD.FilterIndex = 0
'    cD.CancelError = True
'    cD.Flags = 0
'    cD.InitDir = App.Path
    sExportaExcel

End Sub

Public Sub sExportaExcel()

    Dim strNomeArquivo  As String
    Dim X               As Double
    Dim Y               As Double
    Dim vConteudoCelula As String
    Dim vLinha          As Double
    Dim strBody         As String
       
       
    On Error GoTo trata_erro
    
    Screen.MousePointer = vbHourglass
    
    DoEvents
        
    If ExcelSheet Is Nothing Then
        Set ExcelSheet = CreateObject("Excel.Sheet")
    Else
        Set ExcelSheet = Nothing
        Set ExcelSheet = CreateObject("Excel.Sheet")
    End If
    
    If ExcelSheet.Application.Visible = False Then
        ExcelSheet.Application.Visible = True
    End If
    
    vLinha = 1
    
    ExcelSheet.ActiveSheet.Cells(vLinha, 2).Value = lblMeioComunic.Caption
    ExcelSheet.ActiveSheet.Cells(vLinha, 4).Value = Label1.Caption
    ExcelSheet.ActiveSheet.Cells(vLinha, 6).NumberFormat = "@"
    ExcelSheet.ActiveSheet.Cells(vLinha, 6).Value = "'" & lblCiclo.Caption
    
    vLinha = 2
        
    ExcelSheet.ActiveSheet.Columns(7).NumberFormat = "@"
'    ExcelSheet.ActiveSheet.Columns(9).NumberFormat = "@"
'    ExcelSheet.ActiveSheet.Columns(15).NumberFormat = "@"
'    ExcelSheet.ActiveSheet.Columns(17).NumberFormat = "@"
        
    With grdBasePreco
    
        For Y = 1 To .MaxCols
            .Row = 0
            .Col = Y
            vConteudoCelula = .Text
            ExcelSheet.ActiveSheet.Cells(vLinha, Y).Value = vConteudoCelula
        Next Y

        For X = 1 To .MaxRows
                    
            .Row = X
            .Col = 5
            vConteudoCelula = .Text
            
            If Len(Trim(vConteudoCelula)) > 0 Then
            
                vLinha = vLinha + 1
        
                strBody = ""
                
                For Y = 1 To .MaxCols
        
                    .Col = Y
                    vConteudoCelula = .Text
                    ExcelSheet.ActiveSheet.Cells(vLinha, Y).Value = vConteudoCelula
                Next Y
            End If
        Next X
    End With
    
    Screen.MousePointer = vbNormal
            
    MsgBox "Relat�rio Base de Pre�os Exportado com sucesso.", vbInformation, App.Title
        
    Exit Sub
    
trata_erro:
    
    Screen.MousePointer = vbNormal
    
    MsgBox "Ocorreu o erro " & Err.Number & " - " & Err.Description & " ao exportar para o excell!!"

End Sub

Private Sub GravarRelatorio()

    Dim X             As Double
    Dim Linha         As Long
    Dim AlterouColuna As Long
    Dim cSql        As String
    Dim bResp       As Boolean
    Dim cdAnt       As Long
    Dim Y           As Long
    Dim se_Usuario  As String
    Dim ne_ret     As Long
    
    For X = 1 To grdBasePreco.MaxRows
    
        grdBasePreco.Row = X
        grdBasePreco.Col = 7
        
        If grdBasePreco.GetRowItemData(X) = 999 Then
    
'            Me.MousePointer = vbHourglass
'            Me.Refresh
            
'            GeraBasePreco = False
            
            se_Usuario = Space(255)
            ne_ret = GetUserName(se_Usuario, 255)
            se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
            With frmParmRelBasePrc
                cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
                cSql = cSql & "exec stp_pdr_base_preco_u " & .cmbCiclo.ItemData(.cmbCiclo.ListIndex) & " , "
                cSql = cSql & .cmbMeioComunic.ItemData(.cmbMeioComunic.ListIndex) & " , "
                cSql = cSql & .cmbListaPrc.Text & ", "
                cSql = cSql & Val(txtVersao.Text) & ", "
                cSql = cSql & X & ", '"
                cSql = cSql & grdBasePreco.Text & " ', "
                cSql = cSql & "'" & se_Usuario & "', "
                cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
            
                If gObjeto.natPrepareQuery(hSql, cSql) Then
                   Do While gObjeto.natFetchNext(hSql)
                   Loop
                End If
                  
                If gObjeto.nErroSQL <> 0 Then
                   MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                   Exit Sub
                End If
            End With
        End If
    Next X
    
    st_Toolbar.Buttons.Item(1).Enabled = False

End Sub

Private Sub grdBasePreco_Change(ByVal Col As Long, ByVal Row As Long)
    If Col = 7 Then
        grdBasePreco.SetRowItemData Row, 999
        st_Toolbar.Buttons.Item(1).Enabled = True
    End If
End Sub

Private Sub st_Toolbar_ButtonClick(ByVal Button As ComctlLib.Button)

    Select Case Button.Index
    
        Case 1
            Call GravarRelatorio
            
        Case 3
            Unload Me
            
    End Select
    
End Sub

