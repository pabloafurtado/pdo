Attribute VB_Name = "Funcoes_SQL"
'----------------------------------------------------------------------------------------
'Altera��o Projeto Relacionamento
'Data     : 06/08/2003
'Autor    : Renato Ferrarezi
'Altera��o: Cria��o do arry global segmentos para atender os segmentos a serem gravadas
'           as rela��es de venda
'           Altera��o das rotinas de grava��o e cancelamento de relacionamento para
'           ler o array de segmentos
'----------------------------------------------------------------------------------------
Option Explicit

'Vari�veis Modulares
Global cSQL       As String
Global bResp      As Boolean
Global pResp      As Boolean
Global nRet       As Variant
Global DbOra      As New DbConnect

Global sql_cd_ret As Long
Global sql_cd_opr As String
Global sql_nm_tab As String
Global sql_qt_lnh As Long

'Guarda valor da vari�vel se a descri��o de venda foi alterada
Global gbDescricaoAlterada   As Boolean
Global gsDescricaoSugestao   As String
Global gsDescricaoSugestao2  As String
Global gsDescricaoSugestao3  As String
Global gvVendaProduto        As Variant
Global gvProdutoTabelaPreco  As Variant
Global gvIdCodigoEspecial    As Variant
Global gvCdFamiliaProduto    As Variant
Global gvCdLinhaProduto      As Variant
Global gvCdGrupoProduto      As Variant
Global gvCdSubgrupoProduto   As Variant
Global gvIdTipoProduto       As Variant
Global gvIdSituacaoProduto   As Variant
Global gvIdVendaPermitida    As Variant
Global gvAgrupador           As Variant
Global gbData                As Boolean
Global atualPrior            As Variant

Global gCarregaCiclo         As Boolean

Dim avResult() As Variant
