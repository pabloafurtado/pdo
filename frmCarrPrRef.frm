VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmCarrPrRef 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Pre�os e Listas"
   ClientHeight    =   5070
   ClientLeft      =   3675
   ClientTop       =   2055
   ClientWidth     =   5805
   Icon            =   "frmCarrPrRef.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5070
   ScaleWidth      =   5805
   Visible         =   0   'False
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdCarregar 
      Caption         =   "Carregar"
      Default         =   -1  'True
      Height          =   825
      Left            =   2040
      Picture         =   "frmCarrPrRef.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3780
      Width           =   1605
   End
   Begin VB.Frame Frame1 
      Caption         =   "Par�metros para Carga"
      Height          =   3015
      Left            =   60
      TabIndex        =   1
      Top             =   690
      Width           =   5655
      Begin VB.TextBox txtExcCar 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000000&
         Height          =   375
         Left            =   3780
         Locked          =   -1  'True
         TabIndex        =   13
         Text            =   "50"
         Top             =   1920
         Visible         =   0   'False
         Width           =   795
      End
      Begin VB.TextBox txtListaGer 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000000&
         Height          =   375
         Left            =   3780
         Locked          =   -1  'True
         TabIndex        =   12
         Text            =   "9"
         Top             =   1440
         Visible         =   0   'False
         Width           =   795
      End
      Begin VB.TextBox txtListaLib 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000000&
         Height          =   375
         Left            =   3780
         Locked          =   -1  'True
         TabIndex        =   11
         Text            =   "2450"
         Top             =   2400
         Visible         =   0   'False
         Width           =   795
      End
      Begin VB.TextBox txtPrecoCar 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000000&
         Height          =   375
         Left            =   3780
         Locked          =   -1  'True
         TabIndex        =   10
         Text            =   "2450"
         Top             =   960
         Visible         =   0   'False
         Width           =   795
      End
      Begin VB.ComboBox cmbCiclo 
         Height          =   360
         ItemData        =   "frmCarrPrRef.frx":1194
         Left            =   2100
         List            =   "frmCarrPrRef.frx":119B
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   480
         Width           =   1545
      End
      Begin VB.Label lblExcCar 
         Caption         =   "Exce��es de Pre�os Carregados..."
         Height          =   255
         Left            =   300
         TabIndex        =   9
         Top             =   2040
         Visible         =   0   'False
         Width           =   3555
      End
      Begin VB.Image imgExcCar 
         Height          =   480
         Left            =   4620
         Picture         =   "frmCarrPrRef.frx":11A9
         Top             =   1350
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image imgListaLib 
         Height          =   480
         Left            =   4620
         Picture         =   "frmCarrPrRef.frx":14B3
         Top             =   2310
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image imgListaGer 
         Height          =   480
         Left            =   4620
         Picture         =   "frmCarrPrRef.frx":17BD
         Top             =   1830
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image imgPrecoCar 
         Height          =   480
         Left            =   4620
         Picture         =   "frmCarrPrRef.frx":1AC7
         Top             =   870
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblListaLib 
         Caption         =   "Listas de Pre�o Geradas..."
         Height          =   255
         Left            =   300
         TabIndex        =   8
         Top             =   2520
         Visible         =   0   'False
         Width           =   3435
      End
      Begin VB.Label lblListaGer 
         Caption         =   "Relac. Listas X Estrut. Coml efetuada..."
         Height          =   255
         Left            =   300
         TabIndex        =   7
         Top             =   1560
         Visible         =   0   'False
         Width           =   3615
      End
      Begin VB.Label lblPrecoCar 
         Caption         =   "Pre�os de Refer�ncia Carregados..."
         Height          =   255
         Left            =   300
         TabIndex        =   6
         Top             =   1080
         Visible         =   0   'False
         Width           =   3315
      End
      Begin VB.Label Label1 
         Caption         =   "Ciclo:"
         Height          =   255
         Left            =   1380
         TabIndex        =   0
         Top             =   540
         Width           =   555
      End
   End
   Begin VB.Label lblTipoProcesso 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   60
      TabIndex        =   5
      Top             =   4710
      Visible         =   0   'False
      Width           =   60
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   120
      Top             =   3885
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   1
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCarrPrRef.frx":1DD1
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCarrPrRef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nmCicloAnt    As String

Private Sub LimpaTela()

    
    lblPrecoCar.Visible = False
    imgPrecoCar.Visible = False
    lblListaGer.Visible = False
    imgListaGer.Visible = False
    lblListaLib.Visible = False
    imgListaLib.Visible = False
    lblExcCar.Visible = False
    imgExcCar.Visible = False
    txtPrecoCar.Visible = False
    txtListaGer.Visible = False
    txtListaLib.Visible = False
    txtExcCar.Visible = False
    
    
    lblTipoProcesso.Visible = False
    DoEvents
    

End Sub

Private Sub cmbCiclo_Change()

    If Len(cmbCiclo.Text) > 5 Then
        If InStr(1, cmbCiclo.Text, "/") = 0 Then
            cmbCiclo.Text = Left(cmbCiclo.Text, 2) & "/" & Right(cmbCiclo.Text, 4)
        End If
    End If
    
End Sub

Private Sub cmdCarregar_Click()
    
    Dim nmCiclo       As String

    If Trim$(cmbCiclo.Text) = "" Then
        MsgBox "Ciclo deve ser preenchido !", vbInformation, "A T E N � � O"
        cmbCiclo.SetFocus
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    nmCiclo = Right(cmbCiclo.Text, 4) + Left(cmbCiclo, 2)
        
    '----------------------------------------------
    'Inicia transa��o
    '----------------------------------------------
    If Not Runquery("BEGIN TRANSACTION", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        PonteiroMouse vbNormal
        Exit Sub
    End If
            
    '----------------------------------------------
    'Carga do pre�o de refer�ncia
    '----------------------------------------------
    If Not fCargaPrRef(nmCiclo, MSG$) Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        Call LimpaTela
        PonteiroMouse vbNormal
        cmbCiclo.SetFocus
        GoTo Rollback
    End If
    
    '------------------------------------------------------
    'Carga da relacao lista X estrutura comercial
    '-----------------------------------------------------
    If Not CarregarRelacaoListas(nmCiclo, nmCicloAnt, MSG$) Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        Call LimpaTela
        PonteiroMouse vbNormal
        cmbCiclo.SetFocus
        GoTo Rollback
    End If
        
    '----------------------------------------------
    'Carga de excecoes
    '----------------------------------------------
    If Not CarregarExcecoes(nmCiclo, nmCicloAnt, MSG$) Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        Call LimpaTela
        PonteiroMouse vbNormal
        cmbCiclo.SetFocus
        GoTo Rollback
    End If
    
    '----------------------------------------------
    'fecha transa��o
    '----------------------------------------------
    If Not Runquery("commit", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        Call LimpaTela
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    '----------------------------------------------
    'Gera��o da lista de pre�o
    '----------------------------------------------
    ReDim LogGeraLista(0)
    If Not LiberarListas(nmCiclo, MSG$) Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        lblTipoProcesso.Caption = ""
        PonteiroMouse vbNormal
        cmbCiclo.SetFocus
    End If
            
    lblTipoProcesso.Caption = ""
        
    If UBound(LogGeraLista) > 0 Then
        Call ExibeLog
    Else
        MsgBox "Processo de Carga de Pre�o de referencia conclu�do com sucesso!!", vbInformation, Me.Caption
    End If
            
    'SE 6346 - In�cio
    If Not GravaHistorico(5, "I", "Ciclo", nmCiclo, "", "", "", "") Then
        MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
            
    LimpaTela
    
    Exit Sub
    
Rollback:

    If Not Runquery("ROLLBACK", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
End Sub

Private Function CarregarExcecoes(nm_ciclo_operacional As String, pCicloAnt As String, MSG$) As Boolean
        
Dim sql As String
Dim msg_err As String
Dim se_Usuario As String
Dim ne_ret As Long

CarregarExcecoes = False

On Error GoTo ErroCarregarExcecoesf

    MSG$ = ""
    
    PonteiroMouse vbHourglass
    lblTipoProcesso.Caption = "Aguarde, carregando Exce��es de Pre�os ..."
    lblTipoProcesso.Visible = True
    DoEvents

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    msg_err = ""
    
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), "
    sql = sql & "@sql_qt_lnh int, @msg_err varchar(255)"
    sql = sql & " exec st_pi_imp_excecoes " & nm_ciclo_operacional & "," & pCicloAnt & ",'"
    sql = sql & se_Usuario & "', @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, "
    sql = sql & "@sql_qt_lnh out, @msg_err out"
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    
    msg_err = gObjeto.varOutputParam(4)
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_imp_excecoes " & gObjeto.nServerNum & " = " & gObjeto.sServerMsg & " -  " & msg_err
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a st_pi_imp_excecoes = " & gObjeto.nServerNum & " - " & gObjeto.sServerMsg & "  " & msg_err
        Exit Function
    End If
    
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))
    
    txtExcCar.Text = Format(sql_qt_lnh, "##,##0")

    PonteiroMouse vbNormal
    txtExcCar.Visible = True
    imgExcCar.Visible = True
    lblExcCar.Visible = True
    Refresh

    CarregarExcecoes = True
    
    Exit Function

ErroCarregarExcecoesf:
    MSG$ = "CarregarExcecoesf - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function CarregarRelacaoListas(nm_ciclo_operacional As String, pCicloAnt As String, MSG$) As Boolean
            
Dim sql As String
Dim msg_err As String
Dim se_Usuario As String
Dim ne_ret As Long

CarregarRelacaoListas = False

On Error GoTo ErroCarregarRelacaoListasf

    MSG$ = ""
    
    PonteiroMouse vbHourglass
    lblTipoProcesso.Caption = "Aguarde, carregando Relacionamento Lista x Estrut.Coml..."
    lblTipoProcesso.Visible = True
    Refresh

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    msg_err = ""
    
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), "
    sql = sql & "@sql_qt_lnh int, @msg_err varchar(255)"
    sql = sql & " exec st_pi_imp_lista_estrutura " & nm_ciclo_operacional & "," & pCicloAnt & ",'"
    sql = sql & se_Usuario & "', @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, "
    sql = sql & "@sql_qt_lnh out, @msg_err out"
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    
    msg_err = gObjeto.varOutputParam(4)
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_imp_lista_estrutura " & gObjeto.nServerNum & " = " & gObjeto.sServerMsg & " -  " & msg_err
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a st_pi_imp_lista_estrutura = " & gObjeto.nServerNum & " - " & gObjeto.sServerMsg & "  " & msg_err
        Exit Function
    End If
    
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))
        
    PonteiroMouse vbNormal
    
    txtListaGer.Text = Format(sql_qt_lnh, "##,##0")
    lblListaGer.Visible = True
    imgListaGer.Visible = True
    txtListaGer.Visible = True
    Refresh
    
    CarregarRelacaoListas = True
    
    Exit Function

ErroCarregarRelacaoListasf:
    MSG$ = "CarregarRelacaoListasf - Erro (" & Err & ") - " & Error
    Exit Function
                    
End Function

Private Function LiberarListas(nm_ciclo_operacional As String, MSG$) As Boolean
       
Dim sql        As String
Dim msg_err    As String
Dim se_Usuario As String
Dim ne_ret     As Long
Dim segmento   As Integer
Dim cont       As Integer
ReDim LogGeraLista(0)

LiberarListas = False

On Error GoTo ErroLiberarListas

    MSG$ = ""
    
    PonteiroMouse vbHourglass
    lblTipoProcesso.Caption = "Aguarde, Liberando Listas de Pre�os..."
    lblTipoProcesso.Visible = True
    Refresh

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    
    For segmento = 1 To 4
    
        msg_err = ""
                
        sql = "DECLARE @sql_cd_ret int, "
        sql = sql & "@sql_cd_opr char(1), "
        sql = sql & "@sql_nm_tab char(30), "
        sql = sql & "@sql_qt_lnh int, "
        sql = sql & "@msg_err varchar(255)"
        sql = sql & " exec st_pi_cn_gera_lista_cn "
        sql = sql & nm_ciclo_operacional & ","
        sql = sql & segmento & ","
        sql = sql & "'" & se_Usuario & "', "
        sql = sql & "@sql_cd_ret out, "
        sql = sql & "@sql_cd_opr out, "
        sql = sql & "@sql_nm_tab out, "
        sql = sql & "@sql_qt_lnh out, "
        sql = sql & "@msg_err out"
        
        If gObjeto.natPrepareQuery(hSql, sql) Then
           Do While gObjeto.natFetchNext(hSql)
                 
                cont = UBound(LogGeraLista) + 1
                ReDim Preserve LogGeraLista(cont)
                
                If Left(gObjeto.varResultSet(hSql, 3), 1) <> "6" Then
                    LogGeraLista(cont).cd_material = gObjeto.varResultSet(hSql, 0)
                    LogGeraLista(cont).cd_venda = gObjeto.varResultSet(hSql, 1)
                    LogGeraLista(cont).cd_lista = gObjeto.varResultSet(hSql, 2)
                    LogGeraLista(cont).cd_segmento = gObjeto.varResultSet(hSql, 3)
                    LogGeraLista(cont).descricao = gObjeto.varResultSet(hSql, 5)
                End If
                 
           Loop
           
        End If
        
        msg_err = gObjeto.varOutputParam(4)
        
        If gObjeto.nErroSQL <> 0 Then
            MSG$ = "Erro ao executar a st_pi_cn_gera_lista_cn " & gObjeto.nServerNum & " = " & gObjeto.sServerMsg & " -  " & msg_err
            Exit Function
        End If
        
        sql_cd_ret = CInt(gObjeto.varOutputParam(0))
        
        If sql_cd_ret <> 0 Then
            MSG$ = "Erro ao executar a st_pi_cn_gera_lista_cn = " & gObjeto.nServerNum & " - " & gObjeto.sServerMsg & "  " & msg_err
            Exit Function
        End If
        
        sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
        
    Next segmento
    
    txtListaLib.Text = TotalPrecos(nm_ciclo_operacional) 'Format(sql_qt_lnh, "##,##0")
    
    PonteiroMouse vbNormal
    lblListaLib.Visible = True
    imgListaLib.Visible = True
    txtListaLib.Visible = True
    Refresh

    LiberarListas = True
    
    Exit Function

ErroLiberarListas:
    MSG$ = "LiberarListas - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Sub Form_Load()
    
    LimpaTela

    Call Carrega_Ciclo(cmbCiclo)

End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(5) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
    
    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Index
Case 1:
    Unload Me
End Select
End Sub




Private Function fCargaPrRef(nm_ciclo_operacional As String, MSG$) As Boolean

Dim sql As String
Dim msg_err As String
Dim se_Usuario As String
Dim ne_ret As Long

fCargaPrRef = False

On Error GoTo ErrofCargaPrRef

    MSG$ = ""
    
    PonteiroMouse vbHourglass
    lblTipoProcesso.Caption = "Aguarde, carregando Pre�os de Refer�ncia..."
    lblTipoProcesso.Visible = True
    Refresh

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    msg_err = ""
    
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), "
    sql = sql & "@sql_qt_lnh int, @msg_err varchar(255), @nm_ciclo_ant int "
    sql = sql & " exec st_pi_imp_preco_referencia " & nm_ciclo_operacional & ",'"
    sql = sql & se_Usuario & "', @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, "
    sql = sql & "@sql_qt_lnh out, @msg_err out, @nm_ciclo_ant out"
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    
    msg_err = gObjeto.varOutputParam(4)
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_imp_preco_referencia " & gObjeto.nServerNum & " = " & gObjeto.sServerMsg & " -  " & msg_err
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    
    If sql_cd_ret <> 0 Then
        MSG$ = gObjeto.sServerMsg & "  " & msg_err
        Exit Function
    End If
    
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    nmCicloAnt = gObjeto.varOutputParam(5)
    
    txtPrecoCar.Text = Format(sql_qt_lnh, "##,##0")
    
    PonteiroMouse vbNormal
    lblPrecoCar.Visible = True
    imgPrecoCar.Visible = True
    txtPrecoCar.Visible = True
    Refresh

    fCargaPrRef = True
    
    Exit Function

ErrofCargaPrRef:
    MSG$ = "fCargaPrRef - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Sub ExibeLog()

    Dim TelaLog As Form
    Dim cont As Integer
    
    If UBound(LogGeraLista) > 0 Then
    
        MsgBox "Processo de Carga de Pre�o conclu�do, por�m ocorreram erros ao liberar lista de pre�os", vbInformation, "Aten��o"
    
        PonteiroMouse vbHourglass
    
        Set TelaLog = New frmLogLista
        
        Load TelaLog
        DoEvents
        
        For cont = 1 To UBound(LogGeraLista)
            TelaLog.CriaLinha
            
            TelaLog.RecebeDados 1, LogGeraLista(cont).cd_material
            TelaLog.RecebeDados 2, LogGeraLista(cont).cd_venda
            TelaLog.RecebeDados 3, LogGeraLista(cont).cd_lista
            TelaLog.RecebeDados 4, LogGeraLista(cont).cd_segmento
            TelaLog.RecebeDados 5, LogGeraLista(cont).descricao
        Next cont
        
        PonteiroMouse vbNormal
        
        TelaLog.Show
        
    End If
    
End Sub


Private Function TotalPrecos(nm_ciclo_operacional As String) As String


Dim cSql  As String
Dim bResp As Boolean
Dim i     As Integer
Dim avPrecos()       As Variant

On Error GoTo erro

' Carrega Ciclo
cSql = "SELECT COUNT(*) FROM t_pi_preco_cn WHERE nm_ciclo_operacional = " & nm_ciclo_operacional

ReDim avPrecos(0)
   
bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
If bResp = False Then
  MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
  End
End If

If gObjeto.nErroSQL <> 0 Then
  MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
  Exit Function
End If

TotalPrecos = avPrecos(0, i)

Exit Function

erro:
  MsgBox "Erro ao obter total de pre�os gerados " & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
  Exit Function
End Function


