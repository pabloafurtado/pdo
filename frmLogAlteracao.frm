VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmLogAlteracao 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hist�rico de Altera��es"
   ClientHeight    =   4695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10815
   Icon            =   "frmLogAlteracao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4695
   ScaleWidth      =   10815
   Begin ComctlLib.Toolbar Toolbar1 
      Height          =   630
      Left            =   9960
      TabIndex        =   2
      Top             =   120
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1111
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   6255
      Begin VB.ComboBox cboFormulario 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Width           =   6015
      End
   End
   Begin FPSpread.vaSpread grdVisao 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   10575
      _Version        =   131077
      _ExtentX        =   18653
      _ExtentY        =   6588
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   0
      SpreadDesigner  =   "frmLogAlteracao.frx":000C
      UserResize      =   1
      VisibleCols     =   5
      VisibleRows     =   500
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   1800
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmLogAlteracao.frx":0337
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmLogAlteracao.frx":0651
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmLogAlteracao.frx":096B
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmLogAlteracao.frx":0C85
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmLogAlteracao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strSql As String
'SE 6346
Private Sub cboFormulario_Click()

    Dim sql_cd_ret As Integer
    Dim sql_qt_lnh As Integer
    Dim work As String
    
    If cboFormulario.ListIndex = -1 Then
        Exit Sub
    End If

    grdVisao.Visible = False
    
    On Error GoTo PreencheGrid
    
    PonteiroMouse vbHourglass
    
    grdVisao.MaxRows = 0
    
    strSql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
    strSql = strSql & " @sql_nm_tab char(30), @sql_qt_lnh int"
     'trocado o cd_segmento por 1 - fixo
    strSql = strSql & " exec  st_pdr_log_atualizacao_s " & cboFormulario.ItemData(cboFormulario.ListIndex) & ", "
    strSql = strSql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out "
   
   
   If gObjeto.natPrepareQuery(hSql, strSql) Then
      Do While gObjeto.natFetchNext(hSql)
      
         grdVisao.MaxRows = grdVisao.MaxRows + 1
         grdVisao.Row = grdVisao.MaxRows
         
         grdVisao.Col = 1
         grdVisao.Text = gObjeto.varResultSet(hSql, 2)
         grdVisao.Lock = True
         
         grdVisao.Col = 2
         grdVisao.Text = gObjeto.varResultSet(hSql, 3)
         grdVisao.Lock = True
         
         grdVisao.Col = 3
         grdVisao.Text = gObjeto.varResultSet(hSql, 4)
         grdVisao.Lock = True
         
         grdVisao.Col = 4
         grdVisao.Text = gObjeto.varResultSet(hSql, 5)
         grdVisao.Lock = True
         
         grdVisao.Col = 5
         grdVisao.Text = gObjeto.varResultSet(hSql, 6)
         grdVisao.Lock = True
         
         
         
      Loop
   End If
   
   If gObjeto.nErroSQL <> 0 Then
      MSG$ = "Erro ao executar a cboFormulario_Click " & gObjeto.nServerNum & gObjeto.sServerMsg
      grdVisao.Visible = True
      PonteiroMouse vbNormal
      Exit Sub
   End If
    
'   sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'   sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
'   If sql_cd_ret <> 0 Then
'      MSG$ = "Erro ao executar a PreencheGrid " & "RETURN STATUS = " & sql_cd_ret
'      PonteiroMouse vbNormal
'      grdVisao.Visible = True
'      Exit Function
'   End If
   

   PonteiroMouse vbNormal
   grdVisao.Visible = True
   
   Exit Sub
   Resume
PreencheGrid:
   MSG$ = "PreencheGrid - Erro (" & Err & ") - " & Error
   PonteiroMouse vbNormal
   grdVisao.Visible = True
   Exit Sub


End Sub

'SE 6346
Private Sub Form_Load()
    CarregaComboFormulario
End Sub
'SE 6346
Private Sub CarregaComboFormulario()
    
    Dim sql_cd_ret  As Integer
    Dim sql_qt_lnh  As Integer
    
    strSql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30),"
    strSql = strSql & " @sql_qt_lnh int " & vbCrLf
    strSql = strSql & " exec st_pdr_lista_formulario_s "
    strSql = strSql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out"
    
    ReDim avRetorno(0, 0)
       
    bResp = gObjeto.natExecuteQuery(hSql, strSql, avRetorno())
    If bResp = False Then
       MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
       End
    End If
    
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If
         
    cboFormulario.Clear
    
    'Combo1.AddItem ""
    'Combo1.ItemData(Combo1.NewIndex) = 0
    
    For i = 0 To UBound(avRetorno, 2)
        cboFormulario.AddItem (avRetorno(0, i) & " - " & avRetorno(1, i))
        cboFormulario.ItemData(cboFormulario.NewIndex) = avRetorno(0, i)
    Next i

    
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
     If Button.Index = 1 Then
         Unload Me
     End If
End Sub
