Attribute VB_Name = "RelacaoVendaMaterial"
'----------------------------------------------------------------------------------------
'Altera��o para projeto relacionamento
'Data      :06/08/2003
'Autor     :Renato Ferrarezi
'Altera��o : Cria��o do array publico segmentos que indica  quais segmentos ter�o
'            relacionamento de venda
'            Inicializado no evento load do form principal
'----------------------------------------------------------------------------------------
Option Explicit

'Array de segmentos que ter�o relacionamento
Global segmentos

'Constantes da Spread.
Global Const DATA_DENYREAD = 0
Global Const SS_ACTION_GOTO_CELL = 1
Global Const SS_ACTION_SELECT_BLOCK = 2
Global Const SS_ACTION_CLEAR = 3
Global Const SS_ACTION_DELETE_COL = 4
Global Const SS_ACTION_DELETE_ROW = 5
Global Const SS_ACTION_INSERT_COL = 6
Global Const SS_ACTION_INSERT_ROW = 7
Global Const SS_ACTION_LOAD_SPREAD_SHEET = 8
Global Const SS_ACTION_SAVE_ALL = 9
Global Const SS_ACTION_SAVE_VALUES = 10
Global Const SS_ACTION_RECALC = 11
Global Const SS_ACTION_CLEAR_TEXT = 12
Global Const SS_ACTION_PRINT = 13
Global Const SS_ACTION_DESELECT_BLOCK = 14
Global Const SS_ACTION_DSAVE = 15
Global Const SS_ACTION_SET_CELL_BORDER = 16
Global Const SS_ACTION_ADD_MULTISELBLOCK = 17
Global Const SS_ACTION_GET_MULTI_SELECTION = 18
Global Const SS_ACTION_COPY_RANGE = 19
Global Const SS_ACTION_MOVE_RANGE = 20
Global Const SS_ACTION_SWAP_RANGE = 21
Global Const SS_ACTION_CLIPBOARD_COPY = 22
Global Const SS_ACTION_CLIPBOARD_CUT = 23
Global Const SS_ACTION_CLIPBOARD_PASTE = 24
Global Const SS_ACTION_SORT = 25
Global Const SS_ACTION_COMBO_CLEAR = 26
Global Const SS_ACTION_COMBO_REMOVE = 27
Global Const SS_ACTION_RESET = 28
Global Const SS_ACTION_SS_ACTION_SEL_MODE_CLEAR = 29
Global Const SS_ACTION_VMODE_REFRESH = 30
Global Const SS_ACTION_REFRESH_BOUND = 31
Global Const SS_ACTION_SMARTPRINT = 32

'SelectBlockOptions
Global Const SS_SELBLOCKOPT_COLS = 1
Global Const SS_SELBLOCKOPT_ROWS = 2
Global Const SS_SELBLOCKOPT_BLOCKS = 4
Global Const SS_SELBLOCKOPT_ALL = 8

'DAutoSize settings
Global Const SS_AUTOSIZE_MAX_COL_WIDTH = 1
Global Const SS_AUTOSIZE_BEST_GUESS = 2

'cell type
Global Const SS_CELL_TYPE_DATE = 0
Global Const SS_CELL_TYPE_EDIT = 1
Global Const SS_CELL_TYPE_FLOAT = 2
Global Const SS_CELL_TYPE_INTEGER = 3
Global Const SS_CELL_TYPE_PIC = 4
Global Const SS_CELL_TYPE_STATIC_TEXT = 5
Global Const SS_CELL_TYPE_TIME = 6
Global Const SS_CELL_TYPE_BUTTON = 7
Global Const SS_CELL_TYPE_COMBOBOX = 8
Global Const SS_CELL_TYPE_PICTURE = 9
Global Const SS_CELL_TYPE_CHECKBOX = 10
Global Const SS_CELL_TYPE_OWNER_DRAWN = 11

'cell border types
Global Const SS_BORDER_TYPE_NONE = 0
Global Const SS_BORDER_TYPE_OUTLINE = 16
Global Const SS_BORDER_TYPE_LEFT = 1
Global Const SS_BORDER_TYPE_RIGHT = 2
Global Const SS_BORDER_TYPE_TOP = 4
Global Const SS_BORDER_TYPE_BOTTOM = 8

'cell border style
Global Const SS_BORDER_STYLE_DEFAULT = 0
Global Const SS_BORDER_STYLE_SOLID = 1
Global Const SS_BORDER_STYLE_DASH = 2
Global Const SS_BORDER_STYLE_DOT = 3
Global Const SS_BORDER_STYLE_DASH_DOT = 4
Global Const SS_BORDER_STYLE_DASH_DOT_DOT = 5
Global Const SS_BORDER_STYLE_BLANK = 6

'Bound. auto col sizing
Global Const SS_BOUND_COL_NO_SIZE = 0
Global Const SS_BOUND_COL_MAX_SIZE = 1
Global Const SS_BOUND_COL_SMART_SIZE = 2

'row and column header settings
Global Const SS_HEADER_BLANK = 0
Global Const SS_HEADER_NUMBERS = 1
Global Const SS_HEADER_LETTERS = 2

'check box text relative to the check box
Global Const SS_CHECKBOX_TEXT_LEFT = 0
Global Const SS_CHECKBOX_TEXT_RIGHT = 1

'cursorstyle
Global Const SS_CURSOR_STYLE_USER_DEFINED = 0
Global Const SS_CURSOR_STYLE_DEFAULT = 1
Global Const SS_CURSOR_STYLE_ARROW = 2
Global Const SS_CURSOR_STYLE_DEFCOLRESIZE = 3
Global Const SS_CURSOR_STYLE_DEFROWRESIZE = 4

'cursortype
Global Const SS_CURSOR_TYPE_DEFAULT = 0
Global Const SS_CURSOR_TYPE_COLRESIZE = 1
Global Const SS_CURSOR_TYPE_ROWRESIZE = 2
Global Const SS_CURSOR_TYPE_BUTTON = 3
Global Const SS_CURSOR_TYPE_GRAYAREA = 4
Global Const SS_CURSOR_TYPE_LOCKEDCELL = 5
Global Const SS_CURSOR_TYPE_COLHEADER = 6
Global Const SS_CURSOR_TYPE_ROWHEADER = 7

'operation mode
Global Const SS_OP_MODE_NORMAL = 0
Global Const SS_OP_MODE_READONLY = 1
Global Const SS_OP_MODE_ROWMODE = 2
Global Const SS_OP_MODE_SINGLE_SELECT = 3
Global Const SS_OP_MODE_MULTI_SELECT = 4

'sort order
Global Const SS_SORT_ORDER_NONE = 0
Global Const SS_SORT_ORDER_ASCENDING = 1
Global Const SS_SORT_ORDER_DESCENDING = 2

'Sort By
Global Const SS_SORT_BY_ROW = 0
Global Const SS_SORT_BY_COL = 1

'user resize row and columns
Global Const SS_USER_RESIZE_COL = 1
Global Const SS_USER_RESIZE_ROW = 2

'user resize row and columns
Global Const SS_USER_RESIZE_DEFAULT = 0
Global Const SS_USER_RESIZE_ON = 1
Global Const SS_USER_RESIZE_OFF = 2

'style of the virtual mode special scroll bar vscrollspecialtypes
Global Const SS_VSCROLLSPECIAL_NO_HOME_END = 1
Global Const SS_VSCROLLSPECIAL_NO_PAGE_UP_DOWN = 2
Global Const SS_VSCROLLSPECIAL_NO_LINE_UP_DOWN = 4

'position settings
Global Const SS_POSITION_UPPER_LEFT = 0
Global Const SS_POSITION_UPPER_CENTER = 1
Global Const SS_POSITION_UPPER_RIGHT = 2
Global Const SS_POSITION_CENTER_LEFT = 3
Global Const SS_POSITION_CENTER_CENTER = 4
Global Const SS_POSITION_CENTER_RIGHT = 5
Global Const SS_POSITION_BOTTOM_LEFT = 6
Global Const SS_POSITION_BOTTOM_CENTER = 7
Global Const SS_POSITION_BOTTOM_RIGHT = 8

'scroll bar
Global Const SS_SCROLLBAR_NONE = 0
Global Const SS_SCROLLBAR_H_ONLY = 1
Global Const SS_SCROLLBAR_V_ONLY = 2
Global Const SS_SCROLLBAR_BOTH = 3

'print type
Global Const SS_PRINT_ALL = 0
Global Const SS_PRINT_CELL_RANGE = 1
Global Const SS_PRINT_CURRENT_PAGE = 2
Global Const SS_PRINT_PAGE_RANGE = 3

'button types
Global Const SS_CELL_BUTTON_NORMAL = 0
Global Const SS_CELL_BUTTON_TWO_STATE = 1

'button picture align
Global Const SS_CELL_BUTTON_ALIGN_BOTTOM = 0
Global Const SS_CELL_BUTTON_ALIGN_TOP = 1
Global Const SS_CELL_BUTTON_ALIGN_LEFT = 2
Global Const SS_CELL_BUTTON_ALIGN_RIGHT = 3

'button draw mode
Global Const SS_BDM_ALWAYS = 0
Global Const SS_BDM_CURRENT_CELL = 1
Global Const SS_BDM_CURRENT_COLUMN = 2
Global Const SS_BDM_CURRENT_ROW = 4

'date formats
Global Const SS_CELL_DATE_FORMAT_DDMONYY = 0
Global Const SS_CELL_DATE_FORMAT_DDMMYY = 1
Global Const SS_CELL_DATE_FORMAT_MMDDYY = 2
Global Const SS_CELL_DATE_FORMAT_YYMMDD = 3

'Edit case
Global Const SS_CELL_EDIT_CASE_LOWER_CASE = 0
Global Const SS_CELL_EDIT_CASE_NO_CASE = 1
Global Const SS_CELL_EDIT_CASE_UPPER_CASE = 2

'Edit char set
Global Const SS_CELL_EDIT_CHAR_SET_ASCII = 0
Global Const SS_CELL_EDIT_CHAR_SET_ALPHA = 1
Global Const SS_CELL_EDIT_CHAR_SET_ALPHANUMERIC = 2
Global Const SS_CELL_EDIT_CHAR_SET_NUMERIC = 3

'Static text vertical alignment
Global Const SS_CELL_STATIC_V_ALIGN_BOTTOM = 0
Global Const SS_CELL_STATIC_V_ALIGN_CENTER = 1
Global Const SS_CELL_STATIC_V_ALIGN_TOP = 2

'Time
Global Const SS_CELL_TIME_12_HOUR_CLOCK = 0
Global Const SS_CELL_TIME_24_HOUR_CLOCK = 1

'Unit type
Global Const SS_CELL_UNIT_NORMAL = 0
Global Const SS_CELL_UNIT_VGA = 1
Global Const SS_CELL_UNIT_TWIPS = 2

'horizontal align
Global Const SS_CELL_H_ALIGN_LEFT = 0
Global Const SS_CELL_H_ALIGN_RIGHT = 1
Global Const SS_CELL_H_ALIGN_CENTER = 2

'EditmodeAction
Global Const SS_CELL_EDITMODE_EXIT_NONE = 0
Global Const SS_CELL_EDITMODE_EXIT_UP = 1
Global Const SS_CELL_EDITMODE_EXIT_DOWN = 2
Global Const SS_CELL_EDITMODE_EXIT_LEFT = 3
Global Const SS_CELL_EDITMODE_EXIT_RIGHT = 4
Global Const SS_CELL_EDITMODE_EXIT_NEXT = 5
Global Const SS_CELL_EDITMODE_EXIT_PREVIOUS = 6

Global Const SS_TURN_OFF% = 0
Global Const SS_TURN_ON% = 1

'Custom function parameter type
Global Const SS_VALUE_TYPE_LONG = 0
Global Const SS_VALUE_TYPE_DOUBLE = 1
Global Const SS_VALUE_TYPE_STR = 2

'The return status of a custom function
Global Const SS_VALUE_STATUS_OK = 0
Global Const SS_VALUE_STATUS_ERROR = 1
Global Const SS_VALUE_STATUS_EMPTY = 2
Global Const SS_VALUE_STATUS_CLEAR = 3
Global Const SS_VALUE_STATUS_NONE = 4
Global avg_Permissoes()
Global avCodigos()

Function TiraBarra(nValor)

'************************
'Tira tra�o
'************************

Dim v
Dim c
Dim i As Integer

For i = 1 To Len(nValor)
    c = Mid(nValor, i, 1)
    If c = "/" Then c = ""
    v = v & c
Next i

TiraBarra = v

End Function

Function TiraPonto(nValor)

'************************
'Tira tra�o
'************************

Dim v
Dim c
Dim i As Integer

For i = 1 To Len(nValor)
    c = Mid(nValor, i, 1)
    If c = "." Then c = ""
    v = v & c
Next i

TiraPonto = v

End Function

Public Sub DesabilitaObjeto(frm As Form)

    'Desabilita os objetos do frmRelacao
    
    frm.txtDescricao.Enabled = False
    frm.txtDescricaoPreco.Enabled = False
    frm.pnlMaterialOk.Enabled = False
    frm.pnlCiclo.Enabled = False
    frm.pnlEstrutura.Enabled = False
    frm.pnlAbrangencia.Enabled = False
    frm.cmdInserir.Enabled = False
    frm.cmdRetirar.Enabled = False
    frm.Opt_Inicio.Value = False
    frm.Opt_Fim.Value = False
    frm.lstCiclo.Enabled = False

End Sub

Public Sub HabilitaObjeto(frm As Form)

    frm.pnlCiclo.Enabled = True
    frm.pnlEstrutura.Enabled = True
    frm.pnlAbrangencia.Enabled = True

End Sub

Public Sub CarregaMeio(Combo1 As ComboBox)

    'Declara��o de Vari�veis
    Dim cSql  As String
    Dim bResp As Boolean
    Dim i     As Integer
    
    On Error GoTo ciclo
    
    ' Carrega Ciclo
    cSql = "stp_pdr_cons_meio_comunic_s"
    
    ReDim avRetorno(0)
       
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avRetorno())
    If bResp = False Then
       MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
       End
    End If
    
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If
    
    Combo1.Clear
    
    Combo1.AddItem ""
    
    Combo1.ItemData(Combo1.NewIndex) = 0
    
    For i = 0 To UBound(avRetorno, 2)
        Combo1.AddItem avRetorno(0, i) & " - " & avRetorno(1, i)
        Combo1.ItemData(Combo1.NewIndex) = avRetorno(0, i)
    Next i

Exit Sub

ciclo:
  MsgBox "Erro " & Err.Number & " - " & Err.Description & " ao carregar o ciclo.", 48
  Exit Sub
End Sub

Public Sub Carrega_Categoria_Produto(Combo1 As ComboBox)

    'Declara��o de Vari�veis
    Dim cSql  As String
    Dim bResp As Boolean
    Dim i     As Integer
    
    On Error GoTo ErroCategoria
    
    ' Carrega Ciclo
    cSql = "stp_categoria_produto_comerc_s "
    
    ReDim avRetorno(0)
       
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avRetorno())
    If bResp = False Then
       MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
       End
    End If
    
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If
    
    Combo1.Clear
    
    For i = 0 To UBound(avRetorno, 2)
        Combo1.AddItem avRetorno(1, i)
        Combo1.ItemData(Combo1.NewIndex) = avRetorno(0, i)
    Next i

  Exit Sub

ErroCategoria:
  MsgBox "Erro ao Tentar Carregar a Categoria de Produto." & vbCrLf & vbCrLf & Err.Number & " - " & Err.Description, 48
  Exit Sub
  
End Sub

Public Sub Carrega_Linha_Produto(Combo1 As ComboBox)

    'Declara��o de Vari�veis
    Dim cSql  As String
    Dim bResp As Boolean
    Dim i     As Integer
    
    On Error GoTo ErroLinha
    
    ' Carrega Linha de Produto
    cSql = "stp_linha_produto_comercial_s "
    
    ReDim avRetorno(0)
       
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avRetorno())
    If bResp = False Then
       MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
       End
    End If
    
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If
    
    Combo1.Clear
    
    For i = 0 To UBound(avRetorno, 2)
        Combo1.AddItem avRetorno(1, i)
        Combo1.ItemData(Combo1.NewIndex) = avRetorno(0, i)
    Next i

  Exit Sub

ErroLinha:
  MsgBox "Erro ao Tentar Carregar a Linha de Produto." & vbCrLf & vbCrLf & Err.Number & " - " & Err.Description, 48
  Exit Sub
  
End Sub

Public Function ConsisteData(Data2) As Boolean

On Error GoTo Consiste

ConsisteData = False

If CVDate(Data2) < Date Then
  Exit Function
End If

ConsisteData = True
Exit Function

Consiste:
  MsgBox "Data Inv�lida.", 48
  Exit Function
  
End Function

Public Function Acha_Sequencia(produto As String, segmento As String, agrupador As String, estrutura As Variant)

'Declara��o de Vari�veis
Dim cSql  As String
Dim bResp As Boolean
Dim i     As Integer

On Error GoTo Acha_Sequencia

' Carrega Ciclo
cSql = "select nm_sequencia_relacao from t_relacao_venda_material where cd_venda_produto = " & _
        produto & " and cd_segmento_canal = " & segmento & " and nm_agrupador_relacao = " & agrupador & _
        " and cd_estrutura_comercial = " & estrutura

ReDim avRetorno(0)
   
bResp = gObjeto.natExecuteQuery(hSql, cSql, avRetorno())
If bResp = False Then
  MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
  End
End If

If gObjeto.nErroSQL <> 0 Then
  MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
  Exit Function
End If

Acha_Sequencia = avRetorno(0, i)

Exit Function

Acha_Sequencia:
  MsgBox "Erro ao carregar o ciclo.", 48
  Exit Function
End Function

Public Function fConsisteCiclo(cmbIni As Object, cmbFin As Object) As Boolean

    fConsisteCiclo = True
    
    If Trim(cmbIni.Text) = "" Then
        MsgBox "� obrigat�rio preencher o ciclo inicial"
        fConsisteCiclo = False
        Exit Function
    End If

    If Trim(cmbFin.Text) <> "" Then
        If cmbIni.ItemData(cmbIni.ListIndex) > cmbFin.ItemData(cmbFin.ListIndex) Then
            MsgBox "O ciclo final deve ser maior ao ciclo inicial."
            fConsisteCiclo = False
        End If
    End If

End Function

Public Sub Preenche_Grid(vCicloIni As Long, vCicloFin As Long, vDescrCan As Boolean)

    'Executar a QUERY
    'Declara��o de Vari�veis
    Dim avData()   As Variant
    Dim cSql       As String
    Dim bResp      As Boolean
    
    Dim sql_cd_ret As Long
    Dim sql_cd_opr As String
    Dim sql_nm_tab As String
    Dim sql_qt_lnh As Long
    
    If vListaMaterial = "N" Then
        
        cSql = "exec stp_pdr_prod_ativos_ciclo_s " & vCicloIni & " , " & vCicloFin & _
        ",'" & vfiltro & "'," & vOrdena & ",'" & vQuebraLanc & "','" & vQuebraLin & "','" & _
        vQuebraCat & "'"
    
    Else
        cSql = cSql & " exec stp_pdr_mat_ativos_ciclo_s " & vCicloIni & " , " & vCicloFin & _
        ",'" & vfiltro & "'," & vOrdena & ",'" & vQuebraLanc & "','" & vQuebraLin & _
        "','" & vQuebraCat & "'"
        
    End If
     
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())
     
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.nServerNum & " " & gObjeto.sServerMsg, vbOKOnly + vbCritical, "Erro"
        Exit Sub
    End If
    
    Call populaGridCond(avData())
         
    If Not vDescrCan Then
        'Seleciona coluna
        frmRelAtivo.Grid.Col = 7
        frmRelAtivo.Grid.Row = -1
        frmRelAtivo.Grid.Col2 = 7
        frmRelAtivo.Grid.Row2 = -1
        frmRelAtivo.Grid.BlockMode = True
        'Deleta coluna no Grid
        frmRelAtivo.Grid.Action = 4
        frmRelAtivo.Grid.MaxCols = frmRelAtivo.Grid.MaxCols - 1
    End If
     
    frmRelAtivo.Show 1
    
End Sub

Public Sub populaGridCond(avData() As Variant)

    Dim X         As Variant
    Dim contLancL As Variant
    Dim contLancA As Variant
    Dim contLancD As Variant
    Dim primExec  As Variant
    Dim primExec2 As Variant

    For X = 0 To UBound(avData(), 2)
    
        If vQuebraLanc = "S" Then
            If avData(0, X) = 11 And contLancL < 1 Then
                primExec = 0
                primExec2 = 0
                contLancL = contLancL + 1
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 2
                frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, "L A N � A M E N T O S"
                configuraLinha (frmRelAtivo.Grid.MaxRows)
            ElseIf avData(0, X) = 12 And contLancA < 1 Then
                contLancA = contLancA + 1
                primExec = 0
                primExec2 = 0
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 2
                frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, "A T I V O S"
                configuraLinha (frmRelAtivo.Grid.MaxRows)
            ElseIf avData(0, X) = 13 And contLancD < 1 Then
                primExec = 0
                primExec2 = 0
                contLancD = contLancD + 1
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 2
                frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, "D E S C O N T I N U A D O S"
                configuraLinha (frmRelAtivo.Grid.MaxRows)
            End If
        End If
        
        'Quebra de linha
        If vQuebraLin = "S" Then
            If primExec2 = 0 Then
                primExec2 = primExec2 + 1
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, avData(1, X)
                configuraLinha (frmRelAtivo.Grid.MaxRows)
            Else
                If avData(1, X) <> avData(1, X - 1) Then
                    frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                    frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                    frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, avData(1, X)
                    configuraLinha (frmRelAtivo.Grid.MaxRows)
                    primExec = 0
                    
                End If
            End If
        End If
        
        'Quebra de categoria
        If vQuebraCat = "S" Then
            If primExec = 0 Then
                primExec = primExec + 1
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, avData(2, X)
                configuraLinha (frmRelAtivo.Grid.MaxRows)
                frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
            Else
                If avData(2, X) <> avData(2, X - 1) Then
                    frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                    frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, avData(2, X)
                    configuraLinha (frmRelAtivo.Grid.MaxRows)
                    frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
                End If
            End If
        End If
        
        If vListaMaterial = "N" Then
            
            'IMPLEMENTA��O DE LINHAS
            frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
           
            'C�DIGO DE LAN�AMENTO
            frmRelAtivo.Grid.SetText 1, frmRelAtivo.Grid.MaxRows, avData(0, X)
            
            'DESCRI��O DE LINHA
            frmRelAtivo.Grid.SetText 2, frmRelAtivo.Grid.MaxRows, avData(1, X)
            
            'DESCRI��O DE CATEGORIA
            frmRelAtivo.Grid.SetText 3, frmRelAtivo.Grid.MaxRows, avData(2, X)
           
            'C�DIGO DE VENDA
            frmRelAtivo.Grid.SetText 4, frmRelAtivo.Grid.MaxRows, avData(3, X)
            
            'DESCRI��O OFICIAL
            frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, avData(5, X)
            
            'ABRANG�NCIA
            frmRelAtivo.Grid.SetText 6, frmRelAtivo.Grid.MaxRows, avData(4, X)
            
            'DESCRI��O CAN
            frmRelAtivo.Grid.SetText 7, frmRelAtivo.Grid.MaxRows, avData(6, X)
            
            'CICLO INICIAL
            frmRelAtivo.Grid.SetText 8, frmRelAtivo.Grid.MaxRows, avData(7, X)
            
            'DATA INICIAL
            frmRelAtivo.Grid.SetText 9, frmRelAtivo.Grid.MaxRows, avData(8, X)
            
            'CICLO FINAL
            frmRelAtivo.Grid.SetText 10, frmRelAtivo.Grid.MaxRows, avData(9, X)
            
            'DATA FINAL
            frmRelAtivo.Grid.SetText 11, frmRelAtivo.Grid.MaxRows, avData(10, X)
            
            'PERMISS�O DE DIVIS�O DE BOLETO
            frmRelAtivo.Grid.SetText 12, frmRelAtivo.Grid.MaxRows, avData(11, X)
            
        Else
        
            'IMPLEMENTA��O DE LINHAS
            frmRelAtivo.Grid.MaxRows = frmRelAtivo.Grid.MaxRows + 1
           
            'C�DIGO DE LAN�AMENTO
            frmRelAtivo.Grid.SetText 1, frmRelAtivo.Grid.MaxRows, avData(0, X)
            
            'DESCRI��O DE LINHA
            frmRelAtivo.Grid.SetText 2, frmRelAtivo.Grid.MaxRows, avData(1, X)
            
            'DESCRI��O DE CATEGORIA
            frmRelAtivo.Grid.SetText 3, frmRelAtivo.Grid.MaxRows, avData(2, X)
           
            'C�DIGO DE VENDA
            frmRelAtivo.Grid.SetText 4, frmRelAtivo.Grid.MaxRows, avData(3, X)
            
            'DESCRI��O OFICIAL
            frmRelAtivo.Grid.SetText 5, frmRelAtivo.Grid.MaxRows, avData(5, X)
            
            'ABRANG�NCIA
            frmRelAtivo.Grid.SetText 6, frmRelAtivo.Grid.MaxRows, avData(4, X)
            
            'DESCRI��O CAN
            frmRelAtivo.Grid.SetText 7, frmRelAtivo.Grid.MaxRows, avData(6, X)
            
            'CICLO INICIAL
            frmRelAtivo.Grid.SetText 8, frmRelAtivo.Grid.MaxRows, avData(7, X)
            
            'DATA INICIAL
            frmRelAtivo.Grid.SetText 9, frmRelAtivo.Grid.MaxRows, avData(8, X)
            
            'CICLO FINAL
            frmRelAtivo.Grid.SetText 10, frmRelAtivo.Grid.MaxRows, avData(9, X)
            
            'DATA FINAL
            frmRelAtivo.Grid.SetText 11, frmRelAtivo.Grid.MaxRows, avData(10, X)
            
            'PERMISS�O DE DIVIS�O DE BOLETO
            frmRelAtivo.Grid.SetText 12, frmRelAtivo.Grid.MaxRows, avData(11, X)
            
            'CODIGO DO PRODUTO
            frmRelAtivo.Grid.SetText 13, frmRelAtivo.Grid.MaxRows, avData(12, X)
            
            'DESCRICAO DO PRODUTO
            frmRelAtivo.Grid.SetText 14, frmRelAtivo.Grid.MaxRows, avData(13, X)
            
            'PRIORIDADE
            frmRelAtivo.Grid.SetText 15, frmRelAtivo.Grid.MaxRows, avData(14, X)
            
            'CICLO INICIO MATERIAL
            frmRelAtivo.Grid.SetText 16, frmRelAtivo.Grid.MaxRows, avData(15, X)
            
            'DATA INICIO DO MATERIAL
            frmRelAtivo.Grid.SetText 17, frmRelAtivo.Grid.MaxRows, avData(16, X)
            
            'CICLO FINAL MATERIAL
            frmRelAtivo.Grid.SetText 18, frmRelAtivo.Grid.MaxRows, avData(17, X)
            
            'DATA FINAL DO MATERIAL
            frmRelAtivo.Grid.SetText 19, frmRelAtivo.Grid.MaxRows, avData(18, X)
            
        End If
    Next X

End Sub

Public Sub configuraLinha(Numero As Long)

    'Seleciona a linha
    frmRelAtivo.Grid.Col = 1
    frmRelAtivo.Grid.Row = Numero
    frmRelAtivo.Grid.Col2 = frmRelAtivo.Grid.MaxCols
    frmRelAtivo.Grid.Row2 = Numero
    frmRelAtivo.Grid.BlockMode = True
    
    'Formata o texto
'    frmRelAtivo.Grid.FontName = "MS Sans Serif"
    frmRelAtivo.Grid.FontSize = 12
    frmRelAtivo.Grid.FontBold = True
    frmRelAtivo.Grid.FontItalic = False
    frmRelAtivo.Grid.FontStrikethru = False
    frmRelAtivo.Grid.FontUnderline = False

End Sub


