VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmProdSemLista 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Produtos Vigentes Sem Lista de Pre�o"
   ClientHeight    =   6360
   ClientLeft      =   555
   ClientTop       =   4140
   ClientWidth     =   13035
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   13035
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar st_Toolbar 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   13035
      _ExtentX        =   22992
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   4
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Pesquisar"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            ImageIndex      =   4
            Style           =   4
            Object.Width           =   2000
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Height          =   1035
      Left            =   30
      TabIndex        =   1
      Top             =   690
      Width           =   12975
      Begin VB.ComboBox cmbCiclo 
         Height          =   315
         ItemData        =   "frmProdSemLista.frx":0000
         Left            =   2040
         List            =   "frmProdSemLista.frx":0002
         TabIndex        =   3
         Top             =   420
         Width           =   1545
      End
      Begin VB.Label Label1 
         Caption         =   "Ciclo / Ano"
         Height          =   255
         Left            =   840
         TabIndex        =   2
         Top             =   450
         Width           =   975
      End
   End
   Begin FPSpread.vaSpread grdProdSemLista 
      Height          =   4575
      Left            =   30
      TabIndex        =   0
      Top             =   1740
      Width           =   13005
      _Version        =   131077
      _ExtentX        =   22939
      _ExtentY        =   8070
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   4
      MaxRows         =   0
      RetainSelBlock  =   0   'False
      ScrollBarExtMode=   -1  'True
      ScrollBarMaxAlign=   0   'False
      ScrollBarShowMax=   0   'False
      SpreadDesigner  =   "frmProdSemLista.frx":0004
      UserResize      =   1
      VisibleCols     =   500
      VisibleRows     =   500
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   6120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":0414
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":072E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":0A48
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":0D62
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":107C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":1396
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":16B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":19CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmProdSemLista.frx":1CE4
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmProdSemLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nRow As Long
Private Sub Pesquisar()
    'Declara��o de Vari�veis
    Dim avData()    As Variant
    Dim X           As Long
    Dim Y           As Long
    
    If cmbCiclo.Text = "" Then
        MsgBox "Informe o Ciclo de vendas desejado!"
        Exit Sub
    End If

    Me.MousePointer = vbHourglass
    
    grdProdSemLista.MaxRows = 0
    
    Me.MousePointer = vbHourglass
    DoEvents
'    Me.Refresh
    
    cSql = "exec stp_pdr_rel_prd_sem_lista_s " & cmbCiclo.ItemData(cmbCiclo.ListIndex)

    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())
     
    If gObjeto.nErroSQL <> 0 Then
        Me.MousePointer = vbNormal
        MsgBox gObjeto.nServerNum & " " & gObjeto.sServerMsg, vbOKOnly + vbCritical, "Erro"
        Exit Sub
    End If
    
    If UBound(avData, 1) = 0 Then
        Me.MousePointer = vbNormal
        Refresh
        MsgBox "N�o existem produtos vigentes sem lista de pre�o para este ciclo!", vbCritical
        Exit Sub
    End If
        
    With grdProdSemLista
        .Visible = False
        
        For X = 0 To UBound(avData(), 2)
            'IMPLEMENTA��O DE LINHAS
            CriaLinha
                    
            RecebeDados 1, Right(CStr(avData(0, X)), 2) & "/" & Left(CStr(avData(0, X)), 4)
            RecebeDados 2, CStr(avData(1, X))
            RecebeDados 3, CStr(avData(2, X))
            RecebeDados 4, CStr(avData(3, X))
        Next X
        .Visible = True
        Me.MousePointer = vbNormal
    End With
    
    Me.MousePointer = vbNormal
    
End Sub

Public Sub CriaLinha()
    
    nRow = grdProdSemLista.MaxRows + 1
    grdProdSemLista.MaxRows = nRow
    grdProdSemLista.Row = nRow
    
End Sub

Public Sub RecebeDados(pCol As Integer, pDados As String)

    With grdProdSemLista
        nRow = .MaxRows
        .Row = nRow
        .Col = pCol
        .Text = pDados
        .RowHeight(nRow) = 13
    End With
        
End Sub
Private Sub Form_Load()


    Call CarrCicloComPreco(cmbCiclo)
    
    Me.Top = 0
    Me.Left = 0


End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub st_Toolbar_ButtonClick(ByVal Button As ComctlLib.Button)

    Select Case Button.Index
        Case 1  'pesquisar
            Call Pesquisar
        Case 4 ' Sair
            SetStatus ""
            Unload Me
            Set frmProdSemLista = Nothing
    End Select

End Sub
