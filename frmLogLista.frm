VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Begin VB.Form frmLogLista 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Log de Execu��o da gera��o de lista de pre�o"
   ClientHeight    =   6405
   ClientLeft      =   555
   ClientTop       =   4140
   ClientWidth     =   12960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   12960
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Fechar"
      Default         =   -1  'True
      Height          =   375
      Left            =   11010
      TabIndex        =   1
      Top             =   5970
      Width           =   1725
   End
   Begin FPSpread.vaSpread grd_inconsistencia 
      Height          =   5805
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   12840
      _Version        =   131077
      _ExtentX        =   22648
      _ExtentY        =   10239
      _StockProps     =   64
      ButtonDrawMode  =   4
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   0
      OperationMode   =   2
      ProcessTab      =   -1  'True
      ScrollBarExtMode=   -1  'True
      ScrollBarMaxAlign=   0   'False
      ScrollBarShowMax=   0   'False
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmLogLista.frx":0000
      UserResize      =   1
      VisibleCols     =   500
      VisibleRows     =   500
   End
End
Attribute VB_Name = "frmLogLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nRow As Long

Public Sub CriaLinha()
    nRow = grd_inconsistencia.MaxRows + 1
    grd_inconsistencia.MaxRows = nRow
    grd_inconsistencia.Row = nRow
End Sub

Public Sub RecebeDados(pCol As Integer, pDados As String)

    With grd_inconsistencia
        nRow = .MaxRows
        .Row = nRow
        .Col = pCol
        .Text = pDados
    End With
        
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub


