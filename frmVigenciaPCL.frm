VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmVigenciaPCL 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o de Abrang�ncia / Vig�ncia - PCL"
   ClientHeight    =   9000
   ClientLeft      =   1380
   ClientTop       =   1365
   ClientWidth     =   11955
   Icon            =   "frmVigenciaPCL.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   11955
   Begin ComctlLib.Toolbar barFerramenta 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11955
      _ExtentX        =   21087
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   9
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "NovaPesquisa"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "incluir"
            Object.ToolTipText     =   "Inclus�o de material"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "alterar"
            Object.ToolTipText     =   "Altera��o do material selecionado"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "exportar"
            Object.ToolTipText     =   "Exportar para Excel"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "importar"
            Object.ToolTipText     =   "Importar do Excel"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "excluir"
            Object.ToolTipText     =   "Cancelamento do material selecionado"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "sair"
            Object.ToolTipText     =   "Sair "
            Object.Tag             =   "Sair"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5760
      Top             =   4320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame fraBotoes 
      Height          =   915
      Left            =   60
      TabIndex        =   21
      Top             =   7980
      Width           =   11895
      Begin Threed.SSCommand cmdGravar 
         Height          =   705
         Left            =   4140
         TabIndex        =   18
         Top             =   180
         Width           =   1005
         _Version        =   65536
         _ExtentX        =   1773
         _ExtentY        =   1244
         _StockProps     =   78
         Caption         =   "&Gravar"
         Outline         =   0   'False
         Picture         =   "frmVigenciaPCL.frx":0442
      End
      Begin Threed.SSCommand cmdCancelar 
         Cancel          =   -1  'True
         Height          =   705
         Left            =   6780
         TabIndex        =   19
         Top             =   180
         Width           =   1005
         _Version        =   65536
         _ExtentX        =   1773
         _ExtentY        =   1244
         _StockProps     =   78
         Caption         =   "&Cancelar"
         Outline         =   0   'False
         Picture         =   "frmVigenciaPCL.frx":075C
      End
   End
   Begin VB.Frame fraParte3 
      Height          =   3705
      Left            =   60
      TabIndex        =   17
      Top             =   4260
      Width           =   11895
      Begin VB.CheckBox chkMostraCanc 
         Caption         =   "Mostrar abrang�ncias canceladas e encerradas"
         Height          =   255
         Left            =   6660
         TabIndex        =   7
         Top             =   1080
         Width           =   3735
      End
      Begin VB.Frame fraParte4 
         Height          =   675
         Left            =   120
         TabIndex        =   22
         Top             =   360
         Width           =   10455
         Begin VB.Label lblAbrangencia 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1080
            TabIndex        =   5
            Top             =   210
            Width           =   4035
         End
         Begin VB.Label lblVigencia 
            Alignment       =   2  'Center
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6660
            TabIndex        =   6
            Top             =   210
            Width           =   3405
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Abrang�ncia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   150
            TabIndex        =   24
            Top             =   270
            Width           =   900
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Vig�ncia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   5940
            TabIndex        =   23
            Top             =   300
            Width           =   585
         End
      End
      Begin FPSpread.vaSpread spdMaterial 
         Height          =   2235
         Left            =   180
         TabIndex        =   8
         Top             =   1380
         Width           =   11595
         _Version        =   131077
         _ExtentX        =   20452
         _ExtentY        =   3942
         _StockProps     =   64
         DAutoSizeCols   =   0
         EditEnterAction =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   17
         MaxRows         =   0
         ScrollBars      =   2
         SpreadDesigner  =   "frmVigenciaPCL.frx":0A76
         StartingColNumber=   12
         UserResize      =   1
         VisibleCols     =   500
         VisibleRows     =   500
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Caption         =   "MATERIAL / ABRANG�NCIA / VIG�NCIA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   210
         TabIndex        =   20
         Top             =   150
         Width           =   10335
      End
   End
   Begin VB.Frame fraParte2 
      Height          =   1995
      Left            =   60
      TabIndex        =   15
      Top             =   2220
      Width           =   11895
      Begin FPSpread.vaSpread spdAVIG 
         Height          =   1365
         Left            =   1140
         TabIndex        =   2
         Top             =   450
         Width           =   9645
         _Version        =   131077
         _ExtentX        =   17013
         _ExtentY        =   2408
         _StockProps     =   64
         AllowCellOverflow=   -1  'True
         ColHeaderDisplay=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   17
         MaxRows         =   0
         ScrollBars      =   2
         SpreadDesigner  =   "frmVigenciaPCL.frx":0F49
         UserResize      =   0
         VisibleCols     =   500
         VisibleRows     =   500
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "ABRANG�NCIA / VIG�NCIA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   180
         TabIndex        =   16
         Top             =   210
         Width           =   10635
      End
   End
   Begin VB.Frame fraParte1 
      Height          =   1515
      Left            =   60
      TabIndex        =   10
      Top             =   660
      Width           =   11895
      Begin VB.TextBox txtMaterial 
         Height          =   315
         Left            =   2070
         MaxLength       =   8
         TabIndex        =   1
         Top             =   360
         Width           =   1245
      End
      Begin VB.TextBox txtCodVenda 
         Height          =   315
         Left            =   120
         MaxLength       =   7
         TabIndex        =   0
         Top             =   360
         Width           =   1245
      End
      Begin VB.Frame Frame2 
         Enabled         =   0   'False
         Height          =   675
         Left            =   7140
         TabIndex        =   13
         Top             =   120
         Width           =   3195
         Begin VB.CheckBox chkContrLog 
            Caption         =   "Controle Geral log�stico"
            Height          =   375
            Left            =   360
            TabIndex        =   3
            Top             =   180
            Width           =   2475
         End
      End
      Begin VB.Label lblDescricao 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   120
         TabIndex        =   4
         Top             =   1020
         Width           =   10245
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�d. Material"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2100
         TabIndex        =   12
         Top             =   150
         Width           =   960
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�d. Venda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   11
         Top             =   150
         Width           =   840
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   14
         Top             =   810
         Width           =   690
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   11820
      Top             =   2220
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":1491
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":17AB
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":1AC5
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":1DDF
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":20F9
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":2413
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":272D
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":2A47
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   11760
      Top             =   1020
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   7
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":2D61
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":307B
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":3395
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":36AF
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":39C9
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":3CE3
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaPCL.frx":3FFD
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Salvar"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "S&air"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu mnuOpcoes 
      Caption         =   "&Op��es"
      Begin VB.Menu mnuIncluir 
         Caption         =   "&Incluir"
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuAlterar 
         Caption         =   "&Alterar"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuExcluir 
         Caption         =   "&Cancelar"
         Shortcut        =   ^E
      End
   End
End
Attribute VB_Name = "frmVigenciaPCL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim axOperacao    As String
Dim axRefresh     As Boolean   'PARA FAZER O REFRESH DA PENDENCIA
Dim axPend        As Boolean   'INDICA QUE O FORM FOI CHAMADO PELA PENDENCIA PCL
Dim vAchouInicio  As Boolean
Dim vAchouFinal   As Boolean
Dim vMsgListaTec  As Boolean
Dim fonteDados As Integer 'Fonte Dados = 1 - Banco de Dados, Fonte de Dados = 2 - Importa��o Excel
Dim caminhoArquivoExcel As String

Dim vLin          As Integer
Dim vLinAux       As Integer
Dim X             As Integer
Dim Y             As Integer

Dim vVigenciaIni  As Variant
Dim vVigenciaFin  As Variant
Dim vDtInicioAux  As Variant
Dim vDtFinalAux   As Variant
Dim vControle     As Variant
Dim vSeqPend      As Variant
Dim vCiclIni      As Variant
Dim vDataIni      As Variant
Dim vCiclFin      As Variant
Dim vDataFin      As Variant
Dim vCiclIniAnt   As Variant
Dim vDataIniAnt   As Variant
Dim vCiclFinAnt   As Variant
Dim vDataFinAnt   As Variant
Dim vTpEstrut     As Variant
Dim vCdEstrut     As Variant

Dim vCiclIniMat   As Variant
Dim vDataIniMat   As Variant
Dim vCiclFinMat   As Variant
Dim vDataFinMat   As Variant
Dim vMaterial     As Variant
Dim vMaterialAux  As Variant
Dim vTpEstrutAux  As Variant
Dim vCdEstrutAux  As Variant
Dim vStatus       As Variant
Dim vStatusAux    As Variant
Dim vDtIniCiclo   As Variant
Dim vDtFinCiclo   As Variant
Dim cSql          As String

Dim strOperacao   As String 'SE 6346

Private Sub cmdGravar_CtrLogistico()
    Dim vCont     As Integer
    
    Dim vPrior    As Variant
    Dim vPriorAux As Variant
    Dim vProd     As Variant
    Dim vProdAux  As Variant
    Dim vMaterial As Variant
    Dim vAbrangencia As Variant
    Dim vAbrangenciaAux As Variant
    
    axRefresh = False
        
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO003", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO003", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    'VERIFICA A PRIORIDADE
    For X = 1 To spdMaterial.MaxRows
     
        vCont = 0
        
        spdMaterial.GetText 8, X, vPrior
        spdMaterial.GetText 9, X, vStatus
        spdMaterial.GetText 3, X, vAbrangencia
        spdMaterial.GetText 1, X, vMaterial
        
        If vStatus = "ATIVA" Then
            'verificar a prioridade
            If Val(vPrior) = 0 Then
                MsgBox "Prioridade Deve Ser Superior a 0.  ", vbCritical, "Aten��o"
                Exit Sub
            End If
            'prioridade maxima
            'If Val(vPrior) > spdMaterial.MaxRows Then
            '    MsgBox "Prioridade " & Val(vPrior) & " Inv�lida - M�xima -> " & spdMaterial.MaxRows, vbCritical, "Aten��o"
            '    Exit Sub
            'End If
            '
        
            For Y = 1 To spdMaterial.MaxRows
                spdMaterial.GetText 8, Y, vPriorAux
                spdMaterial.GetText 9, Y, vStatusAux
                spdMaterial.GetText 3, Y, vAbrangenciaAux
                If vStatusAux = "ATIVA" Then
                    If Val(vPriorAux) = Val(vPrior) And _
                        Trim(vAbrangencia) = Trim(vAbrangenciaAux) Then
                        vCont = vCont + 1
                    End If
                End If
            Next
            If vCont > 1 Then
               MsgBox "Existem outros materiais com abrangencia, vigencia e Prioridade iguais aos do material " & Val(vMaterial) & " .", vbCritical
               Exit Sub
            End If
        End If
        
    Next
    
    If chkContrLog.Value = 0 Then
       'consiste duplicidade de materiais
       If Not ConsistirVigenciaMaterialDuplicado Then Exit Sub
    
       'consistir a vigencia do material
       If Not ConsistirVigenciaMaterial Then Exit Sub
       
       'Consistir lista tecnica para kits
       If indicaKit = True Then
          If Not ConsistirVigenciaKit Then Exit Sub
       End If
       '
    Else
       'consiste duplicidade de materiais
       If Not ConsistirVigenciaMaterialDuplicado Then Exit Sub
    
       'consiste duplicidade de materiais
       If Not ConsisteAbrangenciaVigencia(spdAVIG) Then Exit Sub
    
       'Consistir lista tecnica para kits
       If indicaKit = True Then
          If Not ConsistirVigenciaKit Then Exit Sub
       End If
       
    End If
    
    
    If indicaKit = True And vMsgListaTec = True Then
       If MsgBox("Entre as Altera��es Existem Mudan�as de Vig�ncia " & vbCrLf & _
                 "Para Kits com Lista T�cnica Diferente." & vbCrLf & vbCrLf & _
                 "Confirma Altera��o?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then Exit Sub
    Else
       If MsgBox("Confirma a Altera��o das Informa��es do Produto.", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    End If
    
    If Not ManutencaoAbrangencia Then Exit Sub
    'SE 6346 - In�cio
    If Not GravaHistorico(4, strOperacao, "Produto", txtCodVenda.Text, "", "", "", "") Then
         MsgBox "Falha ao tentar gravar hist�rico de atualiza��es", vbCritical, "Erro"
         Exit Sub
    End If
        
    If Not DesbloqueiaRegistro(4) Then
        MsgBox "Falha ao tentar desbloquear formul�rio.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
    
        
   'LimpaTela
    LimpaTela
    'If chkContrLog.Value = 1 Then
    '    LimpaTela
    'Else
        spdMaterial.MaxRows = 0
        mnuSalvar.Enabled = False
        cmdGravar.Enabled = False
        cmdCancelar.Enabled = False
        spdMaterial.Enabled = True
        DesprotegeTela
    'End If

    If indicaPCL = True And Val(gCodVenda) > 0 Then
       'desativa
       indicaPCL = False
       'volta para a pendencia pcl
       Unload Me
    End If
End Sub

Private Sub cmdGravar_Outros()
    Dim vCont     As Integer
    Dim vPrior    As Variant
    Dim vPriorAux As Variant
    Dim vProd     As Variant
    Dim vProdAux  As Variant
    Dim vMaterial As Variant
    Dim vAbrangencia As Variant
    Dim vAbrangenciaAux As Variant
    Dim gridDados As vaSpread
    Dim QTDExecucao As Double
    Dim vStatusAvig As Variant
    axRefresh = False
        
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO003", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO003", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
        
    Set gridDados = spdMaterial
    'VERIFICA A PRIORIDADE
    If fonteDados = 2 Then
       QTDExecucao = spdAVIG.MaxRows
    Else
        QTDExecucao = 1
        gridDados.Row = spdMaterial.ActiveRow
    End If
    Dim indiceExecucao As Double
    For indiceExecucao = 1 To QTDExecucao
        If chkContrLog.Value <> vbChecked Then
            spdAVIG.GetText 6, indiceExecucao, vStatusAvig
        Else
            vStatusAvig = "ATIVA"
        End If
        
        If Trim(CStr(vStatusAvig)) = "ATIVA" Then
           Dim cod_estrutura_comercial, tipo_estrutura_comercial As Variant
           spdAVIG.GetText 8, indiceExecucao, tipo_estrutura_comercial
           spdAVIG.GetText 9, indiceExecucao, cod_estrutura_comercial
           If chkContrLog.Value <> vbChecked Then
              SelecionarMaterialAssociado
              Call ImportarCadastroRevista(gridDados, 2, cod_estrutura_comercial, 1, tipo_estrutura_comercial)
           End If
           For X = 1 To gridDados.MaxRows
    
               vCont = 0
               gridDados.GetText 8, X, vPrior
               gridDados.GetText 9, X, vStatus
               gridDados.GetText 3, X, vAbrangencia
               gridDados.GetText 1, X, vMaterial
               If vStatus = "ATIVA" Then
                   'verificar a prioridade
                   If Val(vPrior) = 0 Then
                       MsgBox "Prioridade Deve Ser Superior a 0.  ", vbCritical, "Aten��o"
                       Exit Sub
                   End If
                           
                   For Y = 1 To gridDados.MaxRows
                       gridDados.GetText 8, Y, vPriorAux
                       gridDados.GetText 9, Y, vStatusAux
                       gridDados.GetText 3, Y, vAbrangenciaAux
                       If vStatusAux = "ATIVA" Then
                           If Val(vPriorAux) = Val(vPrior) And _
                               Trim(vAbrangencia) = Trim(vAbrangenciaAux) Then
                               vCont = vCont + 1
                           End If
                       End If
                   Next
                   If vCont > 1 Then
                      MsgBox "Existem outros materiais com abrangencia, vigencia e Prioridade iguais aos do material " & Val(vMaterial) & " .", vbCritical
                      Exit Sub
                   End If
               End If
           Next
           Dim resultado As Boolean
           If chkContrLog.Value = 0 Then
               'consiste duplicidade de materiais
                
                resultado = ConsistirVigenciaMaterialDuplicadoCustom(gridDados, False, indiceExecucao)
                If Not resultado Then
                    Exit Sub
                End If
                resultado = ConsistirVigenciaMaterialCustom(gridDados, indiceExecucao)
               'consistir a vigencia do material
                If Not resultado Then
                    Exit Sub
                End If
               
               'Consistir lista tecnica para kits
                If indicaKit = True Then
                    resultado = ConsistirVigenciaKitCustom(gridDados, indiceExecucao, False)
                    If Not resultado Then
                        Exit Sub
                    End If
                End If
               '
            Else
                resultado = ConsistirVigenciaMaterialDuplicadoCustom(spdMaterial, True, indiceExecucao)
               'consiste duplicidade de materiais
                If Not resultado Then
                    Exit Sub
                End If
            
                resultado = ConsisteAbrangenciaVigencia(spdAVIG)
               'consiste duplicidade de materiais
                If Not resultado Then
                    Exit Sub
                End If
               'Consistir lista tecnica para kits
               If indicaKit = True Then
                    resultado = ConsistirVigenciaKitCustom(gridDados, indiceExecucao, True)
                    If Not resultado Then
                        Exit Sub
                    End If
               End If
               
            End If
            If indiceExecucao = 1 Then
                If indicaKit = True And vMsgListaTec = True Then
                    If MsgBox("Entre as Altera��es Existem Mudan�as de Vig�ncia " & vbCrLf & _
                              "Para Kits com Lista T�cnica Diferente." & vbCrLf & vbCrLf & _
                              "Confirma Altera��o?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then Exit Sub
                 Else
                    If MsgBox("Confirma a Altera��o das Informa��es do Produto.", vbQuestion + vbYesNo) = vbNo Then Exit Sub
                 End If
            End If
            resultado = ManutencaoAbrangenciaCustom(gridDados, indiceExecucao, chkContrLog.Value)
             If Not resultado Then
                Exit Sub
             End If
             'SE 6346 - In�cio
             If Not GravaHistorico(4, strOperacao, "Produto", txtCodVenda.Text, "", "", "", "") Then
                  MsgBox "Falha ao tentar gravar hist�rico de atualiza��es", vbCritical, "Erro"
                  Exit Sub
             End If
                 
             If Not DesbloqueiaRegistro(4) Then
                 MsgBox "Falha ao tentar desbloquear formul�rio.", vbCritical, "Erro"
                 Exit Sub
             End If
             
             If chkContrLog.Value = 1 Then
                 LimpaTela
             Else
                 gridDados.MaxRows = 0
                 mnuSalvar.Enabled = False
                 cmdGravar.Enabled = False
                 cmdCancelar.Enabled = False
                 spdMaterial.Enabled = True
                 DesprotegeTela
             End If
        End If
        
    Next indiceExecucao
    
    MsgBox "Atualiza��o Realizada com Sucesso.", vbInformation, "Aviso"
    ProtegeTela
    If indicaPCL = True And Val(gCodVenda) > 0 Then
       'desativa
       indicaPCL = False
       'volta para a pendencia pcl
       Unload Me
    End If
End Sub


Sub sLiberaSalvar()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
     'If ChecaPermissao("frmPDO003", "M") Then
         'mnuSalvar.Enabled = True
         'cmdGravar.Enabled = True
         'cmdCancelar.Enabled = True
     'End If
   
   'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If bVerificaFuncaoPermissaoLogin("frmPDO003", "M") Then
        mnuSalvar.Enabled = True
        cmdGravar.Enabled = True
        cmdCancelar.Enabled = True
    End If
     
   
End Sub

Private Sub barFerramenta_ButtonClick(ByVal Button As ComctlLib.Button)

    Select Case Button.Key
    
        Case "NovaPesquisa"
             If cmdCancelar.Enabled = True Then
                If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
                   Exit Sub
                End If
             End If
             LimpaTela
             'SE 6346 - In�cio
             If Not DesbloqueiaRegistro(4) Then
                 MsgBox "Falha ao tentar desbloquear formul�rio.", vbCritical, "Erro"
                 Exit Sub
             End If
             
             'SE 6346 - Fim
             fonteDados = 1
             chkMostraCanc.Enabled = True
             cmdGravar.Enabled = False
             cmdCancelar.Enabled = False
        Case "incluir"
             mnuIncluir_Click
             
        Case "alterar"
             mnuAlterar_Click
             
        Case "excluir"
             mnuExcluir_Click
                     
        Case "importar"
            fonteDados = 2
            mnuImportar_Click
        Case "exportar"
               mnuExportar_Click
             
        Case "sair"
             Unload Me
             
    End Select

End Sub

Private Sub chkMostraCanc_Click()

    Dim vStat As Variant
    
    Me.MousePointer = vbHourglass

    spdMaterial.Visible = False
    spdAVIG.Visible = False
    
    
    For X = 1 To spdMaterial.MaxRows
     
        spdMaterial.GetText 9, X, vStat
    
        If vStat <> "ATIVA" Then
           spdMaterial.BlockMode = True
           spdMaterial.Row = X
           spdMaterial.Row2 = X
           spdMaterial.Col = -1
           spdMaterial.Col2 = spdMaterial.MaxCols
           spdMaterial.RowHidden = IIf(chkMostraCanc.Value = 0, True, False)
           spdMaterial.BlockMode = False
        End If
        
    Next
    
    spdMaterial.Visible = True
    spdAVIG.Visible = True

    Me.MousePointer = 1

End Sub

Private Sub cmdCancelar_Click()

    Dim vLinGrid As Integer
    
    vLinGrid = spdAVIG.ActiveRow
    
    If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
        Exit Sub
    End If
    
    If axPend = True Then
       Unload Me
    End If
    
    DesprotegeTela
    fonteDados = 1
    spdAVIG_Click 1, vLinGrid
    
    mnuSalvar.Enabled = False
    cmdGravar.Enabled = False
    cmdCancelar.Enabled = False

End Sub

Private Sub cmdGravar_Click()
    'If chkContrLog.Value = vbChecked Then
        cmdGravar_CtrLogistico
    'Else
    '    cmdGravar_Outros
    'End If
    

End Sub

Private Sub Form_Activate()

    For X = 7 To 17
        spdAVIG.Col = X
        spdAVIG.ColHidden = True
    Next
    
    For X = 10 To 17
        spdMaterial.Col = X
        spdMaterial.ColHidden = True
    Next

    vMsgListaTec = False
    
    vSeqPend = 0
    
    ProtegeTela
    
    If indicaPCL = True And Val(gCodVenda) > 0 Then
       frmPendenciaPCL.spdPendencia.GetText 8, frmPendenciaPCL.spdPendencia.ActiveRow, vSeqPend
       txtCodVenda.Text = Val(gCodVenda)
       txtCodVenda_LostFocus
    End If
    ImageList2.ListImages.Add ImageList2.ListImages.Count + 1, "", MDIpdo001.ImageList1.ListImages(23).Picture
    ImageList2.ListImages.Add ImageList2.ListImages.Count + 1, "", MDIpdo001.ImageList1.ListImages(22).Picture
    barFerramenta.ImageList = ImageList2
    barFerramenta.Buttons(5).Image = 9
    barFerramenta.Buttons(6).Image = 10
    barFerramenta.Buttons(1).Image = 8
    barFerramenta.Buttons(3).Image = 2
    barFerramenta.Buttons(4).Image = 4
    barFerramenta.Buttons(7).Image = 7
    barFerramenta.Buttons(9).Image = 5
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
       SendKeys "{TAB}"
    End If

End Sub

Private Function AtualizarPendencia() As Boolean

    'atualizar os dados do produto de venda
    
    AtualizarPendencia = False
    
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & " exec stp_produto_venda_pendencia_u " & txtCodVenda.Text & "," & vSeqPend & ",'" & UCase(Environ("_U")) & "'"
    cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
     
    If gObjeto.natPrepareQuery(hSql, cSql) Then
       Do While gObjeto.natFetchNext(hSql)
       Loop
    End If
     
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Function
    End If

    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
       Exit Function
    End If

    'atualiza o form de pendencias pcl
    SelecionarPendenciasPCL
    
    AtualizarPendencia = True
    
End Function

Private Sub Form_Load()

    Me.Top = 10
    Me.Left = 10
    fonteDados = 1
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If cmdCancelar.Enabled = True Then
       If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
          Cancel = -1
          Exit Sub
       End If
    End If
    
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(4) Then
        MsgBox "Falha ao tentar desbloquear formul�rio.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
    MDIpdo001.DesbloqueiaMenu
    
End Sub
Private Sub mnuExportar_Click()

On Error GoTo TrataErro

If txtCodVenda.Text = "" Then
    MsgBox "Digite o C�digo de Venda", vbExclamation, "Mensagem"
Else
    If spdAVIG.MaxRows = 0 Then
        MsgBox "N�o existem dados a serem exportados", vbExclamation, "Mensagem"
    Else
       
        If MsgBox("Confirma a Exporta��o dos Dados? ", vbQuestion + vbYesNo) = vbYes Then
            CommonDialog1.Filter = "Arquivo do Excel (*.xls)|*.xls"
            CommonDialog1.FilterIndex = 2
            CommonDialog1.InitDir = App.Path
            CommonDialog1.Flags = cdlOFNHideReadOnly
            CommonDialog1.CancelError = True
            CommonDialog1.ShowSave
            If CarregarDadosParaExportarPCL(1, CDbl(txtCodVenda.Text), chkContrLog.Value, lblDescricao.Caption) Then
                ExportarDadosCadastroRevista 1, CommonDialog1.FileName
            End If

        End If
    End If
End If

Exit Sub

TrataErro:
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    If Err.Number = 32755 Or Err.Number = 1004 Then
        Exit Sub
    End If
    MsgBox Err.Number & " - " & Err.Description, vbCritical, "Erro"

End Sub

Private Sub mnuImportar_Click()

On Error GoTo TrataErro
Dim vStatus As Variant
If txtCodVenda.Text <> "" Then
    If Not IsNumeric(txtCodVenda.Text) Then
        MsgBox "C�digo de Venda inv�lido", vbExclamation, "Mensagem"
    Else
    
        spdAVIG.GetText 6, spdAVIG.ActiveRow, vStatus
        If vStatus <> "ATIVA" And chkContrLog.Value = vbUnchecked Then
            MsgBox "N�o � poss�vel importar vig�ncia PCL para uma abrang�ncia n�o ativa.", vbExclamation, "Mensagem"
            Exit Sub
        End If
        CommonDialog1.CancelError = True
        CommonDialog1.Filter = "Arquivo do Excel (*.xls)|*.xls"
        CommonDialog1.FilterIndex = 2
        CommonDialog1.InitDir = App.Path
        CommonDialog1.Flags = cdlOFNHideReadOnly
        CommonDialog1.ShowOpen

        If CommonDialog1.FileName <> "" Then
            
            caminhoArquivoExcel = CommonDialog1.FileName
            If Dir(caminhoArquivoExcel) = "" Then
                MsgBox "Arquivo n�o encontrado." & vbCrLf & "Informe um arquivo v�lido.", vbExclamation, "Mensagem"
                Exit Sub
            End If
            'caminhoArquivoExcel = "C:\TesteExcel\Exportacao_PCl.xls"
            Dim tamanho As Double
            tamanho = VerTamanhoArquivo(caminhoArquivoExcel)
            Dim mensagem As String
            Dim carregamentoValido As Boolean
            
            carregamentoValido = CarregarListaDadosPcl(caminhoArquivoExcel, tamanho, 1, mensagem, txtCodVenda.Text)
            If Not carregamentoValido Then
                MsgBox mensagem, vbExclamation, "Mensagem"
            Else
                If chkContrLog.Value = 1 Then
                    'ImportarCadastroRevista spdMaterial, 2, 0, 1,0
                Else
                    ImportarCadastroRevistaPCL spdMaterial, 1, 0, 1, 0
                End If
                
                chkMostraCanc.Enabled = False
                cmdGravar.Enabled = True
                cmdCancelar.Enabled = True
                If chkContrLog.Value = vbChecked Then
                    fonteDados = 1
                    SelecionarMaterialAssociado
                    fonteDados = 2
                    Call ImportarCadastroRevistaPCL(spdMaterial, 2, 0, 1, 0)
                    
                End If
                fonteDados = 2
                MsgBox "Importa��o Conclu�da com Sucesso", vbInformation, "Mensagem"
                spdAVIG.Enabled = False
            End If
            
        End If
    End If
Else
    MsgBox "Digite um c�digo de venda.", vbExclamation, "Mensagem"
End If


Exit Sub

TrataErro:
    
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    
    If Err.Number = 32755 Or Err.Number = 1004 Then
        Exit Sub
    End If
    MsgBox Err.Number & " - " & Err.Description
End Sub

Private Sub mnuAlterar_Click()

    If Val(txtCodVenda.Text) <= 0 Or Trim(lblDescricao.Caption) = "" Then
       MsgBox "Informar o Produto.", vbCritical, "Aten��o."
       Exit Sub
    End If
    
    If spdAVIG.MaxRows <= 0 Or spdAVIG.ActiveRow <= 0 Then
       MsgBox "Selecionar a Abrang�ncia.", vbCritical, "Aten��o."
       Exit Sub
    End If
    
    If spdMaterial.MaxRows <= 0 Or spdMaterial.ActiveRow <= 0 Then
       MsgBox "Selecionar o Material.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    spdMaterial.GetText 9, spdMaterial.ActiveRow, vStatus
    
    If vStatus = "CANCELADA" Then
       MsgBox "Materiais Cancelados N�o Podem Ser Alterados.", vbCritical, "Aten��o"
       Exit Sub
    End If

    indicaAltera = 1
    
    frmVigenciaPCLAux.Show (1)
    
End Sub

Private Sub mnuExcluir_Click()

    If Val(txtCodVenda.Text) <= 0 Or Trim(lblDescricao.Caption) = "" Then
       MsgBox "Informar o Produto.", vbCritical, "Aten��o."
       Exit Sub
    End If
    
    If spdAVIG.MaxRows <= 0 Or spdAVIG.ActiveRow <= 0 Then
       MsgBox "Selecionar a Abrang�ncia.", vbCritical, "Aten��o."
       Exit Sub
    End If
    
    If spdMaterial.MaxRows <= 0 Or spdMaterial.ActiveRow <= 0 Then
       MsgBox "Selecionar o Material.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    spdMaterial.GetText 9, spdMaterial.ActiveRow, vStatus
    
    If vStatus = "CANCELADA" Then
       MsgBox "Material J� Cancelado.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    CancelarMaterial
    
End Sub

Private Sub mnuIncluir_Click()
    
    If Val(txtCodVenda.Text) <= 0 Or Trim(lblDescricao.Caption) = "" Then
       MsgBox "Informar o Produto.", vbCritical, "Aten��o."
       Exit Sub
    End If
    
    If chkContrLog.Value = 0 Then
       If spdAVIG.MaxRows <= 0 Or spdAVIG.ActiveRow <= 0 Then
          MsgBox "Selecionar a Abrang�ncia.", vbCritical, "Aten��o."
          Exit Sub
       End If
       
       spdAVIG.GetText 6, spdAVIG.ActiveRow, vStatus
    
       If vStatus = "CANCELADA" Then
          MsgBox "N�o � Permitido Incluir Material Para Abrang�ncia Cancelada.", vbCritical, "Aten��o"
          Exit Sub
       End If
    End If
    
    indicaAltera = 0
    
    frmVigenciaPCLAux.Show (1)
    
End Sub

Private Sub mnuSair_Click()

        If MsgBox("Confirma a Sa�da", vbQuestion + vbYesNo) = vbYes Then
           Unload Me
        End If

End Sub

Private Sub spdAVIG_Click(ByVal Col As Long, ByVal Row As Long)
   
    Dim vAbrang  As Variant
    Dim axVig    As String
    Dim cod_estrutura_comercial, tipo_estrutura_comercial As Variant
    Dim vStatusClick As Variant
On Error GoTo erro
    spdAVIG.GetText 1, spdAVIG.ActiveRow, vAbrang
    spdAVIG.GetText 2, spdAVIG.ActiveRow, vCiclIni
    spdAVIG.GetText 3, spdAVIG.ActiveRow, vDataIni
    spdAVIG.GetText 4, spdAVIG.ActiveRow, vCiclFin
    spdAVIG.GetText 5, spdAVIG.ActiveRow, vDataFin
    spdAVIG.GetText 6, spdAVIG.ActiveRow, vStatusClick
    spdAVIG.GetText 8, spdAVIG.ActiveRow, tipo_estrutura_comercial
    spdAVIG.GetText 9, spdAVIG.ActiveRow, cod_estrutura_comercial
    
    'If Trim(CStr(vStatusClick)) = "ATIVA" Then
        lblAbrangencia.Caption = Trim(vAbrang)
    
        If vCiclIni <> "" Then
           axVig = vCiclIni
        ElseIf vDataIni <> "" Then
               axVig = vDataIni
        End If
        
        If vCiclFin <> "" Then
           axVig = axVig & " a " & vCiclFin
        ElseIf vDataFin <> "" Then
               axVig = axVig & " a " & vDataFin
        End If
        
        lblVigencia.Caption = axVig
        spdMaterial.MaxRows = 0
        chkMostraCanc.Value = 0

        SelecionarMaterialAssociado

'        If fonteDados = 2 Then
'            'Busca os Materiais Associados a Este Codigo de Venda
'           Call ImportarCadastroRevistaPCL(spdMaterial, 2, cod_estrutura_comercial, 1, tipo_estrutura_comercial)
'        End If
    'Else
    '    spdMaterial.MaxRows = 0
    'End If
    Exit Sub
erro:
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    MsgBox Err.Number & " - " & Err.Description, vbCritical, "Aviso"
End Sub

Private Sub BuscarCodigoProduto()

   Dim avData() As Variant

   ReDim avData(0, 0)
   ReDim avParametros(0, 1)
      
   avParametros(0, AV_CONTEUDO) = txtMaterial.Text
   avParametros(0, AV_TIPO) = TIPO_NUMERICO
        
   cSql = " stp_produto_material_s " & FormataParametrosSQL(avParametros)
        
   bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
   If bResp = False Then
      Exit Sub
   End If
        
   If gObjeto.nErroSQL <> 0 Then
      MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
      Exit Sub
   End If
    
    If Val(avData(0, 0)) <= 0 Then
        MsgBox "Produto N�o Encontrado.", vbCritical
        txtMaterial.Text = ""
        txtMaterial.SetFocus
      Exit Sub
   End If
    
   'Descricao
   txtCodVenda.Text = avData(0, 0)
   'seleciona dados do produto
'   SelecionarDadosProduto
     If SelecionarDadosProduto(txtCodVenda.Text) = True Then DesprotegeTela


End Sub

Private Sub LimpaTela()
    
    txtCodVenda.Text = ""
    txtMaterial.Text = ""
    chkContrLog.Value = 0
    lblAbrangencia.Caption = ""
    lblVigencia.Caption = ""
    lblDescricao.Caption = ""
    chkMostraCanc.Value = 0
    txtCodVenda.Enabled = True
    txtMaterial.Enabled = True
    txtCodVenda.SetFocus
    
    spdAVIG.MaxRows = 0
    spdMaterial.MaxRows = 0
    
    fraParte2.Visible = True
    fraParte4.Visible = True
    fraParte3.Top = 4260
    fraParte3.Height = 3705
    spdMaterial.Height = 2235

    ProtegeTela
 
End Sub


Private Sub DesprotegeTela()

    txtCodVenda.Enabled = False
    txtMaterial.Enabled = False
    
    spdAVIG.Enabled = True
    spdMaterial.Enabled = True
    
    barFerramenta.Buttons(1).Enabled = True
    barFerramenta.Buttons(3).Enabled = True
    barFerramenta.Buttons(4).Enabled = True
    barFerramenta.Buttons(5).Enabled = True
    barFerramenta.Buttons(6).Enabled = True
    barFerramenta.Buttons(7).Enabled = True
End Sub

Private Sub SelecionarAbrangenciaVigencia()

   Dim avData() As Variant
   Dim vTipoEst As String

   ReDim avData(0, 0)
   ReDim avParametros(0, 1)
      
   spdAVIG.MaxRows = 0
   spdMaterial.MaxRows = 0
      
   avParametros(0, AV_CONTEUDO) = txtCodVenda.Text
   avParametros(0, AV_TIPO) = TIPO_NUMERICO
        
   cSql = " stp_abrangencia_prod_venda_s " & FormataParametrosSQL(avParametros)
        
   bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
   If bResp = False Then
      Exit Sub
   End If
        
   If gObjeto.nErroSQL <> 0 Then
      MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
      Exit Sub
   End If
   
   strOperacao = "I" 'SE 6346 - Se n�o h� registros cadastrados, � uma inclus�o
   
   If Val(avData(0, 0)) <= 0 Then Exit Sub
   
   strOperacao = "A" 'SE 6346 - Se h� registros cadastrados, � uma altera��o
   
   'Colunas do Spread
   '01 - ABRANG
   '02 - CICLO INI
   '03 - DATA INI
   '04 - CICLO FIN
   '05 - DATA FIN
   '06 - STATUS
   '07 - SEQUENCIA - CPO TABELA
   '08 - TIPO DE ESTRUTURA
   '09 - COD ESTRUTURA
   '10 - RE
   '11 - GV
   '12 - SET
   '13 - CICLO INI ANT
   '14 - DATA INI ANT
   '15 - CICLO FIN ANT
   '16 - DATA FIN ANT
   '17 - OPERACAO - (I)NCLUIR, (A)LTERAR

   Me.MousePointer = vbHourglass
   
   spdAVIG.Visible = False
   spdMaterial.Visible = False
   
   For X = 0 To UBound(avData, 2)
   
       spdAVIG.MaxRows = spdAVIG.MaxRows + 1
       
       vTipoEst = ""
       'tipo estrutura comercial
       If avData(9, X) = "2" Then
          vTipoEst = "RE "
       ElseIf avData(9, X) = "3" Then
              vTipoEst = "GV "
       ElseIf avData(9, X) = "4" Then
              vTipoEst = "ST "
       End If
       
       'abrangencia
       spdAVIG.SetText 1, spdAVIG.MaxRows, vTipoEst & avData(1, X) & " - " & avData(2, X)
       'ciclo inic
       If avData(3, X) <> "" Then
          spdAVIG.SetText 2, spdAVIG.MaxRows, Right(avData(3, X), 2) & "/" & Left(avData(3, X), 4)
       End If
       'data inicio
       spdAVIG.SetText 3, spdAVIG.MaxRows, avData(4, X)
       'ciclo fim
       If avData(5, X) <> "" Then
          spdAVIG.SetText 4, spdAVIG.MaxRows, Right(avData(5, X), 2) & "/" & Left(avData(5, X), 4)
       End If
       'data final
       spdAVIG.SetText 5, spdAVIG.MaxRows, avData(6, X)
       
       'sequencia
       spdAVIG.SetText 7, spdAVIG.MaxRows, avData(8, X)
       'tipo estrutura comercial
       spdAVIG.SetText 8, spdAVIG.MaxRows, avData(9, X)
       'cod estrutura comercial
       spdAVIG.SetText 9, spdAVIG.MaxRows, avData(1, X)
       'regiao estrategia
       spdAVIG.SetText 10, spdAVIG.MaxRows, avData(10, X)
       'gerencia vendas
       spdAVIG.SetText 11, spdAVIG.MaxRows, avData(11, X)
       'setor
       spdAVIG.SetText 12, spdAVIG.MaxRows, avData(12, X)
       
       'ciclo inicio ant
       If avData(3, X) <> "" Then
          spdAVIG.SetText 13, spdAVIG.MaxRows, Right(avData(3, X), 2) & "/" & Left(avData(3, X), 4)
       End If
       'data inicio ant
       spdAVIG.SetText 14, spdAVIG.MaxRows, avData(4, X)
       'ciclo fim ant
       If avData(5, X) <> "" Then
          spdAVIG.SetText 15, spdAVIG.MaxRows, Right(avData(5, X), 2) & "/" & Left(avData(5, X), 4)
       End If
       'data final ant
       spdAVIG.SetText 16, spdAVIG.MaxRows, avData(6, X)
       
       'status
       If Val(avData(7, X)) = 1 Then
          vStatus = "CANCELADA"
       Else
          vStatus = VerificarStatusCiclo(spdAVIG.MaxRows, Val(avData(9, X)), Val(avData(1, X)), "ABR")
       End If
       spdAVIG.SetText 6, spdAVIG.MaxRows, vStatus
       
   Next
    
   spdAVIG.Visible = True
   spdMaterial.Visible = False
    
    Me.MousePointer = 1
    
End Sub

Private Sub SelecionarMaterialAssociado()

    Dim vSequencia As Variant
    Dim avData()   As Variant
    Dim vTipoEst As String

    ReDim avData(0, 0)
    ReDim avParametros(2, 1)

    spdMaterial.MaxRows = 0

    spdAVIG.GetText 7, spdAVIG.ActiveRow, vSequencia

    avParametros(0, AV_CONTEUDO) = txtCodVenda.Text

    avParametros(0, AV_TIPO) = TIPO_NUMERICO

    If chkContrLog.Value = 0 Then
    
       avParametros(1, AV_CONTEUDO) = vSequencia
       avParametros(1, AV_TIPO) = TIPO_NUMERICO
    
       avParametros(2, AV_CONTEUDO) = 1
       avParametros(2, AV_TIPO) = TIPO_NUMERICO
       
    Else
    
       spdAVIG.MaxRows = 0
    
       avParametros(1, AV_CONTEUDO) = 1
       avParametros(1, AV_TIPO) = TIPO_NUMERICO
    
       avParametros(2, AV_CONTEUDO) = 2
       avParametros(2, AV_TIPO) = TIPO_NUMERICO
       
    End If

    cSql = " stp_material_produto_venda_s " & FormataParametrosSQL(avParametros)
        

    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
    If bResp = False Then
       Exit Sub
    End If
        
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Sub
    End If
    
    strOperacao = "I" 'SE 6346 - Se n�o h� registros cadastrados, � uma inclus�o
    
    If Val(avData(0, 0)) <= 0 Then Exit Sub
    
    strOperacao = "A" 'SE 6346 - Se houver registros cadastrados, � uma altera��o
    
    'Colunas do Spread
    '01 - COD MATERIAL
    '02 - DESCRICAO
    '03 - ABRANGENCIA
    '04 - CICLO INI
    '05 - DATA INI
    '06 - CICLO FIN
    '07 - DATA FIN
    '08 - PRIORIDADE
    '09 - STATUS
       
       
    Me.MousePointer = vbHourglass
    
    DoEvents
    
    spdAVIG.Visible = False
    spdMaterial.Visible = False

    For X = 0 To UBound(avData, 2)

        vTipoEst = ""
        If avData(10, X) = "2" Then
            vTipoEst = "RE "
        ElseIf avData(10, X) = "3" Then
            vTipoEst = "GV "
        ElseIf avData(10, X) = "4" Then
            vTipoEst = "ST "
        End If
    
        If chkContrLog.Value = 1 Then
           '
           spdAVIG.MaxRows = spdAVIG.MaxRows + 1
           'abrangencia
           spdAVIG.SetText 1, spdAVIG.MaxRows, vTipoEst & avData(2, X) & " - " & avData(3, X)
           'ciclo inic
           If avData(4, X) <> "" Then
              spdAVIG.SetText 2, spdAVIG.MaxRows, Right(avData(4, X), 2) & "/" & Left(avData(4, X), 4)
           End If
           'data inicio
           spdAVIG.SetText 3, spdAVIG.MaxRows, avData(5, X)
           'ciclo fim
           If avData(6, X) <> "" Then
              spdAVIG.SetText 4, spdAVIG.MaxRows, Right(avData(6, X), 2) & "/" & Left(avData(6, X), 4)
           End If
           'data final
           spdAVIG.SetText 5, spdAVIG.MaxRows, avData(7, X)
           'tipo estrutura comercial
           spdAVIG.SetText 8, spdAVIG.MaxRows, avData(10, X)
           'cod estrutura comercial
           spdAVIG.SetText 9, spdAVIG.MaxRows, avData(2, X)
           'regiao estrategia
           spdAVIG.SetText 10, spdAVIG.MaxRows, avData(11, X)
           'gerencia vendas
           spdAVIG.SetText 11, spdAVIG.MaxRows, avData(12, X)
           'setor
           spdAVIG.SetText 12, spdAVIG.MaxRows, avData(13, X)
           '
        
        End If
        
        'materiais
        spdMaterial.MaxRows = spdMaterial.MaxRows + 1
        
        'cod material
        spdMaterial.SetText 1, spdMaterial.MaxRows, avData(0, X)
        'descr material
        spdMaterial.SetText 2, spdMaterial.MaxRows, avData(1, X)
        'abrangencia
        spdMaterial.SetText 3, spdMaterial.MaxRows, vTipoEst & avData(2, X) & " - " & avData(3, X)
        
        'spdMaterial.SetText 3, spdMaterial.MaxRows, avData(2, X) & " - " & avData(3, X)
        'ciclo inic
        If avData(4, X) <> "" Then
           spdMaterial.SetText 4, spdMaterial.MaxRows, Right(avData(4, X), 2) & "/" & Left(avData(4, X), 4)
        End If
        'data inicio
        spdMaterial.SetText 5, spdMaterial.MaxRows, avData(5, X)
        'ciclo fim
        If avData(6, X) <> "" Then
           spdMaterial.SetText 6, spdMaterial.MaxRows, Right(avData(6, X), 2) & "/" & Left(avData(6, X), 4)
        End If
        'data final
        spdMaterial.SetText 7, spdMaterial.MaxRows, avData(7, X)
        'prioridade
        spdMaterial.SetText 8, spdMaterial.MaxRows, avData(9, X)
       
        'status
        If Val(avData(8, X)) = 1 Then
           vStatus = "CANCELADA"
        Else
           vStatus = VerificarStatusCiclo(spdMaterial.MaxRows, Val(avData(10, X)), Val(avData(2, X)), "MAT")
        End If
        spdMaterial.SetText 9, spdMaterial.MaxRows, vStatus
        
        'seq material
'        spdMaterial.SetText 14, spdMaterial.MaxRows, avData(14, x)
       
        'Aqui guardo para passar para a proc as vigencias antes de alterar.
        'ciclo inic
        If avData(4, X) <> "" Then
           spdMaterial.SetText 14, spdMaterial.MaxRows, Right(avData(4, X), 2) & "/" & Left(avData(4, X), 4)
        End If
        'data inicio
        spdMaterial.SetText 15, spdMaterial.MaxRows, avData(5, X)
        'ciclo fim
        If avData(6, X) <> "" Then
           spdMaterial.SetText 16, spdMaterial.MaxRows, Right(avData(6, X), 2) & "/" & Left(avData(6, X), 4)
        End If
        'data final
        spdMaterial.SetText 17, spdMaterial.MaxRows, avData(7, X)
        
        If vStatus = "CANCELADA" Then
           spdMaterial.BlockMode = True
           spdMaterial.Row = spdMaterial.MaxRows
           spdMaterial.Row2 = spdMaterial.MaxRows
           spdMaterial.Col = -1
           spdMaterial.Col2 = spdMaterial.MaxCols
           spdMaterial.BackColor = &H80FFFF
           spdMaterial.RowHidden = True
           spdMaterial.BlockMode = False
        Else
          If vStatus <> "ATIVA" Then
             spdMaterial.BlockMode = True
             spdMaterial.Row = spdMaterial.MaxRows
             spdMaterial.Row2 = spdMaterial.MaxRows
             spdMaterial.Col = -1
             spdMaterial.Col2 = spdMaterial.MaxCols
             spdMaterial.BackColor = &HFFFF80
             spdMaterial.RowHidden = True
             spdMaterial.BlockMode = False
          End If
        End If
        
        'atualiza status da abrangencia
        If chkContrLog.Value = 1 Then
           'status
           spdMaterial.SetText 9, spdMaterial.MaxRows, vStatus
           'Cancelada ou Encerrada
            If vStatus = "CANCELADA" Then
               spdMaterial.BlockMode = True
               spdMaterial.Row = spdMaterial.MaxRows
               spdMaterial.Row2 = spdMaterial.MaxRows
               spdMaterial.Col = -1
               spdMaterial.Col2 = spdMaterial.MaxCols
               spdMaterial.BackColor = &H80FFFF
               spdMaterial.RowHidden = True
               spdMaterial.BlockMode = False
            Else
            If vStatus <> "ATIVA" Then
                 spdMaterial.BlockMode = True
                 spdMaterial.Row = spdMaterial.MaxRows
                 spdMaterial.Row2 = spdMaterial.MaxRows
                 spdMaterial.Col = -1
                 spdMaterial.Col2 = spdMaterial.MaxCols
                 spdMaterial.BackColor = &HFFFF80
                 spdMaterial.RowHidden = True
                 spdMaterial.BlockMode = False
             End If
            End If
        End If
       
    Next

    spdAVIG.Visible = True
    spdMaterial.Visible = True
    
    Me.MousePointer = 1

    
End Sub

Private Function CancelarMaterial() As Boolean

   Dim vOperacao As Variant
 
   CancelarMaterial = False
  
   spdMaterial.GetText 9, spdMaterial.ActiveRow, vStatus
   spdMaterial.GetText 13, spdMaterial.ActiveRow, vOperacao
   
   If vStatus = "CANCELADA" Then
      MsgBox "Material J� Cancelada.", vbCritical, "Aten��o"
      Exit Function
   End If
   
   If MsgBox("Confirma o Cancelamento do Material.", vbQuestion + vbYesNo, "Aviso") = vbNo Then
      CancelarMaterial = False
      Exit Function
   End If
   
   'indica que esta cancelando uma abrangencia que ainda nao incluir
   If vOperacao = "I" Then
   
      spdMaterial.BlockMode = True
      spdMaterial.Col = -1
      spdMaterial.Row = spdMaterial.ActiveRow
      spdMaterial.Col2 = -1
      spdMaterial.Row2 = spdMaterial.ActiveRow
      'Delete a row in the spreadsheet
      spdMaterial.Action = 5
      spdMaterial.BlockMode = False
      spdMaterial.MaxRows = spdMaterial.MaxRows - 1
      
      If chkContrLog.Value = 1 Then
         spdAVIG.BlockMode = True
         spdAVIG.Col = -1
         spdAVIG.Row = spdMaterial.ActiveRow
         spdAVIG.Col2 = -1
         spdAVIG.Row2 = spdMaterial.ActiveRow
         'Delete a row in the spreadsheet
         spdAVIG.Action = 5
         spdAVIG.BlockMode = False
         spdAVIG.MaxRows = spdAVIG.MaxRows - 1
      End If
      
   Else
   
      'Cancelada ou Encerrada
      spdMaterial.SetText 9, spdMaterial.ActiveRow, "CANCELADA"
      spdMaterial.SetText 13, spdMaterial.ActiveRow, "E"
      
      spdMaterial.BlockMode = True
      spdMaterial.Row = spdMaterial.ActiveRow
      spdMaterial.Row2 = spdMaterial.ActiveRow
      spdMaterial.Col = -1
      spdMaterial.Col2 = spdMaterial.MaxCols
      spdMaterial.BackColor = &H80FFFF
      spdMaterial.RowHidden = True
      spdMaterial.BlockMode = False
      
      If chkContrLog.Value = 1 Then
         spdAVIG.BlockMode = True
         spdAVIG.Row = spdMaterial.ActiveRow
         spdAVIG.Row2 = spdMaterial.ActiveRow
         spdAVIG.Col = -1
         spdAVIG.Col2 = spdAVIG.MaxCols
         spdAVIG.BackColor = &H80FFFF
         spdAVIG.RowHidden = True
         spdAVIG.BlockMode = False
      End If
      
   End If
      
'   MsgBox "Material Cancelado.", vbInformation, "Aviso"
   
   CancelarMaterial = True
   
   sLiberaSalvar
'   cmdGravar.Enabled = True
'   cmdCancelar.Enabled = True

End Function

Private Function ConsisteDados() As Boolean
    
    ConsisteDados = False
    
    If Trim(txtCodVenda.Text) = "" Then
       MsgBox "Favor Informar o Produto.", vbCritical
       Exit Function
    End If
    
    If Trim(lblDescricao.Caption) = "" Then
       MsgBox "Favor Informar o Produto.", vbCritical
       Exit Function
    End If
    
    If spdAVIG.MaxRows <= 0 Then
       MsgBox "N�o Possui Abrang�ncia Cadastrada.", vbCritical
       Exit Function
    End If

    ConsisteDados = True

End Function

Private Sub spdMaterial_DblClick(ByVal Col As Long, ByVal Row As Long)

    mnuAlterar_Click

End Sub

Private Sub txtCodVenda_LostFocus()
    
On Error GoTo erro
    
    If Trim(txtCodVenda.Text) <> "" Then
       txtMaterial.Text = ""
       fonteDados = 1
       'busca informacoes pelo produto
       chkMostraCanc.Enabled = True
       cmdCancelar.Enabled = False
       cmdGravar.Enabled = False
       If SelecionarDadosProduto(txtCodVenda.Text) = True Then DesprotegeTela
    End If
    
    Exit Sub
erro:
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    MsgBox Err.Number & " - " & Err.Description, vbCritical, "Aviso"
End Sub


Private Sub txtMaterial_LostFocus()
    If Trim(txtCodVenda.Text) = "" Then
       If txtMaterial.Text <> "" Then
          BuscarCodigoProduto
       End If
    End If
End Sub

Public Function SelecionarDadosProduto(CodigoVenda As String) As Boolean
  SelecionarDadosProduto = False

   Dim avData() As Variant

    If Trim(CodigoVenda) = "" Then
        MsgBox "Obrigat�rio preencher o c�digo de venda para pesquisa!!", vbCritical
        If fonteDados = 1 Then
            txtCodVenda.SetFocus
        End If
        Exit Function
    End If
    If Not IsNumeric(CodigoVenda) Then
        MsgBox "O c�digo de venda deve ser num�rico!!", vbCritical
        If fonteDados = 1 Then
            txtCodVenda.SetFocus
        End If
        Exit Function
    End If
    If Not ChecaCodigo(Val(CodigoVenda)) Then
        If fonteDados = 1 Then
            txtCodVenda.SetFocus
        End If
        Exit Function
    End If
       
    
    'SE 6346 - In�cio
    Dim CodUsuario As String
    If Not DesbloqueiaRegistro(4) Then
        MsgBox "Falha ao tentar desbloquear formul�rio.", vbCritical, "Erro"
        Exit Function
    End If
       
    If VerificaBloqueioFormulario(4, CDbl(CodigoVenda), 0, CodUsuario) Then
        ExibeMensagemBloqueio ObtemDescricaoFormulario(4), "Produto", CodigoVenda, "", "", CodUsuario
        Exit Function
    End If
    'SE 6346 - Fim
    
    
    ReDim avData(0, 0)

    ReDim avParametros(0, 1)

    avParametros(0, AV_CONTEUDO) = CodigoVenda
    avParametros(0, AV_TIPO) = TIPO_NUMERICO
         
    cSql = " stp_produto_venda_dados_s " & FormataParametrosSQL(avParametros)
         
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
         
    If bResp = False Then
       Exit Function
    End If
         
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Function
    End If
     
    If Val(avData(0, 0)) <= 0 Then
       MsgBox "Produto N�o Cadastrado ou Inativo.", vbCritical
       CodigoVenda = ""
       txtCodVenda.SetFocus
       Exit Function
    End If

    'SE 6346 - In�cio
    If Not BloqueiaRegistro(4, Val(CodigoVenda), 0) Then
        MsgBox "Falha ao tentar realizar bloqueio de registro.", vbCritical, "Erro"
        Exit Function
    End If
    'SE 6346 - Fim

    'Descricao
    lblDescricao.Caption = avData(1, 0)
    
    'Controle do PCL
    If Val(avData(4, 0)) = 1 Then
       chkContrLog.Value = 1
    ElseIf Val(avData(4, 0)) = 2 Then
           chkContrLog.Value = 0
    End If

    EhControleLogistico
    SelecionarDadosProduto = True
End Function
Private Function ConsistirVigenciaMaterial() As Boolean

    Dim vVezes      As Long
    Dim vVigIniAux  As Variant
    
    Dim vMes        As Boolean
    Dim vDia        As Boolean
    Dim vAchou      As Boolean

    ConsistirVigenciaMaterial = False
    
    vLin = spdAVIG.ActiveRow

    spdAVIG.GetText 2, vLin, vCiclIni
    spdAVIG.GetText 3, vLin, vDataIni
    spdAVIG.GetText 4, vLin, vCiclFin
    spdAVIG.GetText 5, vLin, vDataFin
    spdAVIG.GetText 8, vLin, vTpEstrut
    spdAVIG.GetText 9, vLin, vCdEstrut
    
    'seleciona a vigencia do ciclo
    'data inicial
    If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
       vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
       'vigencia
       If Not SelecionarVigenciaCiclo(vCiclIni, vTpEstrut, vCdEstrut) Then Exit Function
       'determinou a data de inicio
       vDataIni = vDataInicioCiclo
    End If
    'data final
    If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
       vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
       'vigencia
       If Not SelecionarVigenciaCiclo(vCiclFin, vTpEstrut, vCdEstrut) Then Exit Function
       'determinou a data de inicio
       vDataFin = vDataFinalCiclo
    End If
    'caso nao tenha final
    If Trim(vDataFin) = "" Then
       vDataFin = "31/12/9999"
    End If
    
    'isso e para determinar se algum material possui a data inicial e final
    'igual a da abrangencia vigencia - isso e obrigatorio
    vAchouInicio = False
    vAchouFinal = False
    
    For X = 1 To spdMaterial.MaxRows
        'dados do material
        spdMaterial.GetText 1, X, vMaterial
        spdMaterial.GetText 4, X, vCiclIniMat
        spdMaterial.GetText 5, X, vDataIniMat
        spdMaterial.GetText 6, X, vCiclFinMat
        spdMaterial.GetText 7, X, vDataFinMat
        spdMaterial.GetText 9, X, vStatus
        
        spdMaterial.SetText 12, X, ""
        
        'seleciona a vigencia do ciclo
        'data inicial
        If Trim(vCiclIniMat) <> "" And Trim(vDataIniMat) = "" Then
           vCiclIniMat = Right(vCiclIniMat, 4) & Left(vCiclIniMat, 2)
           'vigencia
           If Not SelecionarVigenciaCiclo(vCiclIniMat, vTpEstrut, vCdEstrut) Then Exit Function
           'determinou a data de inicio
           vDataIniMat = vDataInicioCiclo
        End If
        'data final
        If Trim(vCiclFinMat) <> "" And Trim(vDataFinMat) = "" Then
           vCiclFinMat = Right(vCiclFinMat, 4) & Left(vCiclFinMat, 2)
           'vigencia
           If Not SelecionarVigenciaCiclo(vCiclFinMat, vTpEstrut, vCdEstrut) Then Exit Function
           'determinou a data de inicio
           vDataFinMat = vDataFinalCiclo
        End If
        'caso nao tenha final
        If Trim(vDataFinMat) = "" Then
           vDataFinMat = "31/12/9999"
        End If
        
        'armazena as vigencias para uso posterior
        spdMaterial.SetText 10, X, vDataIniMat
        spdMaterial.SetText 11, X, vDataFinMat
        
        'controle
        If vStatus = "CANCELADA" Then
        
           spdMaterial.SetText 12, X, "X"
           
        Else
    
           'verifica se possui algum material com vigencia inicial inferior ou
           'se possui vigencia final superior
           If Val(vCiclIniMat) > 0 And Val(vCiclIni) > 0 Then
              If Val(vCiclIniMat) < Val(vCiclIni) Then
                 MsgBox "Data Inicial do Material " & vMaterial & " � Inferior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf Val(vCiclIniMat) = Val(vCiclIni) Then
                     vAchouInicio = True
              End If
           Else
              If DateValue(vDataIniMat) < DateValue(vDataIni) Then
                 MsgBox "Data Inicial do Material " & vMaterial & " � Inferior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf DateValue(vDataIniMat) = DateValue(vDataIni) Then
                     vAchouInicio = True
              End If
           End If
           
           If Val(vCiclFinMat) > 0 And Val(vCiclFin) > 0 Then
              If Val(vCiclFinMat) > Val(vCiclFin) Then
                 MsgBox "Data Final do Material " & vMaterial & " � Superior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf DateValue(vDataFinMat) = DateValue(vDataFin) Then
                     If vAchouFinal = False Then
                        vAchouFinal = True
                        vVigenciaIni = vDataIniMat
                        vVigenciaFin = vDataFinMat
                        spdMaterial.SetText 12, X, "X"
                        'encontrou a data final e inicial igual a da abrangencia pai
                        If DateValue(vVigenciaIni) = DateValue(vDataIni) Then
'                           ConsistirVigenciaMaterial = True
'                           Exit Function
                        End If
                     End If
              End If
           Else
              If DateValue(vDataFinMat) > DateValue(vDataFin) Then
                 MsgBox "Data Final do Material " & vMaterial & " � Superior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf DateValue(vDataFinMat) = DateValue(vDataFin) Then
                     If vAchouFinal = False Then
                        vAchouFinal = True
                        vVigenciaIni = vDataIniMat
                        vVigenciaFin = vDataFinMat
                        spdMaterial.SetText 12, X, "X"
                        'encontrou a data final e inicial igual a da abrangencia pai
                        If DateValue(vVigenciaIni) = DateValue(vDataIni) Then
'                           ConsistirVigenciaMaterial = True
'                           Exit Function
                        End If
                     End If
              End If
           End If
           
        End If
        
    Next
    
    'achou material com data inicial e final iguais a da abrangencia vigencia
    If vAchouInicio = False Then
        MsgBox "A vig�ncia dos materiais deve completar a vig�ncia do Produto/Abrag�ncia." & vbCrLf & _
                "Existem per�odos, no inicio da vig�ncia, n�o cobertos por nenhum material.", vbCritical, "Aten��o"
        Exit Function
    End If
    If vAchouFinal = False Then
        MsgBox "A vig�ncia dos materiais deve completar a vig�ncia do Produto/Abrag�ncia." & vbCrLf & _
                "Existem per�odos, no fim da vig�ncia, n�o cobertos por nenhum material.", vbCritical, "Aten��o"
       Exit Function
    End If
    
    'para determinar se possui mais de um mes
    vMes = False
    vDia = False
    
    'consistir se possui buracos entre a data inicial e final
    vVigIniAux = vVigenciaIni
    
    'meses
    vVezes = DateDiff("m", vDataIni, vVigenciaIni)
    
    If vVezes > 0 Then
       'meses
       vMes = True
       vVezes = vVezes + 1
    Else
       'dias
       vVezes = DateDiff("d", vDataIni, vVigenciaIni)
       'em dias
       If vVezes > 0 Then
          'dias
          vDia = True
          vVezes = vVezes + 1
       End If
    End If
    
    
    For X = 1 To vVezes
    
        If vMes = True Then
           'meses
           If X = 1 Then
              vVigenciaIni = vDataIni
              'Cleiton Cadmus 12/09/2007
              'Colocado a fun��o Format para formatar a data quando tiver zero � esquerda Ex. 01/01/1980
              vVigenciaFin = "01" & Mid(Format(DateAdd("m", 1, DateValue(vDataIni)), "dd/mm/yyyy"), 3)
              vVigenciaFin = DateAdd("d", -1, vVigenciaFin)
           ElseIf X < vVezes Then
                  vVigenciaIni = DateAdd("d", 1, DateValue(vVigenciaFin))
                  'Cleiton Cadmus 12/09/2007
                  'Colocado a fun��o Format para formatar a data quando tiver zero � esquerda Ex. 01/01/1980
                  vVigenciaFin = "01" & Mid(Format(DateAdd("m", 1, DateValue(vVigenciaIni)), "dd/mm/yyyy"), 3)
                  vVigenciaFin = DateAdd("d", -1, vVigenciaFin)
           ElseIf X = vVezes Then
                  vVigenciaIni = DateAdd("d", 1, DateValue(vVigenciaFin))
                  vVigenciaFin = DateAdd("d", -1, vVigIniAux)
                  
                  If DateValue(vVigenciaFin) < DateValue(vVigenciaIni) Then
                     ConsistirVigenciaMaterial = True
                     Exit Function
                  End If
           End If
        Else
           'dias
           If X = 1 Then
              vVigenciaIni = vDataIni
              vVigenciaFin = vDataIni
           Else
              vVigenciaIni = DateAdd("d", 1, DateValue(vVigenciaIni))
              vVigenciaFin = DateAdd("d", 1, DateValue(vVigenciaFin))
           End If
        End If
        
        
        vAchou = False
        
        'procura o periodo
        For Y = 1 To spdMaterial.MaxRows
        
            spdMaterial.GetText 12, Y, vControle
        
            If vControle <> "X" Then
        
               spdMaterial.GetText 10, Y, vDtInicioAux
               spdMaterial.GetText 11, Y, vDtFinalAux
               
               'inicial
               If DateValue(vVigenciaIni) >= DateValue(vDtInicioAux) Then
                  If DateValue(vVigenciaIni) <= DateValue(vDtFinalAux) Then
                     'final
                     If DateValue(vVigenciaFin) >= DateValue(vDtInicioAux) Then
                        If DateValue(vVigenciaFin) <= DateValue(vDtFinalAux) Then
                           vAchou = True
                           Exit For
                        End If
                     End If
                  End If
               End If
            
            End If
            
        Next
        
        If vAchou = False Then
           MsgBox "A vig�ncia dos materiais deve completar a vig�ncia do Produto/Abrag�ncia." & vbCrLf & _
                  "Existem per�odos n�o cobertos por nenhum material.", vbCritical, "Aten��o"
           Exit Function
        End If
        
    Next

    ConsistirVigenciaMaterial = True

End Function

'
'
Private Function ConsistirVigenciaMaterialCustom(gridDadosManipulacao As vaSpread, indiceLinhaGrid As Double) As Boolean

    Dim vVezes      As Long
    Dim vVigIniAux  As Variant
    
    Dim vMes        As Boolean
    Dim vDia        As Boolean
    Dim vAchou      As Boolean

    ConsistirVigenciaMaterialCustom = False
    
    vLin = indiceLinhaGrid

    spdAVIG.GetText 2, vLin, vCiclIni
    spdAVIG.GetText 3, vLin, vDataIni
    spdAVIG.GetText 4, vLin, vCiclFin
    spdAVIG.GetText 5, vLin, vDataFin
    spdAVIG.GetText 8, vLin, vTpEstrut
    spdAVIG.GetText 9, vLin, vCdEstrut
    
    'seleciona a vigencia do ciclo
    'data inicial
    If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
       vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
       'vigencia
       If Not SelecionarVigenciaCiclo(vCiclIni, vTpEstrut, vCdEstrut) Then Exit Function
       'determinou a data de inicio
       vDataIni = vDataInicioCiclo
    End If
    'data final
    If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
       vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
       'vigencia
       If Not SelecionarVigenciaCiclo(vCiclFin, vTpEstrut, vCdEstrut) Then Exit Function
       'determinou a data de inicio
       vDataFin = vDataFinalCiclo
    End If
    'caso nao tenha final
    If Trim(vDataFin) = "" Then
       vDataFin = "31/12/9999"
    End If
    
    'isso e para determinar se algum material possui a data inicial e final
    'igual a da abrangencia vigencia - isso e obrigatorio
    vAchouInicio = False
    vAchouFinal = False
    
    For X = 1 To gridDadosManipulacao.MaxRows
        'dados do material
        gridDadosManipulacao.GetText 1, X, vMaterial
        gridDadosManipulacao.GetText 4, X, vCiclIniMat
        gridDadosManipulacao.GetText 5, X, vDataIniMat
        gridDadosManipulacao.GetText 6, X, vCiclFinMat
        gridDadosManipulacao.GetText 7, X, vDataFinMat
        gridDadosManipulacao.GetText 9, X, vStatus
        
        gridDadosManipulacao.SetText 12, X, ""
        
        'seleciona a vigencia do ciclo
        'data inicial
        If Trim(vCiclIniMat) <> "" And Trim(vDataIniMat) = "" Then
           vCiclIniMat = Right(vCiclIniMat, 4) & Left(vCiclIniMat, 2)
           'vigencia
           If Not SelecionarVigenciaCiclo(vCiclIniMat, vTpEstrut, vCdEstrut) Then Exit Function
           'determinou a data de inicio
           vDataIniMat = vDataInicioCiclo
        End If
        'data final
        If Trim(vCiclFinMat) <> "" And Trim(vDataFinMat) = "" Then
           vCiclFinMat = Right(vCiclFinMat, 4) & Left(vCiclFinMat, 2)
           'vigencia
           If Not SelecionarVigenciaCiclo(vCiclFinMat, vTpEstrut, vCdEstrut) Then Exit Function
           'determinou a data de inicio
           vDataFinMat = vDataFinalCiclo
        End If
        'caso nao tenha final
        If Trim(vDataFinMat) = "" Then
           vDataFinMat = "31/12/9999"
        End If
        
        'armazena as vigencias para uso posterior
        gridDadosManipulacao.SetText 10, X, vDataIniMat
        gridDadosManipulacao.SetText 11, X, vDataFinMat
        
        'controle
        If vStatus = "CANCELADA" Then
        
           gridDadosManipulacao.SetText 12, X, "X"
           
        Else
    
           'verifica se possui algum material com vigencia inicial inferior ou
           'se possui vigencia final superior
           If Val(vCiclIniMat) > 0 And Val(vCiclIni) > 0 Then
              If Val(vCiclIniMat) < Val(vCiclIni) Then
                 MsgBox "Data Inicial do Material " & vMaterial & " � Inferior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf Val(vCiclIniMat) = Val(vCiclIni) Then
                     vAchouInicio = True
              End If
           Else
              If DateValue(vDataIniMat) < DateValue(vDataIni) Then
                 MsgBox "Data Inicial do Material " & vMaterial & " � Inferior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf DateValue(vDataIniMat) = DateValue(vDataIni) Then
                     vAchouInicio = True
              End If
           End If
           
           If Val(vCiclFinMat) > 0 And Val(vCiclFin) > 0 Then
              If Val(vCiclFinMat) > Val(vCiclFin) Then
                 MsgBox "Data Final do Material " & vMaterial & " � Superior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf DateValue(vDataFinMat) = DateValue(vDataFin) Then
                     If vAchouFinal = False Then
                        vAchouFinal = True
                        vVigenciaIni = vDataIniMat
                        vVigenciaFin = vDataFinMat
                        spdMaterial.SetText 12, X, "X"
                        'encontrou a data final e inicial igual a da abrangencia pai
                        If DateValue(vVigenciaIni) = DateValue(vDataIni) Then
'                           ConsistirVigenciaMaterial = True
'                           Exit Function
                        End If
                     End If
              End If
           Else
              If DateValue(vDataFinMat) > DateValue(vDataFin) Then
                 MsgBox "Data Final do Material " & vMaterial & " � Superior a Data do Produto/Abrang�ncia.", vbCritical, "Aten��o"
                 Exit Function
              ElseIf DateValue(vDataFinMat) = DateValue(vDataFin) Then
                     If vAchouFinal = False Then
                        vAchouFinal = True
                        vVigenciaIni = vDataIniMat
                        vVigenciaFin = vDataFinMat
                        spdMaterial.SetText 12, X, "X"
                        'encontrou a data final e inicial igual a da abrangencia pai
                        If DateValue(vVigenciaIni) = DateValue(vDataIni) Then
'                           ConsistirVigenciaMaterial = True
'                           Exit Function
                        End If
                     End If
              End If
           End If
           
        End If
        
    Next
    
    'achou material com data inicial e final iguais a da abrangencia vigencia
    If vAchouInicio = False Then
        MsgBox "A vig�ncia dos materiais deve completar a vig�ncia do Produto/Abrag�ncia." & vbCrLf & _
                "Existem per�odos, no inicio da vig�ncia, n�o cobertos por nenhum material.", vbCritical, "Aten��o"
        Exit Function
    End If
    If vAchouFinal = False Then
        MsgBox "A vig�ncia dos materiais deve completar a vig�ncia do Produto/Abrag�ncia." & vbCrLf & _
                "Existem per�odos, no fim da vig�ncia, n�o cobertos por nenhum material.", vbCritical, "Aten��o"
       Exit Function
    End If
    
    'para determinar se possui mais de um mes
    vMes = False
    vDia = False
    
    'consistir se possui buracos entre a data inicial e final
    vVigIniAux = vVigenciaIni
    
    'meses
    vVezes = DateDiff("m", vDataIni, vVigenciaIni)
    
    If vVezes > 0 Then
       'meses
       vMes = True
       vVezes = vVezes + 1
    Else
       'dias
       vVezes = DateDiff("d", vDataIni, vVigenciaIni)
       'em dias
       If vVezes > 0 Then
          'dias
          vDia = True
          vVezes = vVezes + 1
       End If
    End If
    
    
    For X = 1 To vVezes
    
        If vMes = True Then
           'meses
           If X = 1 Then
              vVigenciaIni = vDataIni
              'Cleiton Cadmus 12/09/2007
              'Colocado a fun��o Format para formatar a data quando tiver zero � esquerda Ex. 01/01/1980
              vVigenciaFin = "01" & Mid(Format(DateAdd("m", 1, DateValue(vDataIni)), "dd/mm/yyyy"), 3)
              vVigenciaFin = DateAdd("d", -1, vVigenciaFin)
           ElseIf X < vVezes Then
                  vVigenciaIni = DateAdd("d", 1, DateValue(vVigenciaFin))
                  'Cleiton Cadmus 12/09/2007
                  'Colocado a fun��o Format para formatar a data quando tiver zero � esquerda Ex. 01/01/1980
                  vVigenciaFin = "01" & Mid(Format(DateAdd("m", 1, DateValue(vVigenciaIni)), "dd/mm/yyyy"), 3)
                  vVigenciaFin = DateAdd("d", -1, vVigenciaFin)
           ElseIf X = vVezes Then
                  vVigenciaIni = DateAdd("d", 1, DateValue(vVigenciaFin))
                  vVigenciaFin = DateAdd("d", -1, vVigIniAux)
                  
                  If DateValue(vVigenciaFin) < DateValue(vVigenciaIni) Then
                     ConsistirVigenciaMaterialCustom = True
                     Exit Function
                  End If
           End If
        Else
           'dias
           If X = 1 Then
              vVigenciaIni = vDataIni
              vVigenciaFin = vDataIni
           Else
              vVigenciaIni = DateAdd("d", 1, DateValue(vVigenciaIni))
              vVigenciaFin = DateAdd("d", 1, DateValue(vVigenciaFin))
           End If
        End If
        
        
        vAchou = False
        
        'procura o periodo
        For Y = 1 To gridDadosManipulacao.MaxRows
        
            gridDadosManipulacao.GetText 12, Y, vControle
        
            If vControle <> "X" Then
        
               gridDadosManipulacao.GetText 10, Y, vDtInicioAux
               gridDadosManipulacao.GetText 11, Y, vDtFinalAux
               
               'inicial
               If DateValue(vVigenciaIni) >= DateValue(vDtInicioAux) Then
                  If DateValue(vVigenciaIni) <= DateValue(vDtFinalAux) Then
                     'final
                     If DateValue(vVigenciaFin) >= DateValue(vDtInicioAux) Then
                        If DateValue(vVigenciaFin) <= DateValue(vDtFinalAux) Then
                           vAchou = True
                           Exit For
                        End If
                     End If
                  End If
               End If
            
            End If
            
        Next
        
        If vAchou = False Then
           MsgBox "A vig�ncia dos materiais deve completar a vig�ncia do Produto/Abrag�ncia." & vbCrLf & _
                  "Existem per�odos n�o cobertos por nenhum material.", vbCritical, "Aten��o"
           Exit Function
        End If
        
    Next

    ConsistirVigenciaMaterialCustom = True
End Function
Private Function ConsistirVigenciaMaterialDuplicado() As Boolean
 
    Dim vGeraErro     As Boolean
    Dim vRE           As Variant
    Dim vGV           As Variant
    Dim vSet          As Variant
    Dim vREAux        As Variant
    Dim vGVAux        As Variant
    Dim vSetAux       As Variant
    Dim vCompara      As String
    Dim vComparaAux   As String


    ConsistirVigenciaMaterialDuplicado = False
    
    For X = 1 To spdMaterial.MaxRows
    
        If chkContrLog.Value = 0 Then
            vLin = spdAVIG.ActiveRow
        Else
            vLin = X
        End If

        spdAVIG.GetText 8, vLin, vTpEstrut
        spdAVIG.GetText 9, vLin, vCdEstrut
        spdAVIG.GetText 10, vLin, vRE
        spdAVIG.GetText 11, vLin, vGV
        spdAVIG.GetText 12, vLin, vSet
        
        'concatena para comparar
        vCompara = ""
        If Val(vRE) > 0 Then
           vCompara = vCompara & vRE
        End If
        If Val(vGV) > 0 Then
           vCompara = vCompara & vGV
        End If
        If Val(vSet) > 0 Then
           vCompara = vCompara & vSet
        End If
    
        'dados do material
        spdMaterial.GetText 1, X, vMaterial
        spdMaterial.GetText 4, X, vCiclIni
        spdMaterial.GetText 5, X, vDataIni
        spdMaterial.GetText 6, X, vCiclFin
        spdMaterial.GetText 7, X, vDataFin
        spdMaterial.GetText 9, X, vStatus
        
        If vStatus = "ATIVA" Then
        
           'seleciona a vigencia do ciclo
           'data inicial
           If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
              If InStr(1, vCiclIni, "/") > 0 Then
                vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
              End If
              'vigencia
              If Not SelecionarVigenciaCiclo(vCiclIni, vTpEstrut, vCdEstrut) Then Exit Function
              'determinou a data de inicio
              vDataIni = vDataInicioCiclo
           End If
           'data final
           If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
              If InStr(1, vCiclFin, "/") > 0 Then
                vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
              End If
              'vigencia
              If Not SelecionarVigenciaCiclo(vCiclFin, vTpEstrut, vCdEstrut) Then Exit Function
              'determinou a data de inicio
              vDataFin = vDataFinalCiclo
           End If
           'caso nao tenha final
           If Trim(vDataFin) = "" Then
              vDataFin = "31/12/9999"
           End If
            
           'verifica duplicidade
           For Y = 1 To spdMaterial.MaxRows
            
               'dados do material
               spdMaterial.GetText 1, Y, vMaterialAux
               spdMaterial.GetText 4, Y, vCiclIniMat
               spdMaterial.GetText 5, Y, vDataIniMat
               spdMaterial.GetText 6, Y, vCiclFinMat
               spdMaterial.GetText 7, Y, vDataFinMat
               spdMaterial.GetText 9, Y, vStatus

               If chkContrLog.Value = 1 Then
                  spdAVIG.GetText 8, Y, vTpEstrutAux
                  spdAVIG.GetText 9, Y, vCdEstrutAux
                  spdAVIG.GetText 10, Y, vREAux
                  spdAVIG.GetText 11, Y, vGVAux
                  spdAVIG.GetText 12, Y, vSetAux
                  
                  'concatena para comparar
                  vComparaAux = ""
                  If Val(vRE) > 0 Then
                     vComparaAux = vComparaAux & vREAux
                  End If
                  If Val(vGV) > 0 Then
                     vComparaAux = vComparaAux & vGVAux
                  End If
                  If Val(vSet) > 0 Then
                     vComparaAux = vComparaAux & vSetAux
                  End If
               End If
                
               If Trim(vMaterial) = Trim(vMaterialAux) And X <> Y And vStatus = "ATIVA" And _
                  (chkContrLog.Value = 0 Or _
                  (chkContrLog.Value = 1 And (Val(vTpEstrut) = 0 Or Val(vTpEstrutAux = 0) Or vCompara = vComparaAux))) Then
                
                  'seleciona a vigencia do ciclo
                  'data inicial
                  If Trim(vCiclIniMat) <> "" And Trim(vDataIniMat) = "" Then
                     vCiclIniMat = Right(vCiclIniMat, 4) & Left(vCiclIniMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclIniMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataIniMat = vDataInicioCiclo
                  End If
                  'data final
                  If Trim(vCiclFinMat) <> "" And Trim(vDataFinMat) = "" Then
                     vCiclFinMat = Right(vCiclFinMat, 4) & Left(vCiclFinMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclFinMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataFinMat = vDataFinalCiclo
                  End If
                  'caso nao tenha final
                  If Trim(vDataFinMat) = "" Then
                     vDataFinMat = "31/12/9999"
                  End If
                   
                  'ira consistir se bao tem conflito de vigencia
                  If VerificaConflitoVigencia(vCiclIni, vCiclFin, vCiclIniMat, vCiclFinMat, vDataIni, vDataFin, vDataIniMat, vDataFinMat) = False Then
                     MsgBox "Conflito de Vig�ncia / Abrang�ncia Entre Materiais Duplicados." & vbCrLf & _
                            "Verifique as linhas " & CStr(X) & " e " & CStr(Y) & ".", vbExclamation, "Aten��o"
                     Exit Function
                  End If
                   
               End If
            
           Next
            
        End If
        
    Next

    ConsistirVigenciaMaterialDuplicado = True

End Function

'
Private Function ConsistirVigenciaMaterialDuplicadoCustom(gridDadosManipulacao As vaSpread, IsControleLogistico As Boolean, indiceLinhaGrid As Double) As Boolean
 
    Dim vGeraErro     As Boolean
    Dim vRE           As Variant
    Dim vGV           As Variant
    Dim vSet          As Variant
    Dim vREAux        As Variant
    Dim vGVAux        As Variant
    Dim vSetAux       As Variant
    Dim vCompara      As String
    Dim vComparaAux   As String
    'indiceLinhaGrid reflete o active row do spdAvig no caso de controle logistico = false

    ConsistirVigenciaMaterialDuplicadoCustom = False
    
    For X = 1 To gridDadosManipulacao.MaxRows
    
        If Not IsControleLogistico Then
            vLin = indiceLinhaGrid
        Else
            vLin = X
        End If
        
        spdAVIG.GetText 8, vLin, vTpEstrut
        spdAVIG.GetText 9, vLin, vCdEstrut
        spdAVIG.GetText 10, vLin, vRE
        spdAVIG.GetText 11, vLin, vGV
        spdAVIG.GetText 12, vLin, vSet
        
        'concatena para comparar
        vCompara = ""
        If Val(vRE) > 0 Then
           vCompara = vCompara & vRE
        End If
        If Val(vGV) > 0 Then
           vCompara = vCompara & vGV
        End If
        If Val(vSet) > 0 Then
           vCompara = vCompara & vSet
        End If
    
        'dados do material
        gridDadosManipulacao.GetText 1, X, vMaterial
        gridDadosManipulacao.GetText 4, X, vCiclIni
        gridDadosManipulacao.GetText 5, X, vDataIni
        gridDadosManipulacao.GetText 6, X, vCiclFin
        gridDadosManipulacao.GetText 7, X, vDataFin
        gridDadosManipulacao.GetText 9, X, vStatus
        
        If vStatus = "ATIVA" Then
        
           'seleciona a vigencia do ciclo
           'data inicial
           If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
              vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
              'vigencia
              If Not SelecionarVigenciaCiclo(vCiclIni, vTpEstrut, vCdEstrut) Then Exit Function
              'determinou a data de inicio
              vDataIni = vDataInicioCiclo
           End If
           'data final
           If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
              vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
              'vigencia
              If Not SelecionarVigenciaCiclo(vCiclFin, vTpEstrut, vCdEstrut) Then Exit Function
              'determinou a data de inicio
              vDataFin = vDataFinalCiclo
           End If
           'caso nao tenha final
           If Trim(vDataFin) = "" Then
              vDataFin = "31/12/9999"
           End If
            
           'verifica duplicidade
           For Y = 1 To gridDadosManipulacao.MaxRows
            
               'dados do material
               gridDadosManipulacao.GetText 1, Y, vMaterialAux
               gridDadosManipulacao.GetText 4, Y, vCiclIniMat
               gridDadosManipulacao.GetText 5, Y, vDataIniMat
               gridDadosManipulacao.GetText 6, Y, vCiclFinMat
               gridDadosManipulacao.GetText 7, Y, vDataFinMat
               gridDadosManipulacao.GetText 9, Y, vStatus
               
               If IsControleLogistico Then
                  spdAVIG.GetText 8, Y, vTpEstrutAux
                  spdAVIG.GetText 9, Y, vCdEstrutAux
                  spdAVIG.GetText 10, Y, vREAux
                  spdAVIG.GetText 11, Y, vGVAux
                  spdAVIG.GetText 12, Y, vSetAux
                  
                  'concatena para comparar
                  vComparaAux = ""
                  If Val(vRE) > 0 Then
                     vComparaAux = vComparaAux & vREAux
                  End If
                  If Val(vGV) > 0 Then
                     vComparaAux = vComparaAux & vGVAux
                  End If
                  If Val(vSet) > 0 Then
                     vComparaAux = vComparaAux & vSetAux
                  End If
               End If
                
               If Trim(vMaterial) = Trim(vMaterialAux) And X <> Y And vStatus = "ATIVA" And _
                  (chkContrLog.Value = 0 Or _
                  (chkContrLog.Value = 1 And (Val(vTpEstrut) = 0 Or Val(vTpEstrutAux = 0) Or vCompara = vComparaAux))) Then
                
                  'seleciona a vigencia do ciclo
                  'data inicial
                  If Trim(vCiclIniMat) <> "" And Trim(vDataIniMat) = "" Then
                     vCiclIniMat = Right(vCiclIniMat, 4) & Left(vCiclIniMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclIniMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataIniMat = vDataInicioCiclo
                  End If
                  'data final
                  If Trim(vCiclFinMat) <> "" And Trim(vDataFinMat) = "" Then
                     vCiclFinMat = Right(vCiclFinMat, 4) & Left(vCiclFinMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclFinMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataFinMat = vDataFinalCiclo
                  End If
                  'caso nao tenha final
                  If Trim(vDataFinMat) = "" Then
                     vDataFinMat = "31/12/9999"
                  End If
                   
                  'ira consistir se bao tem conflito de vigencia
                  If VerificaConflitoVigencia(vCiclIni, vCiclFin, vCiclIniMat, vCiclFinMat, vDataIni, vDataFin, vDataIniMat, vDataFinMat) = False Then
                     MsgBox "Conflito de Vig�ncia / Abrang�ncia Entre Materiais Duplicados.", vbCritical, "Aten��o"
                     Exit Function
                  End If
                   
               End If
            
           Next
            
        End If
        
    Next

    ConsistirVigenciaMaterialDuplicadoCustom = True

End Function

Private Function ConsistirVigenciaKit() As Boolean
 
    Dim vGeraErro As Boolean

    vMsgListaTec = False

    ConsistirVigenciaKit = False
    
    For X = 1 To spdMaterial.MaxRows
    
        If chkContrLog.Value = 0 Then
            vLin = spdAVIG.ActiveRow
        Else
            vLin = X
        End If
        
        spdAVIG.GetText 8, vLin, vTpEstrut
        spdAVIG.GetText 9, vLin, vCdEstrut
    
        'dados do material
        spdMaterial.GetText 1, X, vMaterial
        spdMaterial.GetText 4, X, vCiclIni
        spdMaterial.GetText 5, X, vDataIni
        spdMaterial.GetText 6, X, vCiclFin
        spdMaterial.GetText 7, X, vDataFin
        spdMaterial.GetText 9, X, vStatus
        
        If vStatus = "ATIVA" Then
        
            'seleciona a vigencia do ciclo
            'data inicial
            If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
                vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
                'vigencia
                If Not SelecionarVigenciaCiclo(vCiclIni, vTpEstrut, vCdEstrut) Then Exit Function
                'determinou a data de inicio
                vDataIni = vDataInicioCiclo
            End If
            'data final
            If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
                vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
                'vigencia
                If Not SelecionarVigenciaCiclo(vCiclFin, vTpEstrut, vCdEstrut) Then Exit Function
                'determinou a data de inicio
                vDataFin = vDataFinalCiclo
            End If
            'caso nao tenha final
            If Trim(vDataFin) = "" Then
                vDataFin = "31/12/9999"
            End If
            If Not ExisteListaTecnica(vMaterial) Then
                MsgBox "O Kit da linha " & X & " n�o tem lista t�cnica cadastrada, por isto n�o pode ser relacionado!!", vbCritical
                Exit Function
            End If
        
           For Y = X + 1 To spdMaterial.MaxRows
           
               'dados do material
               spdMaterial.GetText 1, Y, vMaterialAux
               spdMaterial.GetText 4, Y, vCiclIniMat
               spdMaterial.GetText 5, Y, vDataIniMat
               spdMaterial.GetText 6, Y, vCiclFinMat
               spdMaterial.GetText 7, Y, vDataFinMat
               spdMaterial.GetText 9, Y, vStatus
               
               If chkContrLog.Value = 1 Then
                  spdAVIG.GetText 8, Y, vTpEstrutAux
                  spdAVIG.GetText 9, Y, vCdEstrutAux
               End If
           
               
               If vStatus = "ATIVA" And ConsultaListaTecnica(vMaterial, vMaterialAux) Then
               
                  vMsgListaTec = True
                   
                  'seleciona a vigencia do ciclo
                  'data inicial
                  If Trim(vCiclIniMat) <> "" And Trim(vDataIniMat) = "" Then
                     vCiclIniMat = Right(vCiclIniMat, 4) & Left(vCiclIniMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclIniMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataIniMat = vDataInicioCiclo
                  End If
                  'data final
                  If Trim(vCiclFinMat) <> "" And Trim(vDataFinMat) = "" Then
                     vCiclFinMat = Right(vCiclFinMat, 4) & Left(vCiclFinMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclFinMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataFinMat = vDataFinalCiclo
                  End If
                  'caso nao tenha final
                  If Trim(vDataFinMat) = "" Then
                     vDataFinMat = "31/12/9999"
                  End If
                
                
                  'ira consistir se bao tem conflito de vigencia
                  If VerificaConflitoVigencia(vCiclIni, vCiclFin, vCiclIniMat, vCiclFinMat, vDataIni, vDataFin, vDataIniMat, vDataFinMat) = False Then
                     MsgBox "Conflito de Vig�ncia/Abrang�ncia entre as linhas " & X & " e " & Y & ", Materiais com listas t�cnicas diferentes.", vbCritical, "Aten��o"
                     Exit Function
                  End If
                     
               End If
               
           Next
           
        End If
        
    Next

    ConsistirVigenciaKit = True

End Function

'
'
'
Private Function ConsistirVigenciaKitCustom(gridDadosManipulacao As vaSpread, indiceLinhaGrid As Double, IsControleLogistico As Boolean) As Boolean
 
    Dim vGeraErro As Boolean

    vMsgListaTec = False

    ConsistirVigenciaKitCustom = False
    
    For X = 1 To gridDadosManipulacao.MaxRows
    
        If Not IsControleLogistico Then
            vLin = indiceLinhaGrid
        Else
            vLin = X
        End If
        
        spdAVIG.GetText 8, vLin, vTpEstrut
        spdAVIG.GetText 9, vLin, vCdEstrut
    
        'dados do material
        gridDadosManipulacao.GetText 1, X, vMaterial
        gridDadosManipulacao.GetText 4, X, vCiclIni
        gridDadosManipulacao.GetText 5, X, vDataIni
        gridDadosManipulacao.GetText 6, X, vCiclFin
        gridDadosManipulacao.GetText 7, X, vDataFin
        gridDadosManipulacao.GetText 9, X, vStatus
        
        If vStatus = "ATIVA" Then
        
            'seleciona a vigencia do ciclo
            'data inicial
            If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
                vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
                'vigencia
                If Not SelecionarVigenciaCiclo(vCiclIni, vTpEstrut, vCdEstrut) Then Exit Function
                'determinou a data de inicio
                vDataIni = vDataInicioCiclo
            End If
            'data final
            If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
                vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
                'vigencia
                If Not SelecionarVigenciaCiclo(vCiclFin, vTpEstrut, vCdEstrut) Then Exit Function
                'determinou a data de inicio
                vDataFin = vDataFinalCiclo
            End If
            'caso nao tenha final
            If Trim(vDataFin) = "" Then
                vDataFin = "31/12/9999"
            End If
            If Not ExisteListaTecnica(vMaterial) Then
                MsgBox "O Kit da linha " & X & " n�o tem lista t�cnica cadastrada, por isto n�o pode ser relacionado!!", vbCritical
                Exit Function
            End If
        
           For Y = X + 1 To gridDadosManipulacao.MaxRows
           
               'dados do material
               gridDadosManipulacao.GetText 1, Y, vMaterialAux
               gridDadosManipulacao.GetText 4, Y, vCiclIniMat
               gridDadosManipulacao.GetText 5, Y, vDataIniMat
               gridDadosManipulacao.GetText 6, Y, vCiclFinMat
               gridDadosManipulacao.GetText 7, Y, vDataFinMat
               gridDadosManipulacao.GetText 9, Y, vStatus
               
               If IsControleLogistico Then
                  spdAVIG.GetText 8, Y, vTpEstrutAux
                  spdAVIG.GetText 9, Y, vCdEstrutAux
               End If
           
               
               If vStatus = "ATIVA" And ConsultaListaTecnica(vMaterial, vMaterialAux) Then
               
                  vMsgListaTec = True
                   
                  'seleciona a vigencia do ciclo
                  'data inicial
                  If Trim(vCiclIniMat) <> "" And Trim(vDataIniMat) = "" Then
                     vCiclIniMat = Right(vCiclIniMat, 4) & Left(vCiclIniMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclIniMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataIniMat = vDataInicioCiclo
                  End If
                  'data final
                  If Trim(vCiclFinMat) <> "" And Trim(vDataFinMat) = "" Then
                     vCiclFinMat = Right(vCiclFinMat, 4) & Left(vCiclFinMat, 2)
                     'vigencia
                     If Not SelecionarVigenciaCiclo(vCiclFinMat, vTpEstrut, vCdEstrut) Then Exit Function
                     'determinou a data de inicio
                     vDataFinMat = vDataFinalCiclo
                  End If
                  'caso nao tenha final
                  If Trim(vDataFinMat) = "" Then
                     vDataFinMat = "31/12/9999"
                  End If
                
                
                  'ira consistir se bao tem conflito de vigencia
                  If VerificaConflitoVigencia(vCiclIni, vCiclFin, vCiclIniMat, vCiclFinMat, vDataIni, vDataFin, vDataIniMat, vDataFinMat) = False Then
                     MsgBox "Conflito de Vig�ncia/Abrang�ncia entre as linhas " & X & " e " & Y & ", Materiais com listas t�cnicas diferentes.", vbCritical, "Aten��o"
                     Exit Function
                  End If
                     
               End If
               
           Next
           
        End If
        
    Next

    ConsistirVigenciaKitCustom = True

End Function

Private Function ConsultaListaTecnica(vCodKit1, vCodKit2) As Boolean

    ConsultaListaTecnica = False
    
    'no caso do ciclo deve buscar a data final do ciclo
    cSql = "declare @kit_igual char(1), @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & " exec stp_pdr_compara_kit_s " & vCodKit1 & "," & vCodKit2
    cSql = cSql & ",@kit_igual out, @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
    If gObjeto.natPrepareQuery(hSql, cSql) Then
       Do While gObjeto.natFetchNext(hSql)
       Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Function
    End If
          
    sql_cd_ret = CInt(gObjeto.varOutputParam(1))
    sql_cd_opr = gObjeto.varOutputParam(2)
    sql_nm_tab = gObjeto.varOutputParam(3)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(4))
          
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
       Exit Function
    End If
    
    If gObjeto.varOutputParam(0) = "S" Then
       ConsultaListaTecnica = False
    Else
       ConsultaListaTecnica = True
    End If
          
End Function


Private Function ExisteListaTecnica(vCodKit1) As Boolean

    ExisteListaTecnica = False
    
    'no caso do ciclo deve buscar a data final do ciclo
    cSql = "declare @kit_existe char(1), @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & " exec stp_pdr_existe_kit_s " & vCodKit1 & ", 'M'"
    cSql = cSql & ",@kit_existe out, @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
    If gObjeto.natPrepareQuery(hSql, cSql) Then
       Do While gObjeto.natFetchNext(hSql)
       Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Function
    End If
          
    sql_cd_ret = CInt(gObjeto.varOutputParam(1))
    sql_cd_opr = gObjeto.varOutputParam(2)
    sql_nm_tab = gObjeto.varOutputParam(3)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(4))
          
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
       Exit Function
    End If
    
    If gObjeto.varOutputParam(0) = "S" Then
       ExisteListaTecnica = True
    Else
       ExisteListaTecnica = False
    End If
          
End Function
Private Function ManutencaoAbrangencia() As Boolean

   Dim vPriori      As Variant
   Dim vSequencia   As Variant
   Dim vSeqMaterial As Variant
   Dim vOperacao    As Variant
   Dim vTipEstr     As Variant
   Dim vCodEstr     As Variant
   Dim vLin         As Integer

   'spdmaterial
   '01 - MATERIAL
   '02 - DESCR
   '03 - ABRANGENC
   '04 - CICL INI
   '05 - DATA INI
   '06 - CICL FIN
   '07 - DATA FIN
   '08 - PRIORIDADE
   '09 - STATUS
   '10 - INICIO VIGENCIA
   '11 - FINAL VIGENCIA
   '12 - CONTROLE
   '13 - OPERACAO - (I)NCLUIR, (A)LTERAR, (E)XCLUIR
   '14 - ciclo ini sem altera��o
   '15 - dt ini sem altera��o
   '16 - ciclo fin sem altera��o
   '17 - dt fin sem altera��o
   
   ManutencaoAbrangencia = False
   
 On Error GoTo ErroManut
  
   bResp = gObjeto.natPrepareQuery(hSql, "Begin Transaction")
  
   For X = 1 To spdMaterial.MaxRows
   
       spdMaterial.GetText 1, X, vMaterial
       spdMaterial.GetText 4, X, vCiclIni
       spdMaterial.GetText 5, X, vDataIni
       spdMaterial.GetText 6, X, vCiclFin
       spdMaterial.GetText 7, X, vDataFin
       spdMaterial.GetText 8, X, vPriori
       spdMaterial.GetText 9, X, vStatus
       spdMaterial.GetText 13, X, vOperacao
       spdMaterial.GetText 14, X, vCiclIniAnt
       spdMaterial.GetText 15, X, vDataIniAnt
       spdMaterial.GetText 16, X, vCiclFinAnt
       spdMaterial.GetText 17, X, vDataFinAnt
       
       If chkContrLog.Value = 0 Then
          vLin = spdAVIG.ActiveRow
       Else
          vLin = X
       End If
       
       spdAVIG.GetText 7, vLin, vSequencia
       spdAVIG.GetText 8, vLin, vTipEstr
       spdAVIG.GetText 9, vLin, vCodEstr
       
       If vSequencia = "" Then vSequencia = 0
       
       If Trim(vCiclIni) <> "" Then
          vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
       Else
          vCiclIni = 0
       End If
       If Trim(vCiclFin) <> "" Then
          vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
       Else
          vCiclFin = 0
       End If
       
       If Trim(vCiclIniAnt) <> "" Then
          vCiclIniAnt = Right(vCiclIniAnt, 4) & Left(vCiclIniAnt, 2)
       Else
          vCiclIniAnt = 0
       End If
       If Trim(vCiclFinAnt) <> "" Then
          vCiclFinAnt = Right(vCiclFinAnt, 4) & Left(vCiclFinAnt, 2)
       Else
          vCiclFinAnt = 0
       End If
       
       'Inclusao
       If vOperacao = "I" Then
       
          If chkContrLog.Value = 1 Then
             vSequencia = 0
          End If
       
          'chama a procedure de insercao de uma nova Abrangencia Vigencia
          cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
          cSql = cSql & " exec stp_pdr_rel_venda_i " & txtCodVenda.Text & "," & vMaterial & "," & vTipEstr & ","
          cSql = cSql & vCodEstr & "," & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
          cSql = cSql & vPriori & ",'" & UCase(Environ("_U")) & "'," & Val(vSequencia)
          cSql = cSql & " , @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
          
       ElseIf vOperacao = "E" Then
       
              'chama a procedure de cancelamento da Abrangencia Vigencia
              cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
              cSql = cSql & " exec stp_pdr_canc_rel_venda_u " & txtCodVenda.Text & "," & vSequencia & "," '& vSeqMaterial & ","
              cSql = cSql & vMaterial & "," & vTipEstr & "," & vCodEstr & ","
              cSql = cSql & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
              cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
              
       ElseIf vOperacao = "A" Then
       
              'chama a procedure de altera��o da Abrangencia Vigencia
              cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
              cSql = cSql & " exec stp_pdr_rel_venda_u " & txtCodVenda.Text & "," & vSequencia & "," '& vSeqMaterial & ","
              cSql = cSql & vMaterial & "," & vTipEstr & "," & vCodEstr & "," & vCiclIniAnt & ",'"
              cSql = cSql & vDataIniAnt & "'," & vCiclFinAnt & ",'" & vDataFinAnt & "',"
              cSql = cSql & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
              cSql = cSql & vPriori & ", '" & UCase(Environ("_U")) & "'"
              cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
              
       End If
       
       'executa a proc de insert ou de update
        If Trim(vOperacao) <> "" Then
            If gObjeto.natPrepareQuery(hSql, cSql) Then
                Do While gObjeto.natFetchNext(hSql)
                Loop
            End If
            If gObjeto.nErroSQL <> 0 Then
                MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                GoTo Rolback
            End If
        
            sql_cd_ret = CInt(gObjeto.varOutputParam(0))
            sql_cd_opr = gObjeto.varOutputParam(1)
            sql_nm_tab = gObjeto.varOutputParam(2)
            sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
            
            If sql_cd_ret <> 0 Then
                MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
                GoTo Rolback
            End If
        End If

   Next
   
   'verifica a necessidade de se criar a pendencia tipo 6
   If indicaKit = True And vMsgListaTec = True Then
      'chama a procedure de insercao de pendencia
      cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
      cSql = cSql & " exec stp_produto_venda_pendencia_i " & txtCodVenda.Text & "," & 6 & "," & 1 & "," & 0
      cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
      If gObjeto.natPrepareQuery(hSql, cSql) Then
         Do While gObjeto.natFetchNext(hSql)
         Loop
      End If
      If gObjeto.nErroSQL <> 0 Then
         MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
         GoTo Rolback
      End If
          
      If sql_cd_ret <> 0 Then
         MsgBox "Erro ao executar a stp_produto_venda_pendencia_i " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
         GoTo Rolback
      End If
   End If
   
   'ira atualizar o PCL
    If indicaPCL = True And Val(gCodVenda) > 0 Then
        'atualiza a tabela t_pdr_pendencia
        If Not AtualizarPendencia Then
            GoTo Rolback
        End If
    End If
   
   bResp = gObjeto.natPrepareQuery(hSql, "Commit Transaction")
   
   MsgBox "Atualiza��o Realizada com Sucesso.", vbInformation, "Aviso"

   ManutencaoAbrangencia = True
   
   Exit Function
   
ErroManut:

   MsgBox "Erro. " & Err.Number & " - " & Err.Description, vbCritical, "Erro."
   
Rolback:
   bResp = gObjeto.natPrepareQuery(hSql, "Rollback Transaction")
   
End Function

'
'
Private Function ManutencaoAbrangenciaCustom(gridManutencaoDados As vaSpread, indiceLinhaGrid As Double, IsControleLogistico As Boolean) As Boolean

   Dim vPriori      As Variant
   Dim vSequencia   As Variant
   Dim vSeqMaterial As Variant
   Dim vOperacao    As Variant
   Dim vTipEstr     As Variant
   Dim vCodEstr     As Variant
   Dim vLin         As Integer

   'spdmaterial
   '01 - MATERIAL
   '02 - DESCR
   '03 - ABRANGENC
   '04 - CICL INI
   '05 - DATA INI
   '06 - CICL FIN
   '07 - DATA FIN
   '08 - PRIORIDADE
   '09 - STATUS
   '10 - INICIO VIGENCIA
   '11 - FINAL VIGENCIA
   '12 - CONTROLE
   '13 - OPERACAO - (I)NCLUIR, (A)LTERAR, (E)XCLUIR
   '14 - ciclo ini sem altera��o
   '15 - dt ini sem altera��o
   '16 - ciclo fin sem altera��o
   '17 - dt fin sem altera��o
   
   ManutencaoAbrangenciaCustom = False
   
 On Error GoTo ErroManut
  
   bResp = gObjeto.natPrepareQuery(hSql, "Begin Transaction")
  
   For X = 1 To gridManutencaoDados.MaxRows
   
       gridManutencaoDados.GetText 1, X, vMaterial
       gridManutencaoDados.GetText 4, X, vCiclIni
       gridManutencaoDados.GetText 5, X, vDataIni
       gridManutencaoDados.GetText 6, X, vCiclFin
       gridManutencaoDados.GetText 7, X, vDataFin
       gridManutencaoDados.GetText 8, X, vPriori
       gridManutencaoDados.GetText 9, X, vStatus
       gridManutencaoDados.GetText 13, X, vOperacao
       gridManutencaoDados.GetText 14, X, vCiclIniAnt
       gridManutencaoDados.GetText 15, X, vDataIniAnt
       gridManutencaoDados.GetText 16, X, vCiclFinAnt
       gridManutencaoDados.GetText 17, X, vDataFinAnt
       
       If Not IsControleLogistico Then
          vLin = indiceLinhaGrid
       Else
          vLin = X
       End If
    
       spdAVIG.GetText 7, vLin, vSequencia
          
       spdAVIG.GetText 8, vLin, vTipEstr
       spdAVIG.GetText 9, vLin, vCodEstr
       
       If vSequencia = "" Then vSequencia = 0
       
       If fonteDados = 2 Then
            Dim DadosImportados() As Variant
            ReDim DadosImportados(0, 0)
            Dim retorno As Boolean
            
            retorno = RetornarRegistrosAgrupados(DadosImportados, vSequencia, CDbl(txtCodVenda.Text))
            
            If Not retorno Then
                GoTo Rolback:
            End If
            
            If UBound(DadosImportados, 2) > 0 Then
                Dim j As Double
                For j = 0 To UBound(DadosImportados, 2)
                    If Val(DadosImportados(0, j)) = Val(vMaterial) Then
                        If Val(DadosImportados(10, j)) = Val(vTipEstr) Then
                            If Val(DadosImportados(2, j)) = Val(vCodEstr) Then
                                vCiclIniAnt = CStr(DadosImportados(4, j))
                                vDataIniAnt = CStr(DadosImportados(5, j))
                                vCiclFinAnt = CStr(DadosImportados(6, j))
                                vDataFinAnt = CStr(DadosImportados(7, j))
                                Exit For
                            End If
                        End If
                    End If
                Next j
            End If
       Else
                
          If Trim(vCiclIniAnt) <> "" Then
             vCiclIniAnt = Right(vCiclIniAnt, 4) & Left(vCiclIniAnt, 2)
          Else
             vCiclIniAnt = 0
          End If
          If Trim(vCiclFinAnt) <> "" Then
             vCiclFinAnt = Right(vCiclFinAnt, 4) & Left(vCiclFinAnt, 2)
          Else
             vCiclFinAnt = 0
          End If

       End If
       If Trim(vCiclIni) <> "" Then
           vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
        Else
           vCiclIni = 0
        End If
        If Trim(vCiclFin) <> "" Then
           vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
        Else
           vCiclFin = 0
        End If
       'Inclusao
       If vOperacao = "I" Then
       
          If chkContrLog.Value = 1 Then
             vSequencia = 0
          End If
       
          'chama a procedure de insercao de uma nova Abrangencia Vigencia
          cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
          cSql = cSql & " exec stp_pdr_rel_venda_i " & txtCodVenda.Text & "," & vMaterial & "," & vTipEstr & ","
          cSql = cSql & vCodEstr & "," & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
          cSql = cSql & vPriori & ",'" & UCase(Environ("_U")) & "'," & Val(vSequencia)
          cSql = cSql & " , @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
          
       ElseIf vOperacao = "E" Then
       
              'chama a procedure de cancelamento da Abrangencia Vigencia
              cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
              cSql = cSql & " exec stp_pdr_canc_rel_venda_u " & txtCodVenda.Text & "," & vSequencia & "," '& vSeqMaterial & ","
              cSql = cSql & vMaterial & "," & vTipEstr & "," & vCodEstr & ","
              cSql = cSql & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
              cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
              
       ElseIf vOperacao = "A" Then
       
              'chama a procedure de altera��o da Abrangencia Vigencia
              cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
              cSql = cSql & " exec stp_pdr_rel_venda_u " & txtCodVenda.Text & "," & vSequencia & "," '& vSeqMaterial & ","
              cSql = cSql & vMaterial & "," & vTipEstr & "," & vCodEstr & "," & vCiclIniAnt & ",'"
              cSql = cSql & vDataIniAnt & "'," & vCiclFinAnt & ",'" & vDataFinAnt & "',"
              cSql = cSql & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
              cSql = cSql & vPriori & ", '" & UCase(Environ("_U")) & "'"
              cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
              
       End If
       
       'executa a proc de insert ou de update
        If Trim(vOperacao) <> "" Then
            If gObjeto.natPrepareQuery(hSql, cSql) Then
                Do While gObjeto.natFetchNext(hSql)
                Loop
            End If
            If gObjeto.nErroSQL <> 0 Then
                MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                GoTo Rolback
            End If
        
            sql_cd_ret = CInt(gObjeto.varOutputParam(0))
            sql_cd_opr = gObjeto.varOutputParam(1)
            sql_nm_tab = gObjeto.varOutputParam(2)
            sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
            
            If sql_cd_ret <> 0 Then
                MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
                GoTo Rolback
            End If
        End If

   Next
   
   'verifica a necessidade de se criar a pendencia tipo 6
   If indicaKit = True And vMsgListaTec = True Then
      'chama a procedure de insercao de pendencia
      cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
      cSql = cSql & " exec stp_produto_venda_pendencia_i " & txtCodVenda.Text & "," & 6 & "," & 1 & "," & 0
      cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
      If gObjeto.natPrepareQuery(hSql, cSql) Then
         Do While gObjeto.natFetchNext(hSql)
         Loop
      End If
      If gObjeto.nErroSQL <> 0 Then
         MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
         GoTo Rolback
      End If
          
      If sql_cd_ret <> 0 Then
         MsgBox "Erro ao executar a stp_produto_venda_pendencia_i " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
         GoTo Rolback
      End If
   End If
   
   'ira atualizar o PCL
    If indicaPCL = True And Val(gCodVenda) > 0 Then
        'atualiza a tabela t_pdr_pendencia
        If Not AtualizarPendencia Then
            GoTo Rolback
        End If
    End If
   
   bResp = gObjeto.natPrepareQuery(hSql, "Commit Transaction")
   
   ManutencaoAbrangenciaCustom = True
   
   Exit Function
   
ErroManut:

   MsgBox "Erro. " & Err.Number & " - " & Err.Description, vbCritical, "Erro."
   
Rolback:
   bResp = gObjeto.natPrepareQuery(hSql, "Rollback Transaction")
   
End Function

Sub ProtegeTela()
    
    txtCodVenda.Enabled = True
    txtMaterial.Enabled = True
    
    spdAVIG.Enabled = False
    spdAVIG.MaxRows = 0
    spdMaterial.Enabled = False
    spdMaterial.MaxRows = 0
    
    mnuSalvar.Enabled = False
    cmdGravar.Enabled = False
    cmdCancelar.Enabled = False
    
    barFerramenta.Buttons(1).Enabled = False
    barFerramenta.Buttons(3).Enabled = False
    barFerramenta.Buttons(4).Enabled = False
    barFerramenta.Buttons(5).Enabled = False
    barFerramenta.Buttons(6).Enabled = False
    barFerramenta.Buttons(7).Enabled = False
    Me.Refresh
    
End Sub

Public Sub EhControleLogistico()
   'PCL
    If chkContrLog.Value = 1 Then
    
       fraParte2.Visible = False
       fraParte4.Visible = False
       fraParte3.Top = 2220
       fraParte3.Height = 5715
       spdMaterial.Height = 4155
       If fonteDados = 1 Then
           'seleciona diretamente os materiais
           SelecionarMaterialAssociado
       End If
       
    Else
    
       fraParte2.Visible = True
       fraParte4.Visible = True
       fraParte3.Top = 4260
       fraParte3.Height = 3705
       spdMaterial.Height = 2235
       
       'seleciona as abrangencias vigencias
       SelecionarAbrangenciaVigencia
             
    End If
End Sub
