VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmImpExcel 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importar Lista de Pre�os do Excel"
   ClientHeight    =   7260
   ClientLeft      =   420
   ClientTop       =   1920
   ClientWidth     =   12000
   Icon            =   "frmImpExcel.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   12000
   Begin ComctlLib.Toolbar barFerramenta 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   12000
      _ExtentX        =   21167
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "sair"
            Object.ToolTipText     =   "Sair "
            Object.Tag             =   "Sair"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin VB.Frame frmErros 
      Caption         =   "Erros encontrados na planilha de Importa��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Left            =   30
      TabIndex        =   12
      Top             =   3870
      Visible         =   0   'False
      Width           =   11955
      Begin FPSpread.vaSpread spdErros 
         Height          =   2835
         Left            =   180
         TabIndex        =   5
         Top             =   360
         Width           =   11685
         _Version        =   131077
         _ExtentX        =   20611
         _ExtentY        =   5001
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   3
         MaxRows         =   1
         ScrollBars      =   2
         SpreadDesigner  =   "frmImpExcel.frx":0CCA
         VisibleCols     =   500
         VisibleRows     =   500
      End
   End
   Begin ComctlLib.ProgressBar pgbImporta 
      Height          =   495
      Left            =   30
      TabIndex        =   10
      Top             =   3330
      Width           =   11955
      _ExtentX        =   21087
      _ExtentY        =   873
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   30
      TabIndex        =   6
      Top             =   630
      Width           =   11955
      Begin VB.ComboBox cmbCiclo 
         Height          =   360
         ItemData        =   "frmImpExcel.frx":0EB9
         Left            =   1830
         List            =   "frmImpExcel.frx":0EE4
         TabIndex        =   0
         Top             =   180
         Width           =   1545
      End
      Begin VB.TextBox txtCaminho 
         Height          =   360
         Left            =   1830
         TabIndex        =   1
         Top             =   600
         Width           =   9615
      End
      Begin VB.CommandButton cmdArquivo 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8,25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   11490
         TabIndex        =   2
         Top             =   600
         Width           =   405
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo / Ano"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8,25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   960
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo a Importar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8,25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   7
         Top             =   630
         Width           =   1560
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   4440
      TabIndex        =   4
      Top             =   1800
      Width           =   3525
      Begin VB.CommandButton cmdImportar 
         Caption         =   "Importar Excel"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   795
         Left            =   1050
         Picture         =   "frmImpExcel.frx":0F5D
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   210
         Width           =   1605
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3360
      Top             =   1980
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblFuncao 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   60
      TabIndex        =   11
      Top             =   2970
      Visible         =   0   'False
      Width           =   9585
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   780
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":1267
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":1581
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":189B
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":1BB5
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":1ECF
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":21E9
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":2503
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmImpExcel.frx":281D
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmImpExcel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cSql            As String
Dim lNumLin         As Long

'Dim ExcelApp        As New Excel.Application
'Dim ExcelWb         As Excel.Workbook
'Dim ExcelSheet      As Excel.Worksheet

Dim ExcelApp        As Object
Dim ExcelWb         As Object
Dim ExcelSheet      As Object

Dim lQuantListas    As Long
Dim v_qtd_listas    As Long

Dim QtdPrecoFuturo As Integer


Private Sub barFerramenta_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
        Case 1:
            Unload Me
    End Select
End Sub

Private Sub cmbCiclo_Click()
    If cmbCiclo.ListIndex >= 0 And Len(Trim(txtCaminho.Text)) > 0 Then
        cmdImportar.Enabled = True
    End If
End Sub

Private Sub cmdArquivo_Click()
    CommonDialog1.Filter = "Arquivo do Excel (*.xls)|*.xls"
    CommonDialog1.FilterIndex = 2
    CommonDialog1.InitDir = App.Path
    CommonDialog1.Flags = cdlOFNHideReadOnly
    CommonDialog1.ShowOpen
    txtCaminho.Text = CommonDialog1.FileName
    If cmbCiclo.ListIndex >= 0 And Len(Trim(txtCaminho.Text)) > 0 Then
        cmdImportar.Enabled = True
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdImportar_Click()

    If cmbCiclo.ListIndex < 0 Then
        MsgBox "Ciclo n�o informado", vbCritical, ""
        Exit Sub
    End If
    
    If Len(Trim(txtCaminho.Text)) = 0 Then
        MsgBox "Arquivo para importa��o n�o informado", vbCritical, ""
        Exit Sub
    End If
    
    If Importar Then
        MsgBox "Importa��o conclu�da com sucesso!!", vbExclamation
        
        cmbCiclo.ListIndex = -1
        txtCaminho.Text = ""
        cmdImportar.Enabled = False
        cmbCiclo.SetFocus
        
        pgbImporta.Value = 0
        Me.Refresh
    
    End If
    
    Screen.MousePointer = vbNormal
    lblFuncao.Visible = False
    pgbImporta.Visible = False
    pgbImporta.Value = 0
    Me.Refresh
    cmbCiclo.SetFocus

End Sub

Private Function Importar() As Boolean

On Error GoTo ErroImportar

Dim sNomeArq        As String
    
    Importar = False
    
    sNomeArq = txtCaminho.Text 'Tira_Espaco(Tira_Ponto(txtCaminho.Text)) & ".xls"
    
    Set ExcelApp = CreateObject("Excel.Application")

    'Abrir o arquivo do Excel
    If ExcelSheet Is Nothing Then
        Set ExcelWb = ExcelApp.Workbooks.Open(sNomeArq)
    Else
        Set ExcelWb = Nothing
        Set ExcelWb = ExcelApp.Workbooks.Open(sNomeArq)
    End If

    Set ExcelSheet = ExcelWb.ActiveSheet
    
    If pesqPlanilha Then
        If Verifica_Lista_KIT(cmbCiclo.ItemData(cmbCiclo.ListIndex)) Then
            If importaPlan Then
                Importar = True
            End If
        End If
    End If
    
    
    ExcelWb.Save
    ExcelWb.Close
    'N�o utilizar comando ExcelWb.Close por que for�a o Excel perguntar se deseja salvar
    'a altera��o no objeto Excel (por causa da coluna Ponto (6)

    Set ExcelWb = Nothing
    Set ExcelApp = Nothing
    
    Exit Function
    
ErroImportar:

    Screen.MousePointer = vbNormal
    
    MsgBox "Ocorreu o seguinte erro ao importar: " & Err.Number & " - " & Err.Description
    
    
End Function

Private Function pesqPlanilha() As Boolean

Dim lCiclo          As Long
Dim X               As Long
Dim Y               As Long
Dim Z               As Long
Dim lPrimProd       As Long
Dim lUltProd        As Long
Dim cdProd          As Long
Dim bAchou          As Boolean
Dim sCodPai         As String
Dim lContPtoPai     As Long
Dim lContPtoFilho   As Long
Dim lQuantKit       As Long
Dim avData()        As Variant
Dim LinhasEmBranco  As Double
Dim iRedutorPai     As Integer
Dim v_lista As String
Dim a As Integer
Dim v_ultima_lista As Integer
Dim s As Integer


    pesqPlanilha = False
    
    Screen.MousePointer = vbHourglass
    lblFuncao.Caption = "Aguarde, consistindo a planilha excel..."
    lblFuncao.Visible = True
    pgbImporta.Visible = True
    frmErros.Visible = False
    spdErros.MaxRows = 0
    Me.Refresh
    
    On Error GoTo trata_erro
        
    lCiclo = Val(Left(cmbCiclo.Text, 2) & Right(cmbCiclo.Text, 4))
    
    ReDim avData(0, 0)
    
    If ExcelSheet.Cells(2, 1).Value <> lCiclo Then
        MsgBox "Ciclo/ano selecionado diferente do ciclo/ano da planilha de pre�os!  Planilha n�o ser� importada!", vbCritical, Me.Caption
        Exit Function
    End If
    
    X = 1
    
    lCiclo = Val(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2))
    ' Altera��o feita por Anderson Silva - 14/08/2009
    ' aqui a quantidade de listas estava hardcode
    ' vamos fazer um processo para verificar quantas listas tem
    
    a = 10
    v_qtd_listas = 0
    v_lista = "PDR"
    
    While v_lista <> ""
        If Trim(ExcelSheet.Cells(1, a).Value) = "" Then
            v_lista = ""
        Else
            a = a + 1
            v_qtd_listas = v_qtd_listas + 1
        End If
    Wend
    ' fim
    
    lQuantListas = 30
            
    LinhasEmBranco = 0
    lNumLin = 0
    
    For X = 1 To ExcelSheet.Rows.Count
        
        If UCase(Trim(ExcelSheet.Cells(X, 1))) <> "" Or _
             UCase(Trim(ExcelSheet.Cells(X, 2))) <> "" Or _
             UCase(Trim(ExcelSheet.Cells(X, 3))) <> "" Or _
             UCase(Trim(ExcelSheet.Cells(X, 9))) <> "" Then
             
            lNumLin = lNumLin + 1
        Else
            LinhasEmBranco = LinhasEmBranco + 1
        End If
        
        If LinhasEmBranco > 10 Then
            Exit For
        End If
        
    Next X
    
    pgbImporta.Max = lNumLin
    pgbImporta.Value = 0
    
    For X = 2 To lNumLin
    
        pgbImporta.Value = X
        
        ' ***************** verfiica se existe listas que n�o s�o do ciclo ***************************
        sCodPai = Trim(ExcelSheet.Cells(X, 2).Value)
        
        v_ultima_lista = Prox_Cod_Lista(lCiclo) + 1
        If v_qtd_listas >= v_ultima_lista Then
            For s = v_ultima_lista To v_qtd_listas
                If Trim(ExcelSheet.Cells(X, 9 + s).Value) <> "" Then
                    With spdErros
                        .MaxRows = .MaxRows + 1
                        .SetText 1, .MaxRows, X
                        .SetText 2, .MaxRows, sCodPai
                        .SetText 3, .MaxRows, "O Codigo possui listas preecnhidas no arquivo, sendo que n�o existe a lista na base"
                    End With
                    GoTo proximaLinha
                End If
            Next s
        End If
        
        ' ***************** verfiica se existe pre�o em listas de pre�o Brasil  = S ***************************
        If UCase(Trim(ExcelSheet.Cells(X, 8).Value)) = "S" Then
        
            sCodPai = Trim(ExcelSheet.Cells(X, 2).Value)
        
            For s = 1 To v_qtd_listas
                If Trim(ExcelSheet.Cells(X, 9 + s).Value) <> "" Then
                    With spdErros
                        .MaxRows = .MaxRows + 1
                        .SetText 1, .MaxRows, X
                        .SetText 2, .MaxRows, sCodPai
                        .SetText 3, .MaxRows, "O Codigo possui pre�o Brasil como S e possui listas com pre�os preenchidos"
                    End With
                GoTo proximaLinha
                End If
            Next s
        End If
        ' ******************************** FIM
        
        
        If Trim(ExcelSheet.Cells(X, 1).Value) = "" Or _
           Trim(ExcelSheet.Cells(X, 2).Value) = "" Or _
           Trim(ExcelSheet.Cells(X, 3).Value) = "" Or _
           Trim(ExcelSheet.Cells(X, 9).Value) = "" Then
            With spdErros
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, X
                .SetText 2, .MaxRows, sCodPai
                .SetText 3, .MaxRows, "Obrigatorio preenchimento das colunas 1, 2, 3 e 9"
            End With
            GoTo proximaLinha
        End If
        
        If Trim(ExcelSheet.Cells(X, 4).Value) = "" Then
            If Trim(ExcelSheet.Cells(X, 5).Value) = "" Or _
                Trim(ExcelSheet.Cells(X, 7).Value) = "" Or _
                Trim(ExcelSheet.Cells(X, 8).Value) = "" Then
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, sCodPai
                    .SetText 3, .MaxRows, "Obrigatorio preenchimento das colunas 5 a 8 para produtos elementares ou pais de kit"
                End With
                GoTo proximaLinha
            End If
        End If
        
        If Not IsNumeric(ExcelSheet.Cells(X, 1).Value) Or _
            Not IsNumeric(ExcelSheet.Cells(X, 2).Value) Or _
            Not IsNumeric(ConvertePontoDecimal(ExcelSheet.Cells(X, 9).Value, 1)) Then
            With spdErros
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, X
                .SetText 2, .MaxRows, sCodPai
                .SetText 3, .MaxRows, "Colunas 1, 2 e/ou 9 inv�lidas - Devem ser num�ricas!"
            End With
            GoTo proximaLinha
        End If
        
        If ExcelSheet.Cells(X, 5).Value < 0 Or _
           ExcelSheet.Cells(X, 6).Value < 0 Or _
           ExcelSheet.Cells(X, 9).Value < 0 Then
            
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, sCodPai
                    .SetText 3, .MaxRows, "Colunas 5, 6 e/ou 9 inv�lidas - N�o pode conter valor negativo!"
                End With
                GoTo proximaLinha
        End If
        
        If Trim(ExcelSheet.Cells(X, 4).Value) <> "" Then
            If Not IsNumeric(ExcelSheet.Cells(X, 4).Value) Then
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, sCodPai
                    .SetText 3, .MaxRows, "Coluna 4 inv�lida, quando preenchida deve ser num�rica!"
                End With
                GoTo proximaLinha
            End If
        Else
            If Not IsNumeric(ExcelSheet.Cells(X, 5).Value) Then
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, sCodPai
                    .SetText 3, .MaxRows, "Coluna 5 inv�lida - deve ser num�rica!"
                End With
                GoTo proximaLinha
            End If
        End If
        
        If Trim(ExcelSheet.Cells(X, 6).Value) <> "" Then
            If Not IsNumeric(ExcelSheet.Cells(X, 6).Value) Then
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, sCodPai
                    .SetText 3, .MaxRows, "Coluna 6 inv�lida, quando preenchida deve ser num�rica!"
                End With
                GoTo proximaLinha
            End If
        End If
        
        For Y = 10 To (9 + lQuantListas)
            If Trim(ExcelSheet.Cells(X, Y).Value) <> "" Then
                If Not IsNumeric(ConvertePontoDecimal(ExcelSheet.Cells(X, Y).Value, 1)) Then
                    With spdErros
                        .MaxRows = .MaxRows + 1
                        .SetText 1, .MaxRows, X
                        .SetText 2, .MaxRows, sCodPai
                        .SetText 3, .MaxRows, "Coluna " & Y & " inv�lida, quando preenchida deve ser num�rica!"
                    End With
                    GoTo proximaLinha
                End If
            End If
        Next Y
        
        If Val(Trim(ExcelSheet.Cells(X, 5).Value)) > 100 Then
            With spdErros
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, X
                .SetText 2, .MaxRows, sCodPai
                .SetText 3, .MaxRows, "O Redutor de pontos n�o pode ser maior que 100%"
            End With
            GoTo proximaLinha
        End If
        
        If UCase(Trim(ExcelSheet.Cells(X, 7).Value)) <> "REGULAR VENDA" And _
           UCase(Trim(ExcelSheet.Cells(X, 7).Value)) <> "MATERIAL DE APOIO" And _
           UCase(Trim(ExcelSheet.Cells(X, 7).Value)) <> "SALD�O" And _
           UCase(Trim(ExcelSheet.Cells(X, 7).Value)) <> "PRODUTO SEM PRE�O" And _
           UCase(Trim(ExcelSheet.Cells(X, 7).Value)) <> "LAN�AMENTO FUTURO" And _
           UCase(Trim(ExcelSheet.Cells(X, 7).Value)) <> "DESCONTINUADO" And Trim(ExcelSheet.Cells(X, 7).Value) <> "" Then
            With spdErros
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, X
                .SetText 2, .MaxRows, sCodPai
                .SetText 3, .MaxRows, "Status diferente das descri��es permitidas"
            End With
            GoTo proximaLinha
        End If
        
        If UCase(Trim(ExcelSheet.Cells(X, 8).Value)) <> "S" And _
            UCase(Trim(ExcelSheet.Cells(X, 8).Value)) <> "N" And Trim(ExcelSheet.Cells(X, 8).Value) <> "" Then
            With spdErros
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, X
                .SetText 2, .MaxRows, sCodPai
                .SetText 3, .MaxRows, "Pre�o Brasil diferente de S e N"
            End With
            GoTo proximaLinha
        End If
        
        If Trim(ExcelSheet.Cells(X, 4).Value) = "" Then
            cSql = "declare @cd_produto int "
            cSql = cSql & "exec stp_pdr_cons_cod_venda_s " & sCodPai & ", " & lCiclo
            cSql = cSql & ", @cd_produto out"
            
            If gObjeto.natPrepareQuery(hSql, cSql) Then
              Do While gObjeto.natFetchNext(hSql)
              Loop
            End If
        
            cdProd = CLng(gObjeto.varOutputParam(0))
    
            If cdProd = 99999999 Then
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, sCodPai
                    .SetText 3, .MaxRows, "O produto n�o existe ou n�o est� vigente no ciclo informado"
                End With
                GoTo proximaLinha
            End If
        
            If Trim(ExcelSheet.Cells(X, 6).Value) = "" Then
                ExcelSheet.Cells(X, 6).Value = fCalculaPontos(CCur(ExcelSheet.Cells(X, 9).Value), _
                                                              CCur(IIf(Trim(ExcelSheet.Cells(X, 5).Value) = "", "0,00", Trim(ExcelSheet.Cells(X, 5).Value))))
            End If
            iRedutorPai = ExcelSheet.Cells(X, 5).Value
        
        Else
            If Val(Trim(ExcelSheet.Cells(X, 4).Value)) = Val(Trim(ExcelSheet.Cells(X - 1, 2).Value)) Then
                
                sCodPai = Trim(ExcelSheet.Cells(X, 4).Value)
                lContPtoPai = Trim(ExcelSheet.Cells(X - 1, 6).Value)
        
                lContPtoFilho = 0
                lPrimProd = X
                Y = lPrimProd
                Do While Val(Trim(ExcelSheet.Cells(Y, 4).Value)) = sCodPai
                    Y = Y + 1
                Loop
                
                lUltProd = Y - 1
                
                ReDim avData(0, 0)
                
                cSql = "exec stp_pdr_lista_kit_s " & sCodPai & ", " & lCiclo
                
                bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())
                 
                If gObjeto.nErroSQL <> 0 Then
                    MsgBox gObjeto.nServerNum & " " & gObjeto.sServerMsg, vbOKOnly + vbCritical, "Erro"
                    Exit Function
                End If
                
                Y = lPrimProd
                If UBound(avData, 2) <> (lUltProd - lPrimProd) Then
                    With spdErros
                        .MaxRows = .MaxRows + 1
                        .SetText 1, .MaxRows, lPrimProd - 1
                        .SetText 2, .MaxRows, sCodPai
                        .SetText 3, .MaxRows, "Lista t�cnica da planilha � diferente da lista t�cnica cadastrada no sistema"
                    End With
                    GoTo proximaLinha
                End If
                
                Do While Y <= lUltProd
                    bAchou = False
                    For Z = 0 To UBound(avData, 2)
                        If Val(Trim(ExcelSheet.Cells(Y, 2).Value)) = avData(0, Z) Then
                            bAchou = True
                            lQuantKit = avData(1, Z)
                            Exit For
                        End If
                    Next Z
                    If Not bAchou Then
                        With spdErros
                            .MaxRows = .MaxRows + 1
                            .SetText 1, .MaxRows, lPrimProd - 1
                            .SetText 2, .MaxRows, sCodPai
                            .SetText 3, .MaxRows, "Lista t�cnica da planilha � diferente da lista t�cnica cadastrada no sistema"
                        End With
                        GoTo proximaLinha
                    End If
                    If Trim(ExcelSheet.Cells(Y, 6).Value) = "" Then
'                        ExcelSheet.Cells(Y, 6).Value = fCalculaPontos(CCur(ExcelSheet.Cells(Y, 9).Value), _
                                                                      CCur(IIf(Trim(ExcelSheet.Cells(Y, 5).Value) = "", "0,00", Trim(ExcelSheet.Cells(Y, 5).Value))))
                        ExcelSheet.Cells(Y, 6).Value = fCalculaPontos(CCur(ExcelSheet.Cells(Y, 9).Value), CCur(iRedutorPai))
                    End If
               '     lContPtoFilho = lContPtoFilho + (Trim(ExcelSheet.Cells(Y, 6).Value) * 1)
                    lContPtoFilho = lContPtoFilho + (Trim(ExcelSheet.Cells(Y, 6).Value) * lQuantKit)
                    Y = Y + 1
                Loop
                If lContPtoFilho <> lContPtoPai Then
                    With spdErros
                        .MaxRows = .MaxRows + 1
                        .SetText 1, .MaxRows, lPrimProd - 1
                        .SetText 2, .MaxRows, sCodPai
                        .SetText 3, .MaxRows, "A soma dos pontos dos filhos do kit � diferente dos pontos do pai"
                    End With
                End If
                X = lUltProd
            End If
        End If
        
proximaLinha:

    Next X
    
    Screen.MousePointer = vbNormal
            
    If spdErros.MaxRows = 0 Then
        pesqPlanilha = True
        frmErros.Visible = False
    Else
        frmErros.Visible = True
        MsgBox "A planilha cont�m erros, por isto ela n�o ser� importada!", vbCritical, Me.Caption
    End If
    
    Exit Function
    
trata_erro:

    Screen.MousePointer = vbNormal
    MsgBox "Ocorreu o seguinte erro ao importar: " & Err.Number & " - " & Err.Description


End Function

Private Function fCalculaPontos(pPreco As String, pRedutor As String) As Integer

Dim cValor As Currency
Dim lCiclo As Long
    
    cValor = Format(pPreco * (IIf(pRedutor = 0, 100, pRedutor) / 100), "0.00")
    lCiclo = Right(cmbCiclo.Text, 4) + Left(cmbCiclo.Text, 2)
    
    MSG$ = ""
    fCalculaPontos = FU_BuscaPontuacao(CStr(lCiclo), Str(cValor), MSG$)
    If MSG$ <> "" Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        PonteiroMouse vbNormal
        Exit Function
    End If

End Function

Private Function importaPlan() As Boolean
    Dim vAtualizaCicloFuturo As Boolean
    Dim v_ultima_lista As Long
        
    importaPlan = False
    
    vAtualizaCicloFuturo = False
    
    'lCiclo = Val(Left(cmbCiclo.Text, 2) & Right(cmbCiclo.Text, 4))
    
    v_ultima_lista = Prox_Cod_Lista(cmbCiclo.ItemData(cmbCiclo.ListIndex))
    
    If FU_BuscaPrecoFuturoSug(cmbCiclo.ItemData(cmbCiclo.ListIndex), v_ultima_lista, MSG$) = FAIL% Then
         MsgBox MSG$, vbCritical, "A T E N � � O"
         PonteiroMouse vbNormal
    Else
        If QtdPrecoFuturo = 999 Then
            If importaCiclo(cmbCiclo.ItemData(cmbCiclo.ListIndex), "C") Then
            End If
            importaPlan = True
        Else
            If QtdPrecoFuturo > 0 Then
                If MsgBox("Pre�o refer�ncia gerado para ciclos futuros ! Deseja replicar as altera��es para estes ciclos ? ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
                    'If MsgBox("Existem pre�os gerados para ciclos maiores que o ciclo informado!!" & Chr(10) & Chr(13) & _
                    '      "Deseja replicar estas altera��es para todos os ciclos j� gerados? " & Chr(10) & Chr(13) & _
                    '      "Escolha SIM para fazer as altera��es para todos os ciclos, " & Chr(10) & Chr(13) & _
                    '        "        N�O para fazer somente para o ciclo escolhido!", vbYesNo + vbInformation, Me.Caption) = vbYes Then
                
                    If importaCiclo(cmbCiclo.ItemData(cmbCiclo.ListIndex), "T") Then
                    End If
                    importaPlan = True
                Else
                    If importaCiclo(cmbCiclo.ItemData(cmbCiclo.ListIndex), "C") Then
                    End If
                    importaPlan = True
                End If
            Else
                MsgBox "Pre�o refer�ncia gerado para ciclos futuros. Quantidade de listas nos ciclos futuros diferente do ciclo " & cmbCiclo.ItemData(cmbCiclo.ListIndex) & ". As altera��es n�o poder�o ser replicadas para estes ciclos", vbInformation, "A T E N � � O"
                If importaCiclo(cmbCiclo.ItemData(cmbCiclo.ListIndex), "C") Then
                End If
                importaPlan = True
            End If
        End If
    End If
    
    
    'If cmbCiclo.ListIndex > 1 Then
    
    'If FU_BuscaPrecoFuturoSug(Right(cmbCicloL, 4) + Left(cmbCicloL, 2), v_ultima_lista, MSG$) = FAIL% Then
    '     MsgBox MSG$, vbCritical, "A T E N � � O"
    '     PonteiroMouse vbNormal
    'Else
    '     If QtdPrecoFuturo <> 999 Then
    '        If QtdPrecoFuturo > 0 Then
    '            If MsgBox("Pre�o refer�ncia gerado para ciclos futuros ! Deseja replicar as altera��es para estes ciclos ? ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
    '                vAtualizaCicloFuturo = True
    '            End If
    '        Else
    '            MsgBox "Pre�o refer�ncia gerado para ciclos futuros. Quantidade de listas nos ciclos futuros diferente do ciclo " & Right(cmbCicloL, 4) + Left(cmbCicloL, 2) & ". As altera��es n�o poder�o ser replicadas para estes ciclos", vbInformation, "A T E N � � O"
    '            vAtualizaCicloFuturo = False
    '        End If
    '     End If
    'End If
    

End Function
    
Private Function FU_BuscaPrecoFuturoSug(nm_ciclo_operacional As Long, cd_lista As Long, MSG$) As Integer

Dim sql As String

    FU_BuscaPrecoFuturoSug = FAIL%

    On Error GoTo FU_BuscaPrecoFuturoSug

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_futuro_lista_s " & nm_ciclo_operacional & "," & cd_lista & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0
    QtdPrecoFuturo = 999

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            If gObjeto.varResultSet(hSql, 0) = cd_lista Then
                QtdPrecoFuturo = QtdPrecoFuturo + 1
            Else
                QtdPrecoFuturo = 0
                Exit Do
            End If
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_futuro_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    FU_BuscaPrecoFuturoSug = SUCCED%
    Exit Function

FU_BuscaPrecoFuturoSug:
    MSG$ = "FU_BuscaPrecoFuturoSug - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function importaCiclo(lCiclo As Long, ByVal sTipoAtu As String) As Boolean
    Dim X               As Long
    Dim Y               As Long
    Dim Z               As Long
    Dim W               As Long
    Dim lPrimProd       As Long
    Dim lUltProd        As Long

    Dim lCodvda         As Long
    Dim lCodPai         As Long
    Dim sRedutor        As Long
    Dim v_redutor_tabela  As String
    Dim sPreco          As Single
    Dim iPontos         As Integer
    Dim iStatus         As Integer
    Dim lAplica         As Long
    Dim sUsuario        As String

    Dim sExcecao        As String
    Dim cPrecoExc       As Currency
    Dim cPercExc        As Currency
    Dim sPercExc        As String
    Dim iListaExc       As Integer
    Dim lCicloLocal     As Long

    Dim sValorLista As Single
    Dim lCodLista As Integer
    Dim v_preco_brasil As String
    Dim v_redutor_planilha As Long
    Dim v_situacao_venda As Integer


    Screen.MousePointer = vbHourglass
    lblFuncao.Caption = "Aguarde, importando os pre�os da planilha excel..."
    lblFuncao.Visible = True
    pgbImporta.Value = 0
    pgbImporta.Visible = True
    frmErros.Visible = False
    spdErros.MaxRows = 0
    Me.Refresh
    
    sUsuario = UCase(Environ("_U"))

    On Error GoTo trata_erro
        
    ReDim avData(0, 0)
    
    For X = 2 To lNumLin
    
        pgbImporta.Value = X
        
        lCodvda = CLng(Trim(ExcelSheet.Cells(X, 2).Value))
        lCodPai = CLng(Val(Trim(ExcelSheet.Cells(X, 4).Value)))
        sRedutor = CLng(Val(Trim(ExcelSheet.Cells(X, 5).Value)))
        iPontos = CInt(Val(Trim(ExcelSheet.Cells(X, 6).Value)))
        iStatus = fObtemStatus(Trim(ExcelSheet.Cells(X, 7).Value))
        lAplica = CLng(IIf(Trim(ExcelSheet.Cells(X, 8).Value) = "S", 1, 0))
        sPreco = Trim(ExcelSheet.Cells(X, 9).Value)
        
        cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
        cSql = cSql & " exec stp_pdr_atu_pr_ref_m " & lCiclo & ", " & lCodvda & ", "
        cSql = cSql & lCodPai & ", " & sRedutor & ", " & iPontos & ", " & iStatus & ", "
        cSql = cSql & lAplica & ", " & ConvertePontoDecimal(CStr(sPreco), 1) & ", '" & sTipoAtu & "', '" & sUsuario & "', "
        cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
          
        If gObjeto.natPrepareQuery(hSql, cSql) Then
           Do While gObjeto.natFetchNext(hSql)
           Loop
        End If
          
        If gObjeto.nErroSQL <> 0 Then
           MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
           Exit Function
        End If
        
        sql_cd_ret = CInt(gObjeto.varOutputParam(0))
        sql_cd_opr = gObjeto.varOutputParam(1)
        sql_nm_tab = gObjeto.varOutputParam(2)
         
        If sql_cd_ret <> 0 Then
           MsgBox "Erro ao executar a stp_pdr_atu_pr_ref_m, " & "Erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
           Exit Function
        End If
        
        'For Y = 10 To (9 + lQuantListas)
        For Y = 10 To (9 + v_qtd_listas)
        
            If Trim(ExcelSheet.Cells(X, Y).Value) <> "" Then
                    
                cPrecoExc = CCur(Trim(ExcelSheet.Cells(X, Y).Value))
                cPercExc = ((cPrecoExc / CCur(CStr(sPreco))) - 1) * 100
                sPercExc = Format(cPercExc, "#0.00")
                
                iListaExc = Y - 9
                                
                If sTipoAtu = "C" Then
                    For Z = 1 To 4
                        cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
                        cSql = cSql & " exec st_pi_reajuste_lista_m " & lCiclo & ", " & iListaExc & ", "
                        cSql = cSql & Z & ", " & lCodvda & ", " & ConvertePontoDecimal(CStr(sPercExc), 1) & ", '" & sUsuario & "', "
                        cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
                      
                        If gObjeto.natPrepareQuery(hSql, cSql) Then
                           Do While gObjeto.natFetchNext(hSql)
                           Loop
                        End If
                          
                        If gObjeto.nErroSQL <> 0 Then
                           MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                           Exit Function
                        End If
                        
                        sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                        sql_cd_opr = gObjeto.varOutputParam(1)
                        sql_nm_tab = gObjeto.varOutputParam(2)
                         
                        If sql_cd_ret <> 0 Then
                           MsgBox "Erro ao executar a st_pi_reajuste_lista_m, " & "Erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
                           Exit Function
                        End If
                    Next Z
                Else
                    For W = cmbCiclo.ListIndex To 1 Step -1
                    
                        lCicloLocal = Right(cmbCiclo.List(W), 4) & Left(cmbCiclo.List(W), 2)
                        For Z = 1 To 4
                            cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
                            cSql = cSql & " exec st_pi_reajuste_lista_m " & lCicloLocal & ", " & iListaExc & ", "
                            cSql = cSql & Z & ", " & lCodvda & ", " & ConvertePontoDecimal(CStr(sPercExc), 1) & ", '" & sUsuario & "', "
                            cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
                          
                            If gObjeto.natPrepareQuery(hSql, cSql) Then
                               Do While gObjeto.natFetchNext(hSql)
                               Loop
                            End If
                              
                            If gObjeto.nErroSQL <> 0 Then
                               MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                               Exit Function
                            End If
                            
                            sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                            sql_cd_opr = gObjeto.varOutputParam(1)
                            sql_nm_tab = gObjeto.varOutputParam(2)
                             
                            If sql_cd_ret <> 0 Then
                                If sql_cd_ret = 546 Then
                                Else
                                    MsgBox "Erro ao executar a st_pi_reajuste_lista_m, " & "Erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
                                    Exit Function
                               End If
                            End If
                        Next Z
                    Next W
                    
                End If
            End If
        Next Y
        
    Next X
    
    ReDim avData(0, 0)
    Screen.MousePointer = vbHourglass
    lblFuncao.Caption = "Aguarde, gerando listas dos pre�os alterados..."
    lblFuncao.Visible = True
    pgbImporta.Value = 0
    pgbImporta.Visible = True
    frmErros.Visible = False
    spdErros.MaxRows = 0
    Me.Refresh
    
       
    ' ALTERA��O FEITA POR ANDERSON - CADMUS - 15/08/2009
    ' AQUI VAMOS GRAVAR O VALOR DAS LISTAS NAS TABELAS T_PI_PRECO_CN E
    ' T_PI_PRECO_CN_KIT
    For X = 2 To lNumLin
        For Y = 1 To v_qtd_listas
            pgbImporta.Value = X
            
            If Trim(ExcelSheet.Cells(X, 4).Value) = "" Then
                v_preco_brasil = Trim(ExcelSheet.Cells(X, 8).Value)
                v_redutor_planilha = CLng(Val(Trim(ExcelSheet.Cells(X, 5).Value)))
                v_situacao_venda = Verifica_Situacao_Venda(lCiclo, CLng(Trim(ExcelSheet.Cells(X, 2).Value)), Y)
                v_redutor_tabela = Verifica_Redutor_Tabela(lCiclo, CLng(Trim(ExcelSheet.Cells(X, 2).Value)), Y)
            End If
            
            'If Trim(ExcelSheet.Cells(X, 8).Value) = "S" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
            If v_preco_brasil = "S" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
            
                lCodvda = CLng(Trim(ExcelSheet.Cells(X, 2).Value))
                lCodPai = CLng(Val(Trim(ExcelSheet.Cells(X, 4).Value)))
                iPontos = CLng(Val(Trim(ExcelSheet.Cells(X, 6).Value)))
                'iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                sPreco = Trim(ExcelSheet.Cells(X, 9).Value)
                sValorLista = Trim(ExcelSheet.Cells(X, 9).Value)
                lCodLista = Y
        
                cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
                cSql = cSql & " exec stp_pdr_atu_pr_lista_u " & v_redutor_planilha & ", " & lCiclo & ", " & lCodvda & ", "
                cSql = cSql & lCodPai & ", " & iPontos & ", " & ConvertePontoDecimal(CStr(sValorLista), 1) & ", " & lCodLista & ", '" & sTipoAtu & "', '" & sUsuario & "', "
                cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
          
                If gObjeto.natPrepareQuery(hSql, cSql) Then
                    Do While gObjeto.natFetchNext(hSql)
                    Loop
                End If
          
                If gObjeto.nErroSQL <> 0 Then
                    MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                    Exit Function
                End If
        
                sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                sql_cd_opr = gObjeto.varOutputParam(1)
                sql_nm_tab = gObjeto.varOutputParam(2)
         
                If sql_cd_ret <> 0 Then
                    MsgBox "Erro ao executar a stp_pdr_atu_pr_lista_u, " & "Erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
                    Exit Function
                End If
            
            'ElseIf Trim(ExcelSheet.Cells(X, 8).Value) = "N" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) <> "" And IsNumeric(ExcelSheet.Cells(X, 9 + Y).Value) = True Then
            ElseIf v_preco_brasil = "N" Then
                    lCodvda = CLng(Trim(ExcelSheet.Cells(X, 2).Value))
                    lCodPai = CLng(Val(Trim(ExcelSheet.Cells(X, 4).Value)))
                    sPreco = Trim(ExcelSheet.Cells(X, 9).Value)
                    
                    ' CASO TENHA A LISTA NO CICLO INFORMADO, MAS O PRE�O LISTA ESTA VAZIO, VAMOS ALTERAR
                    ' COM O PRE�O REFERENCIA
                    
                    'Verifica a situa��o de venda do produto no ciclo
                    'v_situacao_venda = Verifica_Situacao_Venda(lCiclo, lCodvda, Y)
                    
                    If Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
                        sValorLista = Trim(ExcelSheet.Cells(X, 9).Value)
                        
                        If v_situacao_venda = 0 Then
                            ' calcula os pontos pelo redutor da planilha
                            iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                            iPontos = Format(iPontos * (v_redutor_planilha / 100), "#0")
                            
                            lCodLista = Y
        
                            cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
                            cSql = cSql & " exec stp_pdr_atu_pr_lista_u " & v_redutor_planilha & ", " & lCiclo & ", " & lCodvda & ", "
                            cSql = cSql & lCodPai & ", " & iPontos & ", " & ConvertePontoDecimal(CStr(sValorLista), 1) & ", " & lCodLista & ", '" & sTipoAtu & "', '" & sUsuario & "', "
                            cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
                            
                        Else
                            ' calcula os pontos usando o redutor da tabela t_pi_preco_cn
                            'v_redutor_tabela = Verifica_Redutor_Tabela(lCiclo, lCodvda, Y)
                            iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                            If v_redutor_tabela = "" Then
                                v_redutor_tabela = "100"
                            End If
                            iPontos = Format(iPontos * (ConvertePontoDecimal(v_redutor_tabela, 0) / 100), "#0")
                            
                            
                            lCodLista = Y
        
                            cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
                            cSql = cSql & " exec stp_pdr_atu_pr_lista_u " & v_redutor_tabela & ", " & lCiclo & ", " & lCodvda & ", "
                            cSql = cSql & lCodPai & ", " & iPontos & ", " & ConvertePontoDecimal(CStr(sValorLista), 1) & ", " & lCodLista & ", '" & sTipoAtu & "', '" & sUsuario & "', "
                            cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
                        End If
                        
                    Else
                        sValorLista = Trim(ExcelSheet.Cells(X, 9 + Y).Value)
                        'v_redutor_tabela = Verifica_Redutor_Tabela(lCiclo, lCodvda, Y)
                        iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9 + Y).Value)), MSG$), "#0")
                        If v_redutor_tabela = "" Then
                            v_redutor_tabela = "100"
                        End If
                        
                        iPontos = Format(iPontos * (ConvertePontoDecimal(v_redutor_tabela, 0) / 100), "#0")
                        
                        
                        lCodLista = Y
        
                        cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
                        cSql = cSql & " exec stp_pdr_atu_pr_lista_u " & v_redutor_tabela & ", " & lCiclo & ", " & lCodvda & ", "
                        cSql = cSql & lCodPai & ", " & iPontos & ", " & ConvertePontoDecimal(CStr(sValorLista), 1) & ", " & lCodLista & ", '" & sTipoAtu & "', '" & sUsuario & "', "
                        cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
                        
                    End If
                    ' FIM

                
                    If gObjeto.natPrepareQuery(hSql, cSql) Then
                        Do While gObjeto.natFetchNext(hSql)
                        Loop
                    End If
          
                    If gObjeto.nErroSQL <> 0 Then
                        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
                        Exit Function
                    End If
        
                    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                    sql_cd_opr = gObjeto.varOutputParam(1)
                    sql_nm_tab = gObjeto.varOutputParam(2)
         
                    If sql_cd_ret <> 0 Then
                        MsgBox "Erro ao executar a stp_pdr_atu_pr_lista_u, " & "Erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
                        Exit Function
                    End If
                End If
                
            
        Next Y
    Next X
    
    ReDim avData(0, 0)
    Screen.MousePointer = vbHourglass
    lblFuncao.Caption = "Aguarde, gerando listas dos pre�os alterados..."
    lblFuncao.Visible = True
    pgbImporta.Value = 0
    pgbImporta.Visible = True
    frmErros.Visible = False
    spdErros.MaxRows = 0
    Me.Refresh
    
    ' FIM
    
    
    
    
    
    
    'For X = 2 To lNumLin
   '
   '     pgbImporta.Value = X
   '     lCodPai = CLng(Val(Trim(ExcelSheet.Cells(X, 4).Value)))
   '
   '     If lCodPai = 0 Then
   '         lCodvda = CLng(Trim(ExcelSheet.Cells(X, 2).Value))
   '         sRedutor = CLng(Val(Trim(ExcelSheet.Cells(X, 5).Value)))
   '         iPontos = CInt(Val(Trim(ExcelSheet.Cells(X, 6).Value)))
   '         iStatus = fObtemStatus(Trim(ExcelSheet.Cells(X, 7).Value))
   '         lAplica = CLng(IIf(Trim(ExcelSheet.Cells(X, 8).Value) = "S", 1, 0))
   '         sPreco = Trim(ExcelSheet.Cells(X, 9).Value)
   '
   ''         If sTipoAtu = "C" Then
   '             cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
   '             cSql = cSql & " exec stp_pdr_atu_pr_lista_m " & lCiclo & ", " & lCodvda & ", "
   '             cSql = cSql & sRedutor & ", " & iPontos & ", " & iStatus & ", "
   '             cSql = cSql & lAplica & ", " & ConvertePontoDecimal(CStr(sPreco), 1) & ", '" & sTipoAtu & "', '" & sUsuario & "', "
   '             cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
   '
   '             If gObjeto.natPrepareQuery(hSql, cSql) Then
   '                Do While gObjeto.natFetchNext(hSql)
   '                Loop
   '             End If
   '
   '             If gObjeto.nErroSQL <> 0 Then
   '                MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
   '                Exit Function
   '             End If
   '
                'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                'sql_cd_opr = gObjeto.varOutputParam(1)
                'sql_nm_tab = gObjeto.varOutputParam(2)
                     
    '            If sql_cd_ret <> 0 Then
    '               MsgBox "Erro ao executar a stp_pdr_atu_pr_lista_m, " & "erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
    '               Exit Function
    '            End If
    '        Else
    '            For W = cmbCiclo.ListIndex To 1 Step -1
    '
    '                lCicloLocal = Right(cmbCiclo.List(W), 4) & Left(cmbCiclo.List(W), 2)
    '                cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    '                cSql = cSql & " exec stp_pdr_atu_pr_lista_m " & lCicloLocal & ", " & lCodvda & ", "
    '                cSql = cSql & sRedutor & ", " & iPontos & ", " & iStatus & ", "
    '                cSql = cSql & lAplica & ", " & ConvertePontoDecimal(CStr(sPreco), 1) & ", '" & sTipoAtu & "', '" & sUsuario & "', "
    '                cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    '
    '                If gObjeto.natPrepareQuery(hSql, cSql) Then
     '                  Do While gObjeto.natFetchNext(hSql)
    '                   Loop
     '               End If
     '
     '               If gObjeto.nErroSQL <> 0 Then
    '                   MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
     '                  Exit Function
     ''               End If
     '
     '               sql_cd_ret = CInt(gObjeto.varOutputParam(0))
     '               sql_cd_opr = gObjeto.varOutputParam(1)
     '               sql_nm_tab = gObjeto.varOutputParam(2)
     '   '            sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
      ''
     '               If sql_cd_ret <> 0 Then
     '                  MsgBox "Erro ao executar a stp_pdr_atu_pr_lista_m, " & "erro: " & sql_cd_ret & " - " & gObjeto.sServerMsg
     ''                  Exit Function
      '              End If
      '          Next W
      '      End If
      '  End If
   ' Next X
    

    
    
    Screen.MousePointer = vbNormal
            
    importaCiclo = True
    
    Exit Function
    
trata_erro:

    Screen.MousePointer = vbNormal
    MsgBox "Ocorreu o seguinte erro ao importar: " & Err.Number & " - " & Err.Description

End Function

Private Function fObtemStatus(status As String) As Integer

    Select Case UCase(Trim(status))
        Case "REGULAR VENDA"
            fObtemStatus = 0
        Case "MATERIAL DE APOIO"
            fObtemStatus = 2
        Case "SALD�O"
            fObtemStatus = 3
        Case "PRODUTO SEM PRE�O"
            fObtemStatus = 4
        Case "LAN�AMENTO FUTURO"
            fObtemStatus = 5
        Case "DESCONTINUADO"
            fObtemStatus = 9
    End Select

End Function



Private Sub Form_Load()
    
    Call CarrCicloComPreco(cmbCiclo)
    
    Me.MousePointer = vbNormal
    Refresh
    

End Sub

Private Function Prox_Cod_Lista(v_ciclo As Long) As Integer

Dim cSql  As String
Dim bResp As Boolean
Dim i     As Integer
Dim avPrecos()       As Variant
Dim v_cod_lista1 As Integer


On Error GoTo erro

' VERIFICA O MAIOR CODIGO DE LISTA
cSql = "SELECT max(cd_lista) From t_pi_lista_ciclo Where nm_ciclo_operacional = " & v_ciclo & "  GROUP BY nm_ciclo_operacional"

ReDim avPrecos(0)
   
bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
If bResp = False Then
  MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
  End
End If

If gObjeto.nErroSQL <> 0 Then
  MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
  Exit Function
End If

Prox_Cod_Lista = avPrecos(0, i)

Exit Function

erro:
  MsgBox "Erro ao obter pr�ximo c�digo de lista" & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
  Exit Function
  
End Function

Private Sub Form_Unload(Cancel As Integer)
    
    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub txtCaminho_Change()
    If Len(Trim(txtCaminho.Text)) > 0 Then
        cmdImportar.Enabled = True
    Else
        cmdImportar.Enabled = False
    End If
End Sub

Public Function Verifica_Lista_KIT(lCiclo As Long) As Boolean

   Dim sCod            As String
   Dim sCodPai         As String
   Dim v_valor_pai     As Long
   Dim v_valor_filho   As Long
   Dim lPrimProd       As Long
   Dim Y As Long
   Dim X As Long
   Dim v_lista As Integer
   Dim v_pai As Integer
   Dim v_pontos_pai As Integer
   Dim v_pontos_filho As Integer
   
    Dim v_preco_brasil As String
    Dim v_redutor_planilha As Long
    Dim v_situacao_venda As Integer
    Dim v_redutor_tabela As String
    
    Dim iPontos As Long
    
    Dim v_e_pai As Boolean
    
    v_pontos_pai = 0
    v_pontos_filho = 0
   
   Verifica_Lista_KIT = False
   
   v_pai = 0
   
   ' primeiro teste - verifica se os kits possuem produto pai
    For X = 2 To lNumLin
        ' codigo pai
        If Trim(ExcelSheet.Cells(X, 4).Value) = "" Then
            sCod = Trim(ExcelSheet.Cells(X, 2).Value)
            v_pai = 0
        Else
            If Trim(ExcelSheet.Cells(X, 4).Value) <> sCod Then
                If v_pai = 0 Then
                    With spdErros
                        .MaxRows = .MaxRows + 1
                        .SetText 1, .MaxRows, X
                        .SetText 2, .MaxRows, Trim(ExcelSheet.Cells(X, 4).Value)
                        .SetText 3, .MaxRows, "C�digo Pai n�o encontrado no arquivo"
                        v_pai = 1
                    End With
                End If
                
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, X
                    .SetText 2, .MaxRows, Trim(ExcelSheet.Cells(X, 2).Value)
                    .SetText 3, .MaxRows, "Existe kit no arquivo sendo que n�o existe o c�digo pai"
                End With
            End If
        End If
    Next X
   
   
' segunda consistencia - valor dos filhos igual ao valor do pai
   For Y = 1 To v_qtd_listas
        v_pontos_pai = 0
        v_pontos_filho = 0
        
        For X = 2 To lNumLin
            If Trim(ExcelSheet.Cells(X, 9 + Y).Value) <> "" And IsNumeric(Trim(ExcelSheet.Cells(X, 9 + Y).Value)) = True Then
                lPrimProd = X
                ' verifica se o produto tem codigo pai
                If Trim(ExcelSheet.Cells(X, 4).Value) = "" Then
                    sCod = Trim(ExcelSheet.Cells(X, 2).Value)
                    v_valor_pai = Trim(ExcelSheet.Cells(X, 9 + Y).Value)
                    v_lista = 0
                Else
                    If Trim(ExcelSheet.Cells(X, 4).Value) = sCod Then
                        v_valor_filho = v_valor_filho + Trim(ExcelSheet.Cells(X, 9 + Y).Value)
                        v_lista = 1
                    Else
                        v_lista = 0
                    End If
                End If
            Else
                v_lista = 0
            End If
        Next X
        
        If v_lista = 1 Then
            If v_valor_filho <> v_valor_pai Then
                With spdErros
                    .MaxRows = .MaxRows + 1
                    .SetText 1, .MaxRows, lPrimProd - 1
                    .SetText 2, .MaxRows, sCod
                    .SetText 3, .MaxRows, "A soma do valor dos filhos do kit da lista " & Y & " � diferente do valor do pai"
                    GoTo proximaLinha
                End With
            Else
                v_valor_filho = 0
                v_valor_pai = 0
                
            End If
        End If
        
        For X = 2 To lNumLin
            ' � um pai ou produto elementar
            If Trim(ExcelSheet.Cells(X, 4).Value) = "" Then
                If v_pontos_filho <> 0 Then
                    ' verifica se os filhos s�o diferentes do pai
                    If v_pontos_pai <> v_pontos_filho Then
                        With spdErros
                            .MaxRows = .MaxRows + 1
                            .SetText 1, .MaxRows, X
                            .SetText 2, .MaxRows, Trim(ExcelSheet.Cells(X, 2).Value)
                            .SetText 3, .MaxRows, "A soma dos pontos dos filhos do kit da lista " & Y & " � diferente dos pontos do pai"
                            GoTo proximaLinha
                        End With
                    
                    End If
                End If
            
            
                v_pontos_pai = 0
                v_pontos_filho = 0
                v_preco_brasil = Trim(ExcelSheet.Cells(X, 8).Value)
                v_redutor_planilha = CLng(Val(Trim(ExcelSheet.Cells(X, 5).Value)))
                v_situacao_venda = Verifica_Situacao_Venda(lCiclo, CLng(Trim(ExcelSheet.Cells(X, 2).Value)), Y)
                v_redutor_tabela = Verifica_Redutor_Tabela(lCiclo, CLng(Trim(ExcelSheet.Cells(X, 2).Value)), Y)
                
                ' pegar pontos do pai
                If v_preco_brasil = "S" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
                        v_pontos_pai = CLng(Val(Trim(ExcelSheet.Cells(X, 6).Value)))
                
                ElseIf v_preco_brasil = "N" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
                    If v_situacao_venda = 0 Then
                        ' calcula os pontos pelo redutor da planilha
                        iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                        v_pontos_pai = Format(iPontos * (v_redutor_planilha / 100), "#0")
    
                    Else
                        ' calcula os pontos usando o redutor da tabela t_pi_preco_cn
                        iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                        If v_redutor_tabela = "" Then
                            v_redutor_tabela = "100"
                        End If
                        'v_pontos_pai = Format(iPontos * (v_redutor_tabela / 100), "#0")
                        v_pontos_pai = Format(iPontos * (ConvertePontoDecimal(v_redutor_tabela, 0) / 100), "#0")
                    End If
                Else
                    ' busca preco da tabela
                    iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9 + Y).Value)), MSG$), "#0")
                    If v_redutor_tabela = "" Then
                        v_redutor_tabela = "100"
                    End If
                    'v_pontos_pai = Format(iPontos * (v_redutor_tabela / 100), "#0")
                    v_pontos_pai = Format(iPontos * (ConvertePontoDecimal(v_redutor_tabela, 0) / 100), "#0")
                End If
                v_e_pai = True
            Else
                v_e_pai = False
         
                
            '**********************************************************************************************************
                
            ' agora vamos comparar os pontos filhos
            If v_preco_brasil = "S" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
                ' se o preco = S e o valor esta vazio temos de pegar os pontos da planilha
                
                iPontos = CLng(Val(Trim(ExcelSheet.Cells(X, 6).Value)))
                v_pontos_filho = v_pontos_filho + iPontos
                            
            ElseIf v_preco_brasil = "N" And Trim(ExcelSheet.Cells(X, 9 + Y).Value) = "" Then
                               
                    If v_situacao_venda = 0 Then
                        ' calcula os pontos pelo redutor da planilha
                        iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                        v_pontos_filho = Format(v_pontos_filho + (iPontos * (v_redutor_planilha / 100)), "#0")
                    Else
                        ' calcula os pontos usando o redutor da tabela t_pi_preco_cn
                        'v_redutor_tabela = Verifica_Redutor_Tabela(lCiclo, lCodvda, Y)
                        iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9).Value)), MSG$), "#0")
                        If v_redutor_tabela = "" Then
                            v_redutor_tabela = "100"
                        End If
                        v_pontos_filho = Format(v_pontos_filho + (iPontos * (ConvertePontoDecimal(v_redutor_tabela, 0) / 100)), "#0")
                        
                    
                    End If
            Else
                    iPontos = Format(FU_BuscaPontuacao(Str(lCiclo), Str(Trim(ExcelSheet.Cells(X, 9 + Y).Value)), MSG$), "#0")
                
                    If v_redutor_tabela = "" Then
                        v_redutor_tabela = "100"
                    End If
            
                    iPontos = Format(iPontos * (v_redutor_tabela / 100), "#0")
                    
                    'v_pontos_filho = Format(v_pontos_filho + (iPontos * (v_redutor_tabela / 100)), "#0")
                    v_pontos_filho = Format(v_pontos_filho + (iPontos * (ConvertePontoDecimal(v_redutor_tabela, 0) / 100)), "#0")
                                        
            
            End If
            End If
        Next X
        
        
    'Next y
    
proximaLinha:

    Next Y
    
    Screen.MousePointer = vbNormal
            
    If spdErros.MaxRows = 0 Then
        Verifica_Lista_KIT = True
        frmErros.Visible = False
    Else
        frmErros.Visible = True
        MsgBox "A planilha cont�m erros, por isto ela n�o ser� importada!", vbCritical, Me.Caption
    End If
    
    Exit Function

End Function

Public Function Verifica_Situacao_Venda(v_ciclo As Long, v_produto As Long, v_lista As Long) As Integer
    Dim cSql  As String
    Dim bResp As Boolean
    Dim i     As Integer
    Dim avPrecos()       As Variant
    Dim v_cod_lista1 As Integer


    On Error GoTo erro
    
    
    cSql = "SELECT id_status_venda FROM t_pi_preco_cn"
    cSql = cSql & " Where cd_venda_produto = " & v_produto
    cSql = cSql & " AND nm_ciclo_operacional = " & v_ciclo
    cSql = cSql & " AND cd_lista = " & v_lista
    cSql = cSql & " AND cd_segmento_canal = 1"

    ReDim avPrecos(0)
   
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
    If bResp = False Then
        MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
        End
    End If

    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
    End If

    Verifica_Situacao_Venda = avPrecos(0, i)

Exit Function

erro:
  MsgBox "Erro ao obter pr�ximo c�digo de lista" & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
  Exit Function
End Function

Public Function Verifica_Redutor_Tabela(v_ciclo As Long, v_produto As Long, v_lista As Long) As String
    Dim cSql  As String
    Dim bResp As Boolean
    Dim i     As Integer
    Dim avPrecos()       As Variant
    
    On Error GoTo erro
    
    
    cSql = "SELECT pc_redutor_pontos FROM t_pi_preco_cn"
    cSql = cSql & " Where cd_venda_produto = " & v_produto
    cSql = cSql & " AND nm_ciclo_operacional = " & v_ciclo
    cSql = cSql & " AND cd_lista = " & v_lista
    cSql = cSql & " AND cd_segmento_canal = 1"

    ReDim avPrecos(0)
   
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
    If bResp = False Then
        MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
        End
    End If

    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
    End If

    Verifica_Redutor_Tabela = avPrecos(0, i)

Exit Function

erro:
  MsgBox "Erro ao obter pr�ximo c�digo de lista" & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
  Exit Function
End Function

