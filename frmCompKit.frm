VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Begin VB.Form frmCompKit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pre�os CN - Componentes"
   ClientHeight    =   5160
   ClientLeft      =   2745
   ClientTop       =   4680
   ClientWidth     =   10260
   ControlBox      =   0   'False
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5160
   ScaleWidth      =   10260
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_salvar 
      Caption         =   "Confirmar"
      Enabled         =   0   'False
      Height          =   690
      Left            =   3330
      Picture         =   "frmCompKit.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   4320
      Width           =   1380
   End
   Begin VB.CommandButton cmd_fechar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   690
      Left            =   5760
      Picture         =   "frmCompKit.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4320
      Width           =   1380
   End
   Begin FPSpread.vaSpread grid_prod 
      Height          =   4140
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   10080
      _Version        =   131077
      _ExtentX        =   17780
      _ExtentY        =   7303
      _StockProps     =   64
      AutoCalc        =   0   'False
      ColsFrozen      =   2
      DisplayRowHeaders=   0   'False
      EditEnterAction =   7
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   7
      MaxRows         =   1
      NoBeep          =   -1  'True
      ProcessTab      =   -1  'True
      ScrollBars      =   2
      ShadowColor     =   -2147483638
      SpreadDesigner  =   "frmCompKit.frx":0614
      UserResize      =   0
      VisibleCols     =   500
      VisibleRows     =   500
   End
End
Attribute VB_Name = "frmCompKit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim bm_novo As Boolean
Dim sql_cd_ret As Long
Dim sql_qt_lnh As Long
Dim id_tipo_produto As String
Dim txt_produto As String

Private Sub sAcertaPontoTotal(Col As Long, Row As Long)

    Dim i As Integer
    Dim total As Double
    Dim dPontoUnit As Double
    Dim dPontoTot As Double
    Dim qt_pontos As Integer
    Dim preco_rounded As Double
    Dim aux_arredonda As String
    Dim nmCiclo As Long

    total = 0
    
    grid_prod.Col = Col
    grid_prod.Row = Row
    dPontoUnit = grid_prod.Text
    
    For i = 1 To grid_prod.MaxRows - 1
        grid_prod.Row = i
        total = total + CInt(grid_prod.Text)
    Next i
    
    grid_prod.Row = grid_prod.MaxRows
    grid_prod.Col = Col
    
    grid_prod.Text = Format(CStr(total), "0")
    
    grid_prod.Col = 3
    grid_prod.Row = Row
    dPontoTot = dPontoUnit * Val(grid_prod.Text)
    
    grid_prod.Col = 7
    grid_prod.Text = Format(dPontoTot, "#0")
          
    total = 0
    
    grid_prod.Col = 7
    
    For i = 1 To grid_prod.MaxRows - 1
        grid_prod.Row = i
        total = total + CInt(grid_prod.Text)
    Next i
    
    grid_prod.Row = grid_prod.MaxRows
    grid_prod.Col = 7
    
    grid_prod.Text = Format(CStr(total), "0")

End Sub



Public Sub RecebeParametro(cd_item_kit As String, dc_produto As String, qt_item_kit As String, vl_preco_referencia_kit As String, vl_total As String, qt_pontos_referencia_kit As String, flag_tela As Integer)
                                        
'   If grid_prod.MaxRows = 0 Then
'      grid_prod.MaxRows = 1
'   End If

    Dim lTotPonto As Long
    Dim X As Integer
    Dim vTot_Pontos As Integer

   bm_novo = False
      
    If grid_prod.Row >= grid_prod.MaxRows Then
       grid_prod.MaxRows = grid_prod.MaxRows + 1
       grid_prod.Row = grid_prod.MaxRows
    Else
       grid_prod.Row = grid_prod.Row + 1
    End If
    
    AcertaGrid grid_prod.Row, flag_tela
    
    grid_prod.Col = 1
    grid_prod.Text = cd_item_kit
    
    grid_prod.Col = 2
    If cd_item_kit = "" Then
        grid_prod.ForeColor = RGB(0, 0, 255)
    Else
        grid_prod.ForeColor = RGB(0, 0, 0)
    End If
    grid_prod.Text = dc_produto
    
    grid_prod.Col = 3
    If cd_item_kit = "" Then
        grid_prod.ForeColor = RGB(0, 0, 255)
    Else
        grid_prod.ForeColor = RGB(0, 0, 0)
    End If
   
    grid_prod.Text = Format(qt_item_kit, "0")
    
    grid_prod.Col = 4
    If cd_item_kit = "" Then
        grid_prod.ForeColor = RGB(0, 0, 255)
    Else
        grid_prod.ForeColor = RGB(0, 0, 0)
    End If
    'grid_prod.Text = vl_preco_referencia_kit
    'grid_prod.CellType = 1
    'grid_prod.Position = 1

       
    grid_prod.Value = Format(ConvertePontoDecimal(vl_preco_referencia_kit, 0), "#0.00")
        
    If Trim$(vl_preco_referencia_kit) = "0" Then
       bm_novo = True
    End If
      
    grid_prod.Col = 5
    If cd_item_kit = "" Then
        grid_prod.ForeColor = RGB(0, 0, 255)
    Else
        grid_prod.ForeColor = RGB(0, 0, 0)
    End If
    If Val(qt_item_kit) = 0 Then
        grid_prod.Text = ""
    Else
        grid_prod.Value = Format(Val(qt_pontos_referencia_kit) / Val(qt_item_kit), "0")
    End If
    
    grid_prod.Col = 6
    If cd_item_kit = "" Then
        grid_prod.ForeColor = RGB(0, 0, 255)
    Else
        grid_prod.ForeColor = RGB(0, 0, 0)
    End If
    'grid_prod.CellType = 1
    grid_prod.Value = Format(ConvertePontoDecimal(vl_total, 0), "#0.00")
    
    
'    grid_prod.Col = 4
'    If cd_item_kit = "" Then
'        grid_prod.ForeColor = RGB(0, 0, 255)
'    Else
'        grid_prod.ForeColor = RGB(0, 0, 0)
'    End If
'    'grid_prod.Text = vl_preco_referencia_kit
'    'grid_prod.Value = Format(ConvertePontoDecimal(vl_preco_referencia_kit, 0), "#0.00")
'    grid_prod.CellType = 1
'    grid_prod.Value = Format(ConvertePontoDecimal(vl_preco_referencia_kit, 0), "#0.00")
'
'    If Trim$(vl_preco_referencia_kit) = "0" Then
'       bm_novo = True
'    End If
'
'    grid_prod.Col = 5
'
'    If cd_item_kit = "" Then
'        grid_prod.ForeColor = RGB(0, 0, 255)
'    Else
'        grid_prod.ForeColor = RGB(0, 0, 0)
'    End If
'    If Val(qt_item_kit) = 0 Then
'        grid_prod.Text = ""
'    Else
'        grid_prod.Text = Format(Val(qt_pontos_referencia_kit) / Val(qt_item_kit), "0")
'    End If
'
'    grid_prod.Col = 6
'    If cd_item_kit = "" Then
'        grid_prod.ForeColor = RGB(0, 0, 255)
'    Else
'        grid_prod.ForeColor = RGB(0, 0, 0)
'    End If
'    grid_prod.Value = Format(ConvertePontoDecimal(vl_total, 0), "#0.00")
              
'    lTotPonto = Val(qt_item_kit) * Val(qt_pontos_referencia_kit)
    
    grid_prod.Col = 7
    If cd_item_kit = "" Then
        grid_prod.ForeColor = RGB(0, 0, 255)
    Else
        grid_prod.ForeColor = RGB(0, 0, 0)
    End If
    grid_prod.Text = Format(qt_pontos_referencia_kit, "0")
    
    If cd_item_kit = "" Then
        grid_prod.Col = -1
        grid_prod.Lock = True
    End If
    
    If UCase(dc_produto) = "TOTAL" Then
        grid_prod.Col = 5
        For X = 1 To grid_prod.MaxRows - 1
            grid_prod.Row = X
            vTot_Pontos = vTot_Pontos + Val(grid_prod.Text)
        Next X
        grid_prod.Row = grid_prod.MaxRows
        grid_prod.Text = Format(vTot_Pontos, "0")
    End If

End Sub


Private Sub AcertaGrid(ne_Linha As Integer, flag_tela As Integer)

   grid_prod.Row = ne_Linha
   
   grid_prod.Col = 1
'   grid_prod.TypeHAlign = 1
   grid_prod.Lock = True
   
   grid_prod.Col = 2
'   grid_prod.TypeHAlign = 0
   grid_prod.Lock = True
   
   grid_prod.Col = 3
'   grid_prod.TypeHAlign = 0
   If flag_tela = 1 Then
       grid_prod.Lock = True
   Else
       grid_prod.Lock = True
   End If
   
   grid_prod.Col = 4
'   grid_prod.TypeHAlign = 0
   grid_prod.Lock = False
   
   grid_prod.Col = 5
'   grid_prod.TypeHAlign = 0
   If flag_tela = 1 Then
       grid_prod.Lock = True
   Else
       grid_prod.Lock = True
   End If
         
   grid_prod.Col = 6
'   grid_prod.TypeHAlign = 0
   grid_prod.Lock = True
   
   grid_prod.Col = 7
'   grid_prod.TypeHAlign = 0
   grid_prod.Lock = True
   
End Sub


Private Sub cmd_fechar_Click()

If cmd_salvar.Enabled = True Then
    If MsgBox("As Informa��es da janela " & Me.Caption & " foram alteradas. Deseja limpa-las e perder as informa��es?", vbInformation + vbYesNo, "ATEN��O") = vbNo Then
        Exit Sub
    Else
        Unload Me
        Set frmCompKit = Nothing
    End If
Else
    Unload Me
    Set frmCompKit = Nothing
    
End If
End Sub

Private Sub cmd_salvar_Click()
    Salvar_Componentes
End Sub



Private Sub Form_Load()
 
 gisysmenu = GetSystemMenu(Me.hWnd, 0)

   iCounter = 8
   For gimenucount = iCounter To 6 Step -1
       giretval = RemoveMenu(gisysmenu, _
       gimenucount, MF_BYPOSITION)
   Next gimenucount
   
   Me.Top = 4665
   Me.Left = 795
   
   
   grid_prod.Row = 0
    
End Sub



Private Sub grid_prod_Change(ByVal Col As Long, ByVal Row As Long)

Dim i As Integer
Dim total As Double
Dim Valor As Double
Dim vl_preco_total As Double
Dim vl_unitario As Double
Dim qt_pontos As Integer
Dim preco_rounded As Double
Dim aux_arredonda As String
Dim nmCiclo As Long

    If Col = 4 Then
    
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
         If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
           cmd_salvar.Enabled = False
         Else
           cmd_salvar.Enabled = True
         End If
        
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
        'If Not ChecaPermissao("frmPDO011", "M") Then
           'cmd_salvar.Enabled = False
        'Else
           'cmd_salvar.Enabled = True
        'End If
        
        grid_prod.Col = Col
        grid_prod.Row = Row
        
        If grid_prod.Text = "" Then
            grid_prod.Text = "0"
        End If
        grid_prod.Text = Format(grid_prod.Text, "#0.00")
        
        'arredondamento
        grid_prod.Text = Format(grid_prod.Text, "#0.00")
        aux_arredonda = grid_prod.Text
        aux_arredonda = Format(aux_arredonda, "#0.00")
        aux_arredonda = Format(aux_arredonda, "#0.00")
        
        If aux_arredonda <> grid_prod.Text Then
            preco_rounded = CDbl(grid_prod.Text)
            grid_prod.Text = ConvertePontoDecimal(Str(preco_rounded), 0)
            grid_prod.Text = Format(grid_prod.Text, "#0.00")
            grid_prod.Text = Format(grid_prod.Text, "#0.00")
        End If
        
        vl_unitario = grid_prod.Text
        grid_prod.Col = 3
        vl_preco_total = vl_unitario * Val(grid_prod.Text)
        
        grid_prod.Col = 6
        'grid_prod.Value = Format(vl_preco_total, "#0.00")
        grid_prod.Value = vl_preco_total
        
        grid_prod.Row = 1
        grid_prod.Col = 6
        
        total = 0
        For i = 0 To grid_prod.MaxRows - 2
            total = total + CDbl(ConvertePontoDecimal(grid_prod.Text, 0))
            grid_prod.Row = grid_prod.Row + 1
        Next i
        
        grid_prod.Row = grid_prod.MaxRows
        grid_prod.Col = 6
        
        grid_prod.Text = Format(ConvertePontoDecimal(Str(total), 0), "#0.00")
        
        'Valor = Format(CDbl(vl_unitario) * (CDbl(frmPrecoRef.txt_redutor) / 100), "0.00")
        'Valor = Format(vl_unitario, "##0.00")
        'Valor = Format(CDbl(vl_unitario), "0.00")
        
        Valor = Format(vl_unitario * (CDbl(frmPrecoRef.txt_redutor) / 100), "0.00")
        Valor = Format(CDbl(Valor), "0.00")
        nmCiclo = Right(frmPrecoRef.txt_cicloano, 4) + _
                   Left(frmPrecoRef.txt_cicloano, 2)
        
        grid_prod.Row = Row
        grid_prod.Col = 5
        
        grid_prod.Text = Format(FU_BuscaPontuacao(Str(nmCiclo), Str(Valor), MSG$), "#0")
        
        Call sAcertaPontoTotal(grid_prod.Col, Row)
        
    End If
  
  
    If Col = 5 Then
    
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
        'If Not ChecaPermissao("frmPDO011", "M") Then
           'cmd_salvar.Enabled = False
        'Else
           'cmd_salvar.Enabled = True
        'End If
        
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
        If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
           cmd_salvar.Enabled = False
        Else
           cmd_salvar.Enabled = True
        End If

        Call sAcertaPontoTotal(Col, Row)

  End If
End Sub

Private Sub grid_prod_KeyPress(KeyAscii As Integer)

If grid_prod.Col = 4 Then
   If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
      KeyAscii = 0
      Beep
   End If
End If

End Sub

Private Sub grid_prod_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)

If Col = 4 Then

    grid_prod.Col = Col
    grid_prod.Row = Row
    grid_prod.Text = Format(grid_prod.Text, "#0.00")
    grid_prod.Col = NewCol
    grid_prod.Row = NewRow
    
End If


End Sub

' mmb - inicio
' Renato - altera��o para ratear os pontos dos filhos
Public Function RateioKit(Preco_Base As Double, Ponto_Pai As Integer) As Boolean

Dim perc As Double
Dim prc_novo As Double
Dim prc_tot_novo As Double
Dim tot_novo As Double
Dim dif As Double
Dim Ponto_Filho As Integer
Dim qtde As Integer
Dim qtde_tot As Integer
Dim total As Double
Dim conta As Integer
Dim Preco_filho As Double
Dim pou_filho As Integer
Dim pru_filho As Double

RateioKit = False
Ponto_Filho = 0
Preco_filho = 0

For conta = 1 To frmCompKit.grid_prod.MaxRows - 1
    frmCompKit.grid_prod.Row = conta
    frmCompKit.grid_prod.Col = 5
    pou_filho = CInt(frmCompKit.grid_prod.Text)
    frmCompKit.grid_prod.Col = 3
    pou_filho = pou_filho * CInt(frmCompKit.grid_prod.Text)
    Ponto_Filho = Ponto_Filho + pou_filho
    
    frmCompKit.grid_prod.Col = 6
    pru_filho = CDbl(frmCompKit.grid_prod.Text)
    Preco_filho = Preco_filho + pru_filho
    
Next
If Preco_Base = 0 Then
   MsgBox "Pre�o do pai � igual a zero, rateio cancelado!", vbInformation, "A T E N � � O !"
   Exit Function
End If
If Ponto_Pai = 0 Then
   MsgBox "Ponto do pai � igual a zero, rateio cancelado!", vbInformation, "A T E N � � O !"
   Exit Function
End If

If Preco_filho = 0 Then
   Preco_filho = 1
End If
If Ponto_Filho = 0 Then
   Ponto_Filho = 1
End If

' Busca o percentual
perc = CDbl(Format((Preco_Base / Preco_filho), "0.0000"))
tot_novo = 0
prc_tot_novo = 0

For conta = 1 To frmCompKit.grid_prod.MaxRows - 1
      With frmCompKit.grid_prod
         .Row = conta
                          
         ' Quantidade
         .Col = 3
         qtde = CInt(.Text)
                  
         ' Pre�o Unit�rio referencia
         .Col = 4
         prc_novo = CDbl(.Text)
                           
         ' Calcula o novo pre�o
         .TypeHAlign = 1
         
         'Reduz o pre�o levando em conta a quantidade
         prc_novo = Format(CDbl(prc_novo * qtde * perc), "0.00")
         
         'Retorna ao pre�o unit�rio
         .Text = Format(ConvertePontoDecimal(CStr(prc_novo / qtde), 0), "0.00")
         
         prc_tot_novo = prc_novo
         
         tot_novo = tot_novo + prc_tot_novo
         
         ' Insere valoes no grid
         
         .Col = 6

         .Text = Format(ConvertePontoDecimal(CStr(prc_novo), 0), "#0.00")
         
        
      End With
Next

tot_novo = CDbl(Format(tot_novo, "0.00"))
dif = CDbl(Format((Preco_Base - tot_novo), "0.00"))

' Se existe diferen�a
If dif <> 0 Then
    With frmCompKit.grid_prod
        .Row = 1
        ' Quantidade
        .Col = 3
        qtde = CInt(.Text)
        ' Pre�o Unit�rio
        .Col = 4
        prc_novo = CDbl(.Text)
                     
        ' Soma diferen�a com o pre�o unit�rio
        prc_novo = CDbl(Format(((prc_novo * qtde) + dif), "0.000"))
        prc_tot_novo = prc_novo
        total = total + dif
        
        .Text = Format(ConvertePontoDecimal((prc_novo / qtde), Virgula), "0.00")
        
        ' Insere valoes no grid
        .Col = 6

        .Text = Format(ConvertePontoDecimal(CStr(prc_novo), 0), "#0.00")
        
    End With
End If
' mmb - fim


' *****************************************
' Rateio de Pontos  -> Renato   02/02/2000
' *****************************************

' Busca o percentual
perc = CDbl(Format((Ponto_Pai / Ponto_Filho), "0.0000"))
tot_novo = 0
prc_tot_novo = 0

For conta = 1 To frmCompKit.grid_prod.MaxRows - 1
      With frmCompKit.grid_prod
         .Row = conta
                          
         ' Quantidade
         .Col = 3
         qtde = CInt(.Text)

         ' Ponto do filho
         .Col = 5
         prc_novo = CDbl(.Text)

         ' Calcula o novo Ponto
         .Col = 5
         .TypeHAlign = 1
         prc_novo = CDbl(Format((prc_novo * perc), "0"))
         
         .Text = Format(ConvertePontoDecimal(CStr(prc_novo), 0), "#0")

         
         tot_novo = tot_novo + (prc_novo * qtde)

      End With
Next

tot_novo = CDbl(Format(tot_novo, "0"))
dif = CDbl(Format((Ponto_Pai - tot_novo), "0"))

' Se existe diferen�a
If dif <> 0 Then
    With frmCompKit.grid_prod
        .Row = 1
        ' Ponto
        .Col = 4
        prc_novo = CDbl(.Text)
        While prc_novo + dif <= 0
            .Row = .Row + 1
            ' Ponto
            .Col = 4
            prc_novo = CDbl(.Text)
        Wend
        ' Soma diferen�a com o Ponto

        prc_novo = CDbl(Format((prc_novo + dif), "0.000"))
        prc_tot_novo = prc_novo
        total = total + dif


        ' Insere valoes no grid
        .Col = 4
        .Text = Format(ConvertePontoDecimal(CStr(prc_novo), 0), "#0")
    End With
End If

Ponto_Filho = 0
Preco_filho = 0


' Atualiza Totais
For conta = 1 To frmCompKit.grid_prod.MaxRows - 1
    frmCompKit.grid_prod.Row = conta
    frmCompKit.grid_prod.Col = 5
    pou_filho = CInt(frmCompKit.grid_prod.Text)
    frmCompKit.grid_prod.Col = 3
    Ponto_Filho = Ponto_Filho + pou_filho * CInt(frmCompKit.grid_prod.Text)
    frmCompKit.grid_prod.Col = 4
    Preco_filho = Preco_filho + CDbl(frmCompKit.grid_prod.Text)
Next
frmCompKit.RecebeParametro "", "TOTAL", "", "", Str(Preco_filho), Str(Ponto_Filho), 0

If Salvar_Componentes() Then
   RateioKit = True
Else
   RateioKit = False
End If

End Function

Public Function Salvar_Componentes() As Boolean

    Dim i                        As Integer
    Dim preco_componentes        As Double
    Dim cd_venda_item_kit        As Long
    Dim qt_pontos                As Long
    Dim cd_item_kit              As String
    Dim dc_produto               As String
    Dim qt_item_kit              As String
    Dim vl_preco_referencia_kit  As String
    Dim vl_total                 As String
    Dim qt_pontos_referencia_kit As String
    Dim nm_ciclo_operacional     As String
    Dim gPrecoBase               As Boolean
    Dim Occur                    As Integer

    Salvar_Componentes = False
    
    'cmd_salvar.Enabled = False
    
    'checar se somatoria dos filhos bate com o preco pai
    
    grid_prod.Col = 4
    grid_prod.Row = 1
    
    For i = 0 To grid_prod.MaxRows - 2
        If Trim$(grid_prod.Text) = "0,00" Or Trim$(grid_prod.Text) = "0" Then
            MsgBox "Existem componentes com pre�o igual a zero!", vbInformation, "A T E N � � O"
            grid_prod.Action = 0
            Exit Function
        End If
        
        grid_prod.Row = grid_prod.Row + 1
    Next i
    
    grid_prod.Row = grid_prod.MaxRows
    grid_prod.Col = 7
    qt_pontos = CInt(Trim$(grid_prod.Text))
    grid_prod.Col = 6
    preco_componentes = CDbl(Trim$(grid_prod.Text))
    
    If Trim$(frmPrecoRef.txt_precobase) = "" Then
        frmPrecoRef.txt_precobase = "0"
    End If
    
    If Format(CStr(preco_componentes), "#0.00") <> ConvertePontoDecimal(frmPrecoRef.txt_precobase, 0) Then
       MsgBox "Somat�ria do pre�o dos componentes n�o pode ser diferente do pre�o pai !", vbInformation, "A T E N � � O"
       Exit Function
    Else
        gPrecoBase = True
    End If
    
    ' Renato
    If Format(CStr(qt_pontos), "0") <> Format(frmPrecoRef.txt_pontos, "0") Then
        MsgBox "Somat�ria dos pontos dos componentes n�o pode ser diferente dos pontos pai !", vbInformation, "A T E N � � O"
        grid_prod.Col = 5
        For i = 1 To grid_prod.MaxRows - 1
            grid_prod.Row = i
            grid_prod.Lock = False
        Next i
        Exit Function
    End If
        
    nm_ciclo_operacional = Right(frmPrecoRef.txt_cicloano, 4) + Left(frmPrecoRef.txt_cicloano, 2)
        
    grid_prod.Row = 1
    
    For i = 1 To grid_prod.MaxRows - 1
            
        grid_prod.Row = i
        
        grid_prod.Col = 1
        cd_venda_item_kit = grid_prod.Text
        
        For Occur = 1 To UBound(v_componentes)
            If v_componentes(Occur).cd_venda_item = cd_venda_item_kit Then
                grid_prod.Col = 4
                v_componentes(Occur).vl_preco_referencia_kit = grid_prod.Text
                
                grid_prod.Col = 5
                v_componentes(Occur).qt_pontos_referencia_kit = grid_prod.Text
            End If
        Next Occur
        
    Next i
        
    Salvar_Componentes = True
    cmd_salvar.Enabled = False
    
    Unload Me
    Set frmCompKit = Nothing
    
End Function


Private Sub vaSpread1_Advance(ByVal AdvanceNext As Boolean)

End Sub
