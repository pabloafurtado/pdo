VERSION 5.00
Object = "{CDF3B183-D408-11CE-AE2C-0080C786E37D}#2.0#0"; "EDT32X20.OCX"
Object = "{BE4F3AC8-AEC9-101A-947B-00DD010F7B46}#1.0#0"; "MSOUTL32.OCX"
Begin VB.Form frmVigenciaPCLAux 
   Caption         =   "Associa��o - Material X Abrang�ncia X Vig�ncia"
   ClientHeight    =   5025
   ClientLeft      =   1575
   ClientTop       =   4185
   ClientWidth     =   8685
   ControlBox      =   0   'False
   Icon            =   "frmVigenciaPCLAux.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5025
   ScaleWidth      =   8685
   Begin VB.TextBox txtPrioridade 
      Alignment       =   2  'Center
      Height          =   360
      Left            =   6840
      MaxLength       =   1
      TabIndex        =   1
      Top             =   270
      Width           =   495
   End
   Begin VB.ComboBox cmbMaterial 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmVigenciaPCLAux.frx":0442
      Left            =   150
      List            =   "frmVigenciaPCLAux.frx":0444
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   285
      Width           =   6435
   End
   Begin VB.Frame Frame4 
      Height          =   3105
      Left            =   4680
      TabIndex        =   10
      Top             =   750
      Width           =   3975
      Begin EditLib.fpMask txtDtFinal 
         Height          =   315
         Left            =   2220
         TabIndex        =   6
         Top             =   2355
         Width           =   1335
         _Version        =   131072
         _ExtentX        =   2355
         _ExtentY        =   556
         _StockProps     =   68
         BackColor       =   -2147483643
         ForeColor       =   0
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ButtonDisable   =   0   'False
         ButtonHide      =   0   'False
         ButtonIncrement =   1
         ButtonMin       =   0
         ButtonMax       =   100
         ButtonStyle     =   0
         ButtonWidth     =   0
         ButtonWrap      =   -1  'True
         ThreeDText      =   0
         ThreeDTextHighlightColor=   -2147483633
         ThreeDTextShadowColor=   -2147483632
         ThreeDTextOffset=   1
         AlignTextH      =   1
         AlignTextV      =   1
         AllowNull       =   0   'False
         NoSpecialKeys   =   0
         AutoAdvance     =   0   'False
         AutoBeep        =   0   'False
         CaretInsert     =   0
         CaretOverWrite  =   3
         UserEntry       =   0
         HideSelection   =   -1  'True
         InvalidColor    =   -2147483637
         InvalidOption   =   0
         MarginLeft      =   3
         MarginTop       =   3
         MarginRight     =   3
         MarginBottom    =   3
         NullColor       =   -2147483637
         OnFocusAlignH   =   0
         OnFocusAlignV   =   0
         OnFocusNoSelect =   0   'False
         OnFocusPosition =   0
         ControlType     =   0
         AllowOverflow   =   0   'False
         BestFit         =   0   'False
         ClipMode        =   0
         DataFormat      =   -10968
         Mask            =   "##/##/####"
         PromptChar      =   " "
         PromptInclude   =   0   'False
         RequireFill     =   0   'False
         BorderGrayAreaColor=   -2147483637
         NoPrefix        =   0   'False
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         AutoTab         =   0   'False
         MouseIcon       =   "frmVigenciaPCLAux.frx":0446
      End
      Begin EditLib.fpMask txtDtInicio 
         Height          =   315
         Left            =   2280
         TabIndex        =   4
         Top             =   915
         Width           =   1335
         _Version        =   131072
         _ExtentX        =   2355
         _ExtentY        =   556
         _StockProps     =   68
         BackColor       =   -2147483643
         ForeColor       =   0
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ButtonDisable   =   0   'False
         ButtonHide      =   0   'False
         ButtonIncrement =   1
         ButtonMin       =   0
         ButtonMax       =   100
         ButtonStyle     =   0
         ButtonWidth     =   0
         ButtonWrap      =   -1  'True
         ThreeDText      =   0
         ThreeDTextHighlightColor=   -2147483633
         ThreeDTextShadowColor=   -2147483632
         ThreeDTextOffset=   1
         AlignTextH      =   1
         AlignTextV      =   1
         AllowNull       =   0   'False
         NoSpecialKeys   =   0
         AutoAdvance     =   0   'False
         AutoBeep        =   0   'False
         CaretInsert     =   0
         CaretOverWrite  =   3
         UserEntry       =   0
         HideSelection   =   -1  'True
         InvalidColor    =   -2147483637
         InvalidOption   =   0
         MarginLeft      =   3
         MarginTop       =   3
         MarginRight     =   3
         MarginBottom    =   3
         NullColor       =   -2147483637
         OnFocusAlignH   =   0
         OnFocusAlignV   =   0
         OnFocusNoSelect =   0   'False
         OnFocusPosition =   0
         ControlType     =   0
         AllowOverflow   =   0   'False
         BestFit         =   0   'False
         ClipMode        =   0
         DataFormat      =   -10968
         Mask            =   "##/##/####"
         PromptChar      =   " "
         PromptInclude   =   0   'False
         RequireFill     =   0   'False
         BorderGrayAreaColor=   -2147483637
         NoPrefix        =   0   'False
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         AutoTab         =   0   'False
         MouseIcon       =   "frmVigenciaPCLAux.frx":0462
      End
      Begin VB.ComboBox cmbCicloFinal 
         Height          =   360
         ItemData        =   "frmVigenciaPCLAux.frx":047E
         Left            =   330
         List            =   "frmVigenciaPCLAux.frx":0480
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   2310
         Width           =   1605
      End
      Begin VB.ComboBox cmbCicloInicial 
         Height          =   360
         ItemData        =   "frmVigenciaPCLAux.frx":0482
         Left            =   360
         List            =   "frmVigenciaPCLAux.frx":0484
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   900
         Width           =   1545
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Data Final:"
         ForeColor       =   &H80000007&
         Height          =   240
         Left            =   2220
         TabIndex        =   17
         Top             =   1980
         Width           =   960
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Data Inicial:"
         ForeColor       =   &H80000007&
         Height          =   240
         Left            =   2280
         TabIndex        =   16
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo Final"
         Height          =   240
         Left            =   360
         TabIndex        =   14
         Top             =   1950
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo Inicial"
         Height          =   240
         Left            =   420
         TabIndex        =   12
         Top             =   600
         Width           =   1005
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Vig�ncia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   780
         TabIndex        =   11
         Top             =   150
         Width           =   915
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   2370
      TabIndex        =   9
      Top             =   3870
      Width           =   3525
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Default         =   -1  'True
         Height          =   765
         Left            =   390
         Picture         =   "frmVigenciaPCLAux.frx":0486
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   180
         Width           =   1065
      End
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   765
         Left            =   2040
         Picture         =   "frmVigenciaPCLAux.frx":0790
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   180
         Width           =   1125
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3105
      Left            =   120
      TabIndex        =   7
      Top             =   750
      Width           =   4455
      Begin MSOutl.Outline outEstrutura 
         Height          =   2340
         Left            =   180
         TabIndex        =   2
         ToolTipText     =   "Estrutura Comercial"
         Top             =   540
         Width           =   4215
         _Version        =   65536
         _ExtentX        =   7435
         _ExtentY        =   4128
         _StockProps     =   77
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   4
         PicturePlus     =   "frmVigenciaPCLAux.frx":0A9A
         PictureMinus    =   "frmVigenciaPCLAux.frx":0C0C
         PictureLeaf     =   "frmVigenciaPCLAux.frx":0D7E
         PictureOpen     =   "frmVigenciaPCLAux.frx":0EF0
         PictureClosed   =   "frmVigenciaPCLAux.frx":1062
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Abrang�ncia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   1155
         TabIndex        =   8
         Top             =   150
         Width           =   1350
      End
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Material:"
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   180
      TabIndex        =   19
      Top             =   30
      Width           =   765
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "Prioridade:"
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   6840
      TabIndex        =   18
      Top             =   30
      Width           =   990
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Confirmar"
      End
      Begin VB.Menu mnuCancelar 
         Caption         =   "C&ancelar"
      End
   End
End
Attribute VB_Name = "frmVigenciaPCLAux"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim varRE      As Integer
Dim varGV      As Integer
Dim varSET     As Integer
Dim vCtrlPCL   As Boolean

Dim vCiclIni   As Variant
Dim vDataIni   As Variant
Dim vCiclFin   As Variant
Dim vDataFin   As Variant
Dim cSql       As String
    
Private Sub cmdConfirmar_Click()

    Dim vTip  As String
    Dim vCod  As String
    Dim vPos  As Integer
    Dim vLin  As Integer
    Dim vOper As Variant
    Dim vStatus As Variant
    
    If cmbMaterial.Text = "" Then
       MsgBox "O Material � Obrigat�rio.", vbCritical, "Aten��o"
       Exit Sub
    End If
    If txtPrioridade.Text = "" Then
       MsgBox "A Prioridade � Obrigat�ria.", vbCritical, "Aten��o"
       Exit Sub
    End If
    If Trim(outEstrutura.Text) = "" Then
       MsgBox "Abrang�ncia � Obrigat�rio.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    'codigo da estrutura comercial
    vCod = outEstrutura.ItemData(outEstrutura.ListIndex)
    'tipo da estrutura comercial
    vTip = "0"
    If Val(varRE) > 0 Then
       vTip = "2"
    End If
    If Val(varGV) > 0 Then
       vTip = "3"
    End If
    If Val(varSET) > 0 Then
       vTip = "4"
    End If

    If cmbCicloInicial.Text = "" And txtDtInicio.UnFmtText = "" Then
       MsgBox "O In�cio da Vig�ncia � Obrigat�rio.", vbCritical, "Aten��o"
       Exit Sub
    End If
    If cmbCicloInicial.Text <> "" And txtDtInicio.UnFmtText <> "" Then
       MsgBox "Escolha In�cio da Vig�ncia pelo Ciclo ou pela Data.", vbCritical, "Aten��o"
       cmbCicloInicial.SetFocus
       Exit Sub
    End If
    If cmbCicloFinal.Text <> "" And txtDtFinal.UnFmtText <> "" Then
       MsgBox "Escolha Fim da Vig�ncia pelo Ciclo ou pela Data.", vbCritical, "Aten��o"
       cmbCicloFinal.SetFocus
       Exit Sub
    End If

    If txtDtInicio.Text <> "" Then
        If Not IsDate(txtDtInicio.Text) Then
            MsgBox "Aten��o...A data inicial informada est� inv�lida.", vbCritical, "Aviso"
            txtDtInicio.SetFocus
            Exit Sub
        End If
    End If
    
    If txtDtFinal.Text <> "" Then
        If Not IsDate(txtDtFinal.Text) Then
            MsgBox "Aten��o...A data final informada est� inv�lida.", vbCritical, "Aviso"
            txtDtFinal.SetFocus
            Exit Sub
        End If
    End If

    'validar vigencia
    If Not ValidarVigencia(vTip, vCod) Then Exit Sub
    
    If indicaAltera = 1 Then
       frmVigenciaPCL.spdMaterial.Row = frmVigenciaPCL.spdMaterial.ActiveRow
    Else
       frmVigenciaPCL.spdMaterial.MaxRows = frmVigenciaPCL.spdMaterial.MaxRows + 1
       frmVigenciaPCL.spdMaterial.Row = frmVigenciaPCL.spdMaterial.MaxRows
       'material
       frmVigenciaPCL.spdMaterial.SetText 1, frmVigenciaPCL.spdMaterial.Row, cmbMaterial.ItemData(cmbMaterial.ListIndex)
    End If
    
    'descr material
    vPos = InStr(1, cmbMaterial.Text, "-")
    frmVigenciaPCL.spdMaterial.SetText 2, frmVigenciaPCL.spdMaterial.Row, Trim(Mid(cmbMaterial.Text, vPos + 1))
    'estrutura comercial
    If Val(vTip) = 0 Then
       frmVigenciaPCL.spdMaterial.SetText 3, frmVigenciaPCL.spdMaterial.Row, vCod & " - " & Trim(outEstrutura.Text)
    Else
       frmVigenciaPCL.spdMaterial.SetText 3, frmVigenciaPCL.spdMaterial.Row, Trim(outEstrutura.Text)
    End If
    'ciclo inicial
    frmVigenciaPCL.spdMaterial.SetText 4, frmVigenciaPCL.spdMaterial.Row, cmbCicloInicial.Text
    'data inicial
    'Cleiton - Cadmus 12/09/2007
    'Colocado o format na data para grava��o na base, pois ocorria erro na procedure de grava��o.
    frmVigenciaPCL.spdMaterial.SetText 5, frmVigenciaPCL.spdMaterial.Row, Format(txtDtInicio.Text, "dd/mm/yyyy")
    'ciclo final
        frmVigenciaPCL.spdMaterial.SetText 6, frmVigenciaPCL.spdMaterial.Row, cmbCicloFinal.Text
    'data final
    'Cleiton - Cadmus 12/09/2007
    'Colocado o format na data para grava��o na base, pois ocorria erro na procedure de grava��o.
    frmVigenciaPCL.spdMaterial.SetText 7, frmVigenciaPCL.spdMaterial.Row, Format(txtDtFinal.Text, "dd/mm/yyyy")
    'prioridade
    frmVigenciaPCL.spdMaterial.SetText 8, frmVigenciaPCL.spdMaterial.Row, txtPrioridade.Text
    'status
'    frmVigenciaPCL.spdMaterial.SetText 9, frmVigenciaPCL.spdMaterial.Row, "ATIVA"
    frmVigenciaPCL.spdMaterial.GetText 9, frmVigenciaPCL.spdMaterial.Row, vStatus
    If vStatus = "CANCELADA" Then
    Else
        vStatus = VerificarStatusCiclo(frmVigenciaPCL.spdMaterial.Row, Val(vTip), Val(vCod), "MAT")
    End If
    frmVigenciaPCL.spdMaterial.SetText 9, frmVigenciaPCL.spdMaterial.Row, vStatus
    
    'inclusao ou alteracao
    frmVigenciaPCL.spdMaterial.GetText 13, frmVigenciaPCL.spdMaterial.Row, vOper
    
    If vOper <> "I" Then
       If indicaAltera = 1 Then
          frmVigenciaPCL.spdMaterial.SetText 13, frmVigenciaPCL.spdMaterial.Row, "A"
       Else
          frmVigenciaPCL.spdMaterial.SetText 13, frmVigenciaPCL.spdMaterial.Row, "I"
       End If
    End If
    
    If vStatus = "CANCELADA" Then
        frmVigenciaPCL.spdMaterial.BlockMode = True
        frmVigenciaPCL.spdMaterial.Row2 = frmVigenciaPCL.spdMaterial.Row
        frmVigenciaPCL.spdMaterial.Col = -1
        frmVigenciaPCL.spdMaterial.Col2 = frmVigenciaPCL.spdMaterial.MaxCols
        frmVigenciaPCL.spdMaterial.BackColor = &H80FFFF
'        frmVigenciaPCL.spdMaterial.RowHidden = True
        frmVigenciaPCL.spdMaterial.BlockMode = False
    Else
        If vStatus <> "ATIVA" Then
            frmVigenciaPCL.spdMaterial.BlockMode = True
            frmVigenciaPCL.spdMaterial.Row2 = frmVigenciaPCL.spdMaterial.Row
            frmVigenciaPCL.spdMaterial.Col = -1
            frmVigenciaPCL.spdMaterial.Col2 = frmVigenciaPCL.spdMaterial.MaxCols
            frmVigenciaPCL.spdMaterial.BackColor = &HFFFF80
'            frmVigenciaPCL.spdMaterial.RowHidden = True
            frmVigenciaPCL.spdMaterial.BlockMode = False
        Else
            frmVigenciaPCL.spdMaterial.BlockMode = True
            frmVigenciaPCL.spdMaterial.Row2 = frmVigenciaPCL.spdMaterial.Row
            frmVigenciaPCL.spdMaterial.Col = -1
            frmVigenciaPCL.spdMaterial.Col2 = frmVigenciaPCL.spdMaterial.MaxCols
            frmVigenciaPCL.spdMaterial.BackColor = vbWhite
'            frmVigenciaPCL.spdMaterial.RowHidden = True
            frmVigenciaPCL.spdMaterial.BlockMode = False
        End If
    End If

    
    'controle logistico
    If frmVigenciaPCL.chkContrLog.Value = 1 Then
       '
       If indicaAltera = 1 Then
          vLin = frmVigenciaPCL.spdMaterial.ActiveRow
       Else
          frmVigenciaPCL.spdAVIG.MaxRows = frmVigenciaPCL.spdAVIG.MaxRows + 1
          
          vLin = frmVigenciaPCL.spdAVIG.MaxRows
       End If
       
       'abrangencia
       'frmVigenciaPCL.spdAVIG.SetText 1, vLin, vCod & " - " & Trim(outEstrutura.Text)
        If Val(vTip) = 0 Then
            frmVigenciaPCL.spdAVIG.SetText 1, vLin, vCod & " - " & Trim(outEstrutura.Text)
        Else
            frmVigenciaPCL.spdAVIG.SetText 1, vLin, Trim(outEstrutura.Text)
        End If
       'ciclo inic
       frmVigenciaPCL.spdAVIG.SetText 2, vLin, cmbCicloInicial.Text
       'data inicio
       frmVigenciaPCL.spdAVIG.SetText 3, vLin, txtDtInicio.Text
       'ciclo fim
       frmVigenciaPCL.spdAVIG.SetText 4, vLin, cmbCicloFinal.Text
       'data final
       frmVigenciaPCL.spdAVIG.SetText 5, vLin, txtDtFinal.Text
       'tipo estrutura comercial
       frmVigenciaPCL.spdAVIG.SetText 8, vLin, vTip
       'cod estrutura comercial
       frmVigenciaPCL.spdAVIG.SetText 9, vLin, vCod
       'regiao estrategia
       frmVigenciaPCL.spdAVIG.SetText 10, vLin, Val(varRE)
       'gerencia vendas
       frmVigenciaPCL.spdAVIG.SetText 11, vLin, Val(varGV)
       'setor
       frmVigenciaPCL.spdAVIG.SetText 12, vLin, Val(varSET)
       '
    End If

    
    frmVigenciaPCL.sLiberaSalvar
'    frmVigenciaPCL.cmdGravar.Enabled = True
'    frmVigenciaPCL.cmdCancelar.Enabled = True
    frmVigenciaPCL.spdAVIG.Enabled = False
    
    Unload Me

End Sub

Private Sub cmdCancelar_Click()

    Dim X As Integer

    indicaAltera = 0
       
    For X = 1 To outEstrutura.ListCount - 1
        outEstrutura.Expand(X) = False
    Next
    
    Unload Me

End Sub

Private Sub Form_Activate()

    Dim X         As Integer
    Dim vLin      As Integer
    Dim vLinMat   As Integer
    Dim vInt      As Integer
    
    Dim vSeq      As Variant
    Dim vTip      As Variant
    Dim vRE       As Variant
    Dim vGV       As Variant
    Dim vSet      As Variant
    Dim vCod      As Variant
    Dim vMaterial As Variant
    Dim vPrior    As Variant
    
    'carregar o combo dos materiais do produto de venda
    CarregarMateriaisProduto
    
    'verificar se e controle do pcl
    vCtrlPCL = IIf(frmVigenciaPCL.chkContrLog.Value = 1, True, False)
    
    vLin = frmVigenciaPCL.spdAVIG.ActiveRow
    vLinMat = frmVigenciaPCL.spdMaterial.ActiveRow
    
    If vCtrlPCL = True Then
       vLin = vLinMat
    End If
    
    frmVigenciaPCL.spdMaterial.GetText 1, vLinMat, vMaterial
    frmVigenciaPCL.spdMaterial.GetText 4, vLinMat, vCiclIni
    frmVigenciaPCL.spdMaterial.GetText 5, vLinMat, vDataIni
    frmVigenciaPCL.spdMaterial.GetText 6, vLinMat, vCiclFin
    frmVigenciaPCL.spdMaterial.GetText 7, vLinMat, vDataFin
    frmVigenciaPCL.spdMaterial.GetText 8, vLinMat, vPrior
    
    frmVigenciaPCL.spdAVIG.GetText 7, vLin, vSeq
    frmVigenciaPCL.spdAVIG.GetText 8, vLin, vTip
    frmVigenciaPCL.spdAVIG.GetText 9, vLin, vCod
    frmVigenciaPCL.spdAVIG.GetText 10, vLin, vRE
    frmVigenciaPCL.spdAVIG.GetText 11, vLin, vGV
    frmVigenciaPCL.spdAVIG.GetText 12, vLin, vSet
    
    outEstrutura.Enabled = True
    
    'limpar as variaveis
    cmbCicloInicial.ListIndex = -1
    cmbCicloFinal.ListIndex = -1
    txtDtInicio.Text = ""
    txtDtFinal.Text = ""
    
    varRE = 0
    varGV = 0
    varSET = 0
    
    If vCtrlPCL = False Or (vCtrlPCL = True And indicaAltera = 1) Then
    
       varRE = Val(vRE)
       varGV = Val(vGV)
       varSET = Val(vSet)
    
       'abrangencia nao pode alterar mesmo sendo inclusao ja vem definido
       'brasil
       If vTip = 0 Then
          outEstrutura.ListIndex = 0
       End If
       'regiao estrategica
       For X = 0 To outEstrutura.ListCount - 1
           If Val(vRE) > 0 Then
              If outEstrutura.ItemData(X) = vRE And outEstrutura.Indent(X) = 1 Then
                 outEstrutura.ListIndex = X
                 If Val(vGV) > 0 Then
                    outEstrutura.Expand(X) = True
                    vInt = outEstrutura.Indent(X)
                 End If
                 Exit For
              End If
           End If
       Next
       'gerencia de vendas
       If Val(vGV) > 0 Then
          For X = outEstrutura.ListIndex + 1 To outEstrutura.ListCount - 1
              If outEstrutura.ItemData(X) = vGV And outEstrutura.Indent(X) = 2 Then
                 outEstrutura.ListIndex = X
                 If Val(vSet) > 0 Then
                    outEstrutura.Expand(X) = True
                    vInt = outEstrutura.Indent(X)
                 End If
                 Exit For
              End If
          Next
       End If
       'setor
       If Val(vSet) > 0 Then
          For X = outEstrutura.ListIndex + 1 To outEstrutura.ListCount - 1
              If outEstrutura.ItemData(X) = vSet And outEstrutura.Indent(X) = 3 Then
                 outEstrutura.ListIndex = X
                 Exit For
              End If
          Next
       End If
       'desabilitado
       outEstrutura.Enabled = False
    Else
       'habilitado
       outEstrutura.Enabled = True
    End If
    
    
    If indicaAltera = 1 Then
       'material
       If Val(vMaterial) > 0 Then
          For X = 0 To cmbMaterial.ListCount - 1
              If cmbMaterial.ItemData(X) = Val(vMaterial) Then
                 cmbMaterial.ListIndex = X
                 Exit For
              End If
          Next
       End If
    
       'data de vigencia
       txtDtInicio.Text = vDataIni
       txtDtFinal.Text = vDataFin
       
       'ciclo
       If vCiclIni <> "" Then
          vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
          For X = 0 To cmbCicloInicial.ListCount - 1
              If cmbCicloInicial.ItemData(X) = vCiclIni Then
                 cmbCicloInicial.ListIndex = X
                 Exit For
              End If
          Next
       End If
       If vCiclFin <> "" Then
          vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
          For X = 0 To cmbCicloFinal.ListCount - 1
              If cmbCicloFinal.ItemData(X) = vCiclFin Then
                 cmbCicloFinal.ListIndex = X
                 Exit For
              End If
          Next
       End If
       'prioridade
       txtPrioridade.Text = vPrior
       '
    End If
    
    
    If indicaAltera = 1 Then
       cmbMaterial.Enabled = False
    Else
       cmbMaterial.Enabled = True
    End If

End Sub

Private Sub Form_Load()

    Dim s As String
    
    s = UCase(Environ("_U"))
    
    Screen.MousePointer = 11
    
    'Carrega listbox de ciclo
    Call Carrega_Ciclo(cmbCicloInicial)
    Call Carrega_Ciclo(cmbCicloFinal)

    'Insere item na outline
    outEstrutura.Clear
   'outEstrutura.AddItem "N�VEL BRASIL", 0
   'outEstrutura.Indent(0) = 0
   'outEstrutura.ItemData(0) = 1

    'Carrega Estrutura Comercial
    Call Carrega_Estrutura_Comercial(outEstrutura)
    
    
    Screen.MousePointer = 1

End Sub
Private Sub mnuCancelar_Click()
    indicaAltera = 0
    Unload Me
End Sub

Private Sub mnuSalvar_Click()
    cmdConfirmar_Click
End Sub

Private Sub outEstrutura_Click()

    If outEstrutura.ListIndex = 0 Then
       varRE = 0
       varGV = 0
       varSET = 0
    End If
    
    If outEstrutura.Indent(outEstrutura.ListIndex) = 1 Then
       varRE = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
       varGV = 0
       varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 2 Then
           varGV = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
           varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 3 Then
           varSET = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
    End If
    
End Sub

Private Sub outEstrutura_DblClick()
    
    If outEstrutura.ListIndex = 0 Then
       varRE = 0
       varGV = 0
       varSET = 0
    End If
    
    If outEstrutura.Expand(outEstrutura.ListIndex) Then
       outEstrutura.Expand(outEstrutura.ListIndex) = False
    Else
       outEstrutura.Expand(outEstrutura.ListIndex) = True
    End If

    If outEstrutura.Indent(outEstrutura.ListIndex) = 1 Then
       varRE = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
       varGV = 0
       varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 2 Then
           varGV = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
           varSET = 0
    ElseIf outEstrutura.Indent(outEstrutura.ListIndex) = 3 Then
           varSET = Val(outEstrutura.ItemData(outEstrutura.ListIndex))
    End If

End Sub

Private Sub outEstrutura_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
       If outEstrutura.Expand(outEstrutura.ListIndex) Then
          outEstrutura.Expand(outEstrutura.ListIndex) = False
       Else
          outEstrutura.Expand(outEstrutura.ListIndex) = True
       End If
    End If

End Sub

Private Sub CarregarMateriaisProduto()

    Dim X As Integer

    ReDim avData(0, 0)
    ReDim avParametros(1, 1)
      
    cmbMaterial.Clear
      
    avParametros(0, AV_CONTEUDO) = frmVigenciaPCL.txtcodvenda.Text
    avParametros(0, AV_TIPO) = TIPO_NUMERICO
    
    cSql = " stp_lista_material_produto_s " & FormataParametrosSQL(avParametros)
        
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
    If bResp = False Then
       Exit Sub
    End If
        
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Sub
    End If
   
    If Val(avData(0, 0)) <= 0 Then Exit Sub
   
    indicaKit = False
   
    For X = 0 To UBound(avData, 2)
        cmbMaterial.AddItem avData(0, X) & " - " & avData(1, X)
        cmbMaterial.ItemData(cmbMaterial.NewIndex) = avData(0, X)
        'indica se e kit ou nao
        If Val(avData(2, X)) = 2 Or Val(avData(2, X)) = 3 Or Val(avData(2, X)) = 4 Then
           indicaKit = True
        End If
    Next

End Sub


Private Function ValidarVigencia(vTipo, vCod) As Boolean

    Dim vInicio As Long
    Dim vFinal  As Long
 
    vCiclIni = cmbCicloInicial.Text
    vDataIni = txtDtInicio.Text
    vCiclFin = cmbCicloFinal.Text
    vDataFin = txtDtFinal.Text
    
    ValidarVigencia = False

    If Trim(vCiclIni) <> "" And Trim(vCiclFin) <> "" Then
       '
       vInicio = Right(vCiclIni, 4) & Left(vCiclIni, 2)
       vFinal = Right(vCiclFin, 4) & Left(vCiclFin, 2)
       '
       If vFinal < vInicio Then
          MsgBox "A Vig�ncia Final deve ser Igual ou Superior a Vig�ncia Inicial.", vbCritical, "Aten��o"
          Exit Function
       End If
       '
    Else
       'seleciona a vigencia do ciclo
       'data inicial
       If Trim(vCiclIni) <> "" And Trim(vDataIni) = "" Then
          vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
          'vigencia
          If Not SelecionarVigenciaCiclo(vCiclIni, vTipo, vCod) Then Exit Function
          'determinou a data de inicio
          vDataIni = vDataInicioCiclo
       End If
       'data final
       If Trim(vCiclFin) <> "" And Trim(vDataFin) = "" Then
          vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
          'vigencia
          If Not SelecionarVigenciaCiclo(vCiclFin, vTipo, vCod) Then Exit Function
          'determinou a data de inicio
          vDataFin = vDataFinalCiclo
       End If
       'caso nao tenha final
       If Trim(vDataFin) = "" Then
          vDataFin = "31/12/9999"
       End If
       
      'If Trim(vDataIni) = "" Then
      '   MsgBox "N�o Possui Ciclo Cadastrado Para a Estrutura Comercial Selecionada.", vbCritical, "Aten��o"
      '   Exit Function
      'End If
        
       If DateValue(vDataFin) < DateValue(vDataIni) Then
          MsgBox "A Vig�ncia Final deve ser Igual ou Superior a Vig�ncia Inicial.", vbCritical, "Aten��o"
          Exit Function
       End If
    End If
    
    ValidarVigencia = True

End Function

