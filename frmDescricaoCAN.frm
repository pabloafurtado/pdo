VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmDescricaoCAN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o da descri��o de produto usada pelo Capta/CAN"
   ClientHeight    =   4200
   ClientLeft      =   930
   ClientTop       =   2535
   ClientWidth     =   11280
   ControlBox      =   0   'False
   Icon            =   "frmDescricaoCAN.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   11280
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Height          =   1965
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   11025
      Begin VB.TextBox txtDescrCom 
         Height          =   675
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1140
         Width           =   10725
      End
      Begin VB.TextBox txtDescrCAN 
         Height          =   315
         Left            =   1560
         MaxLength       =   40
         TabIndex        =   2
         Top             =   420
         Width           =   6345
      End
      Begin VB.TextBox txtCodVenda 
         Height          =   315
         Left            =   180
         MaxLength       =   6
         TabIndex        =   1
         Top             =   420
         Width           =   1245
      End
      Begin VB.Label lblDescrCom 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o Comercial"
         Height          =   195
         Left            =   150
         TabIndex        =   10
         Top             =   900
         Width           =   1455
      End
      Begin VB.Label lblDescrCan 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o CAN"
         Height          =   195
         Left            =   1590
         TabIndex        =   6
         Top             =   150
         Width           =   1095
      End
      Begin VB.Label lblCodVenda 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Venda"
         Height          =   195
         Left            =   150
         TabIndex        =   5
         Top             =   150
         Width           =   840
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   765
      Left            =   5880
      Picture         =   "frmDescricaoCAN.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3300
      Width           =   1005
   End
   Begin VB.CommandButton cmdSalvar 
      Caption         =   "Salvar"
      Height          =   765
      Left            =   4320
      Picture         =   "frmDescricaoCAN.frx":0614
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3300
      Width           =   1065
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   420
      Top             =   3600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":091E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":0C38
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":0F52
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":126C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblManutDescCan 
      AutoSize        =   -1  'True
      Caption         =   "MANUTEN��O DA DESCRI��O DE PRODUTO USADA PELO CAPTA/CAN"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   2580
      TabIndex        =   8
      Top             =   840
      Width           =   6390
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   9720
      Top             =   3540
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":1586
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":18A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":1BBA
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":1ED4
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":21EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":2508
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":2822
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDescricaoCAN.frx":2B3C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Salvar"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuCancelar 
         Caption         =   "&Cancelar"
         Shortcut        =   ^Q
      End
      Begin VB.Menu MNUBAR1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "Sai&r"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmDescricaoCAN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim cSql          As String
Dim vOperacao     As String


Private Sub ProtegeTela()
        
    txtcodvenda.Text = ""
    txtDescrCAN.Text = ""
    txtDescrCom.Text = ""
    txtDescrCAN.Tag = txtDescrCAN.Text

    txtcodvenda.Enabled = True
    txtDescrCAN.Enabled = False
    txtcodvenda.SetFocus

End Sub
Private Sub DesprotegeTela()
        
    txtcodvenda.Enabled = False
    txtDescrCAN.Enabled = True
    txtDescrCAN.SetFocus

End Sub

Private Sub cmdCancelar_Click()
    
    If txtDescrCAN.Text <> txtDescrCAN.Tag Then
        If MsgBox("Confirma o cancelamento da altera��o?", vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
            'SE 6346 - In�cio
            If Not DesbloqueiaRegistro(2) Then
                MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
            End If
            'SE 6346 - Fim
            ProtegeTela
        Else
            txtDescrCAN.SetFocus
        End If
    Else
        'SE 6346 - In�cio
        If Not DesbloqueiaRegistro(2) Then
            MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        End If
        'SE 6346 - Fim
        ProtegeTela
    End If
    
End Sub

Private Sub cmdSalvar_Click()

    If txtDescrCAN.Text <> txtDescrCAN.Tag Then
        If Trim(txtDescrCAN.Text) = "" Then
            MsgBox "� obrigat�rio preencher a descri��o!"
            Exit Sub
        End If
        If SalvaAlteracao(txtCodVenda, txtDescrCAN) Then
            MsgBox "Descri��o alterada com sucesso!", vbInformation, ""
            txtDescrCAN.Text = txtDescrCAN.Tag
        End If
    End If
    
    'SE 6346 - In�cio
    vOperacao = "A"
    If Not GravaHistorico(2, vOperacao, "Produto", txtcodvenda, "", "", "", "") Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
    End If
    If Not DesbloqueiaRegistro(2) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
    End If
    'SE 6346 - Fim
    
    ProtegeTela
    
End Sub

Private Sub Form_Activate()

    txtcodvenda.SetFocus
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(2) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
    End If
    'SE 6346 - Fim
    
    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub mnuSair_Click()
    If txtDescrCAN.Text <> txtDescrCAN.Tag Then
        If MsgBox("Confirma o cancelamento da altera��o?", vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
            Unload Me
        End If
    Else
        Unload Me
    End If
End Sub

Private Sub mnuSalvar_Click()
    cmdSalvar_Click
End Sub

Private Sub txtCodVenda_KeyPress(KeyAscii As Integer)

    If KeyAscii <> 13 Then
        Exit Sub
    End If
    
    If txtcodvenda <> "" Then
        If IsNumeric(txtcodvenda.Text) Then
            'SE 6346 - In�cio
            Dim CodUsuario As String
            If Not DesbloqueiaRegistro(2) Then
                MsgBox "Falha ao desbloquear registros.", vbCritical, "Erro"
                Exit Sub
            End If
            
            If VerificaBloqueioFormulario(2, CDbl(txtcodvenda.Text), 0, CodUsuario) Then
                ExibeMensagemBloqueio ObtemDescricaoFormulario(1), "Produto", txtcodvenda.Text, Empty, Empty, CodUsuario
                Exit Sub
            End If
            'SE 6346 - Fim
            sCarregaDados txtcodvenda.Text, txtcodvenda, txtDescrCAN, txtDescrCom
        Else
            MsgBox "Digite um c�digo de venda num�rico!!", vbCritical
        End If
    Else
        MsgBox "Digite o c�digo de venda do Produto que deseja pesquisar", vbCritical
    End If
        
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
        Case 1
        mnuSair_Click
    End Select
End Sub


Public Function SalvaAlteracao(codvenda As Long, descrProd As String) As Boolean


    Dim cSql  As String
    Dim cRetorno As String
    
    On Error GoTo Err
    
    SalvaAlteracao = True
    
'   -----------------------
'   EXECUTAR RFC PARA ALTERAR DESCRICAO DO CODIGO DE VENDA CAM NO SAP CRM
    'Valida altera��o no SAP CRM
    cRetorno = ValidaViaSap(codvenda, descrProd)
    If Trim(cRetorno) <> "OK" Then
       MsgBox "A altera��o do material " & codvenda & "-" & descrProd & " n�o foi realizada .( SAP/CRM-" & cRetorno & " )", vbCritical
       SalvaAlteracao = False
       Exit Function
    Else
      '----
        cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
        cSql = cSql & " exec stp_pdr_descricao_can_u " & codvenda & ",'" & descrProd & "', @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
         
        If gObjeto.natPrepareQuery(hSql, cSql) Then
           Do While gObjeto.natFetchNext(hSql)
           Loop
        End If
         
        If gObjeto.nErroSQL <> 0 Then
           MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
           SalvaAlteracao = False
           Exit Function
        End If
         
        sql_cd_ret = CInt(gObjeto.varOutputParam(0))
        sql_cd_opr = gObjeto.varOutputParam(1)
        sql_nm_tab = gObjeto.varOutputParam(2)
        sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
        
        If sql_cd_ret <> 0 Then
           MsgBox "Erro ao executar a stp_pdr_descricao_can_u " & "RETURN STATUS = " & sql_cd_ret & " "
               SalvaAlteracao = False
           Exit Function
        End If
    End If
    
Exit Function

Err:
        MsgBox "Erro ao Salvar Altera��o.", vbCritical, "Aten��o"
            SalvaAlteracao = False
End Function


Public Sub sCarregaDados(codvenda As Long, txtcodvenda As Object, txtDescrCAN As Object, txtDescrCom As Object)

    'Executar a QUERY
    'Declara��o de Vari�veis
    
    Dim avData() As Variant
    Dim cSql     As String
    Dim bResp    As Boolean
    Dim ctReg    As Integer
    
    On Error GoTo Err
'    ReDim avData(2, 0)
    Dim sql_cd_ret As Long
    Dim sql_cd_opr As String
    Dim sql_nm_tab As String
    Dim sql_qt_lnh As Long
        
     cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
     cSql = cSql & " exec stp_pdr_descricao_can_s " & codvenda & ", @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
     
     bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())

     If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
     End If

     
    If IsEmpty(avData(0, 0)) Then
        MsgBox "Produto n�o cadastrado!"
        Exit Sub
    End If

    txtDescrCAN.Text = Trim(avData(0, 0))
    txtDescrCAN.Tag = txtDescrCAN.Text
       
    txtDescrCom = Trim(avData(1, 0))
     
     
    'SE 6346 - In�cio
    If Not BloqueiaRegistro(2, CDbl(codvenda), 0) Then
       MsgBox "Falha ao tentar bloquear registro.", vbCritical, "Erro"
    End If
    'SE 6346 - Fim
     
    DesprotegeTela
Exit Sub

Err:
    MsgBox "Erro " & Err.Number & " - " & Err.Description & " ao pesquisar a descri��o Capta/CAN", vbCritical
End Sub
