VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Begin VB.Form frmPendenciaPCL 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pendências PCL"
   ClientHeight    =   7230
   ClientLeft      =   2250
   ClientTop       =   4320
   ClientWidth     =   13845
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7230
   ScaleWidth      =   13845
   Begin VB.Frame fraMaterial 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8,25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   1245
      Left            =   90
      TabIndex        =   3
      Top             =   5940
      Width           =   13695
      Begin Threed.SSCommand cmdRefresh 
         Height          =   765
         Left            =   3720
         TabIndex        =   4
         Top             =   240
         Width           =   1065
         _Version        =   65536
         _ExtentX        =   1879
         _ExtentY        =   1349
         _StockProps     =   78
         Caption         =   "&Atualizar"
         Outline         =   0   'False
         Picture         =   "frmPendenciaPCL.frx":0000
      End
      Begin Threed.SSCommand cmdSair 
         Cancel          =   -1  'True
         Height          =   765
         Left            =   8790
         TabIndex        =   5
         Top             =   270
         Width           =   1065
         _Version        =   65536
         _ExtentX        =   1879
         _ExtentY        =   1349
         _StockProps     =   78
         Caption         =   "&Sair"
         Outline         =   0   'False
         Picture         =   "frmPendenciaPCL.frx":031A
      End
   End
   Begin VB.Frame fraParte3 
      Height          =   5805
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   13695
      Begin VB.CheckBox chkMostraCanc 
         Caption         =   "Mostrar pendências encerradas"
         Height          =   255
         Left            =   10200
         TabIndex        =   6
         Top             =   300
         Width           =   3195
      End
      Begin FPSpread.vaSpread spdPendencia 
         Height          =   4965
         Left            =   240
         TabIndex        =   1
         Top             =   720
         Width           =   13215
         _Version        =   131077
         _ExtentX        =   23310
         _ExtentY        =   8758
         _StockProps     =   64
         DAutoSizeCols   =   0
         DisplayRowHeaders=   0   'False
         EditEnterAction =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   8
         MaxRows         =   1
         OperationMode   =   2
         ScrollBars      =   2
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmPendenciaPCL.frx":0634
         UserResize      =   1
         VisibleCols     =   500
         VisibleRows     =   500
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Caption         =   "PENDÊNCIAS PCL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   315
         Left            =   5550
         TabIndex        =   2
         Top             =   300
         Width           =   2955
      End
   End
End
Attribute VB_Name = "frmPendenciaPCL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cSql          As String

Private Sub chkMostraCanc_Click()

    Dim vDtEncer As Variant
    Dim X        As Integer

    Me.MousePointer = vbHourglass
    
    spdPendencia.Visible = False
    
    For X = 1 To spdPendencia.MaxRows
     
        spdPendencia.GetText 6, X, vDtEncer
    
        If vDtEncer <> "" Then
           spdPendencia.BlockMode = True
           spdPendencia.Row = X
           spdPendencia.Row2 = X
           spdPendencia.Col = -1
           spdPendencia.Col2 = spdPendencia.MaxCols
           spdPendencia.RowHidden = IIf(chkMostraCanc.Value = 0, True, False)
           spdPendencia.BlockMode = False
        End If
        
    Next

    spdPendencia.Visible = True
  
    Me.MousePointer = 1

End Sub

Private Sub cmdRefresh_Click()

   SelecionarPendenciasPCL

End Sub

Private Sub cmdSair_Click()

    Unload Me
                
End Sub

Private Sub Form_Load()
 
   spdPendencia.Col = 8
   spdPendencia.ColHidden = True

   'carrega as pendencias para o pcl
       
    Me.MousePointer = vbHourglass
   
   SelecionarPendenciasPCL
   
    Me.MousePointer = 1
   
   indicaPCL = False
  
  
End Sub

Private Sub Form_Unload(Cancel As Integer)

    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub spdPendencia_DblClick(ByVal Col As Long, ByVal Row As Long)
   
    Dim vDtEncerra As Variant
    Dim vCodVenda  As Variant
    
    spdPendencia.GetText 6, Row, vDtEncerra
    spdPendencia.GetText 2, Row, vCodVenda
    
    gCodVenda = 0
    If Trim(vDtEncerra) = "" Then
       gCodVenda = vCodVenda
       indicaPCL = True
       frmVigenciaPCL.Show
    End If

End Sub
