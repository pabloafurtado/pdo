VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmDivisaoBoleto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o do indicador de divis�o de boletos"
   ClientHeight    =   3255
   ClientLeft      =   1365
   ClientTop       =   4020
   ClientWidth     =   10035
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   10035
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10035
      _ExtentX        =   17701
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdSalvar 
      Caption         =   "Salvar"
      Height          =   765
      Left            =   3480
      Picture         =   "frmDivisaoBoleto.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2160
      Width           =   1065
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   765
      Left            =   5700
      Picture         =   "frmDivisaoBoleto.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2160
      Width           =   1005
   End
   Begin VB.Frame Frame5 
      Height          =   1005
      Left            =   7080
      TabIndex        =   9
      Top             =   1020
      Width           =   2865
      Begin VB.CheckBox chkbox 
         Caption         =   "Permite a divis�o de boletos"
         Height          =   615
         Left            =   210
         TabIndex        =   4
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   1005
      Left            =   60
      TabIndex        =   1
      Top             =   1020
      Width           =   6945
      Begin VB.TextBox txtCodVenda 
         Height          =   315
         Left            =   180
         TabIndex        =   2
         Top             =   420
         Width           =   1245
      End
      Begin VB.TextBox txtDescricao 
         Height          =   315
         Left            =   1560
         TabIndex        =   3
         Top             =   420
         Width           =   5145
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Venda"
         Height          =   195
         Left            =   150
         TabIndex        =   8
         Top             =   150
         Width           =   840
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         Height          =   195
         Left            =   1590
         TabIndex        =   7
         Top             =   150
         Width           =   720
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   900
      Top             =   2340
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDivisaoBoleto.frx":0614
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDivisaoBoleto.frx":092E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDivisaoBoleto.frx":0C48
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmDivisaoBoleto.frx":0F62
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000A&
      Caption         =   "MANUTEN��O DO INDICADOR DE DIVIS�O DE BOLETOS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   2520
      TabIndex        =   10
      Top             =   720
      Width           =   5085
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Salvar"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuCancelar 
         Caption         =   "&Cancelar"
         Shortcut        =   ^Q
      End
      Begin VB.Menu mnuBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "Sai&r"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmDivisaoBoleto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim axPend As Boolean
Dim cSql          As String

Sub sProtegeTela()

    chkbox.Enabled = False
    cmdSalvar.Enabled = False
    mnuSalvar.Enabled = False

End Sub

Sub sDesprotegeTela()

    chkbox.Enabled = True
    cmdSalvar.Enabled = True
    mnuSalvar.Enabled = True

End Sub



Private Sub chkbox_Click()

    If axPend = False Then
        axPend = True
    Else
        axPend = False
    End If

End Sub

Private Sub cmdSalvar_Click()

    Dim vStat As Integer
    Dim vRet  As Long
    Dim vLin  As Long
    Dim vTab  As String
    Dim vOper As String
    Dim vMsg  As String

    If axPend Then
        If MsgBox("Confirma a Altera��o do Status 'Permite a divis�o de boletos'.", _
                    vbQuestion + vbYesNo, "Aten��o") = vbNo Then Exit Sub
    Else
        MsgBox "N�o foi feita nenhuma altera��o para ser salva", , Me.Caption
        Unload Me
        Exit Sub
    End If
    
    vStat = chkbox.Value
    'Anderson
    nRet = DbOra.DbCall("P_ATUALIZA_DIVISAO_BOLETO", txtCodVenda.Text, vStat, vRet, vLin, vTab, vOper, vMsg)

    If nRet <> "" Then
       MsgBox " Falha na execu��o"
       Exit Sub
    End If
   
    If vMsg <> "OK" Then
    
       MsgBox "Erro ao Alterar o Status. " & vOper & " - " & vTab & " - " & vMsg, "Aten��o"
       
    Else
   
       MsgBox "Altera��o efetuada com sucesso!", vbInformation, "Aviso"
    
       frmVigenciaComercial.chkDivisao.Value = chkbox.Value
    
       Unload Me
       
    End If

End Sub

Private Sub cmdCancelar_Click()

    If axPend = True Then
        If MsgBox("Confirma o cancelamento da altera��o efetuada?", vbYesNo, Me.Caption) = vbYes Then
            Unload Me
        End If
    Else
        Unload Me
    End If

End Sub

Private Sub Form_Activate()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO002", "M") Then
        'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        'sProtegeTela
    'Else
        'sDesprotegeTela
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO002", "M") Then
        MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        sProtegeTela
    Else
        sDesprotegeTela
    End If

    txtCodVenda.Text = frmVigenciaComercial.txtCodVenda.Text
    txtDescricao.Text = frmVigenciaComercial.txtDescricao.Text
    chkbox.Value = frmVigenciaComercial.chkDivisao.Value
    axPend = False
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    'MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub mnuSair_Click()
    cmdCancelar_Click
End Sub

Private Sub mnuSalvar_Click()
    cmdSalvar_Click
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
    Case 1
        cmdCancelar_Click
    End Select
End Sub

