VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmCadEmail 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Emails de Pend�ncias"
   ClientHeight    =   4095
   ClientLeft      =   3885
   ClientTop       =   3300
   ClientWidth     =   6465
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmCadEmail.frx":0000
   ScaleHeight     =   4095
   ScaleWidth      =   6465
   Begin VB.CommandButton cmdAssociar 
      Height          =   435
      Left            =   60
      Picture         =   "frmCadEmail.frx":0542
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Incluir Email"
      Top             =   1680
      Width           =   795
   End
   Begin VB.CommandButton cmdDesassociar 
      Height          =   435
      Left            =   60
      Picture         =   "frmCadEmail.frx":084C
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Excluir Email"
      Top             =   2340
      Width           =   795
   End
   Begin VB.Frame Frame1 
      Height          =   1245
      Left            =   30
      TabIndex        =   2
      Top             =   0
      Width           =   6315
      Begin VB.TextBox txtCodigo 
         Alignment       =   1  'Right Justify
         Height          =   360
         Left            =   1245
         TabIndex        =   4
         Top             =   180
         Width           =   1545
      End
      Begin VB.TextBox txtDescricao 
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   3
         Top             =   660
         Width           =   4935
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   240
         Left            =   150
         TabIndex        =   6
         Top             =   270
         Width           =   660
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o "
         Height          =   240
         Left            =   150
         TabIndex        =   5
         Top             =   720
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1005
      Left            =   90
      TabIndex        =   0
      Top             =   3030
      Width           =   6255
      Begin VB.CommandButton cmdSalvar 
         Caption         =   "Salvar"
         Enabled         =   0   'False
         Height          =   585
         Left            =   2490
         Picture         =   "frmCadEmail.frx":0B56
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   270
         Width           =   1425
      End
   End
   Begin FPSpread.vaSpread grdEmail 
      Height          =   1725
      Left            =   930
      TabIndex        =   7
      Top             =   1290
      Width           =   5415
      _Version        =   131077
      _ExtentX        =   9551
      _ExtentY        =   3043
      _StockProps     =   64
      DisplayColHeaders=   0   'False
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   1
      MaxRows         =   0
      OperationMode   =   3
      ScrollBarExtMode=   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmCadEmail.frx":0E60
      StartingRowNumber=   0
      UserResize      =   2
      VisibleCols     =   5
      VisibleRows     =   20
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   6660
      Top             =   4170
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":11FF
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":1519
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":1833
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":1B4D
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":1E67
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":2181
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":249B
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCadEmail.frx":27B5
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCadEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vAlterar    As Boolean
Dim vCodigo     As String
Dim vDescricao  As String
Dim vEmails     As String
Dim vOper As String * 1
Dim bOk As Boolean
Dim cSql          As String

Public Function OK() As Boolean
    OK = bOk
End Function

Public Sub Incluir()
    Call LimpaCampos
    txtCodigo.Text = "4"
    txtCodigo.Locked = True
    txtdescricao.Enabled = True
    vAlterar = True
    vOper = "I"
'    txtDescricao.SetFocus
    Me.Show 1

End Sub

Private Sub SelCodigo()
    txtCodigo.SelStart = 0
    txtCodigo.SelLength = Len(txtCodigo.Text)
End Sub

Public Sub ConvEmail(ByRef pEmails As String)

    Dim Ini As Long
    Dim Pos As Long
    Dim X As Long
    
    Ini = 1
    Pos = 0
    X = 0
    If Len(pEmails) > 0 Then
        
        Pos = InStr(Ini, pEmails, ",")
        Do While Pos <> 0
            X = X + 1
            grdEmail.MaxRows = X
            grdEmail.Row = X
            grdEmail.Col = 1
            grdEmail.Text = Mid(pEmails, Ini, Pos - Ini)
            Ini = Pos + 1
            Pos = InStr(Ini, pEmails, ",")
        Loop
        X = X + 1
        grdEmail.MaxRows = X
        grdEmail.Row = X
        grdEmail.Col = 1
        grdEmail.Text = Mid(pEmails, Ini, Len(pEmails) - Ini + 1)
    End If
    
End Sub

Public Sub Excluir()
    If Val(txtCodigo.Text) > 0 Then
        If MsgBox("Confirma a exclus�o ?", vbQuestion + vbDefaultButton2 + vbYesNo, "A T E N � � O") = vbYes Then
            MsgBox "Exclus�o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
            LimpaCampos
        End If
    End If
End Sub

Public Sub Alterar(ByRef pCodigo As Long, ByRef pDescricao As String, ByRef pEmails As String)
    Dim X As Long

    txtCodigo.Locked = True
    txtdescricao.Enabled = True
    
    txtCodigo.Text = pCodigo
    txtdescricao.Text = pDescricao
    Call ConvEmail(pEmails)
    vAlterar = True
    vOper = "A"
    
    Me.Show 1
'    txtDescricao.SetFocus
'    pCodigo = CLng(codigoCad)
    pDescricao = descricaoCad
    pEmails = emailCad
    
End Sub

Private Sub Gravar()
    Dim sEmails As String
    Dim X As Long
    
    vCodigo = txtCodigo.Text
    vDescricao = txtdescricao.Text
    sEmails = ""
    For X = 1 To grdEmail.MaxRows
        grdEmail.Row = X
        grdEmail.Col = 1
        sEmails = sEmails & grdEmail.Text & ","
    Next X
    If Len(sEmails) > 1 Then
        If Len(sEmails) > 256 Then
            MsgBox "Tamanho da lista de emails (" & Format(Len(sEmails) - 1) & " posi��es) maior que a capacidade do parametro - 255 posi��es!!", vbCritical
            Exit Sub
        Else
            vEmails = Left(sEmails, Len(sEmails) - 1)
        End If
    End If

    'chama a procedure de ATUALIZA��O dos parametros de email
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    
    If vOper = "A" Then
        cSql = cSql & " exec stp_pdr_cad_email_u " & Val(vCodigo)
        cSql = cSql & ",'" & vDescricao & "', '" & vEmails & "', "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    Else
        cSql = cSql & " exec stp_pdr_cad_email_i " & vCodigo
        cSql = cSql & ",'" & vDescricao & "', '" & vEmails & "', "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    End If
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_cad_email_i " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Sub
    End If
    MsgBox "Grava��o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
    vAlterar = False
    bOk = True
    Unload Me
'    LimpaCampos
End Sub


Private Sub cmdAssociar_Click()

    Dim X                As Double
    Dim vEmail           As String
    Dim vRelacionarEmail As Boolean
        
    vEmail = InputBox("Informe o Email", "Cadastro de Emails")
    
    If vEmail = "" Then Exit Sub
        
    vRelacionarEmail = True

    For X = 1 To grdEmail.MaxRows
        grdEmail.Row = X
        grdEmail.Col = 1
        If vEmail = grdEmail.Text Then
            vRelacionarEmail = False
            Exit For
        End If
    Next X

    If vRelacionarEmail Then
        grdEmail.MaxRows = grdEmail.MaxRows + 1
        grdEmail.Row = grdEmail.MaxRows
        grdEmail.Col = 1
        grdEmail.Text = vEmail
        cmdSalvar.Enabled = True
    End If

End Sub

Private Sub cmdDesassociar_Click()

    Dim X As Double
    
    For X = 1 To grdEmail.MaxRows
        grdEmail.Row = X
        If grdEmail.SelModeSelected Then
            grdEmail.Action = 5
            grdEmail.MaxRows = grdEmail.MaxRows - 1
        End If
    Next

End Sub

Private Sub cmdSalvar_Click()
    
    Call Gravar

End Sub

Private Sub txtDescricao_Change()
    If vAlterar Then
        cmdSalvar.Enabled = True
    End If
End Sub

Private Sub LimpaCampos()
    txtCodigo.Text = ""
    txtCodigo.Locked = False
'    txtCodigo.SetFocus
    txtdescricao.Text = ""
    txtdescricao.Enabled = False
    cmdSalvar.Enabled = False
    grdEmail.MaxRows = 0
End Sub

Public Function codigoCad() As String
    codigoCad = vCodigo
End Function

Public Function descricaoCad() As String
    descricaoCad = vDescricao
End Function

Public Function emailCad() As String
    emailCad = vEmails
End Function
