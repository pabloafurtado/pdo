VERSION 5.00
Begin VB.Form frmCadMeioCmc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro Meio de Comunica��o"
   ClientHeight    =   2355
   ClientLeft      =   1530
   ClientTop       =   2925
   ClientWidth     =   7665
   Icon            =   "frmCadMeioCmc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   7665
   Begin VB.Frame Frame1 
      Height          =   1245
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7575
      Begin VB.TextBox txtCodigo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   0
         Top             =   180
         Width           =   1545
      End
      Begin VB.TextBox txtDescricao 
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   1
         Top             =   660
         Width           =   4935
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   240
         Left            =   150
         TabIndex        =   7
         Top             =   270
         Width           =   660
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o "
         Height          =   240
         Left            =   150
         TabIndex        =   6
         Top             =   720
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1035
      Left            =   1440
      TabIndex        =   4
      Top             =   1290
      Width           =   4575
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   645
         Left            =   2460
         Picture         =   "frmCadMeioCmc.frx":08E2
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   270
         Width           =   1425
      End
      Begin VB.CommandButton cmdSalvar 
         Caption         =   "Salvar"
         Enabled         =   0   'False
         Height          =   645
         Left            =   660
         Picture         =   "frmCadMeioCmc.frx":0BEC
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   270
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmCadMeioCmc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vAlterar    As Boolean
Dim vOper       As String * 1
Dim vCodigo     As String
Dim vDescricao  As String
Dim vDataInicial As String
Dim vDataFinal  As String
Dim vOK         As Integer
Dim cSql          As String

Public Sub Incluir(maxCodigo As Long)
    txtCodigo.Text = maxCodigo + 1
    txtCodigo.Locked = True
    txtdescricao.Enabled = True
    vAlterar = True
    vOper = "I"
    Me.Show 1
End Sub

Public Sub Alterar(pCodigo As Long, pDescricao As String)
    txtdescricao.Enabled = True
    txtCodigo.Text = pCodigo
    txtdescricao.Text = pDescricao
    vAlterar = True
    vOper = "A"
    Me.Show 1
End Sub

Private Sub SelCodigo()
    txtCodigo.SelStart = 0
    txtCodigo.SelLength = Len(txtCodigo.Text)
End Sub

Private Sub Gravar()
    
    If Not ValidaCampos Then
        Exit Sub
    End If

    vCodigo = txtCodigo.Text
    vDescricao = txtdescricao.Text
    vOK = vbCancel
    
    'chama a procedure de cancelamento da Abrangencia Vigencia
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    
    If vOper = "A" Then
        cSql = cSql & " exec stp_pdr_cad_meio_comunic_u " & Val(vCodigo) & ",'" & vDescricao & "', "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    Else
        cSql = cSql & " exec stp_pdr_cad_meio_comunic_i " & vCodigo & ",'" & vDescricao & "', "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    End If
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Sub
    End If
           
    vOK = vbOK
    MsgBox "Grava��o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    vOK = vbCancel
    Unload Me
End Sub

Private Sub cmdSalvar_Click()
    Call Gravar
End Sub

Private Sub Form_Load()
    vOK = vbCancel
End Sub

Private Sub txtDescricao_Change()
    If vAlterar Then
        cmdSalvar.Enabled = True
    End If
End Sub

Public Function codigoCad() As String
    codigoCad = vCodigo
End Function

Public Function descricaoCad() As String
    descricaoCad = vDescricao
End Function

Public Function OK() As Integer
    OK = vOK
End Function

Private Function ValidaCampos() As Boolean
    
    ValidaCampos = False

    If Trim(txtdescricao.Text) = "" Then
        MsgBox "� obrigat�rio o preenchimento da descri��o do meio de comunica��o!!"
        Exit Function
    End If
    
    ValidaCampos = True
    
End Function
