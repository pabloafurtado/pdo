VERSION 5.00
Begin VB.Form frmContrLog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Controle Geral Log�stico"
   ClientHeight    =   2805
   ClientLeft      =   1665
   ClientTop       =   6585
   ClientWidth     =   10080
   Icon            =   "frmContrLog.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2805
   ScaleWidth      =   10080
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   3180
      TabIndex        =   7
      Top             =   1680
      Width           =   3525
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   765
         Left            =   2040
         Picture         =   "frmContrLog.frx":0442
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   180
         Width           =   1125
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "&Salvar"
         Default         =   -1  'True
         Height          =   765
         Left            =   420
         Picture         =   "frmContrLog.frx":074C
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   180
         Width           =   1065
      End
   End
   Begin VB.Frame Frame5 
      Height          =   705
      Left            =   3600
      TabIndex        =   5
      Top             =   960
      Width           =   2715
      Begin VB.CheckBox chkbox 
         Caption         =   "Controle Geral log�stico"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   180
         Width           =   2475
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   945
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10065
      Begin VB.TextBox txtCodVenda 
         Height          =   315
         Left            =   180
         TabIndex        =   2
         Top             =   420
         Width           =   1245
      End
      Begin VB.TextBox txtDescricao 
         Height          =   315
         Left            =   1560
         TabIndex        =   1
         Top             =   420
         Width           =   8325
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Venda"
         Height          =   240
         Left            =   150
         TabIndex        =   4
         Top             =   150
         Width           =   1065
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         Height          =   240
         Left            =   1590
         TabIndex        =   3
         Top             =   150
         Width           =   1050
      End
   End
End
Attribute VB_Name = "frmContrLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cSql          As String

Sub sProtegeTela()

    chkbox.Enabled = False
    cmdConfirmar.Enabled = False

End Sub

Sub sDesprotegeTela()

    chkbox.Enabled = True
    cmdConfirmar.Enabled = True

End Sub

Private Sub cmdConfirmar_Click()
    
    If chkbox.Value = 1 Then
    
       If VerificarAbrangenciaAtiva = True Then
       
          If MsgBox("Aten��o - O Controle Geral Logistico acionado far� com que o controle de abrang�ncia " + Chr(13) + Chr(10) + _
                    "                 fique somente com a �rea de logistica. " + Chr(13) + Chr(10) + _
                    "                 � importante informar a �rea de logistica sobre esta altera��o" + Chr(13) + Chr(13) + Chr(10) + _
                    "                 Confirma a altera��o do Controle?", vbExclamation + vbCritical + vbYesNo + vbDefaultButton2) = vbNo Then
                          
             frmVigenciaComercial.chkCGL.Value = 0
             
             Exit Sub
          End If
                    
          'rotina de Atualizacao do ID de Controle
          If Not AtualizarControlePCL Then Exit Sub
               
          frmVigenciaComercial.spdAVIG.MaxRows = 0
          frmVigenciaComercial.spdAVIG.Enabled = False
          frmVigenciaComercial.Toolbar1.Buttons(3).Enabled = False
          frmVigenciaComercial.Toolbar1.Buttons(4).Enabled = False
          frmVigenciaComercial.Toolbar1.Buttons(5).Enabled = False
          frmVigenciaComercial.chkCGL.Value = chkbox.Value
             
       Else
       
            'rotina de Atualizacao do ID de Controle
            If Not AtualizarControlePCL Then Exit Sub
        
            frmVigenciaComercial.spdAVIG.MaxRows = 0
            frmVigenciaComercial.spdAVIG.Enabled = False
            frmVigenciaComercial.Toolbar1.Buttons(3).Enabled = False
            frmVigenciaComercial.Toolbar1.Buttons(4).Enabled = False
            frmVigenciaComercial.Toolbar1.Buttons(5).Enabled = False
            frmVigenciaComercial.chkCGL.Value = chkbox.Value
            
       End If
        
    Else
    
        'rotina de Atualizacao do ID de Controle
        If Not AtualizarControlePCL Then Exit Sub
    
        frmVigenciaComercial.spdAVIG.Enabled = True
        frmVigenciaComercial.Toolbar1.Buttons(3).Enabled = True
        frmVigenciaComercial.Toolbar1.Buttons(4).Enabled = True
        frmVigenciaComercial.Toolbar1.Buttons(5).Enabled = True
        frmVigenciaComercial.chkCGL.Value = chkbox.Value
        
    End If
    
    MsgBox "Indicador de Controle Geral Log�stico Alterado com Sucesso.", vbInformation, "Aviso"
    
    'mostrar novamente as abrangencias / vigencias
    SelecionarAbrangenciaVigencia
    
    Unload Me

End Sub

Private Sub cmdCancelar_Click()

        Unload Me

End Sub

Private Sub Form_Activate()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO002", "M") Then
        'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        'sProtegeTela
    'Else
        'sDesprotegeTela
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO002", "M") Then
        MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        sProtegeTela
    Else
        sDesprotegeTela
    End If
    

    txtCodVenda.Text = frmVigenciaComercial.txtCodVenda.Text
    txtDescricao.Text = frmVigenciaComercial.txtDescricao.Text
    chkbox.Value = frmVigenciaComercial.chkCGL.Value

End Sub

Private Function VerificarAbrangenciaAtiva() As Boolean
 
      Dim X     As Integer
      Dim vStat As Variant

      VerificarAbrangenciaAtiva = False
      
      For X = 1 To frmVigenciaComercial.spdAVIG.MaxRows
      
          frmVigenciaComercial.spdAVIG.GetText 6, X, vStat
    
          If vStat = "ATIVA" Then
             VerificarAbrangenciaAtiva = True
             Exit Function
          End If
          
      Next

End Function

Private Function AtualizarControlePCL() As Boolean

     AtualizarControlePCL = False

     cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
     cSql = cSql & " exec stp_produto_venda_contr_pcl_u " & txtCodVenda.Text & " , " & IIf(chkbox.Value = 1, 1, 2) & _
     ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
     
     If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
     End If
     
     If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
     End If

     sql_cd_ret = CInt(gObjeto.varOutputParam(0))
     sql_cd_opr = gObjeto.varOutputParam(1)
     sql_nm_tab = gObjeto.varOutputParam(2)
     sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
     
     If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a stp_produto_venda_contr_pcl_u " & "RETURN STATUS = " & sql_cd_ret & " "
        Exit Function
     End If
     
     AtualizarControlePCL = True

End Function

Private Sub SelecionarAbrangenciaVigencia()

   Dim avData() As Variant
   Dim X        As Integer
   Dim vTipoEst As String

   ReDim avData(0, 0)
   ReDim avParametros(0, 1)
      
   frmVigenciaComercial.spdAVIG.MaxRows = 0
      
   avParametros(0, AV_CONTEUDO) = frmVigenciaComercial.txtCodVenda.Text
   avParametros(0, AV_TIPO) = TIPO_NUMERICO
        
   cSql = " stp_abrangencia_prod_venda_s " & FormataParametrosSQL(avParametros)
        
   bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
   If bResp = False Then
      Exit Sub
   End If
        
   If gObjeto.nErroSQL <> 0 Then
      MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
      Exit Sub
   End If
      
   If Val(avData(0, 0)) <= 0 Then Exit Sub
   
   
   'Colunas do Spread
   '01 - ABRANG
   '02 - CICLO INI
   '03 - DATA INI
   '04 - CICLO FIN
   '05 - DATA FIN
   '06 - STATUS
   '07 - SEQUENCIA - CPO TABELA
   '08 - TIPO DE ESTRUTURA
   '09 - COD ESTRUTURA
   '10 - RE
   '11 - GV
   '12 - SET
   '13 - CICLO INI ANT
   '14 - DATA INI ANT
   '15 - CICLO FIN ANT
   '16 - DATA FIN ANT
   '17 - OPERACAO - (I)NCLUIR, (A)LTERAR

   
   For X = 0 To UBound(avData, 2)
   
       With frmVigenciaComercial.spdAVIG
   
            .MaxRows = .MaxRows + 1
            
            vTipoEst = ""
            'tipo estrutura comercial
            If avData(9, X) = "2" Then
               vTipoEst = "RE - "
            ElseIf avData(9, X) = "3" Then
                   vTipoEst = "GV - "
            ElseIf avData(9, X) = "4" Then
                   vTipoEst = "ST - "
            End If
       
            'abrangencia
            .SetText 1, .MaxRows, vTipoEst & avData(1, X) & " - " & avData(2, X)
            'ciclo inic
            If avData(3, X) <> "" Then
               .SetText 2, .MaxRows, Right(avData(3, X), 2) & "/" & Left(avData(3, X), 4)
            End If
            'data inicio
            .SetText 3, .MaxRows, avData(4, X)
            'ciclo fim
            If avData(5, X) <> "" Then
               .SetText 4, .MaxRows, Right(avData(5, X), 2) & "/" & Left(avData(5, X), 4)
            End If
            'data final
            .SetText 5, .MaxRows, avData(6, X)
        
            'status
            If Val(avData(7, X)) = 1 Then
               'cancelada
               .SetText 6, .MaxRows, "CANCELADA"
               .BlockMode = True
               .Row = .MaxRows
               .Row2 = .MaxRows
               .Col = -1
               .Col2 = .MaxCols
               .BackColor = vbYellow
               .RowHidden = True
               .BlockMode = False
            Else
               'ativa
               .SetText 6, .MaxRows, "ATIVA"
            End If
        
            'sequencia
            .SetText 7, .MaxRows, avData(8, X)
            'tipo estrutura comercial
            .SetText 8, .MaxRows, avData(9, X)
            'cod estrutura comercial
            .SetText 9, .MaxRows, avData(1, X)
            'regiao estrategia
            .SetText 10, .MaxRows, avData(10, X)
            'gerencia vendas
            .SetText 11, .MaxRows, avData(11, X)
            'setor
            .SetText 12, .MaxRows, avData(12, X)
            
            'ciclo inicio ant
            If avData(3, X) <> "" Then
               .SetText 13, .MaxRows, Right(avData(3, X), 2) & "/" & Left(avData(3, X), 4)
            End If
            'data inicio ant
            .SetText 14, .MaxRows, avData(4, X)
            'ciclo fim ant
            If avData(5, X) <> "" Then
               .SetText 15, .MaxRows, Right(avData(5, X), 2) & "/" & Left(avData(5, X), 4)
            End If
            'data final ant
            .SetText 16, .MaxRows, avData(6, X)
            
       End With
       
   Next
    
End Sub

