VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmRelPrdMeioCmc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relaciona Produto X Meio de Comunica��o"
   ClientHeight    =   7515
   ClientLeft      =   465
   ClientTop       =   2130
   ClientWidth     =   15165
   Icon            =   "frmRelPrdMeioCmc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   15165
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   15165
      _ExtentX        =   26749
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   5
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Nova pesquisa"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            Object.Width           =   1000
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Pesquisar"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   4
            Object.Width           =   1500
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraRelacao 
      Height          =   4815
      Left            =   0
      TabIndex        =   5
      Top             =   1560
      Width           =   15135
      Begin VB.CommandButton cmdAssociar 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7260
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Incluir Relacionamento"
         Top             =   1140
         Width           =   675
      End
      Begin VB.CommandButton cmdAssociarTudo 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7260
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Excluir Relacionamento"
         Top             =   1800
         Width           =   675
      End
      Begin VB.CommandButton cmdDesassociar 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7260
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Excluir Relacionamento"
         Top             =   3210
         Width           =   675
      End
      Begin VB.CommandButton cmdDesassociarTudo 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7260
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Excluir Relacionamento"
         Top             =   3870
         Width           =   675
      End
      Begin FPSpread.vaSpread grdProdNRel 
         Height          =   4095
         Left            =   90
         TabIndex        =   18
         Top             =   660
         Width           =   7125
         _Version        =   131077
         _ExtentX        =   12568
         _ExtentY        =   7223
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   4
         MaxRows         =   1
         OperationMode   =   5
         ScrollBars      =   2
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmRelPrdMeioCmc.frx":0322
         UserResize      =   2
         VisibleCols     =   5
         VisibleRows     =   20
      End
      Begin FPSpread.vaSpread grdProdRel 
         Height          =   4095
         Left            =   7950
         TabIndex        =   19
         Top             =   660
         Width           =   7125
         _Version        =   131077
         _ExtentX        =   12568
         _ExtentY        =   7223
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GridColor       =   8421504
         MaxCols         =   4
         MaxRows         =   1
         OperationMode   =   5
         ScrollBars      =   2
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmRelPrdMeioCmc.frx":0692
         UserResize      =   2
         VisibleCols     =   5
         VisibleRows     =   20
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Produtos relacionados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8700
         TabIndex        =   15
         Top             =   300
         Width           =   5775
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Produtos n�o relacionados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   7395
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   15135
      Begin VB.ComboBox cmbMeioCom 
         Height          =   360
         ItemData        =   "frmRelPrdMeioCmc.frx":09F6
         Left            =   8640
         List            =   "frmRelPrdMeioCmc.frx":0A03
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   300
         Width           =   4785
      End
      Begin VB.ComboBox cmbCiclo 
         Height          =   360
         ItemData        =   "frmRelPrdMeioCmc.frx":0A4D
         Left            =   1410
         List            =   "frmRelPrdMeioCmc.frx":0A78
         TabIndex        =   2
         Top             =   270
         Width           =   1605
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Meio de Comunica��o"
         Height          =   240
         Left            =   6420
         TabIndex        =   3
         Top             =   360
         Width           =   2025
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo / Ano"
         Height          =   240
         Left            =   210
         TabIndex        =   1
         Top             =   330
         Width           =   960
      End
   End
   Begin VB.Frame Frame8 
      Height          =   1005
      Left            =   1620
      TabIndex        =   7
      Top             =   6450
      Width           =   12135
      Begin ComctlLib.ProgressBar pgbProcesso 
         Height          =   435
         Left            =   2970
         TabIndex        =   16
         Top             =   480
         Visible         =   0   'False
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   767
         _Version        =   327682
         Appearance      =   1
      End
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   735
         Left            =   9570
         Picture         =   "frmRelPrdMeioCmc.frx":0AF1
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   180
         Width           =   1545
      End
      Begin VB.CommandButton cmdSalvar 
         Caption         =   "&Salvar"
         Default         =   -1  'True
         Height          =   735
         Left            =   1080
         Picture         =   "frmRelPrdMeioCmc.frx":0DFB
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   180
         Width           =   1545
      End
      Begin VB.Label lblProcesso 
         Caption         =   "Aguarde... efetuando as altera��es solicitadas!"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3000
         TabIndex        =   17
         Top             =   180
         Visible         =   0   'False
         Width           =   4530
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   390
      Top             =   6330
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":1105
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":141F
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":1739
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":1A53
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":1D6D
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":2087
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":23A1
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":26BB
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmRelPrdMeioCmc.frx":29D5
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmRelPrdMeioCmc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cSql          As String
Dim strOperacao   As String 'SE 6346

Private Function CarregarProdutosdoCiclo() As Boolean

    Dim X          As Long
    Dim lCiclo     As Long
    Dim iMeio      As Integer
    
    CarregarProdutosdoCiclo = False
    
    Me.MousePointer = vbHourglass
    Refresh
    
    ReDim avData(0, 0)
    
    lCiclo = Val(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2))
    iMeio = cmbMeioCom.ItemData(cmbMeioCom.ListIndex)

    cSql = "exec stp_pdr_rel_meio_prod_s " & lCiclo & ", " & iMeio

    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)

    If bResp = False Then
        Me.MousePointer = vbNormal
        Refresh
        Exit Function
    End If

    If gObjeto.nErroSQL <> 0 Then
        Me.MousePointer = vbNormal
        Refresh
        MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
        Exit Function
    End If
    strOperacao = "A" 'SE 6346
    If Val(avData(2, 0)) = 0 Then
        strOperacao = "I" 'SE 6346
        If MsgBox("N�o existe nenhum produto relacionado para este ciclo/Meio..." & Chr(10) & Chr(13) & _
                  "Deseja copiar o relacionamento do ultimo ciclo cadastrado?", vbYesNo + vbExclamation) = vbYes Then
            If sCarrCicloAnt(lCiclo, iMeio) Then
                CarregarProdutosdoCiclo = True
                Me.MousePointer = vbNormal
                Refresh
                Exit Function
            End If
        End If
    End If
    
    grdProdNRel.Visible = False
    grdProdRel.Visible = False
    
    grdProdNRel.MaxRows = 0
    grdProdRel.MaxRows = 0
    
    For X = 0 To UBound(avData, 2)
        
        If avData(2, X) = "0" Then
            With grdProdNRel
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, avData(0, X)
                .SetText 2, .MaxRows, avData(1, X)
                .SetText 3, .MaxRows, avData(2, X)
                .SetText 4, .MaxRows, avData(3, X)
            End With
        Else
            With grdProdRel
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, avData(0, X)
                .SetText 2, .MaxRows, avData(1, X)
                .SetText 3, .MaxRows, avData(2, X)
                .SetText 4, .MaxRows, avData(3, X)
            End With
        End If
    Next X

    grdProdNRel.Visible = True
    grdProdRel.Visible = True
    
    Me.MousePointer = vbNormal
    Refresh
   
    CarregarProdutosdoCiclo = True
    
End Function

Private Function sCarrCicloAnt(lCiclo As Long, iMeio As Integer) As Boolean

    Dim vSequencia As Variant
    Dim X          As Long
    Dim vLin       As Long
    Dim sCateg     As String
    
    sCarrCicloAnt = False
    
    ReDim avData(0, 0)
    
    lCiclo = Val(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2))
    iMeio = cmbMeioCom.ItemData(cmbMeioCom.ListIndex)

    cSql = "exec stp_pdr_rel_meio_prod_ant_s " & lCiclo & ", " & iMeio

    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)

    If bResp = False Then
       Exit Function
    End If

    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Function
    End If
   
    If Val(avData(2, 0)) = 0 Then
        MsgBox "N�o existe nenhuma rela��o cadastrada para este meio de comunica��o..." & Chr(10) & Chr(13) & _
                  "por isto n�o foi possivel carregar de ciclos anteriores!", vbExclamation
        Exit Function
    End If
    
    grdProdNRel.Visible = False
    grdProdRel.Visible = False
    cmdSalvar.Enabled = True
    cmdCancelar.Enabled = True
    
    grdProdNRel.MaxRows = 0
    grdProdRel.MaxRows = 0
    
    For X = 0 To UBound(avData, 2)
        
        If avData(2, X) = "0" Then
            With grdProdNRel
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, avData(0, X)
                .SetText 2, .MaxRows, avData(1, X)
                .SetText 3, .MaxRows, 0
                .SetText 4, .MaxRows, avData(3, X)
            End With
        Else
            With grdProdRel
                .MaxRows = .MaxRows + 1
                .SetText 1, .MaxRows, avData(0, X)
                .SetText 2, .MaxRows, avData(1, X)
                .SetText 3, .MaxRows, 0
                .SetText 4, .MaxRows, avData(3, X)
            End With
        End If
    Next X

    grdProdNRel.Visible = True
    grdProdRel.Visible = True
    
    sCarrCicloAnt = True
    


End Function


Private Sub cmdAssociar_Click()

    Dim X               As Double
    Dim Y               As Double
    Dim vCodigoMeio     As String
    Dim vRelacionarMeio As Boolean
   
    With grdProdRel
        .Visible = False
        For X = 1 To grdProdNRel.MaxRows
            grdProdNRel.Row = X
            If grdProdNRel.SelModeSelected Then
                .MaxRows = .MaxRows + 1
                .Row = .MaxRows
                For Y = 1 To .MaxCols
                grdProdNRel.Col = Y
                .SetText Y, .MaxRows, grdProdNRel.Text
                Next Y
            End If
        Next X
        .Visible = True
    End With

    With grdProdNRel
        .Visible = False
        For X = 1 To .MaxRows
            .Row = X
            If .SelModeSelected Then
                .BlockMode = True
                .Col = -1
                .Col2 = .MaxCols
                .Row2 = .Row
                'Delete a row in the spreadsheet
                .Action = 5
                .BlockMode = False
                .MaxRows = .MaxRows - 1
                cmdSalvar.Enabled = True
                cmdCancelar.Enabled = True
                X = X - 1
            End If
        Next X
        .Visible = True
    End With

End Sub

Private Sub cmdAssociarTudo_Click()

    Dim X               As Double
    Dim Y               As Double
    Dim vCodigoMeio     As String
    Dim vRelacionarMeio As Boolean
    
    cmdSalvar.Enabled = True
    cmdCancelar.Enabled = True
    
    With grdProdRel
        .Visible = False
        For X = 1 To grdProdNRel.MaxRows
            grdProdNRel.Row = X
            .MaxRows = .MaxRows + 1
            .Row = .MaxRows
            For Y = 1 To .MaxCols
                grdProdNRel.Col = Y
                .SetText Y, .MaxRows, grdProdNRel.Text
            Next Y
        Next X
        .Visible = True
    End With
    
    With grdProdNRel
        .Visible = False
        For X = 1 To .MaxRows
            .Row = X
            .BlockMode = True
            .Col = -1
            .Col2 = -1
            .Row2 = .Row
            'Delete a row in the spreadsheet
            .Action = 5
            .BlockMode = False
            .MaxRows = .MaxRows - 1
        Next X
        .Visible = True
    End With

End Sub

Private Sub cmdCancelar_Click()
    If MsgBox("Confirma o Cancelamento da Manuten��o?", vbQuestion + vbYesNo) = vbNo Then
       Exit Sub
    End If
            
    'limpar campos
    'protege os controles
    ProtegeTela
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(9) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
End Sub

Private Sub cmdDesassociar_Click()

    Dim X               As Double
    Dim Y               As Double
    Dim vCodigoMeio     As String
    Dim vRelacionarMeio As Boolean
   
    
    With grdProdNRel
        .Visible = False
        For X = 1 To grdProdRel.MaxRows
            grdProdRel.Row = X
            If grdProdRel.SelModeSelected Then
                .MaxRows = .MaxRows + 1
                .Row = .MaxRows
                For Y = 1 To .MaxCols
                    grdProdRel.Col = Y
                    .SetText Y, .MaxRows, grdProdRel.Text
                Next Y
            End If
        Next X
        .Visible = True
    End With
    
    With grdProdRel
        .Visible = False
        For X = 1 To .MaxRows
            .Row = X
            If .SelModeSelected Then
                .BlockMode = True
                .Col = -1
                .Col2 = .MaxCols
                .Row2 = .Row
                'Delete a row in the spreadsheet
                .Action = 5
                .BlockMode = False
                .MaxRows = .MaxRows - 1
                cmdSalvar.Enabled = True
                cmdCancelar.Enabled = True
                X = X - 1
            End If
        Next X
        .Visible = True
    End With

End Sub

Private Sub cmdDesassociarTudo_Click()

    Dim X               As Double
    Dim Y               As Double
    Dim vCodigoMeio     As String
    Dim vRelacionarMeio As Boolean
    
    cmdSalvar.Enabled = True
    cmdCancelar.Enabled = True
   
    With grdProdNRel
        .Visible = False
        For X = 1 To grdProdRel.MaxRows
            grdProdRel.Row = X
            .MaxRows = .MaxRows + 1
            .Row = .MaxRows
            For Y = 1 To .MaxCols
                grdProdRel.Col = Y
                .SetText Y, .MaxRows, grdProdRel.Text
            Next Y
        Next X
        .Visible = True
    End With
    
    With grdProdRel
        .Visible = False
        For X = 1 To .MaxRows
            .Row = X
            .BlockMode = True
            .Col = -1
            .Col2 = -1
            .Row2 = .Row
            'Delete a row in the spreadsheet
            .Action = 5
            .BlockMode = False
            .MaxRows = .MaxRows - 1
        Next X
        .Visible = True
    End With


End Sub

Private Sub cmdSalvar_Click()

Dim X As Long
Dim lCodVenda As Long
Dim lQtReg As Long

    Me.MousePointer = vbHourglass
    
    lQtReg = grdProdNRel.MaxRows + grdProdRel.MaxRows
    
    pgbProcesso.Value = 0
    pgbProcesso.Max = lQtReg
    
    lblProcesso.Visible = True
    pgbProcesso.Visible = True
    
    Refresh
    
    If Not Runquery("BEGIN TRANSACTION", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        PonteiroMouse vbArrow
        Exit Sub
    End If
    
    With grdProdNRel
        For X = 1 To .MaxRows
            pgbProcesso.Value = pgbProcesso.Value + 1
            .Col = 3
            .Row = X
            If .Text = "1" Then
                .Col = 1
                lCodVenda = Val(.Text)
                If Not ExcluiRelac(lCodVenda) Then
                    Exit Sub
                End If
            End If
        Next X
    End With
    
    With grdProdRel
        For X = 1 To .MaxRows
            pgbProcesso.Value = pgbProcesso.Value + 1
            .Col = 3
            .Row = X
            If .Text = "0" Then
                .Col = 1
                lCodVenda = Val(.Text)
                If Not IncluiRelac(lCodVenda) Then
                    Exit Sub
                End If
            End If
        Next X
    End With
    
    If Not Runquery("COMMIT", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        PonteiroMouse vbArrow
        Exit Sub
    End If
    
    MsgBox "Altera��o efetuada com sucesso!", vbExclamation
    'SE 6346 - In�cio
    If Not GravaHistorico(9, strOperacao, "Ciclo", Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2), "Meio de Comunica��o", CStr(cmbMeioCom.ItemData(cmbMeioCom.ListIndex)), "", "") Then
        MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
        Exit Sub
    End If
    If Not DesbloqueiaRegistro(9) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
    Me.MousePointer = vbNormal
    pgbProcesso.Value = 0
    
    lblProcesso.Visible = False
    pgbProcesso.Visible = False
        
    Refresh
    
    ProtegeTela

End Sub

Private Sub Form_Load()

    If Not CarregaCombos Then
        Unload Me
    End If

    ProtegeTela

End Sub
Sub ProtegeTela()

    
    fraRelacao.Enabled = False
    
    grdProdRel.MaxRows = 0
    grdProdNRel.MaxRows = 0
    
    cmbCiclo.ListIndex = -1
    cmbMeioCom.ListIndex = -1
    
    cmbCiclo.Enabled = True
    cmbMeioCom.Enabled = True
    
    Toolbar1.Buttons(1).Enabled = False
    Toolbar1.Buttons(3).Enabled = True
    
    cmdSalvar.Enabled = False
    cmdCancelar.Enabled = False
    
    
    cmdAssociar.Enabled = False
    cmdAssociarTudo.Enabled = False
    cmdDesassociar.Enabled = False
    cmdDesassociarTudo.Enabled = False
            
    Me.Show
    cmbCiclo.SetFocus

End Sub
Function CarregaCombos() As Boolean
    
    Dim X          As Long
    Dim vLin       As Long
    
    CarregaCombos = False
    
    Call CarrCicloComPreco(cmbCiclo)

    Call CarregaMeio(cmbMeioCom)

    CarregaCombos = True

End Function


Sub DesprotegeTela()

    fraRelacao.Enabled = True
    
    cmbCiclo.Enabled = False
    cmbMeioCom.Enabled = False
    
    Toolbar1.Buttons(1).Enabled = True
    Toolbar1.Buttons(3).Enabled = False
    
    cmdAssociar.Enabled = True
    cmdAssociarTudo.Enabled = True
    cmdDesassociar.Enabled = True
    cmdDesassociarTudo.Enabled = True
    
    grdProdRel.SetFocus

End Sub

Private Sub Form_Unload(Cancel As Integer)

    MDIpdo001.DesbloqueiaMenu
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(9) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)

 
    Select Case Button.Index
    
        Case 1
            If cmdSalvar.Enabled = True Then
               If MsgBox("Confirma o Cancelamento da Manuten��o?", vbQuestion + vbYesNo) = vbNo Then
                  Exit Sub
               End If
            End If
            
            'limpar campos
            'protege os controles
            ProtegeTela
            'SE 6346 - In�cio
            If Not DesbloqueiaRegistro(9) Then
                MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
                Exit Sub
            End If
            'SE 6346 - Fim
        Case 3
            
            If cmbCiclo.ListIndex = -1 Then
                MsgBox "Informe um Ciclo ", vbInformation, Me.Caption
                Exit Sub
            End If
            
            If cmbMeioCom.ListIndex = -1 Then
                MsgBox "Informe um Meio de Comunica��o ", vbInformation, Me.Caption
                Exit Sub
            End If
            
            'SE 6346 - In�cio
            If Not DesbloqueiaRegistro(9) Then
                MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
                Exit Sub
            End If
            Dim CodUsuario As String
            If VerificaBloqueioFormulario(9, CDbl(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2)), CDbl(cmbMeioCom.ItemData(cmbMeioCom.ListIndex)), CodUsuario) Then
                ExibeMensagemBloqueio ObtemDescricaoFormulario(9), "Ciclo", Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2), "Meio Comunica��o", CStr(cmbMeioCom.ItemData(cmbMeioCom.ListIndex)), CodUsuario
                Exit Sub
            End If
            'SE 6346 - Fim

            DesprotegeTela
            If Not CarregarProdutosdoCiclo Then
                Unload Me
            End If
            
            'SE 6346 - In�cio
            If Not BloqueiaRegistro(9, CDbl(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2)), CDbl(cmbMeioCom.ItemData(cmbMeioCom.ListIndex))) Then
                MsgBox "Falha ao bloquear registro.", vbCritical, "Erro"
                Exit Sub
            End If
            'SE 6346 - Fim
        Case 5
            If cmdSalvar.Enabled = True Then
               If MsgBox("Confirma o Cancelamento da Manuten��o?", vbQuestion + vbYesNo) = vbNo Then
                  Exit Sub
               End If
            End If
            Unload Me
    
    End Select

End Sub

Private Function IncluiRelac(lCodVenda As Long) As Boolean
    
    Dim lCiclo     As Long
    Dim iMeio      As Integer
    
    IncluiRelac = False
    
    ReDim avData(0, 0)
    
    lCiclo = Val(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2))
    iMeio = cmbMeioCom.ItemData(cmbMeioCom.ListIndex)

    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    
    cSql = cSql & " exec stp_pdr_rel_prod_meio_i " & lCiclo & ", "
    cSql = cSql & iMeio & ", " & lCodVenda & ", "
    cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
    cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_rel_prod_meio_i " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Function
    End If
           
    IncluiRelac = True
           
           
End Function

Private Function ExcluiRelac(lCodVenda As Long) As Boolean
    
    Dim lCiclo     As Long
    Dim iMeio      As Integer
    
    ExcluiRelac = False
    
    ReDim avData(0, 0)
    
    lCiclo = Val(Right(cmbCiclo.Text, 4) & Left(cmbCiclo.Text, 2))
    iMeio = cmbMeioCom.ItemData(cmbMeioCom.ListIndex)

    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    
    cSql = cSql & " exec stp_pdr_rel_prod_meio_d " & lCiclo & ", "
    cSql = cSql & iMeio & ", " & lCodVenda & ", "
    cSql = cSql & " @sql_cd_ret out, "
    cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_rel_prod_meio_i " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Function
    End If

    ExcluiRelac = True

End Function

