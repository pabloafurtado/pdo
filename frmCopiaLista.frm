VERSION 5.00
Begin VB.Form frmCopiaLista 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Lista"
   ClientHeight    =   2010
   ClientLeft      =   3000
   ClientTop       =   1545
   ClientWidth     =   4770
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2010
   ScaleWidth      =   4770
   Begin VB.Frame Frame1 
      Caption         =   "Ciclos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1860
      Left            =   45
      TabIndex        =   0
      Top             =   90
      Width           =   4650
      Begin VB.CommandButton cmd_cancela 
         Caption         =   "&Cancela"
         Height          =   420
         Left            =   3240
         TabIndex        =   6
         Top             =   1125
         Width           =   1230
      End
      Begin VB.CommandButton cmd_ok 
         Caption         =   "&Ok"
         Height          =   420
         Left            =   3240
         TabIndex        =   5
         Top             =   405
         Width           =   1230
      End
      Begin VB.TextBox txt_ciclopara 
         Height          =   330
         Left            =   1530
         TabIndex        =   4
         Top             =   1080
         Width           =   1230
      End
      Begin VB.TextBox txt_ciclode 
         Height          =   330
         Left            =   1530
         TabIndex        =   3
         Top             =   540
         Width           =   1230
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo Ano Para:"
         Height          =   240
         Left            =   150
         TabIndex        =   2
         Top             =   1170
         Width           =   1380
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ciclo Ano De:"
         Height          =   240
         Left            =   270
         TabIndex        =   1
         Top             =   630
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmCopiaLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim sql_cd_ret As Long
Dim sql_qt_lnh As Long


Private Function FU_ExisteLista(nm_ciclo_operacional As String) As Integer
Dim sql As String

FU_ExisteLista = FAIL%

On Error GoTo FU_ExisteLista

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pi_cn_existe_lista_s " & nm_ciclo_operacional & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

If gObjeto.natPrepareQuery(hSqlTlm, sql) Then
    Do While gObjeto.natFetchNext(hSqlTlm)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_existe_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

sql_cd_ret = CInt(gObjeto.varOutputParam(0))
sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

If sql_cd_ret <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_existe_lista_s " & "RETURN STATUS = " & sql_cd_ret
    Exit Function
End If

FU_ExisteLista = SUCCED%
Exit Function

FU_ExisteLista:
    MSG$ = "FU_ExisteLista - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_CargaLista(nm_ciclo_operacional_de As String, nm_ciclo_operacional_para As String) As Integer
Dim sql As String

FU_CargaLista = FAIL%

On Error GoTo FU_CargaLista

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pi_cn_carga_lista_s " & nm_ciclo_operacional_de & "," & nm_ciclo_operacional_para & _
",'" & Le_Usuario() & "',@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_carga_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

sql_cd_ret = CInt(gObjeto.varOutputParam(0))
sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

If sql_cd_ret <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_carga_lista_s " & "RETURN STATUS = " & sql_cd_ret
    Exit Function
End If

FU_CargaLista = SUCCED%
Exit Function

FU_CargaLista:
    MSG$ = "FU_CargaLista - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Sub cmd_carga_Click()

End Sub

Private Sub cmd_cancela_Click()
Unload Me
Set frm_CN_ciclo_de_para = Nothing

End Sub

Private Sub cmd_ok_Click()
Dim nm_ciclo_operacional_de As String
Dim nm_ciclo_operacional_para As String

If txt_ciclode.Text = "" Then
   MsgBox "Ciclo Ano De deve ser preenchido!", vbExclamation, "A T E N � � O"
   txt_ciclode.SetFocus
   Exit Sub
End If

If txt_ciclopara.Text = "" Then
   MsgBox "Ciclo Ano Para deve ser preenchido!", vbExclamation, "A T E N � � O"
   txt_ciclopara.SetFocus
   Exit Sub
End If

If txt_ciclode.Text = "" Then
   Exit Sub
End If

txt_ciclode.MaxLength = 7
If InStr(1, txt_ciclode, "/") = 0 Then
    txt_ciclode.Text = Format(txt_ciclode, "0#/####")
End If
nm_ciclo_operacional_de = Right(txt_ciclode, 4) + Left(txt_ciclode, 2)

MSG$ = ""
If Trim$(txt_ciclode) <> "" Then
    If Not Valida_Ciclo(nm_ciclo_operacional_de) Then
        If MSG$ <> "" Then
           MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
           Exit Sub
        Else
           MsgBox "Ciclo Ano De ainda n�o foi criado!", vbOKOnly + vbInformation, "A T E N � � O"
           txt_ciclode.SetFocus
           Exit Sub
        End If
    End If
End If

If FU_ExisteLista(nm_ciclo_operacional_de) = FAIL% Then
    MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
    txt_ciclode.SetFocus
    Exit Sub
End If
    
If sql_qt_lnh = 0 Then
    MsgBox "N�o existe lista para o ciclo " & txt_ciclode.Text, vbOKOnly + vbInformation, "A T E N � � O"
    txt_ciclode.SetFocus
    Exit Sub
End If


If txt_ciclopara.Text = "" Then
   Exit Sub
End If

txt_ciclopara.MaxLength = 7
If InStr(1, txt_ciclopara, "/") = 0 Then
    txt_ciclopara.Text = Format(txt_ciclopara, "0#/####")
End If
nm_ciclo_operacional_para = Right(txt_ciclopara, 4) + Left(txt_ciclopara, 2)

MSG$ = ""
If Trim$(txt_ciclopara) <> "" Then
    If Not Valida_Ciclo(nm_ciclo_operacional_para) Then
        If MSG$ <> "" Then
           MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
           Exit Sub
        Else
           MsgBox "Ciclo Ano Para ainda n�o foi criado!", vbOKOnly + vbInformation, "A T E N � � O"
           txt_ciclopara.SetFocus
           Exit Sub
        End If
    End If
    
    If FU_ExisteLista(nm_ciclo_operacional_para) = FAIL% Then
        MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
        txt_ciclopara.SetFocus
        Exit Sub
    End If
        
    If sql_qt_lnh > 0 Then
        MsgBox "J� existe lista para o ciclo " & txt_ciclopara, vbOKOnly + vbInformation, "A T E N � � O"
        txt_ciclopara.SetFocus
        Exit Sub
    End If
    
End If


If FU_CargaLista(nm_ciclo_operacional_de, nm_ciclo_operacional_para) = FAIL% Then
    MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
    Exit Sub
End If

MsgBox "Carga efetuada com sucesso !", vbOKOnly + vbInformation, "A T E N � � O"

End Sub

Private Sub Form_Load()
gisysmenu = GetSystemMenu(Me.hWnd, 0)

   iCounter = 8
   For gimenucount = iCounter To 6 Step -1
       giretval = RemoveMenu(gisysmenu, _
       gimenucount, MF_BYPOSITION)
   Next gimenucount

End Sub

Private Sub txt_cicloano_Change()

End Sub

Private Sub txt_ciclode_GotFocus()
   txt_ciclode.Text = Left$(txt_ciclode.Text, 2) & Right$(txt_ciclode.Text, 4)
   txt_ciclode.MaxLength = 6
   SetaSelecao txt_ciclode

End Sub

Private Sub txt_ciclode_KeyPress(KeyAscii As Integer)
   If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 13) And (KeyAscii <> 8) Then
      KeyAscii = 0
      Beep
      Exit Sub
   End If
        
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        txt_ciclode_LostFocus
        txt_ciclopara.SetFocus
    End If

End Sub

Private Sub txt_ciclode_LostFocus()
Dim nm_ciclo_operacional As String

If txt_ciclode.Text = "" Then
   Exit Sub
End If

    txt_ciclode.MaxLength = 7
    If InStr(1, txt_ciclode, "/") = 0 Then
        txt_ciclode.Text = Format(txt_ciclode, "0#/####")
    End If
    nm_ciclo_operacional = Right(txt_ciclode, 4) + Left(txt_ciclode, 2)
    
    MSG$ = ""
    If Trim$(txt_ciclode) <> "" Then
        If Not Valida_Ciclo(nm_ciclo_operacional) Then
            If MSG$ <> "" Then
               MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
               Exit Sub
            Else
               MsgBox "Ciclo Ano De ainda n�o foi criado!", vbOKOnly + vbInformation, "A T E N � � O"
               txt_ciclode.SetFocus
               Exit Sub
            End If
        End If
        
        If FU_ExisteLista(nm_ciclo_operacional) = FAIL% Then
            MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
            txt_ciclode.SetFocus
            Exit Sub
        End If
        
        If sql_qt_lnh = 0 Then
            MsgBox "N�o existe lista para o ciclo " + txt_ciclode, vbOKOnly + vbInformation, "A T E N � � O"
            txt_ciclode.SetFocus
            Exit Sub
        End If
    End If

End Sub

Private Sub txt_ciclopara_GotFocus()
   txt_ciclopara.Text = Left$(txt_ciclopara.Text, 2) & Right$(txt_ciclopara.Text, 4)
   txt_ciclopara.MaxLength = 6
   SetaSelecao txt_ciclopara
End Sub

Private Sub txt_ciclopara_KeyPress(KeyAscii As Integer)
Dim nm_ciclo_operacional As String

   If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 13) And (KeyAscii <> 8) Then
      KeyAscii = 0
      Beep
      Exit Sub
   End If

    If KeyAscii = 13 Then
        KeyAscii = 0
        If txt_ciclopara.Text = "" Then
           Exit Sub
        End If
        
        txt_ciclopara.MaxLength = 7
        If InStr(1, txt_ciclopara, "/") = 0 Then
            txt_ciclopara.Text = Format(txt_ciclopara, "0#/####")
        End If
        nm_ciclo_operacional = Right(txt_ciclopara, 4) + Left(txt_ciclopara, 2)
        
        MSG$ = ""
        If Trim$(txt_ciclopara) <> "" Then
            If Not Valida_Ciclo(nm_ciclo_operacional) Then
                If MSG$ <> "" Then
                   MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
                   Exit Sub
                Else
                   MsgBox "Ciclo Ano Para ainda n�o foi criado!", vbOKOnly + vbInformation, "A T E N � � O"
                   txt_ciclopara.SetFocus
                   Exit Sub
                End If
            End If
            If FU_ExisteLista(nm_ciclo_operacional) = FAIL% Then
                MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
                Exit Sub
            End If
            If sql_qt_lnh > 1 Then
                MsgBox "J� existe lista para o ciclo " & txt_ciclopara, vbOKOnly + vbInformation, "A T E N � � O"
                txt_ciclopara.SetFocus
                Exit Sub
            End If
            
        End If
        cmd_ok_Click
    End If

End Sub

