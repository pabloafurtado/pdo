VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmParmRelBasePrc 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relat�rio de Base de Pre�os"
   ClientHeight    =   4035
   ClientLeft      =   3000
   ClientTop       =   2550
   ClientWidth     =   6120
   Icon            =   "frmParmRelBasePrc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4035
   ScaleWidth      =   6120
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   6120
      _ExtentX        =   10795
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   1
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consultar"
      Height          =   855
      Left            =   3750
      Picture         =   "frmParmRelBasePrc.frx":0E42
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   3090
      Width           =   1425
   End
   Begin VB.CommandButton cmdGeraRelatorio 
      Caption         =   "Gerar"
      Height          =   855
      Left            =   1050
      Picture         =   "frmParmRelBasePrc.frx":1284
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3090
      Width           =   1425
   End
   Begin VB.Frame Frame1 
      Caption         =   "Par�metros para Impress�o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   270
      TabIndex        =   2
      Top             =   780
      Width           =   5595
      Begin VB.ComboBox cmbListaPrc 
         Height          =   360
         ItemData        =   "frmParmRelBasePrc.frx":16C6
         Left            =   1860
         List            =   "frmParmRelBasePrc.frx":16E2
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1560
         Width           =   1005
      End
      Begin VB.ComboBox cmbMeioComunic 
         Height          =   360
         ItemData        =   "frmParmRelBasePrc.frx":1706
         Left            =   1860
         List            =   "frmParmRelBasePrc.frx":1713
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   990
         Width           =   3435
      End
      Begin VB.ComboBox cmbCiclo 
         Height          =   360
         ItemData        =   "frmParmRelBasePrc.frx":1741
         Left            =   1860
         List            =   "frmParmRelBasePrc.frx":176C
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   420
         Width           =   1515
      End
      Begin VB.Label Label3 
         Caption         =   "Lista"
         Height          =   255
         Left            =   480
         TabIndex        =   8
         Top             =   1620
         Width           =   555
      End
      Begin VB.Label Label2 
         Caption         =   "Meio de Comunica��o"
         Height          =   495
         Left            =   480
         TabIndex        =   4
         Top             =   930
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Ciclo:"
         Height          =   255
         Left            =   510
         TabIndex        =   0
         Top             =   480
         Width           =   555
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   5580
      Top             =   3240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmParmRelBasePrc.frx":17E5
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmParmRelBasePrc.frx":1AFF
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmParmRelBasePrc.frx":1E19
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmParmRelBasePrc.frx":2133
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmParmRelBasePrc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbCiclo_LostFocus()
    
    If cmbCiclo.ListIndex > 0 Then
        If Not Combo_Lista(Me.cmbListaPrc, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
           SetStatus "Erro na leitura..."
           MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
        End If
    End If

End Sub

Private Sub cmdConsultar_Click()

    If cmbCiclo.Text = "" Then
        MsgBox "Informe o Ciclo de vendas desejado!"
        Exit Sub
    End If


    If cmbMeioComunic.Text = "" Then
        MsgBox "Informe o Meio de comunica��o!"
        Exit Sub
    End If

    If cmbListaPrc.Text = "" Then
        MsgBox "Informe a lista de pre�os a usar na gera��o!"
        Exit Sub
    End If

    
    If ConsBasePreco Then
       frmRelBasePrc.Show 1
    Else
         MsgBox "Relat�rio n�o gerado", vbInformation, Me.Caption
    End If

End Sub

Private Sub cmdGeraRelatorio_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO017", "M") Then
        'MsgBox "Gera��o de relat�rio de base de pre�o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO017", "M") Then
        MsgBox "Gera��o de relat�rio de base de pre�o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        Exit Sub
    End If

    If cmbCiclo.Text = "" Then
        MsgBox "Informe o Ciclo de vendas desejado!"
        Exit Sub
    End If

    If cmbMeioComunic.Text = "" Then
        MsgBox "Informe o Meio de comunica��o!"
        Exit Sub
    End If

    If cmbListaPrc.Text = "" Then
        MsgBox "Informe a lista de pre�os a usar na gera��o!"
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    If Val(QtdRelatorioBase(cmbListaPrc.Text, cmbMeioComunic.ItemData(cmbMeioComunic.ListIndex), cmbCiclo.ItemData(cmbCiclo.ListIndex))) > 0 Then
        If MsgBox("Relat�rio de Base de Pre�os j� gerado para o Ciclo / Meio Comunica��o / Lista " & Chr(10) & Chr(10) & _
                   "Deseja Regerar o relat�rio ? ", vbQuestion + vbYesNo, "Aten��o") <> vbYes Then
                   
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    If GeraBasePreco Then
        If ConsBasePreco Then
            Screen.MousePointer = vbNormal
            frmRelBasePrc.Show 1
        Else
            Screen.MousePointer = vbNormal
            MsgBox "N�o foram encontrados dados para a gera��o do relat�rio", vbInformation, Me.Caption
        End If
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Function QtdRelatorioBase(cd_lista_preco As String, cd_meio_comunicacao As String, nm_ciclo_operacional As String) As String

    Dim cSql  As String
    Dim bResp As Boolean
    Dim i     As Integer
    Dim avRelBase()       As Variant
    
    On Error GoTo erro
    
    cSql = "SELECT ISNULL(count(*),0) "
    cSql = cSql & " FROM t_base_preco "
    cSql = cSql & " WHERE "
    cSql = cSql & "     nm_ciclo_operacional = " & nm_ciclo_operacional
    cSql = cSql & " AND cd_meio_comunicacao = " & cd_meio_comunicacao
    cSql = cSql & " AND cd_lista_preco = " & cd_lista_preco
    
    ReDim avRelBase(0)
    
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avRelBase())
    If bResp = False Then
        MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Function
    End If
    
    QtdRelatorioBase = avRelBase(0, i)
    
    Exit Function
    
erro:
    MsgBox "Erro ao obter total de pre�os gerados " & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
    Exit Function
    
End Function



Private Function GeraBasePreco() As Boolean
    'Executar a QUERY
    'Declara��o de Vari�veis
    Dim avData()    As Variant
    Dim cSql        As String
    Dim bResp       As Boolean
    Dim cdAnt       As Long
    Dim X           As Long
    Dim Y           As Long
    Dim se_Usuario  As String
    Dim ne_ret     As Long
    
    Screen.MousePointer = vbHourglass
    DoEvents
    Me.Refresh
    DoEvents
    
    GeraBasePreco = False
    
    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & "exec stp_pdr_gera_base_prc_i " & cmbCiclo.ItemData(cmbCiclo.ListIndex) & " , "
    cSql = cSql & cmbMeioComunic.ItemData(cmbMeioComunic.ListIndex) & " , "
    cSql = cSql & cmbListaPrc.Text & ", "
    cSql = cSql & "'" & se_Usuario & "', "
    cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"

    If gObjeto.natPrepareQuery(hSql, cSql) Then
       Do While gObjeto.natFetchNext(hSql)
       Loop
    End If
      
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Function
    End If
    
    sql_cd_ret = Val(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))
     
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar stp_pdr_gera_base_prc_i - RETURN STATUS = " & _
               sql_cd_ret & " - " & sql_cd_opr & " - " & sql_nm_tab
       Exit Function
    End If
        
    If sql_qt_lnh = 0 Then
        Screen.MousePointer = vbNormal
        Refresh
        MsgBox "N�o existem produtos a relacionar para este ciclo / meio / lista!", vbCritical
        Exit Function
    End If
        
    Screen.MousePointer = vbNormal
    DoEvents
    Me.Refresh
    DoEvents
    GeraBasePreco = True

End Function

Private Function ConsBasePreco() As Boolean
    'Executar a QUERY
    'Declara��o de Vari�veis
    Dim avData()    As Variant
    Dim cSql        As String
    Dim bResp       As Boolean
    Dim cdAnt       As Long
    Dim DescAnt     As String
    Dim codAnt      As Long
    Dim LenCol      As Double
    Dim X           As Long
    Dim Y           As Long
    Dim v_valor     As String
    Dim v_cod_venda As Long
    Dim v_preco_dif As String
    Dim v_valor_texto As String
    
    Screen.MousePointer = vbHourglass
    Me.Refresh
    
    ConsBasePreco = False
    
    cSql = "exec stp_pdr_cons_rel_base_prc_s " & cmbCiclo.ItemData(cmbCiclo.ListIndex) & " , "
    cSql = cSql & cmbMeioComunic.ItemData(cmbMeioComunic.ListIndex) & " , "
    cSql = cSql & cmbListaPrc.Text
         
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())
     
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.nServerNum & " " & gObjeto.sServerMsg, vbOKOnly + vbCritical, "Erro"
        Exit Function
    End If
    
    If UBound(avData, 1) = 0 Then
        Screen.MousePointer = vbNormal
        Refresh
'       MsgBox "N�o existe relat�rio gerado para este ciclo / meio / lista!", vbCritical
        Exit Function
    End If
        
    Load frmRelBasePrc
    
    With frmRelBasePrc
        .lblCiclo.Caption = cmbCiclo.Text
        .lblMeioComunic.Caption = cmbMeioComunic.Text
        With .grdBasePreco
            .Visible = False
            .SetText 6, 0, "Lista de Pre�os " & Format(cmbListaPrc, "00")
            .MaxRows = 0
            .MaxCols = 8
            frmRelBasePrc.txtVersao.Text = Val(avData(14, 0))
            
            For X = 0 To UBound(avData(), 2)
                'IMPLEMENTA��O DE LINHAS
                .MaxRows = .MaxRows + 1
                .Row = .MaxRows
                        
                'DESCRI��O
                .SetText 5, .MaxRows, avData(4, X)
                
                If Val(avData(2, X)) <> 0 Then
                    'linha
                    .SetText 1, .MaxRows, avData(0, X)
                    'categoria
                    .SetText 2, .MaxRows, avData(1, X)
                    'C�DIGO DE VENDA
                    .SetText 3, .MaxRows, avData(2, X)
                    v_cod_venda = avData(2, X)
                    'pontos
                    .SetText 4, .MaxRows, avData(3, X)
                    'preco da lista
                    
                    'v_valor = avData(2, X)
                    'v_componentes(i).vl_preco_referencia_kit = ConvertePontoDecimal(v_valor, 0) 'gObjeto.varResultSet(hSql, 2)
     '               If (CDbl(avData(5, X)) - Int(avData(5, X))) = 0 Then
     '                   v_valor = avData(5, X)
     '                   .SetText 6, .MaxRows, Format(ConvertePontoDecimal(Str(avData(5, X)), 0), "###,##0.00")
     '               Else
     '                   v_valor = Str(avData(5, X))
     '                   .SetText 6, .MaxRows, Format(ConvertePontoDecimal(Str(avData(5, X)), 0), "###,##0.00")
     '               End If
                    
                    '.SetText 6, .MaxRows, Format(CDbl(ConvertePontoDecimal(Str(avData(5, X)), 0)), "###,##0.00")
                    '.Col = 6
                    '.SetText 6, .MaxRows, Format(CDbl(ConvertePontoDecimal(CStr(avData(5, X)), 0)), "###,##0.00")
                    
                    v_valor_texto = avData(5, X)
                    v_valor_texto = ConvertePontoDecimal(v_valor_texto, 0)
                    v_valor_texto = Format(v_valor_texto, "#0.00")
                    '.SetText 6, .MaxRows, Format(CDbl(ConvertePontoDecimal(Str(avData(5, X)), 0)), "###,##0.00")
                    '.SetText 6, .MaxRows, gObjeto.varResultSet(hSql, 2)
                    '.SetText 6, .MaxRows, ConvertePontoDecimal(.SetText 6, 0)
                    '.SetText 6, .MaxRows, Format(txt_precobase, "#0.00")
                    
                    .SetText 6, .MaxRows, v_valor_texto
                    
                    v_valor = avData(5, X)
                    cdAnt = Val(avData(2, X))
                    DescAnt = avData(4, X)
                    
                    codAnt = avData(2, X)
                    'observacao
                    
                    
                    .SetText 7, .MaxRows, avData(6, X)
                    
                    .SetColItemData 7, 9  'seta o item data da coluna 7 com valor inicial igual a zero
                                          'caso ocorra alteracao na coluna o item data sera alterado para 1
                                          'para obter o item data usa-se o metodo GetColItemData
                                          
                                          
                    ' nova coluna pre�o diferenciado
                    'If v_cod_venda = 6671 Then
                    'Stop
                    'End If
                    v_preco_dif = Verifica_Preco_Diferenciado(cmbCiclo.ItemData(cmbCiclo.ListIndex), cmbListaPrc.Text, v_valor, v_cod_venda)
                    .SetText 8, .MaxRows, v_preco_dif
                    '
                                                              
                    Y = 9
                    Do While DescAnt = avData(4, X) And codAnt = avData(2, X)
                        If Val(Trim(avData(8, X))) <> 0 Or (Val(Trim(avData(9, X))) <> 1 And Val(Trim(avData(9, X))) <> 0) Then
                            If Y > .MaxCols Then
                                .MaxCols = Y
                            End If
                            .SetText Y, .MaxRows, avData(7, X)
                             LenCol = Len(Trim(avData(7, X)))
                            If .ColWidth(Y) < LenCol Then
                                .ColWidth(Y) = LenCol
                            End If
                            Y = Y + 1
                        End If
                        X = X + 1
                        If X > UBound(avData, 2) Then
                            Exit Do
                        End If
                    Loop
                    X = X - 1
                Else
                    'se for filho de kit, ent�o proteger a coluna observa��o para n�o digitar
                    .Col = 7
                    .Col2 = 7
                    .Row2 = .Row
                    .BlockMode = True
                    .Lock = True
                End If
                
            Next X
            .Visible = True
            Screen.MousePointer = vbNormal
        End With
    End With

    ConsBasePreco = True


End Function



Private Sub Form_Load()

    Me.Show
    Screen.MousePointer = vbHourglass
    Refresh
    
    If Not CarrCicloComLista(cmbCiclo) Then
        Screen.MousePointer = vbNormal
        Unload Me
        Exit Sub
    End If
    
    Call CarregaMeio(cmbMeioComunic)
    
    cmbCiclo.SetFocus
    Screen.MousePointer = vbNormal
    Refresh


End Sub

Private Sub Form_Unload(Cancel As Integer)

    MDIpdo001.DesbloqueiaMenu

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    
    Unload Me

End Sub


Public Function Verifica_Preco_Diferenciado(v_ciclo As Long, v_lista As Long, v_preco As String, v_codigo As Long) As String
    Dim v_resultado As String
    Dim avData()    As Variant
    Dim cSql        As String
    Dim bResp       As Boolean
    Dim X As Long
    Dim sql As String
    Dim v_preco_ref As String
   
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec st_pdr_cn_preco_base_s " & v_ciclo & "," & v_codigo & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"


    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            If CDbl(gObjeto.varResultSet(hSql, 2)) - Int(gObjeto.varResultSet(hSql, 2)) > 0 Then
                v_preco_ref = Str(gObjeto.varResultSet(hSql, 2))
                v_preco_ref = ConvertePontoDecimal(v_preco_ref, 0)
                v_preco_ref = Format(v_preco_ref, "#0.00")
                
            Else
                v_preco_ref = gObjeto.varResultSet(hSql, 2)
                v_preco_ref = ConvertePontoDecimal(v_preco_ref, 0)
                v_preco_ref = Format(v_preco_ref, "#0.00")
    
            End If
        Loop
    End If

    
    
    
    v_resultado = ""

    cSql = "SELECT DISTINCT c.no_estrutura_comercial"
    cSql = cSql & " FROM t_pi_estrutura_lista a, t_pi_preco_cn b, t_estrutura_comercial c"
    cSql = cSql & " Where b.cd_venda_produto = " & v_codigo
    cSql = cSql & " AND b.nm_ciclo_operacional = " & v_ciclo
    cSql = cSql & " AND b.cd_lista = " & v_lista
    cSql = cSql & " AND b.vl_preco_lista <> " & Str(v_preco_ref)
    cSql = cSql & " and b.cd_lista = a.cd_lista"
    cSql = cSql & " AND b.nm_ciclo_operacional = a.nm_ciclo_operacional"
    cSql = cSql & " AND b.cd_segmento_canal = a.cd_segmento_canal"
    cSql = cSql & " AND a.cd_tipo_estrutura_comercial  = c.cd_tipo_estrutura_comercial"
    cSql = cSql & " AND a.cd_estrutura_comercial = c.cd_estrutura_comercial"
         
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData())
    
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.nServerNum & " " & gObjeto.sServerMsg, vbOKOnly + vbCritical, "Erro"
        Exit Function
    End If
    
    'If UBound(avData, 1) = 0 Then
    '    Screen.MousePointer = vbNormal
    '    Refresh
    '    Exit Function
    'End If
            
    For X = 0 To UBound(avData(), 2)
        If v_resultado = "" Then
            v_resultado = Trim(avData(0, X))
        Else
            v_resultado = v_resultado & ", " & Trim(avData(0, X))
        End If
    Next X
    
    Verifica_Preco_Diferenciado = v_resultado
    
End Function


