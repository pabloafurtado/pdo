VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmPrecoRef 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o Pre�o de Refer�ncia"
   ClientHeight    =   5880
   ClientLeft      =   1005
   ClientTop       =   3165
   ClientWidth     =   11070
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5880
   ScaleWidth      =   11070
   Begin TabDlg.SSTab SSTab1 
      Height          =   5655
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   9975
      _Version        =   327681
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Refer�ncia"
      TabPicture(0)   =   "frmPrecoRef.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ImageList1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "grd_Pai_Kit"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "st_Toolbar"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Lista"
      TabPicture(1)   =   "frmPrecoRef.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Toolbar1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame1(1)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lblMensagem"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      Begin ComctlLib.Toolbar Toolbar1 
         Height          =   660
         Left            =   -74880
         TabIndex        =   26
         Top             =   480
         Width           =   10500
         _ExtentX        =   18521
         _ExtentY        =   1164
         ButtonWidth     =   1032
         ButtonHeight    =   1005
         AllowCustomize  =   0   'False
         Appearance      =   1
         ImageList       =   "ImageList2"
         _Version        =   327682
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   7
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Description     =   "Novo"
               Object.ToolTipText     =   "Gravar"
               Object.Tag             =   ""
               ImageIndex      =   8
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.ToolTipText     =   "Nova Pesquisa"
               Object.Tag             =   ""
               ImageIndex      =   10
            EndProperty
            BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.ToolTipText     =   "Pesquisar"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
            BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.ToolTipText     =   "Sair"
               Object.Tag             =   ""
               ImageIndex      =   7
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
      Begin ComctlLib.Toolbar st_Toolbar 
         Height          =   660
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   10500
         _ExtentX        =   18521
         _ExtentY        =   1164
         ButtonWidth     =   1032
         ButtonHeight    =   1005
         AllowCustomize  =   0   'False
         Appearance      =   1
         ImageList       =   "ImageList1"
         _Version        =   327682
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   12
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Description     =   "Novo"
               Object.ToolTipText     =   "Carrega Lista"
               Object.Tag             =   ""
               ImageIndex      =   4
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.ToolTipText     =   "Excluir"
               Object.Tag             =   ""
               ImageIndex      =   9
            EndProperty
            BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.ToolTipText     =   "Gravar"
               Object.Tag             =   ""
               ImageIndex      =   8
            EndProperty
            BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.ToolTipText     =   "Nova pesquisa"
               Object.Tag             =   ""
               ImageIndex      =   10
            EndProperty
            BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Object.Visible         =   0   'False
               Key             =   ""
               Object.ToolTipText     =   "Imprimir"
               Object.Tag             =   ""
               ImageIndex      =   3
            EndProperty
            BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.ToolTipText     =   "Pesquisar"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
            BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               Object.Width           =   200
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Enabled         =   0   'False
               Key             =   ""
               Object.ToolTipText     =   "Reaj. Individual da lista de pre�o"
               Object.Tag             =   ""
               ImageIndex      =   11
            EndProperty
            BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               Object.Width           =   500
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button12 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.ToolTipText     =   "Sair"
               Object.Tag             =   ""
               ImageIndex      =   7
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
      Begin FPSpread.vaSpread grd_Pai_Kit 
         Height          =   2175
         Left            =   120
         TabIndex        =   10
         Top             =   5760
         Width           =   7785
         _Version        =   131077
         _ExtentX        =   13732
         _ExtentY        =   3836
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "frmPrecoRef.frx":0038
      End
      Begin VB.Frame Frame1 
         Height          =   3855
         Index           =   1
         Left            =   -74880
         TabIndex        =   27
         Top             =   1200
         Width           =   10575
         Begin VB.TextBox txtredutorlista 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   9240
            TabIndex        =   44
            Top             =   1680
            Width           =   855
         End
         Begin VB.ComboBox cmbStatusL 
            Height          =   315
            ItemData        =   "frmPrecoRef.frx":0192
            Left            =   4920
            List            =   "frmPrecoRef.frx":019F
            Style           =   2  'Dropdown List
            TabIndex        =   43
            Top             =   3240
            Width           =   2175
         End
         Begin VB.TextBox txtprecosugestao 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   4920
            TabIndex        =   42
            Top             =   2520
            Width           =   1995
         End
         Begin VB.Frame Frame6 
            Caption         =   "Lista"
            Enabled         =   0   'False
            Height          =   735
            Left            =   120
            TabIndex        =   49
            Top             =   3000
            Width           =   1695
            Begin VB.ComboBox cmbLista 
               Enabled         =   0   'False
               Height          =   360
               ItemData        =   "frmPrecoRef.frx":01D5
               Left            =   120
               List            =   "frmPrecoRef.frx":01D7
               TabIndex        =   38
               Top             =   240
               Width           =   1425
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Setor"
            Height          =   735
            Left            =   120
            TabIndex        =   48
            Top             =   2280
            Width           =   3015
            Begin VB.ComboBox cmbSetor 
               Height          =   360
               ItemData        =   "frmPrecoRef.frx":01D9
               Left            =   120
               List            =   "frmPrecoRef.frx":01DB
               TabIndex        =   37
               Top             =   240
               Width           =   2745
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Ger�ncia de Venda"
            Height          =   735
            Left            =   120
            TabIndex        =   47
            Top             =   1560
            Width           =   3015
            Begin VB.ComboBox cmbGerVenda 
               Height          =   360
               ItemData        =   "frmPrecoRef.frx":01DD
               Left            =   120
               List            =   "frmPrecoRef.frx":01DF
               TabIndex        =   36
               Top             =   240
               Width           =   2745
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Regi�o Estrat�gica"
            Height          =   735
            Left            =   120
            TabIndex        =   46
            Top             =   840
            Width           =   3015
            Begin VB.ComboBox cmbRegEstrat 
               Height          =   360
               ItemData        =   "frmPrecoRef.frx":01E1
               Left            =   120
               List            =   "frmPrecoRef.frx":01E3
               TabIndex        =   35
               Top             =   240
               Width           =   2745
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Ciclo / Ano"
            Height          =   735
            Left            =   120
            TabIndex        =   33
            Top             =   120
            Width           =   1815
            Begin VB.ComboBox cmbCicloL 
               Height          =   360
               ItemData        =   "frmPrecoRef.frx":01E5
               Left            =   120
               List            =   "frmPrecoRef.frx":01E7
               TabIndex        =   34
               Top             =   240
               Width           =   1545
            End
         End
         Begin VB.CommandButton cmdcomponentes 
            Enabled         =   0   'False
            Height          =   705
            Left            =   9240
            Picture         =   "frmPrecoRef.frx":01E9
            Style           =   1  'Graphical
            TabIndex        =   28
            ToolTipText     =   "Listar Componentes"
            Top             =   3000
            Width           =   795
         End
         Begin VB.TextBox txtpontos 
            Alignment       =   1  'Right Justify
            Height          =   330
            Left            =   9240
            MaxLength       =   3
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   2520
            Width           =   870
         End
         Begin VB.TextBox txtprecobase 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   360
            Left            =   4920
            TabIndex        =   41
            Top             =   1680
            Width           =   1995
         End
         Begin VB.TextBox txtdescricao 
            Enabled         =   0   'False
            Height          =   360
            Left            =   4935
            TabIndex        =   40
            Top             =   960
            Width           =   4920
         End
         Begin VB.TextBox txtcodvenda 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   4935
            MaxLength       =   9
            TabIndex        =   39
            Top             =   375
            Width           =   1515
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Redutor de Pontos (%)"
            Height          =   240
            Left            =   7200
            TabIndex        =   53
            Top             =   1800
            Width           =   2025
         End
         Begin ComctlLib.ImageList ImageList2 
            Left            =   9600
            Top             =   240
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   32
            ImageHeight     =   32
            MaskColor       =   12632256
            _Version        =   327682
            BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
               NumListImages   =   11
               BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":04F3
                  Key             =   ""
               EndProperty
               BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":080D
                  Key             =   ""
               EndProperty
               BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":0B27
                  Key             =   ""
               EndProperty
               BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":0E41
                  Key             =   ""
               EndProperty
               BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":115B
                  Key             =   ""
               EndProperty
               BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":1475
                  Key             =   ""
               EndProperty
               BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":178F
                  Key             =   ""
               EndProperty
               BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":1AA9
                  Key             =   ""
               EndProperty
               BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":1DC3
                  Key             =   ""
               EndProperty
               BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":20DD
                  Key             =   ""
               EndProperty
               BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
                  Picture         =   "frmPrecoRef.frx":23F7
                  Key             =   ""
               EndProperty
            EndProperty
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Status"
            Height          =   240
            Left            =   3360
            TabIndex        =   51
            Top             =   3360
            Width           =   555
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Pre�o Sugest�o"
            Height          =   240
            Left            =   3360
            TabIndex        =   50
            Top             =   2640
            Width           =   1455
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   " Pontos"
            Height          =   240
            Left            =   7200
            TabIndex        =   32
            Top             =   2640
            Width           =   675
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Pre�o Base"
            Enabled         =   0   'False
            Height          =   240
            Left            =   3360
            TabIndex        =   31
            Top             =   1800
            Width           =   1065
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Descri��o"
            Enabled         =   0   'False
            Height          =   240
            Left            =   3360
            TabIndex        =   30
            Top             =   1080
            Width           =   930
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "C�digo de Venda"
            Height          =   240
            Left            =   3360
            TabIndex        =   29
            Top             =   360
            Width           =   1590
         End
      End
      Begin VB.Frame Frame1 
         Height          =   3795
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   1200
         Width           =   7815
         Begin VB.TextBox txt_cicloano 
            Height          =   330
            Left            =   4020
            TabIndex        =   16
            Top             =   420
            Visible         =   0   'False
            Width           =   1230
         End
         Begin VB.TextBox txt_codigo 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   2295
            MaxLength       =   9
            TabIndex        =   1
            Top             =   855
            Width           =   1515
         End
         Begin VB.TextBox txt_produto 
            Enabled         =   0   'False
            Height          =   360
            Left            =   2295
            TabIndex        =   2
            Top             =   1260
            Width           =   5280
         End
         Begin VB.TextBox txt_precobase 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   2295
            TabIndex        =   3
            Top             =   1665
            Width           =   915
         End
         Begin VB.TextBox txt_redutor 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   2295
            TabIndex        =   4
            Top             =   2100
            Width           =   915
         End
         Begin VB.TextBox txt_status 
            Height          =   330
            Left            =   4095
            MaxLength       =   1
            TabIndex        =   15
            ToolTipText     =   "0 - TMKT e COM, 2 - TMKT, 4 - n�o vai para TMKT e nem COM, 9 - Inativo "
            Top             =   2520
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txt_pontos 
            Alignment       =   1  'Right Justify
            Height          =   330
            Left            =   5745
            MaxLength       =   3
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   2535
            Width           =   870
         End
         Begin VB.CommandButton cmd_componentes 
            Enabled         =   0   'False
            Height          =   705
            Left            =   6840
            Picture         =   "frmPrecoRef.frx":2711
            Style           =   1  'Graphical
            TabIndex        =   14
            ToolTipText     =   "Listar Componentes"
            Top             =   3000
            Width           =   795
         End
         Begin VB.Frame fraAplica_Reajuste 
            Height          =   735
            Left            =   2280
            TabIndex        =   13
            Top             =   2880
            Width           =   975
            Begin VB.OptionButton optPreco 
               Caption         =   "Sim"
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   6
               Top             =   180
               Value           =   -1  'True
               Width           =   735
            End
            Begin VB.OptionButton optPreco 
               Caption         =   "N�o"
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   7
               Top             =   480
               Width           =   735
            End
         End
         Begin VB.ComboBox cmbCiclo 
            Height          =   360
            ItemData        =   "frmPrecoRef.frx":2A1B
            Left            =   2295
            List            =   "frmPrecoRef.frx":2A1D
            TabIndex        =   0
            Top             =   420
            Width           =   1545
         End
         Begin VB.ComboBox cmbStatus 
            Height          =   315
            ItemData        =   "frmPrecoRef.frx":2A1F
            Left            =   2280
            List            =   "frmPrecoRef.frx":2A2C
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   2520
            Width           =   2175
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ciclo / Ano"
            Height          =   240
            Left            =   240
            TabIndex        =   25
            Top             =   450
            Width           =   960
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "C�digo de Venda"
            Height          =   240
            Left            =   240
            TabIndex        =   24
            Top             =   855
            Width           =   1590
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Descri��o Produto"
            Height          =   240
            Left            =   240
            TabIndex        =   23
            Top             =   1305
            Width           =   1680
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Pre�o Refer�ncia"
            Height          =   240
            Left            =   240
            TabIndex        =   22
            Top             =   1755
            Width           =   1575
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Redutor de Pontos (%)"
            Height          =   240
            Left            =   240
            TabIndex        =   21
            Top             =   2160
            Width           =   2025
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Status"
            Height          =   240
            Left            =   240
            TabIndex        =   20
            Top             =   2610
            Width           =   555
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Pre�o Brasil"
            Height          =   240
            Left            =   240
            TabIndex        =   19
            Top             =   3120
            Width           =   1095
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   " Pontos"
            Height          =   240
            Left            =   4860
            TabIndex        =   18
            Top             =   2610
            Width           =   675
         End
         Begin VB.Label lblGravando 
            AutoSize        =   -1  'True
            Caption         =   "..."
            Height          =   195
            Left            =   3360
            TabIndex        =   17
            Top             =   3330
            Visible         =   0   'False
            Width           =   135
         End
      End
      Begin VB.Label lblMensagem 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   375
         Left            =   -74760
         TabIndex        =   52
         Top             =   5160
         Width           =   10335
      End
      Begin ComctlLib.ImageList ImageList1 
         Left            =   7320
         Top             =   3480
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   327682
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   11
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":2A62
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":2D7C
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":3096
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":33B0
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":36CA
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":39E4
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":3CFE
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":4018
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":4332
               Key             =   ""
            EndProperty
            BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":464C
               Key             =   ""
            EndProperty
            BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "frmPrecoRef.frx":4966
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmPrecoRef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type produtos_venda
    cd_produto          As String
    dc_produto          As String
    vl_preco_referencia As String
End Type

Private Type reg_materiais
    ciclo           As String
    cd_material     As String
    dc_produto      As String
    id_tipo_produto As Byte
    novo            As Boolean
    vigente         As Boolean
    Gravar          As Boolean
End Type

Private Type reg_ciclo_futuro
    ciclo               As String
    cd_material_vigente As String
End Type

Private v_produtos_venda() As produtos_venda
Dim bm_Alterado            As Boolean
Dim bm_novo                As Boolean
Dim id_tipo_produto        As String
Dim id_tipo_produto_futuro As String
Dim sql_cd_ret             As Long
Dim sql_qt_lnh             As Long
Dim sql_nm_tab             As String
Dim recalcula              As Boolean
Dim produto                As String
Dim ciclo                  As String
Dim v_cd_material          As String
Dim v_cd_material_futuro   As String
Dim v_cd_material_vigente  As String
Dim QtdPrecoFuturo         As Integer
Dim QtdPrecoBase           As Integer
Dim ciclosFuturos()        As reg_ciclo_futuro
Dim Materiais()            As reg_materiais
Dim MateriaisFuturos()     As reg_materiais
Dim contMat                As Integer

Dim i                    As Integer
Dim preco_componentes    As Double
Dim nm_ciclo_operacional As String
Dim nm_ciclo_futuro      As String
Dim flag_preco           As Boolean
Dim ponto_componente     As Integer
Dim total                As Double
Dim qtde                 As Integer
Dim vl_PrecoTotal        As Double
Dim Pontos               As Integer
Dim PontosTot            As Double

Dim preco_rounded  As Double
Dim aux_arredonda  As String
Dim txt_pontos_aux As String
Dim execucaoExterna As Boolean

Dim strOperacaoRef       As String 'SE6346
Dim blGravaLog           As Boolean 'SE6346
Dim strOperacaoLista     As String 'SE6346



Private Function FU_BuscaPrecoRefProduto(nm_ciclo_operacional As String, cd_venda_produto As String, MSG$, Lista As Boolean) As Integer
Dim sql     As String
Dim X       As Long

FU_BuscaPrecoRefProduto = FAIL%

On Error GoTo FU_BuscaPrecoRefProduto

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pdr_cn_preco_base_s " & nm_ciclo_operacional & "," & cd_venda_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        sql_qt_lnh = sql_qt_lnh + 1
        If Lista Then
            id_tipo_produto = gObjeto.varResultSet(hSql, 0)
            
            ' Checa se habilita ou n�o redutor de pre�os
                      
            txt_precobase.Text = gObjeto.varResultSet(hSql, 2)
            txt_precobase.Text = ConvertePontoDecimal(txt_precobase, 0)
            txt_precobase.Text = Format(txt_precobase, "#0.00")
            txt_precobase.Tag = txt_precobase.Text
            gPrecoBase = True
            txt_redutor.Text = gObjeto.varResultSet(hSql, 3)
            txt_redutor.Text = ConvertePontoDecimal(txt_redutor, 0)
            
            For X = 0 To cmbStatus.ListCount - 1
               If cmbStatus.ItemData(X) = gObjeto.varResultSet(hSql, 1) Then
                   cmbStatus.ListIndex = X
                   Exit For
               End If
            Next X
            txt_status.Text = gObjeto.varResultSet(hSql, 1)
            txt_pontos.Text = gObjeto.varResultSet(hSql, 5)
            
            If gObjeto.varResultSet(hSql, 6) = "1" Then
               optPreco(0).Value = True
            ElseIf gObjeto.varResultSet(hSql, 6) = "0" Then
               optPreco(1).Value = True
            End If
        End If
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_preco_base_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

'If sql_cd_ret <> 0 Then
'    MSG$ = "Erro ao executar a st_pi_cn_preco_base_s " & "RETURN STATUS = " & sql_cd_ret
'    Exit Function
'End If

FU_BuscaPrecoRefProduto = SUCCED%
Exit Function

FU_BuscaPrecoRefProduto:
    MSG$ = "FU_BuscaPrecoRefProduto - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_BuscaPrecoRefProdutoL(nm_ciclo_operacional As String, cd_venda_produto As String, MSG$, Lista As Boolean) As Integer
    Dim sql     As String
    Dim X       As Long

    FU_BuscaPrecoRefProdutoL = FAIL%

    On Error GoTo FU_BuscaPrecoRefProdutoL

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec st_pdr_cn_preco_base_s " & nm_ciclo_operacional & "," & cd_venda_produto & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            sql_qt_lnh = sql_qt_lnh + 1
            If Lista Then
                'id_tipo_produto = gObjeto.varResultSet(hSql, 0)
                ' Checa se habilita ou n�o redutor de pre�os
                      
                If CDbl(gObjeto.varResultSet(hSql, 2)) - Int(gObjeto.varResultSet(hSql, 2)) > 0 Then
                    txtprecobase.Text = Str(gObjeto.varResultSet(hSql, 2))
                    txtprecobase.Text = ConvertePontoDecimal(txtprecobase, 0)
                    txtprecobase.Text = Format(txtprecobase, "#0.00")
                    txtprecobase.Tag = txtprecobase.Text
                
                    gPrecoBase = True
                Else
                    txtprecobase.Text = gObjeto.varResultSet(hSql, 2)
                    txtprecobase.Text = ConvertePontoDecimal(txtprecobase, 0)
                    txtprecobase.Text = Format(txtprecobase, "#0.00")
                    txtprecobase.Tag = txtprecobase.Text
                
                    gPrecoBase = True
                End If
                'txt_redutor.Text = gObjeto.varResultSet(hSql, 3)
                'txt_redutor.Text = ConvertePontoDecimal(txt_redutor, 0)
                
                'For X = 0 To cmbStatusL.ListCount - 1
                '    If cmbStatusL.ItemData(X) = gObjeto.varResultSet(hSql, 1) Then
                '        cmbStatusL.ListIndex = X
                '    Exit For
                'End If
                'Next X
                'txtstatus.Text = gObjeto.varResultSet(hSql, 1)
                'txtpontos.Text = gObjeto.varResultSet(hSql, 5)
            
                'If gObjeto.varResultSet(hSql, 6) = "1" Then
                '    optPreco(0).Value = True
                'ElseIf gObjeto.varResultSet(hSql, 6) = "0" Then
                '    optPreco(1).Value = True
                'End If
            End If
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_preco_base_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If


    FU_BuscaPrecoRefProdutoL = SUCCED%
    Exit Function

FU_BuscaPrecoRefProdutoL:
    MSG$ = "FU_BuscaPrecoRefProdutoL - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_BuscaPrecoSugProdutoL(nm_ciclo_operacional As String, cd_venda_produto As String, MSG$, Lista As Integer) As Integer
    
    Dim sql         As String
    Dim i           As Integer
    Dim avData()    As Variant
    Dim X           As Long
    Dim v_valor     As String
    Dim Y As Long

    FU_BuscaPrecoSugProdutoL = FAIL%

    On Error GoTo FU_BuscaPrecoSugProdutoL
    
    'sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    'sql = sql & " exec st_pi_cn_preco_ref_kit_s " & nm_ciclo_operacional & "," & cd_produto '& _
    '",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    'sql = " exec st_pi_cn_preco_ref_kit_s " & nm_ciclo_operacional & "," & cd_produto
    
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec st_pdr_cn_preco_sug_s " & Lista & ", " & nm_ciclo_operacional & "," & cd_venda_produto & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0

    bResp = gObjeto.natExecuteQuery(hSql, sql, avData())
 
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_preco_base_kit_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    For X = 0 To UBound(avData, 2)

        sql_qt_lnh = sql_qt_lnh + 1
    
        If Lista Then
            If CDbl(avData(2, X)) - Int(avData(2, X)) > 0 Then
                id_tipo_produto = avData(0, X)
                txtprecosugestao.Text = Str(avData(2, X))
                txtprecosugestao.Text = ConvertePontoDecimal(txtprecosugestao, 0)
                txtprecosugestao.Text = Format(txtprecosugestao, "#0.00")
                txtprecosugestao.Tag = txtprecosugestao.Text
            Else
                id_tipo_produto = avData(0, X)
                ' Checa se habilita ou n�o redutor de pre�os
                txtprecosugestao.Text = avData(2, X)
                txtprecosugestao.Text = ConvertePontoDecimal(txtprecosugestao, 0)
                txtprecosugestao.Text = Format(txtprecosugestao, "#0.00")
                txtprecosugestao.Tag = txtprecosugestao.Text
            End If
            For Y = 0 To cmbStatusL.ListCount - 1
                If cmbStatusL.ItemData(Y) = avData(1, X) Then
                    cmbStatusL.ListIndex = Y
                Exit For
            End If
            Next Y
            txtpontos.Text = avData(3, X)
            txtredutorlista.Text = avData(4, X)
            txtredutorlista.Text = ConvertePontoDecimal(txtredutorlista.Text, 0)
        End If
    Next X

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pdr_cn_preco_sug_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    FU_BuscaPrecoSugProdutoL = SUCCED%
    Exit Function

FU_BuscaPrecoSugProdutoL:
    MSG$ = "FU_BuscaPrecoSugProdutoL - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_ValidaPrecoProduto(nm_ciclo_operacional As String, cd_produto As String, MSG$) As Integer
Dim sql As String
Dim i As Integer

FU_ValidaPrecoProduto = FAIL%

On Error GoTo FU_ValidaPrecoProduto

Erase v_produtos_venda

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pi_cn_produto_venda_s " & nm_ciclo_operacional & "," & cd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

i = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        ReDim Preserve v_produtos_venda(0 To i)
        v_produtos_venda(i).cd_produto = gObjeto.varResultSet(hSql, 0)
        v_produtos_venda(i).dc_produto = gObjeto.varResultSet(hSql, 1)
        v_produtos_venda(i).vl_preco_referencia = gObjeto.varResultSet(hSql, 2)
        i = i + 1
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_produto_venda_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

sql_cd_ret = CInt(gObjeto.varOutputParam(0))
sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

If sql_cd_ret <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_produto_venda_s " & "RETURN STATUS = " & sql_cd_ret
    Exit Function
End If

FU_ValidaPrecoProduto = SUCCED%
Exit Function

FU_ValidaPrecoProduto:
    MSG$ = "FU_ValidaPrecoProduto - Erro (" & Err & ") - " & Error
    Exit Function

End Function



Private Function FU_AtualizaPrecoRef(nm_ciclo_operacional As String, cd_produto As String, id_tipo_produto As String, id_status_venda As String, vl_preco_referencia As String, pc_redutor_pontos As String, qt_pontos As String, pc_redutor_preco As String, MSG$) As Integer

Dim sql          As String
Dim dt_parametro As String
Dim se_Usuario   As String
Dim ne_ret       As Integer
Dim qtdciclo     As Integer

FU_AtualizaPrecoRef = FAIL%

On Error GoTo FU_AtualizaPrecoRef

   se_Usuario = Space(255)
   ne_ret = GetUserName(se_Usuario, 255)
   se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
   dt_parametro = Format(Now(), "mm/dd/yyyy hh:mm:ss")

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_preco_ref_m " & nm_ciclo_operacional & ","
sql = sql & cd_produto & ","
sql = sql & id_tipo_produto & ","
sql = sql & id_status_venda & "," & ConvertePontoDecimal(vl_preco_referencia, 1) & ","
sql = sql & ConvertePontoDecimal(pc_redutor_preco, 1) & ","
sql = sql & ConvertePontoDecimal(pc_redutor_pontos, 1) & ","
sql = sql & qt_pontos & ","
sql = sql & IIf(optPreco(0).Value = True, 1, 0) & ","
sql = sql & Chr(39) & Le_Usuario & Chr(39) & ","
sql = sql & Chr(39) & dt_parametro & Chr(39) & ","
sql = sql & " @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out "

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_pdr_preco_ref_m " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
    Exit Function
End If

sql_cd_ret = Val(gObjeto.varOutputParam(0))
sql_qt_lnh = Val(gObjeto.varOutputParam(3))

If Val(v_cd_material_vigente) <= 0 And sql_qt_lnh > 0 Then
    v_cd_material_vigente = cd_produto
End If

If sql_qt_lnh > 0 Then
    For qtdciclo = 1 To UBound(ciclosFuturos)
        If Right(ciclosFuturos(qtdciclo).ciclo, 4) + Left(ciclosFuturos(qtdciclo).ciclo, 2) = nm_ciclo_operacional Then
            ciclosFuturos(qtdciclo).cd_material_vigente = cd_produto
        End If
    Next qtdciclo
End If

If sql_cd_ret <> 0 Then
    MSG$ = "Erro ao executar a stp_pdr_preco_ref_m " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
    Exit Function
End If

FU_AtualizaPrecoRef = SUCCED%
Exit Function

FU_AtualizaPrecoRef:
    MSG$ = "FU_AtualizaPrecoRef - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_AtualizaPrecoSug(v_redutor As String, nm_ciclo_operacional As String, cd_produto As String, id_tipo_produto As String, cd_lista As String, qt_pontos As String, vl_preco_referencia As String, MSG$) As Integer

    Dim sql          As String
    Dim dt_parametro As String
    Dim se_Usuario   As String
    Dim ne_ret       As Integer
    Dim qtdciclo     As Integer

    FU_AtualizaPrecoSug = FAIL%

    On Error GoTo FU_AtualizaPrecoSug

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    dt_parametro = Format(Now(), "mm/dd/yyyy hh:mm:ss")



    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_sugestao_u " & Str(v_redutor) & ", " & nm_ciclo_operacional & ", " & cd_lista & ", "
    sql = sql & cd_produto & ","
    sql = sql & id_tipo_produto & ","
    sql = sql & Le_Usuario & "," & qt_pontos & "," & ConvertePontoDecimal(vl_preco_referencia, 1) & ","
    sql = sql & " @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out "

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sugestao_u " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
        Exit Function
    End If

    sql_cd_ret = Val(gObjeto.varOutputParam(0))
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))

    'If Val(v_cd_material_vigente) <= 0 And sql_qt_lnh > 0 Then
    '    v_cd_material_vigente = cd_produto
    'End If

    'If sql_qt_lnh > 0 Then
    '    For qtdciclo = 1 To UBound(ciclosFuturos)
    '        If Right(ciclosFuturos(qtdciclo).ciclo, 4) + Left(ciclosFuturos(qtdciclo).ciclo, 2) = nm_ciclo_operacional Then
    '            ciclosFuturos(qtdciclo).cd_material_vigente = cd_produto
    '        End If
    '    Next qtdciclo
    'End If

    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sugestao_u " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
        Exit Function
    End If

    FU_AtualizaPrecoSug = SUCCED%
    Exit Function

FU_AtualizaPrecoSug:
    MSG$ = "FU_AtualizaPrecoSug - Erro (" & Err & ") - " & Error
    Exit Function

End Function
Private Function FU_AtualizaPrecoFutSug(v_redutor As String, nm_ciclo_operacional As String, cd_produto As String, id_tipo_produto As String, cd_lista As String, qt_pontos As String, vl_preco_referencia As String, MSG$) As Integer

    Dim sql          As String
    Dim dt_parametro As String
    Dim se_Usuario   As String
    Dim ne_ret       As Integer
    Dim qtdciclo     As Integer

    FU_AtualizaPrecoFutSug = FAIL%

    On Error GoTo FU_AtualizaPrecoFutSug

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    dt_parametro = Format(Now(), "mm/dd/yyyy hh:mm:ss")

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_sug_fut_u " & Str(v_redutor) & ", " & nm_ciclo_operacional & ", " & cd_lista & ", "
    sql = sql & cd_produto & ","
    sql = sql & id_tipo_produto & ","
    sql = sql & Le_Usuario & "," & qt_pontos & "," & ConvertePontoDecimal(vl_preco_referencia, 1) & ","
    sql = sql & IIf(blGravaLog, 1, 0) & ","
    sql = sql & " @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out "

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sug_fut_u " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
        Exit Function
    End If

    sql_cd_ret = Val(gObjeto.varOutputParam(0))
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))

    'If Val(v_cd_material_vigente) <= 0 And sql_qt_lnh > 0 Then
    '    v_cd_material_vigente = cd_produto
    'End If

    'If sql_qt_lnh > 0 Then
    '    For qtdciclo = 1 To UBound(ciclosFuturos)
    '        If Right(ciclosFuturos(qtdciclo).ciclo, 4) + Left(ciclosFuturos(qtdciclo).ciclo, 2) = nm_ciclo_operacional Then
    '            ciclosFuturos(qtdciclo).cd_material_vigente = cd_produto
    '        End If
    '    Next qtdciclo
    'End If

    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sug_fut_u " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
        Exit Function
    End If

    FU_AtualizaPrecoFutSug = SUCCED%
    Exit Function

FU_AtualizaPrecoFutSug:
    MSG$ = "FU_AtualizaPrecoFutSug - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_AtualizaStatusVenda(v_status As String, nm_ciclo_operacional As String, cd_produto As String, cd_lista As String, MSG$) As Boolean

    Dim sql          As String
    Dim dt_parametro As String
    Dim se_Usuario   As String
    Dim ne_ret       As Integer
    Dim qtdciclo     As Integer

    FU_AtualizaStatusVenda = False

    On Error GoTo FU_AtualizaStatusVenda

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    dt_parametro = Format(Now(), "mm/dd/yyyy hh:mm:ss")

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_status_venda_u " & v_status & ", " & nm_ciclo_operacional & ", " & cd_lista & ", "
    sql = sql & cd_produto & ","
    sql = sql & Le_Usuario & ","
    sql = sql & " @sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out "

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_status_venda_u " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
        Exit Function
    End If

    sql_cd_ret = Val(gObjeto.varOutputParam(0))
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))

    'If Val(v_cd_material_vigente) <= 0 And sql_qt_lnh > 0 Then
    '    v_cd_material_vigente = cd_produto
    'End If

    'If sql_qt_lnh > 0 Then
    '    For qtdciclo = 1 To UBound(ciclosFuturos)
    '        If Right(ciclosFuturos(qtdciclo).ciclo, 4) + Left(ciclosFuturos(qtdciclo).ciclo, 2) = nm_ciclo_operacional Then
    '            ciclosFuturos(qtdciclo).cd_material_vigente = cd_produto
    '        End If
    '    Next qtdciclo
    'End If

    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_status_venda_u " & gObjeto.sServerMsg & Chr(10) & Chr(10) & "Ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4)
        Exit Function
    End If

    FU_AtualizaStatusVenda = True
    Exit Function

FU_AtualizaStatusVenda:
    MSG$ = "FU_AtualizaStatusVenda - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_AtualizaPrecoProduto(nm_ciclo_operacional As String, cd_produto As String, vl_preco_referencia As String, MSG$) As Integer
Dim sql As String

FU_AtualizaPrecoProduto = FAIL%

On Error GoTo FU_AtualizaPrecoProduto


sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pi_cn_produto_venda_u " & nm_ciclo_operacional & "," & cd_produto & _
"," & ConvertePontoDecimal(vl_preco_referencia, 1) & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_produto_venda_u - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

sql_cd_ret = CInt(gObjeto.varOutputParam(0))
sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

If sql_cd_ret <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret
    Exit Function
End If

FU_AtualizaPrecoProduto = SUCCED%
Exit Function

FU_AtualizaPrecoProduto:
    MSG$ = "FU_AtualizaPrecoProduto - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_AtualizaPontosFilhos(nm_ciclo_operacional As String, cd_produto As String, MSG$) As Integer
Dim sql As String
Dim msg_err As String

FU_AtualizaPontosFilhos = FAIL%

On Error GoTo FU_AtualizaPontosFilhos


sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err varchar(255)"
sql = sql & " exec st_pi_cn_produto_pontos_u " & nm_ciclo_operacional & "," & cd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out, @msg_err out"

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_produto_pontos_u - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

sql_cd_ret = CInt(gObjeto.varOutputParam(0))
sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
msg_err = gObjeto.varOutputParam(4)

If sql_cd_ret <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_produto_pontos_u " & "RETURN STATUS = " & sql_cd_ret & " " & msg_err
    Exit Function
End If

FU_AtualizaPontosFilhos = SUCCED%
Exit Function

FU_AtualizaPontosFilhos:
    MSG$ = "FU_AtualizaPontosFilhos - Erro (" & Err & ") - " & Error
    Exit Function

End Function



Private Function FU_BuscaProdutoKit(nm_ciclo_operacional, cd_produto As String, MSG$) As Integer

    Dim sql  As String
    Dim i    As Integer
    

FU_BuscaProdutoKit = FAIL%

On Error GoTo FU_BuscaProdutoKit

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pi_cn_produto_venda_kit_s " & cd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0

    If gObjeto.natPrepareQuery(hSql, sql) Then
       Do While gObjeto.natFetchNext(hSql)
       
            sql_qt_lnh = sql_qt_lnh + 1
            
            i = UBound(v_componentes) + 1
            ReDim Preserve v_componentes(i)
            
            v_componentes(i).cd_produto_pai = cd_produto
            v_componentes(i).cd_venda_item = gObjeto.varResultSet(hSql, 4)
            
            v_componentes(i).cd_item_kit = gObjeto.varResultSet(hSql, 0)
            v_componentes(i).dc_produto = gObjeto.varResultSet(hSql, 1)
            v_componentes(i).vl_preco_referencia_kit = "0"
            v_componentes(i).qt_item_kit = gObjeto.varResultSet(hSql, 3)
            v_componentes(i).qt_pontos_referencia_kit = 0
            
            v_componentes(i).novo = True
            v_componentes(i).KitOK = False
            
            ' i = i + 1
       Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_produto_venda_kit_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    'If sql_cd_ret <> 0 Then
    '    MSG$ = "Erro ao executar a st_pi_cn_produto_kit_s " & "RETURN STATUS = " & sql_cd_ret
    '    Exit Function
    'End If
    
    'If i = 0 Then ReDim v_componentes(0)
    
    FU_BuscaProdutoKit = SUCCED%
    Exit Function

FU_BuscaProdutoKit:
    MSG$ = "FU_BuscaProdutoKit - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_BuscaProdutoKit_Futuro(nm_ciclo_operacional, cd_produto As String, MSG$) As Integer

    Dim sql  As String
    Dim i    As Integer
    

FU_BuscaProdutoKit_Futuro = FAIL%

On Error GoTo FU_BuscaProdutoKit_Futuro

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec st_pi_cn_produto_venda_kit_s " & cd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0

    If gObjeto.natPrepareQuery(hSql, sql) Then
       Do While gObjeto.natFetchNext(hSql)
       
            sql_qt_lnh = sql_qt_lnh + 1
            
            i = UBound(v_componentes_futuros) + 1
            ReDim Preserve v_componentes_futuros(i)
            
            v_componentes_futuros(i).cd_produto_pai = cd_produto
            v_componentes_futuros(i).cd_venda_item = gObjeto.varResultSet(hSql, 4)
            
            v_componentes_futuros(i).cd_item_kit = gObjeto.varResultSet(hSql, 0)
            v_componentes_futuros(i).dc_produto = gObjeto.varResultSet(hSql, 1)
            v_componentes_futuros(i).vl_preco_referencia_kit = "0"
            v_componentes_futuros(i).qt_item_kit = gObjeto.varResultSet(hSql, 3)
            v_componentes_futuros(i).qt_pontos_referencia_kit = 0
            
            v_componentes_futuros(i).novo = True
            v_componentes_futuros(i).KitOK = False
            v_componentes_futuros(i).ciclo = nm_ciclo_operacional
            
            ' i = i + 1
       Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_produto_venda_kit_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    'If sql_cd_ret <> 0 Then
    '    MSG$ = "Erro ao executar a st_pi_cn_produto_kit_s " & "RETURN STATUS = " & sql_cd_ret
    '    Exit Function
    'End If
    
    'If i = 0 Then ReDim v_componentes_futuroS(0)
    
    FU_BuscaProdutoKit_Futuro = SUCCED%
    Exit Function

FU_BuscaProdutoKit_Futuro:
    MSG$ = "FU_BuscaProdutoKit_Futuro - Erro (" & Err & ") - " & Error
    Exit Function

End Function


'Private Function FU_BuscaPrecoRefKit(nm_ciclo_operacional As String, cd_produto As String, MSG$) As Integer
'
'Dim sql As String
'Dim i As Integer
'
'FU_BuscaPrecoRefKit = FAIL%
'
'On Error GoTo FU_BuscaPrecoRefKit
'
'sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
'sql = sql & " exec st_pi_cn_preco_ref_kit_s " & nm_ciclo_operacional & "," & cd_produto & _
'",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
'
'sql_qt_lnh = 0
'
'If gObjeto.natPrepareQuery(hSql, sql) Then
'    Do While gObjeto.natFetchNext(hSql)
'        sql_qt_lnh = sql_qt_lnh + 1
'
'        i = UBound(v_componentes) + 1
'        ReDim Preserve v_componentes(i)
'
'        v_componentes(i).cd_produto_pai = cd_produto
'        v_componentes(i).cd_venda_item = gObjeto.varResultSet(hSql, 5)
'
'        v_componentes(i).cd_item_kit = gObjeto.varResultSet(hSql, 0)
'        v_componentes(i).dc_produto = gObjeto.varResultSet(hSql, 1)
'        v_componentes(i).vl_preco_referencia_kit = gObjeto.varResultSet(hSql, 2)
'        v_componentes(i).qt_item_kit = gObjeto.varResultSet(hSql, 3)
'        v_componentes(i).qt_pontos_referencia_kit = gObjeto.varResultSet(hSql, 4)
'
'        v_componentes(i).novo = False
'        v_componentes(i).KitOK = False
'        'i = i + 1
'    Loop
'End If
'
'If gObjeto.nErroSQL <> 0 Then
'    MSG$ = "Erro ao executar a st_pi_cn_preco_base_kit_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
'    Exit Function
'End If
'
''sql_cd_ret = CInt(gObjeto.varOutputParam(0))
''sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
'
''If sql_cd_ret <> 0 Then
''    MSG$ = "Erro ao executar a st_pi_cn_preco_base_kit_s " & "RETURN STATUS = " & sql_cd_ret
''    Exit Function
''End If
'
''If i = 0 Then ReDim v_componentes(0)
'
'FU_BuscaPrecoRefKit = SUCCED%
'Exit Function
'
'FU_BuscaPrecoRefKit:
'    MSG$ = "FU_BuscaPrecoRefKit - Erro (" & Err & ") - " & Error
'    Exit Function
'
'End Function

Private Function FU_BuscaPrecoRefKit(nm_ciclo_operacional As String, cd_produto As String, MSG$) As Integer

Dim sql         As String
Dim i           As Integer
Dim avData()    As Variant
Dim X           As Long
Dim v_valor     As String

FU_BuscaPrecoRefKit = FAIL%

On Error GoTo FU_BuscaPrecoRefKit

'sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
'sql = sql & " exec st_pi_cn_preco_ref_kit_s " & nm_ciclo_operacional & "," & cd_produto '& _
'",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql = " exec st_pi_cn_preco_ref_kit_s " & nm_ciclo_operacional & "," & cd_produto

sql_qt_lnh = 0

bResp = gObjeto.natExecuteQuery(hSql, sql, avData())
 
If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_preco_base_kit_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

For X = 0 To UBound(avData, 2)

    sql_qt_lnh = sql_qt_lnh + 1
    
    i = UBound(v_componentes) + 1
    ReDim Preserve v_componentes(i)
    
    v_componentes(i).cd_produto_pai = cd_produto
    v_componentes(i).cd_venda_item = avData(5, X) 'gObjeto.varResultSet(hSql, 5)
    
    v_componentes(i).cd_item_kit = avData(0, X) 'gObjeto.varResultSet(hSql, 0)
    v_componentes(i).dc_produto = avData(1, X) 'gObjeto.varResultSet(hSql, 1)
    v_valor = avData(2, X)
    v_componentes(i).vl_preco_referencia_kit = ConvertePontoDecimal(v_valor, 0) 'gObjeto.varResultSet(hSql, 2)
    'v_componentes(i).vl_preco_referencia_kit = avData(2, X) 'gObjeto.varResultSet(hSql, 2)
    v_componentes(i).qt_item_kit = avData(3, X) 'gObjeto.varResultSet(hSql, 3)
    v_componentes(i).qt_pontos_referencia_kit = avData(4, X) 'gObjeto.varResultSet(hSql, 4)
    
    v_componentes(i).novo = False
    v_componentes(i).KitOK = False
    
    
    
    
    
    
Next X

FU_BuscaPrecoRefKit = SUCCED%
Exit Function

FU_BuscaPrecoRefKit:
    MSG$ = "FU_BuscaPrecoRefKit - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_BuscaPrecoSugKit(nm_ciclo_operacional As String, cd_produto As String, cd_lista As String, MSG$) As Integer

Dim sql         As String
Dim i           As Integer
Dim avData()    As Variant
Dim X           As Long
Dim v_valor     As String

FU_BuscaPrecoSugKit = FAIL%

On Error GoTo FU_BuscaPrecoSugKit

'sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
'sql = sql & " exec st_pi_cn_preco_ref_kit_s " & nm_ciclo_operacional & "," & cd_produto '& _
'",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql = " exec st_pi_cn_preco_sug_kit_s " & nm_ciclo_operacional & "," & cd_produto & "," & cd_lista

sql_qt_lnh = 0

bResp = gObjeto.natExecuteQuery(hSql, sql, avData())
 
If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a st_pi_cn_preco_sug_kit_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

For X = 0 To UBound(avData, 2)
    
    'If v_componentes(i).cd_produto_pai <> "" Then
        sql_qt_lnh = sql_qt_lnh + 1
    
        i = UBound(v_componentes) + 1
        ReDim Preserve v_componentes(i)
    
        If UBound(avData, 2) = 0 Then
            v_componentes(i).cd_produto_pai = ""
            v_componentes(i).cd_venda_item = ""
            v_componentes(i).cd_item_kit = ""
            v_componentes(i).dc_produto = ""
            v_valor = 0
            v_componentes(i).vl_preco_referencia_kit = ConvertePontoDecimal(v_valor, 0) 'gObjeto.varResultSet(hSql, 2)
            v_componentes(i).qt_item_kit = ""
            v_componentes(i).qt_pontos_referencia_kit = ""
        Else
            v_componentes(i).cd_produto_pai = cd_produto
            v_componentes(i).cd_venda_item = avData(5, X) 'gObjeto.varResultSet(hSql, 5)
            v_componentes(i).cd_item_kit = avData(0, X) 'gObjeto.varResultSet(hSql, 0)
            v_componentes(i).dc_produto = avData(1, X) 'gObjeto.varResultSet(hSql, 1)
            v_valor = avData(2, X)
            v_componentes(i).vl_preco_referencia_kit = ConvertePontoDecimal(v_valor, 0) 'gObjeto.varResultSet(hSql, 2)
            v_componentes(i).qt_item_kit = avData(3, X) 'gObjeto.varResultSet(hSql, 3)
            v_componentes(i).qt_pontos_referencia_kit = avData(4, X) 'gObjeto.varResultSet(hSql, 4)
        End If
    
        v_componentes(i).novo = False
        v_componentes(i).KitOK = False
    'End If
    
Next X

FU_BuscaPrecoSugKit = SUCCED%
Exit Function

FU_BuscaPrecoSugKit:
    MSG$ = "FU_BuscaPrecoSugKit - Erro (" & Err & ") - " & Error
    Exit Function

End Function



Private Function FU_ExisteProduto(cd_vnd_produto As String, MSG$) As Integer

Dim sql As String

FU_ExisteProduto = FAIL%

On Error GoTo FU_ExisteProduto

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_produto_venda_s " & cd_vnd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        sql_qt_lnh = sql_qt_lnh + 1
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_pdr_produto_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

'If sql_cd_ret <> 0 Then
'    MSG$ = "Erro ao executar a st_pi_cn_produto_s " & "RETURN STATUS = " & sql_cd_ret
'    Exit Function
'End If

FU_ExisteProduto = SUCCED%
Exit Function

FU_ExisteProduto:
    MSG$ = "FU_ExisteProduto - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Sub cmbCiclo_Click()
    txt_cicloano.Text = Left$(cmbCiclo.Text, 2) & Right(cmbCiclo.Text, 4)
    Call txt_cicloano_KeyPress(13)
End Sub

Private Sub cmbCiclo_GotFocus()
    Call txt_cicloano_GotFocus
End Sub

Private Sub cmbCiclo_LostFocus()
    Call txt_cicloano_LostFocus
End Sub

Private Sub cmbCicloL_Click()
    If cmbCicloL.Text = "" Then
        Exit Sub
    End If

    txtcodvenda.Text = ""
    txtdescricao.Text = ""
    txtprecobase.Text = ""
    txtprecosugestao = ""
    txtpontos.Text = ""
    cmbStatusL.ListIndex = -1
    txtredutorlista.Text = ""
    cmbRegEstrat.ListIndex = -1
    cmbGerVenda.ListIndex = -1
    cmbSetor.ListIndex = -1

'    If Not PreencheGrid(Right$(cmbCicloL.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
'       SetStatus "Erro na leitura..."
'       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
'    End If
'
'    If Not Combo_Lista(Me.cmbListas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
'       SetStatus "Erro na leitura..."
'       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
'    End If
'
'    If cmbListas.Enabled Then
'        cmbListas.SetFocus
'    End If
'    If cmbRegEstrat.Enabled Then
'        cmbRegEstrat.SetFocus
'    End If
End Sub


Private Sub cmbGerVenda_Click()
   If cmbCicloL.ListIndex = -1 Then
        MsgBox "� preciso selecionar um ciclo", 16, "PDR"
        cmbGerVenda.Text = ""
        cmbCicloL.SetFocus
        Exit Sub
    End If
    
   If cmbRegEstrat.ListIndex = -1 Then
        MsgBox "� preciso selecionar uma Regi�o Estrategica", 16, "PDR"
        cmbGerVenda.Text = ""
        cmbRegEstrat.SetFocus
      Exit Sub
   End If
   
    
   If cmbGerVenda.ListIndex = -1 Then
      Exit Sub
   End If
   
   cmbSetor.Clear
   cmbSetor.ListIndex = -1
'  cmbSetor.Text = ""

    txtcodvenda.Text = ""
   txtdescricao.Text = ""
   txtprecobase.Text = ""
   txtprecosugestao = ""
   txtpontos.Text = ""
   txtredutorlista.Text = ""
   cmbStatusL.ListIndex = -1

   If Not Lista_Estrutura(cmbSetor, 4, cmbRegEstrat.ItemData(cmbRegEstrat.ListIndex), _
                                       cmbGerVenda.ItemData(cmbGerVenda.ListIndex)) Then
        SetStatus "Erro na leitura..."
        'MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
        lblMensagem.Caption = MSG$
        PonteiroMouse vbNormal
   Else
        lblMensagem.Caption = ""
   End If
   
   
   
   If Verifica_Est_Lista(3, cmbCicloL.ItemData(cmbCicloL.ListIndex), cmbGerVenda.ItemData(cmbGerVenda.ListIndex)) <> 0 Then
       cmbLista.Text = Verifica_Est_Lista(3, cmbCicloL.ItemData(cmbCicloL.ListIndex), cmbGerVenda.ItemData(cmbGerVenda.ListIndex))
   Else
       cmbLista.Text = ""
   End If
   
End Sub


Private Sub cmbRegEstrat_Click()
    If cmbCicloL.ListIndex = -1 Then
        MsgBox "� preciso selecionar um ciclo", 16, "PDR"
        cmbRegEstrat.Text = ""
        cmbCicloL.SetFocus
        Exit Sub
    End If

   If cmbRegEstrat.ListIndex = -1 Then
      Exit Sub
   End If
   
   cmbGerVenda.Clear
   cmbGerVenda.ListIndex = -1
   
   cmbSetor.Clear
   cmbSetor.ListIndex = -1
   
   txtcodvenda.Text = ""
   txtdescricao.Text = ""
   txtprecobase.Text = ""
   txtprecosugestao = ""
   txtpontos.Text = ""
   txtredutorlista.Text = ""
   cmbStatusL.ListIndex = -1
   
     
   DoEvents
   
   If Not Lista_Estrutura(Me.cmbGerVenda, 3, cmbRegEstrat.ItemData(cmbRegEstrat.ListIndex), 0) Then
      SetStatus "Erro na leitura..."
      'MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      lblMensagem.Caption = MSG$
   Else
      lblMensagem.Caption = ""
   End If
    
   cmbLista.Text = Verifica_Est_Lista(2, cmbCicloL.ItemData(cmbCicloL.ListIndex), cmbRegEstrat.ItemData(cmbRegEstrat.ListIndex))

End Sub


Private Sub cmbSetor_Click()
    If cmbCicloL.ListIndex = -1 Then
        MsgBox "� preciso selecionar um ciclo", 16, "PDR"
        cmbSetor.Text = ""
        cmbCicloL.SetFocus
        Exit Sub
    End If
    
    
    If cmbRegEstrat.ListIndex = -1 Then
        MsgBox "� preciso selecionar uma Regi�o Estrategica", 16, "PDR"
        cmbSetor.Text = ""
        cmbRegEstrat.SetFocus
      Exit Sub
   End If
   
    
   If cmbGerVenda.ListIndex = -1 Then
       MsgBox "� preciso selecionar uma Gerencia de Vendas", 16, "PDR"
       cmbSetor.Text = ""
       cmbGerVenda.SetFocus
      Exit Sub
   End If
   

   If Verifica_Est_Lista(4, cmbCicloL.ItemData(cmbCicloL.ListIndex), cmbSetor.ItemData(cmbSetor.ListIndex)) <> 0 Then
       cmbLista.Text = Verifica_Est_Lista(4, cmbCicloL.ItemData(cmbCicloL.ListIndex), cmbSetor.ItemData(cmbSetor.ListIndex))
   Else
       cmbLista.Text = ""
   End If
   lblMensagem.Caption = ""
   
   txtcodvenda.Text = ""
   txtdescricao.Text = ""
   txtprecobase.Text = ""
   txtprecosugestao = ""
   txtpontos.Text = ""
   txtredutorlista.Text = ""
   cmbStatusL.ListIndex = -1
   
End Sub


Private Sub cmbStatus_Change()

    If cmbStatus.ListIndex <> -1 Then
        txt_status.Text = cmbStatus.ItemData(cmbStatus.ListIndex)
    End If

End Sub

Private Sub cmbStatus_Click()

    If cmbStatus.ListIndex <> -1 Then
        txt_status.Text = cmbStatus.ItemData(cmbStatus.ListIndex)
    End If

End Sub

Private Sub cmbStatusL_Click()
    Dim nm_ciclo As String
    If Mid(cmbStatusL.Text, 1, 1) = "2" Then
        txtredutorlista.Text = 100
        nm_ciclo = Right(cmbCicloL, 4) + Left(cmbCicloL, 2)
        txtprecosugestao.Text = Format(txtprecosugestao.Text, "##0.00")
        ' calcular pontos com redutor
        txtpontos.Text = Format(FU_BuscaPontuacao(Str(nm_ciclo), Str(txtprecosugestao), MSG$), "#0")
        txtpontos.Text = Format(txtpontos.Text * (txtredutorlista.Text / 100), "#0")
        ' fim
    End If
End Sub


Private Sub cmd_componentes_Click()
Dim i As Integer
Dim total As Double
Dim nm_ciclo_operacional As String
Dim Preco As String
Dim vl_PrecoTotal As Double
Dim qtde As Integer
Dim Pontos As Integer
Dim PontosTot As Integer

Unload frmCompKit

Load frmCompKit


If Val(Trim$(txt_precobase)) = 0 Then
    MsgBox "Pre�o refer�ncia n�o pode ser zero !", vbInformation, "A T E N � � O"
    txt_precobase.SetFocus
    Exit Sub
End If

If Trim$(txt_redutor) = "" Then
    MsgBox "Percentual redutor de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
    txt_redutor.SetFocus
    Exit Sub
End If

If Trim$(txt_pontos) = "" Then
    MsgBox "N�mero de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
    txt_pontos.SetFocus
    Exit Sub
End If

If Trim$(txt_status) = "" Then
    txt_status = "4"
End If

nm_ciclo_operacional = Right(txt_cicloano, 4) + Left(txt_cicloano, 2)

total = 0
qtde = 0

For i = 1 To UBound(v_componentes)
        
    If v_componentes(i).cd_produto_pai = v_cd_material Then
    
        'If Not v_componentes(i).novo Then
            'If v_componentes(i).vl_preco_referencia_kit <> "" Then
            '    total = total + (CDbl(ConvertePontoDecimal(Str(v_componentes(i).vl_preco_referencia_kit), 0)) * v_componentes(i).qt_item_kit)
            '    vl_PrecoTotal = CDbl(ConvertePontoDecimal(Str(v_componentes(i).vl_preco_referencia_kit), 0)) * v_componentes(i).qt_item_kit
           '
           '
           ' frmCompKit.RecebeParametro v_componentes(i).cd_venda_item, _
           '     v_componentes(i).dc_produto, v_componentes(i).qt_item_kit, _
           '     v_componentes(i).vl_preco_referencia_kit, Str(vl_PrecoTotal), _
           '     v_componentes(i).qt_pontos_referencia_kit * _
           '     v_componentes(i).qt_item_kit, 0
           '
           '     qtde = qtde + v_componentes(i).qt_item_kit
           '     Pontos = Pontos + v_componentes(i).qt_pontos_referencia_kit * v_componentes(i).qt_item_kit
           ' End If
            
            total = total + (CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) * v_componentes(i).qt_item_kit)
            vl_PrecoTotal = CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) * v_componentes(i).qt_item_kit
            
            frmCompKit.RecebeParametro v_componentes(i).cd_venda_item, _
                v_componentes(i).dc_produto, v_componentes(i).qt_item_kit, _
                v_componentes(i).vl_preco_referencia_kit, Str(vl_PrecoTotal), _
                v_componentes(i).qt_pontos_referencia_kit * _
                v_componentes(i).qt_item_kit, 0
                
            qtde = qtde + v_componentes(i).qt_item_kit
            Pontos = Pontos + v_componentes(i).qt_pontos_referencia_kit * v_componentes(i).qt_item_kit
        
    End If
    
Next i
    
If total > 0 Then
    frmCompKit.RecebeParametro "", "TOTAL", Str(qtde), "", Str(total), Str(Pontos), 0
Else
    frmCompKit.RecebeParametro "", "TOTAL", Str(qtde), "", Str(0), Str(Pontos), 0
End If
    
frmCompKit.Caption = "Pre�os CN - Componentes"

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
'If Not ChecaPermissao("frmPDO011", "M") Then
   'frmCompKit.cmd_salvar.Enabled = False
'End If

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
   frmCompKit.cmd_salvar.Enabled = False
End If

frmCompKit.Show 1

End Sub

Private Sub Combo1_Change()

End Sub

Private Sub Combo3_Change()

End Sub

Private Sub Combo4_Change()

End Sub

Private Sub cmdcomponentes_Click()
    Dim i As Integer
    Dim total As Double
    Dim nm_ciclo_operacional As String
    Dim Preco As String
    Dim vl_PrecoTotal As Double
    Dim qtde As Integer
    Dim Pontos As Integer
    Dim PontosTot As Integer

    Unload frmCompKitL

    Load frmCompKitL
    
    If Val(Trim$(txtprecosugestao)) = 0 Then
        MsgBox "Pre�o Sugest�o n�o pode ser zero !", vbInformation, "A T E N � � O"
        txt_precobase.SetFocus
        Exit Sub
    End If

    'If Trim$(txt_redutor) = "" Then
    '    MsgBox "Percentual redutor de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
    '    txt_redutor.SetFocus
    '    Exit Sub
    'End If

    If Trim$(txtpontos) = "" Then
        MsgBox "N�mero de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
        txt_pontos.SetFocus
        Exit Sub
    End If

    'If Trim$(txt_status) = "" Then
    '    txt_status = "4"
    'End If

    nm_ciclo_operacional = Right(cmbCicloL, 4) + Left(cmbCicloL, 2)

    total = 0
    qtde = 0
    
    For i = 1 To UBound(v_componentes)
        
        If v_componentes(i).cd_produto_pai = txtcodvenda.Text Then
    
            'If Not v_componentes(i).novo Then
            If v_componentes(i).vl_preco_referencia_kit <> "" Then
                total = total + (CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) * v_componentes(i).qt_item_kit)
                vl_PrecoTotal = CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) * v_componentes(i).qt_item_kit
            
                frmCompKitL.RecebeParametro v_componentes(i).cd_item_kit, _
                    v_componentes(i).dc_produto, v_componentes(i).qt_item_kit, _
                    v_componentes(i).vl_preco_referencia_kit, Str(vl_PrecoTotal), _
                    v_componentes(i).qt_pontos_referencia_kit * _
                    v_componentes(i).qt_item_kit, 0
                
                qtde = qtde + v_componentes(i).qt_item_kit
                Pontos = Pontos + v_componentes(i).qt_pontos_referencia_kit * v_componentes(i).qt_item_kit
        
            'Else
            '    frmCompKit.RecebeParametro v_componentes(i).cd_venda_item, _
            '    v_componentes(i).dc_produto, v_componentes(i).qt_item_kit, "0", "0", _
            '    v_componentes(i).qt_pontos_referencia_kit * v_componentes(i).qt_item_kit, 0
            '    qtde = qtde + v_componentes(i).qt_item_kit
            '    Pontos = Pontos + v_componentes(i).qt_pontos_referencia_kit * v_componentes(i).qt_item_kit
            'End If
            End If
        
        End If
    
    Next i
    
    If total > 0 Then
        frmCompKitL.RecebeParametro "", "TOTAL", Str(qtde), "", Str(total), Str(Pontos), 0
    Else
        frmCompKitL.RecebeParametro "", "TOTAL", Str(qtde), "", Str(0), Str(Pontos), 0
    End If
    
    frmCompKitL.Caption = "Pre�os CN - Componentes"

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO011", "M") Then
        'frmCompKitL.cmdSalvarComp.Enabled = False
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
        frmCompKitL.cmdSalvarComp.Enabled = False
    End If

    frmCompKitL.Show 1

End Sub

Private Sub Form_Activate()

If produto <> "" And ciclo <> "" Then
   txt_cicloano.Text = ciclo
   txt_Codigo.Text = produto
   txt_Codigo_KeyPress (13)
End If

End Sub

Private Function FU_BuscaPrecoFuturo(nm_ciclo_operacional As String, cd_vnd_produto As String, MSG$, Lista As Boolean) As Integer

Dim sql As String

FU_BuscaPrecoFuturo = FAIL%

On Error GoTo FU_BuscaPrecoFuturo

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_preco_futuro_s " & nm_ciclo_operacional & "," & cd_vnd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0
QtdPrecoFuturo = 0
ReDim ciclosFuturos(0)

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        QtdPrecoFuturo = QtdPrecoFuturo + 1
        
        ReDim Preserve ciclosFuturos(QtdPrecoFuturo)
        ciclosFuturos(QtdPrecoFuturo).ciclo = Mid(gObjeto.varResultSet(hSql, 0), 5, 6) & "/" & Mid(gObjeto.varResultSet(hSql, 0), 1, 4)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_prd_preco_futuro_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If


FU_BuscaPrecoFuturo = SUCCED%
Exit Function

FU_BuscaPrecoFuturo:
    MSG$ = "FU_BuscaPrecoFuturo - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_BuscaPrecoProduto(nm_ciclo_operacional As String, MSG$, Lista As Boolean) As Integer

Dim sql As String

FU_BuscaPrecoProduto = FAIL%

On Error GoTo FU_BuscaPrecoProduto

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_base_preco_s " & nm_ciclo_operacional & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0
QtdPrecoBase = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        QtdPrecoBase = gObjeto.varResultSet(hSql, 0)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_prd_base_preco_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If


FU_BuscaPrecoProduto = SUCCED%
Exit Function

FU_BuscaPrecoProduto:
    MSG$ = "FU_BuscaPrecoProduto - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function Ver_Pontos_Ciclo(v_valor As Long, ciclo As String) As Integer

Dim cSql  As String
Dim bResp As Boolean
Dim i     As Integer
Dim avPrecos()       As Variant
Dim v_cod_lista1 As Integer


On Error GoTo erro

' VERIFICA O MAIOR CODIGO DE LISTA
cSql = "SELECT qt_pontos_faixa_ciclo FROM t_pi_pontuacao_ciclo "
cSql = cSql & " Where nm_ciclo_operacional = " & ciclo
cSql = cSql & " AND " & v_valor & " BETWEEN vl_minimo AND vl_maximo"

ReDim avPrecos(0)
   
bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
If bResp = False Then
  MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
  End
End If

If gObjeto.nErroSQL <> 0 Then
  MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
  Exit Function
End If

Ver_Pontos_Ciclo = avPrecos(0, i)

Exit Function

erro:
  MsgBox "Erro ao obter os pre�os desse ciclo" & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
  Exit Function
  
End Function


Private Sub Form_Load()

    gisysmenu = GetSystemMenu(Me.hWnd, 0)
    
    iCounter = 8
    For gimenucount = iCounter To 6 Step -1
        giretval = RemoveMenu(gisysmenu, _
        gimenucount, MF_BYPOSITION)
    Next gimenucount
    
    Call CarrCicloComPreco(cmbCiclo)
    Call CarrCicloComPreco(cmbCicloL)
    
    If Not Lista_Estrutura(cmbRegEstrat, 2, 0, 0) Then
       SetStatus "Erro na leitura..."
       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
    End If
    
    Me.Top = 0
    Me.Left = 0
    Me.Height = 6360
    
    bm_Alterado = False
    bm_novo = False
    
    ReDim ReajusteLista(0)
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO011", "M") Then
       'st_Toolbar.Buttons.Item(3).Enabled = False
    'End If

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
       st_Toolbar.Buttons.Item(3).Enabled = False
    End If
    
End Sub

Private Sub Form_Resize()

    If Me.WindowState = 0 Then
        If Me.Width <= 3900 Then
            Me.Width = 3900
        End If
    End If

    ' Me.st_Toolbar.Buttons(9).Width = Me.Width - (Me.st_Toolbar.Buttons(1).Width * 8)

End Sub

Sub sExcluir()

    Dim Z                    As Integer
    Dim k                    As Integer
    Dim vAtualizaCicloFuturo As Boolean
    
    vAtualizaCicloFuturo = False

    If MsgBox("Deseja excluir o pre�o refer�ncia deste produto ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
        
        If FU_BuscaPrecoFuturo(Right(txt_cicloano, 4) + Left(txt_cicloano, 2), Trim$(txt_Codigo), MSG$, False) = FAIL% Then
             MsgBox MSG$, vbCritical, "A T E N � � O"
             PonteiroMouse vbNormal
        Else
             If QtdPrecoFuturo > 0 Then
                  If MsgBox("Lista de pre�o de ciclos futuros j� foi gerada ate o ciclo " & ciclosFuturos(QtdPrecoFuturo).ciclo & Chr(10) & Chr(10) & _
                            "Deseja excluir o pre�o do produto das listas dos ciclos futuros ? ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
                            
                        vAtualizaCicloFuturo = True
                  End If
             End If
        End If
    
        PonteiroMouse vbHourglass
        
        ' Inicia transa��o
        If Not Runquery("BEGIN TRANSACTION", hSql) Then
            SetStatus "Erro na grava��o..."
            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
            PonteiroMouse vbNormal
            Exit Sub
        End If
                
        nm_ciclo_operacional = Right(txt_cicloano, 4) + Left(txt_cicloano, 2)
            
        '---------------------------------
        ' excluindo preco de referencia
        '---------------------------------
        For contMat = 1 To UBound(Materiais)
        
            If Not Delete_Produto(nm_ciclo_operacional, Trim$(txt_Codigo.Text), Materiais(contMat).cd_material, id_tipo_produto) Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                PonteiroMouse vbNormal
                Exit Sub
            End If

            
            
            If vAtualizaCicloFuturo Then
            
                For k = 1 To UBound(ciclosFuturos)
                
                    nm_ciclo_futuro = Right(ciclosFuturos(k).ciclo, 4) + Left(ciclosFuturos(k).ciclo, 2)
                    
                    If Not Delete_Produto(nm_ciclo_futuro, Trim$(txt_Codigo.Text), Materiais(contMat).cd_material, id_tipo_produto) Then
                        MsgBox MSG$, vbCritical, "A T E N � � O"
                        If Not Runquery("ROLLBACK", hSql) Then
                            SetStatus "Erro na grava��o..."
                            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                            PonteiroMouse vbNormal
                            Exit Sub
                        End If
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If

                
                
                Next k
                
            End If
                
        Next contMat
        
        'SE 6346 - In�cio
        If Not GravaHistorico(6, "E", "Ciclo", nm_ciclo_operacional, "Produto", Trim$(txt_Codigo.Text), "", "") Then
            MsgBox "Falha ao gravar hist�rico.", vbCritical, "A T E N � � O"
            If Not Runquery("ROLLBACK", hSql) Then
                SetStatus "Erro na grava��o..."
                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            PonteiroMouse vbNormal
            Exit Sub
        End If

        For k = 1 To UBound(ciclosFuturos)
        
            nm_ciclo_futuro = Right(ciclosFuturos(k).ciclo, 4) + Left(ciclosFuturos(k).ciclo, 2)
            
            'SE 6346 - In�cio
            If Not GravaHistorico(6, "E", "Ciclo", nm_ciclo_futuro, "Produto", Trim$(txt_Codigo.Text), "", "") Then
                MsgBox "Falha ao gravar hist�rico.", vbCritical, "A T E N � � O"
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                PonteiroMouse vbNormal
                Exit Sub
            End If
            'SE 6346 - Fim

        
        
        Next k

        'SE 6346 - Fim
        
        
        
        If Not Runquery("COMMIT", hSql) Then
            SetStatus "Erro na grava��o..."
            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
            PonteiroMouse vbNormal
            Exit Sub
        End If
        
        MsgBox "Exclus�o efetuada!", vbInformation, "A V I S O !"
                

        If FU_BuscaPrecoBase(Right(txt_cicloano, 4) + Left(txt_cicloano, 2), MSG$, False) = FAIL% Then
             MsgBox MSG$, vbCritical, "A T E N � � O"
             PonteiroMouse vbNormal
        Else
            If QtdPrecoBase > 0 Then
                MsgBox "Relat�rio Bases de Pre�os j� foi gerado !!! Necess�rio gerar novamente!! ", vbInformation, "A T E N � � O"
            End If
        End If
        
        
        cmd_componentes.Enabled = False
        
        txt_cicloano.Enabled = True
        cmbCiclo.Enabled = True
        txt_Codigo.Enabled = True
        txt_Codigo = ""
        txt_produto.Enabled = False
        txt_produto = ""
        txt_precobase.Enabled = True
        txt_precobase = ""
        txt_redutor.Enabled = True
        txt_redutor = ""
        txt_status.Enabled = True
        txt_status = ""
        txt_pontos.Enabled = True
        txt_pontos = ""
        cmbCiclo.SetFocus
        
        bm_Alterado = False
        bm_novo = False
        recalcula = False
        st_Toolbar.Buttons.Item(2).Enabled = False
        st_Toolbar.Buttons.Item(3).Enabled = False
        Me.Height = 6360
        st_Toolbar.Buttons(6).Enabled = False
        
        sProtegeTela
        
    End If
    
    
    
        
End Sub

Sub sSalvar()

    Dim Z                    As Integer
    Dim k                    As Integer
    Dim vAtualizaCicloFuturo As Boolean
        
        
    blGravaLog = True 'SE 6346
    
    If Trim$(txt_cicloano) = "" Then
        MsgBox "Ciclo / Ano deve ser preenchido !", vbInformation, "A T E N � � O"
        cmbCiclo.SetFocus
        Exit Sub
    End If
    
    If Trim$(txt_Codigo) = "" Then
        MsgBox "C�digo do produto deve ser preenchido !", vbInformation, "A T E N � � O"
        txt_Codigo.SetFocus
        Exit Sub
    End If
    
    txt_redutor = Format(txt_redutor, "#0.00")
    txt_precobase = Format(txt_precobase, "#0.00")
    
    txt_precobase = Format(txt_precobase, "#0.00")
    aux_arredonda = txt_precobase
    aux_arredonda = Format(aux_arredonda, "#0.0")
    aux_arredonda = Format(aux_arredonda, "#0.00")
           
    If cmbStatus.ListIndex < 0 Then
        MsgBox "Informe o status !", vbInformation, "A T E N � � O"
        cmbStatus.SetFocus
        Exit Sub
    End If
    
    txt_status = Val(cmbStatus.Text)
        
    ' Renato 16/2/2000
    ' Caso status = 9 ou 4, n�o consistencia o produto
    
    If txt_status.Text <> "9" And txt_status.Text <> "4" Then
    
        If Trim$(txt_precobase) = "" Or Trim$(txt_precobase) = "0,00" Then
            MsgBox "Pre�o refer�ncia n�o pode ser zero !", vbInformation, "A T E N � � O"
            txt_precobase.SetFocus
            Exit Sub
        End If
     
        If Trim$(txt_redutor) = "" Or (Not IsNumeric(txt_redutor.Text)) Then
            MsgBox "Percentual redutor de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
            txt_redutor.SetFocus
            Exit Sub
        End If
     
        If CDbl(Trim$(txt_redutor)) < CDbl("0,00") Or CDbl(Trim$(txt_redutor)) > CDbl("100,00") Then
            MsgBox "Redutor de Pontos deve estar no intervalo de 0 a 100 !", vbInformation, "A T E N � � O"
            txt_redutor.SetFocus
            Exit Sub
        End If
     
        If Trim$(txt_pontos) = "" Then
            MsgBox "N�mero de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
            txt_pontos.SetFocus
            Exit Sub
        End If
     
        If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
            If Trim$(txt_pontos) = "0" Then
                MsgBox "N�mero de Pontos de pai de componente n�o pode ser zero !", vbInformation, "A T E N � � O"
                txt_pontos.SetFocus
                Exit Sub
            End If
        End If
     
    End If
        
    nm_ciclo_operacional = Right(txt_cicloano, 4) + Left(txt_cicloano, 2)
    
    If Not ValidaKit Then
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    txt_pontos_aux = Trim$(txt_pontos)
    txt_redutor_LostFocus
    
    ' Inicia transa��o
    If Not Runquery("BEGIN TRANSACTION", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    If Trim$(txt_pontos_aux) <> Trim$(txt_pontos) Then
        txt_pontos = txt_pontos_aux
    End If
        
    vAtualizaCicloFuturo = False
    
    st_Toolbar.Buttons.Item(3).Enabled = False
    
    If FU_BuscaPrecoFuturo(Right(txt_cicloano, 4) + Left(txt_cicloano, 2), Trim$(txt_Codigo), MSG$, False) = FAIL% Then
         MsgBox MSG$, vbCritical, "A T E N � � O"
         PonteiroMouse vbNormal
    Else
         If QtdPrecoFuturo > 0 Then
              If MsgBox("Lista de pre�o de ciclos futuros j� foi gerada ate o ciclo " & ciclosFuturos(QtdPrecoFuturo).ciclo & Chr(10) & Chr(10) & _
                        "Deseja replicar as manuten��es para os pr�ximos ciclos ? ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
                        
                    vAtualizaCicloFuturo = True
              End If
         End If
    End If
    
    PonteiroMouse vbHourglass
    
    lblGravando.Visible = True
    
    '---------------------------------
    ' gravando preco de referencia
    '---------------------------------
    For contMat = 1 To UBound(Materiais)
    
        If Materiais(contMat).Gravar Then
    
            lblGravando.Caption = "Gerando Pre�o de Refer�ncia " & Materiais(contMat).cd_material
            DoEvents
                                    
            If FU_AtualizaPrecoRef(nm_ciclo_operacional, Materiais(contMat).cd_material, id_tipo_produto, txt_status, txt_precobase, txt_redutor, txt_pontos, 0, MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                    PonteiroMouse vbNormal
                    lblGravando.Visible = False
                    Exit Sub
                End If
                PonteiroMouse vbNormal
                Exit Sub
            End If
            'SE 6346 - In�cio
            If blGravaLog Then
                If Not GravaHistorico(6, strOperacaoRef, "Ciclo", nm_ciclo_operacional, "Produto", Trim$(txt_Codigo), "", "") Then
                    If Not Runquery("ROLLBACK", hSql) Then
                        SetStatus "Erro na grava��o..."
                        MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                        PonteiroMouse vbNormal
                        lblGravando.Visible = False
                        Exit Sub
                    End If
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                blGravaLog = False
            End If
            'SE 6346 - Fim
            '------------------------------------
            'gravando componentes de kit
            '------------------------------------
            If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
                    
                For Z = 1 To UBound(v_componentes)
                
                    If Materiais(contMat).cd_material = v_componentes(Z).cd_produto_pai Then
                
                        lblGravando.Caption = "Gerando Pre�o de Refer�ncia " & v_componentes(Z).cd_produto_pai
                        DoEvents
                    
                        If FU_AtualizaPrecoRefKit(nm_ciclo_operacional, _
                                                  v_componentes(Z).cd_produto_pai, _
                                                  v_componentes(Z).cd_item_kit, _
                                                  ConvertePontoDecimal(v_componentes(Z).vl_preco_referencia_kit, 1), _
                                                  ConvertePontoDecimal(v_componentes(Z).qt_pontos_referencia_kit, 1), MSG$) = FAIL% Then
                                                  
                            MsgBox MSG$, vbCritical, "A T E N � � O"
                            If Not Runquery("ROLLBACK", hSql) Then
                                SetStatus "Erro na grava��o..."
                                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                                PonteiroMouse vbNormal
                                lblGravando.Visible = False
                                Exit Sub
                            End If
                            PonteiroMouse vbNormal
                            Exit Sub
                        End If
                        
                    End If
                    
                Next Z
                    
            End If
        Else
        
            If Not Delete_Produto_Kit_Divergente(nm_ciclo_operacional, Materiais(contMat).cd_material) Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                PonteiroMouse vbNormal
                Exit Sub
            End If
            'SE 6346 - In�cio
            If blGravaLog Then
                If Not GravaHistorico(6, strOperacaoRef, "Ciclo", nm_ciclo_operacional, "Produto", Trim$(txt_Codigo), "", "") Then
                    If Not Runquery("ROLLBACK", hSql) Then
                        SetStatus "Erro na grava��o..."
                        MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                        PonteiroMouse vbNormal
                        lblGravando.Visible = False
                        Exit Sub
                    End If
                    
                    MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                blGravaLog = False
            End If
            'SE 6346 - Fim
        End If
            
    Next contMat
    
    
    '-----------------------------------------------
    ' atualizando os pre�os para os ciclos FUTUROS
    '-----------------------------------------------
    If vAtualizaCicloFuturo Then
            
        For k = 1 To UBound(ciclosFuturos)
            blGravaLog = True 'SE 6346
            nm_ciclo_futuro = Right(ciclosFuturos(k).ciclo, 4) + Left(ciclosFuturos(k).ciclo, 2)
                        
            '-----------------------------------------------------------------
            'obtem os materiais do c�digo de venda informado no ciclo futuro
            '-----------------------------------------------------------------
            If FU_ObtemMateriaisFuturos(nm_ciclo_futuro, Trim$(txt_Codigo), MSG$) = FAIL% Then
                 MsgBox MSG$, vbCritical, "A T E N � � O"
                 If Not Runquery("ROLLBACK", hSql) Then
                     SetStatus "Erro na grava��o..."
                     MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                     PonteiroMouse vbNormal
                     lblGravando.Visible = False
                     Exit Sub
                 End If
                 PonteiroMouse vbNormal
                 Exit Sub
            End If
            
            If sql_qt_lnh = 0 Then
                Call RegistraLog("", txt_Codigo.Text, "", Str(Z), "N�o h� materiais cadastrados para o c�digo de venda informado no ciclo " & nm_ciclo_futuro)
            End If
            
            '------------------------------------------
            'obtem os ites de kit dos outros materiais
            '------------------------------------------
            ReDim v_componentes_futuros(0)
            
            If Trim$(id_tipo_produto_futuro) = "2" Or Trim$(id_tipo_produto_futuro) = "3" Or Trim$(id_tipo_produto_futuro) = "4" Then
                For contMat = 1 To UBound(Materiais)
                    If FU_BuscaProdutoKit_Futuro(nm_ciclo_futuro, Trim$(MateriaisFuturos(contMat).cd_material), MSG$) = FAIL% Then
                        MsgBox MSG$, vbCritical, "A T E N � � O"
                        If Not Runquery("ROLLBACK", hSql) Then
                            SetStatus "Erro na grava��o..."
                            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                            PonteiroMouse vbNormal
                            lblGravando.Visible = False
                            Exit Sub
                        End If
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                    If sql_qt_lnh <= 0 Then
                        MateriaisFuturos(contMat).Gravar = False
                    End If
                Next contMat
            End If
            Call ConfereComposicaoKit(v_cd_material, True, ciclosFuturos(k).ciclo)
            
            '-------------------------------------
            ' gravando preco de referencia futuro
            '-------------------------------------
            For contMat = 1 To UBound(MateriaisFuturos)
            
                If MateriaisFuturos(contMat).Gravar And MateriaisFuturos(contMat).ciclo = nm_ciclo_futuro Then
            
                    If FU_AtualizaPrecoRef(nm_ciclo_futuro, MateriaisFuturos(contMat).cd_material, id_tipo_produto_futuro, txt_status, txt_precobase, txt_redutor, txt_pontos, 0, MSG$) = FAIL% Then
                        MsgBox MSG$, vbCritical, "A T E N � � O"
                        If Not Runquery("ROLLBACK", hSql) Then
                            SetStatus "Erro na grava��o..."
                            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                            PonteiroMouse vbNormal
                            lblGravando.Visible = False
                            Exit Sub
                        End If
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                    'SE 6346 - In�cio
                    If blGravaLog Then
                        If Not GravaHistorico(6, strOperacaoRef, "Ciclo", nm_ciclo_futuro, "Produto", Trim$(txt_Codigo), "", "") Then
                            If Not Runquery("ROLLBACK", hSql) Then
                                SetStatus "Erro na grava��o..."
                                MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                                PonteiroMouse vbNormal
                                lblGravando.Visible = False
                                Exit Sub
                            End If
                            
                            MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                            PonteiroMouse vbNormal
                            Exit Sub
                        End If
                        blGravaLog = False
                    End If
                    'SE 6346 - Fim
                    '------------------------------------
                    'gravando componentes de kit
                    '------------------------------------
                    If Trim$(id_tipo_produto_futuro) = "2" Or Trim$(id_tipo_produto_futuro) = "3" Or Trim$(id_tipo_produto_futuro) = "4" Then
                            
                        For Z = 1 To UBound(v_componentes)
                        
                            If MateriaisFuturos(contMat).cd_material = v_componentes(Z).cd_produto_pai And v_componentes_futuros(Z).ciclo = nm_ciclo_futuro Then
                        
                                lblGravando.Caption = "Gerando Pre�o de Refer�ncia " & v_componentes(Z).cd_produto_pai
                                DoEvents
                            
                                If FU_AtualizaPrecoRefKit(nm_ciclo_futuro, _
                                                          v_componentes(Z).cd_produto_pai, _
                                                          v_componentes(Z).cd_item_kit, _
                                                          ConvertePontoDecimal(v_componentes(Z).vl_preco_referencia_kit, 1), _
                                                          ConvertePontoDecimal(v_componentes(Z).qt_pontos_referencia_kit, 1), MSG$) = FAIL% Then
                                                          
                                    MsgBox MSG$, vbCritical, "A T E N � � O"
                                    If Not Runquery("ROLLBACK", hSql) Then
                                        SetStatus "Erro na grava��o..."
                                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                                        PonteiroMouse vbNormal
                                        lblGravando.Visible = False
                                        Exit Sub
                                    End If
                                    PonteiroMouse vbNormal
                                    Exit Sub
                                End If
                                
                            End If
                            
                        Next Z
                            
                    End If
                    
                Else
                    If MateriaisFuturos(contMat).ciclo = nm_ciclo_futuro Then
                        If Not Delete_Produto_Kit_Divergente(nm_ciclo_futuro, MateriaisFuturos(contMat).cd_material) Then
                            MsgBox MSG$, vbCritical, "A T E N � � O"
                            If Not Runquery("ROLLBACK", hSql) Then
                                SetStatus "Erro na grava��o..."
                                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                                PonteiroMouse vbNormal
                                Exit Sub
                            End If
                            PonteiroMouse vbNormal
                            Exit Sub
                        End If
                        'SE 6346 - In�cio
                        If blGravaLog Then
                            If Not GravaHistorico(6, strOperacaoRef, "Ciclo", nm_ciclo_futuro, "Produto", Trim$(txt_Codigo), "", "") Then
                                If Not Runquery("ROLLBACK", hSql) Then
                                    SetStatus "Erro na grava��o..."
                                    MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                                    PonteiroMouse vbNormal
                                    Exit Sub
                                End If
                                MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                                PonteiroMouse vbNormal
                                Exit Sub
                            End If
                            blGravaLog = False
                        End If
                        'SE 6346 - Fim
                    End If
                End If
                
            Next contMat
        
        Next k
                
    End If
    
    blGravaLog = True 'SE 6346
    '---------------------------------
    ' gravando reajuste individual
    '---------------------------------
    For Z = 1 To UBound(ReajusteLista)
    
        lblGravando.Caption = "Atualizando Reajuste Individual"
        DoEvents
        
        If Not Grava_Percentual(ReajusteLista(Z).nm_ciclo_operacional, _
                                ReajusteLista(Z).cd_lista, _
                                ReajusteLista(Z).cd_Segmento_canal, _
                                txt_Codigo.Text, _
                                ReajusteLista(Z).pc_reajuste_lista, _
                                ReajusteLista(Z).Excluir) Then
            MsgBox MSG$, vbCritical, "A T E N � � O"
            If Not Runquery("ROLLBACK", hSql) Then
                SetStatus "Erro na grava��o..."
                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                lblGravando.Visible = False
                PonteiroMouse vbNormal
                Exit Sub
            End If
            PonteiroMouse vbNormal
            Exit Sub
        End If
        'SE 6346 - In�cio
        If blGravaLog Then
            If Not GravaHistorico(7, strOperacaoRef, "Ciclo", ReajusteLista(Z).nm_ciclo_operacional, "Produto", txt_Codigo.Text, "", "") Then
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                    lblGravando.Visible = False
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            blGravaLog = False
        End If
        'SE 6346 - Fim
        
        
        If vAtualizaCicloFuturo Then
        
            For k = 1 To UBound(ciclosFuturos)
            
                nm_ciclo_futuro = Right(ciclosFuturos(k).ciclo, 4) + Left(ciclosFuturos(k).ciclo, 2)
                
                If Not Grava_Percentual(nm_ciclo_futuro, _
                                        ReajusteLista(Z).cd_lista, _
                                        ReajusteLista(Z).cd_Segmento_canal, _
                                        txt_Codigo.Text, _
                                        ReajusteLista(Z).pc_reajuste_lista, _
                                        ReajusteLista(Z).Excluir) Then
                    MsgBox MSG$, vbCritical, "A T E N � � O"
                    If Not Runquery("ROLLBACK", hSql) Then
                        SetStatus "Erro na grava��o..."
                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                        lblGravando.Visible = False
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                'SE 6346 - In�cio
                If Not GravaHistorico(7, strOperacaoRef, "Ciclo", nm_ciclo_futuro, "Produto", txt_Codigo.Text, "", "") Then
                    
                    If Not Runquery("ROLLBACK", hSql) Then
                        SetStatus "Erro na grava��o..."
                        MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                        lblGravando.Visible = False
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                    MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                'SE 6346 - Fim
            
            Next k
            
        End If
        
    Next Z
    
    If Not Runquery("COMMIT", hSql) Then
        SetStatus "Erro na grava��o..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
        PonteiroMouse vbNormal
        Exit Sub
    End If
        
    '------------------------------
    ' Gerando lista de preco
    '------------------------------
    PonteiroMouse vbHourglass
    
    If Val(v_cd_material_vigente) <= 0 Then
        MsgBox "N�o h� materiais vig�ntes para o c�digo de venda informado. A lista de pre�o n�o ser� gerada", vbInformation, "Aten��o"
        lblGravando.Visible = False
        PonteiroMouse vbNormal
        Exit Sub
    End If
            
    lblGravando.Caption = "Gerando Lista de Pre�o "
    DoEvents
    
    For Z = 1 To 4  'segmento canal de 1 a 4
     
        If Not Gera_Lista(nm_ciclo_operacional, Z, v_cd_material, 0) Then
            SetStatus "Erro na Gera��o..."
            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
            lblGravando.Visible = False
            PonteiroMouse vbNormal
            Exit Sub
        End If
        
        If vAtualizaCicloFuturo Then
        
            For k = 1 To UBound(ciclosFuturos)
            
                lblGravando.Caption = "Gerando Lista de Pre�o no ciclo " & ciclosFuturos(k).ciclo
                DoEvents
            
                nm_ciclo_futuro = Right(ciclosFuturos(k).ciclo, 4) + Left(ciclosFuturos(k).ciclo, 2)
                
                If Val(ciclosFuturos(k).cd_material_vigente) > 0 Then
                
                    If Not Gera_Lista(nm_ciclo_futuro, Z, ciclosFuturos(k).cd_material_vigente, 0) Then
                        SetStatus "Erro na Gera��o..."
                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                        lblGravando.Visible = False
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                Else
                    Call RegistraLog("", txt_Codigo.Text, "", Str(Z), "N�o h� pre�o de refer�ncia para o c�digo de venda no ciclo " & ciclosFuturos(k).ciclo)
                End If
            
            Next k
            
        End If
        
    Next
            
    lblGravando.Visible = False
    DoEvents
    PonteiroMouse vbNormal
    
    If FU_BuscaPrecoBase(Right(txt_cicloano, 4) + Left(txt_cicloano, 2), MSG$, False) = FAIL% Then
         MsgBox MSG$, vbCritical, "A T E N � � O"
         PonteiroMouse vbNormal
    Else
        If QtdPrecoBase > 0 Then
            MsgBox "Relat�rio Bases de Pre�os j� foi gerado !!! Necess�rio gerar novamente!! ", vbInformation, "A T E N � � O"
        End If
    End If
    
    cmd_componentes.Enabled = False
    sProtegeTela
        
    If UBound(LogGeraLista) > 0 Then
        MsgBox "Atualiza��o efetuada, por�m ocorreram inconsist�ncias ao gerar lista de pre�os !", vbInformation
        Call ExibeLog
    Else
        MsgBox "Atualiza��o efetuada com sucesso !", vbInformation
    End If
    

    
    ReDim LogGeraLista(0)
    
    
'    If Not Altera_Filho_Kit(nm_ciclo_operacional, Trim$(txt_codigo), ConvertePontoDecimal(txt_precobase, Ponto)) Then
'        MsgBox MSG$, vbCritical, "A T E N � � O"
'    If Not Runquery("ROLLBACK", hSql) Then
'        SetStatus "Erro na grava��o..."
'        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
'        PonteiroMouse vbNormal
'        Exit Sub
'    End If
'        PonteiroMouse vbNormal
'        Exit Sub
'    End If
    
    
    'If Not Lista_Filho_Kit(nm_ciclo_operacional, Trim$(v_cd_material)) Then
    '    MsgBox MSG$, vbCritical, "A T E N � � O"
    '    If Not Runquery("ROLLBACK", hSql) Then
    '        SetStatus "Erro na grava��o..."
    '        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
    '        PonteiroMouse vbNormal
    '        Exit Sub
    '    End If
     
    '        PonteiroMouse vbNormal
    '    Exit Sub
    'End If
    
    
    
    'If grd_Pai_Kit.MaxRows <> 0 Then
    '    Exit Sub
    'End If
    

End Sub


Private Function ValidaKit() As Boolean

    ValidaKit = False
    
    'checar se preco pai bate com preco filhos
    If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
        
        preco_componentes = 0
        ponto_componente = 0
        
        For i = 0 To UBound(v_componentes)
            If v_componentes(i).cd_produto_pai = v_cd_material Then
                preco_componentes = preco_componentes + (CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) * CInt(v_componentes(i).qt_item_kit))
                ponto_componente = ponto_componente + CInt(v_componentes(i).qt_pontos_referencia_kit) * CInt(v_componentes(i).qt_item_kit)
                
                If CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) = 0 Then
                    MsgBox "Existem componentes com pre�o igual a zero!", vbInformation, "A T E N � � O"
                    Exit Function
                End If
                
            End If
        Next i
        
        If Trim(preco_componentes) <> Trim(CDbl(txt_precobase)) Then
           MsgBox "Somat�ria do pre�o dos componentes n�o pode ser diferente do pre�o pai !", vbInformation, "A T E N � � O"
           Exit Function
        End If
            
        If Trim(ponto_componente) <> Trim(Val(txt_pontos)) Then
            MsgBox "Somat�ria dos pontos dos componentes n�o pode ser diferente dos pontos pai !", vbInformation, "A T E N � � O"
            Exit Function
        End If
                    
    End If
    
    ValidaKit = True

End Function

Private Function ValidaKitL() As Boolean

    ValidaKitL = False
    
    'checar se preco pai bate com preco filhos
    If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
        
        preco_componentes = 0
        ponto_componente = 0
        
        For i = 0 To UBound(v_componentes)
            If v_componentes(i).cd_venda_item = txtcodvenda Then
                preco_componentes = preco_componentes + (CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) * CInt(v_componentes(i).qt_item_kit))
                ponto_componente = ponto_componente + CInt(v_componentes(i).qt_pontos_referencia_kit) * CInt(v_componentes(i).qt_item_kit)
                
                If CDbl(ConvertePontoDecimal(v_componentes(i).vl_preco_referencia_kit, 0)) = 0 Then
                    MsgBox "Existem componentes com pre�o igual a zero!", vbInformation, "A T E N � � O"
                    Exit Function
                End If
                
            End If
        Next i
        
        If Trim(preco_componentes) <> Trim(CDbl(txtprecosugestao)) Then
           MsgBox "Somat�ria do pre�o dos componentes n�o pode ser diferente do pre�o pai !", vbInformation, "A T E N � � O"
           Exit Function
        End If
            
        If Trim(ponto_componente) <> Trim(Val(txtpontos)) Then
            MsgBox "Somat�ria dos pontos dos componentes n�o pode ser diferente dos pontos pai !", vbInformation, "A T E N � � O"
            Exit Function
        End If
                    
    End If
    
    ValidaKitL = True

End Function
Sub sProtegeTela()
    
    'MsgBox "Iniciando a sub sProtegeTela "
    
    txt_Codigo.Enabled = True
    txt_Codigo = ""
    'MsgBox "1"
    
    
    txt_produto.Enabled = False
    txt_produto = ""
    'MsgBox "2"
    
    txt_precobase.Enabled = True
    txt_precobase = ""
    'MsgBox "3"
    
    txt_redutor.Enabled = True
    txt_redutor = ""
    'MsgBox "4"
    
    txt_status.Enabled = True
    txt_status = ""
    'MsgBox "5"
    
    txt_pontos.Enabled = True
    txt_pontos = ""
    'MsgBox "6"
    
    txt_cicloano.Enabled = True
    'MsgBox "7"
    
    cmbCiclo.Enabled = True
    cmbCiclo.SetFocus
    'MsgBox "8"
    
    bm_Alterado = False
    bm_novo = False
    recalcula = False
    
    'MsgBox "9"
    
    Me.Height = 6360
    st_Toolbar.Buttons(6).Enabled = False
    
    'MsgBox "10"
    
    v_cd_material = 0
    v_cd_material_vigente = 0
    ReDim Materiais(0)
    ReDim v_componentes(0)
    ReDim ReajusteLista(0)
    
    'MsgBox "11"
    
    st_Toolbar.Buttons.Item(2).Enabled = False
    st_Toolbar.Buttons.Item(3).Enabled = False

    'MsgBox "12"
End Sub

Sub sProtegeTelaL()
    cmdcomponentes.Enabled = False
         
    cmbCicloL.Enabled = True
    txtcodvenda.Enabled = True
    txtcodvenda = ""
    txtdescricao.Enabled = False
    txtdescricao = ""
    txtprecobase.Enabled = False
    txtprecobase = ""
    txtprecosugestao.Enabled = False
    txtprecosugestao = ""
'    txtstatus.Enabled = False
'    txtstatus = ""
    cmbStatusL.ListIndex = -1
    txtpontos.Enabled = True
    txtpontos = ""
    txtredutorlista.Text = ""
    cmbGerVenda.Text = ""
    cmbSetor.Text = ""
    cmbRegEstrat.Text = ""
    cmbLista.Text = ""
    
    v_cd_material = 0
    v_cd_material_vigente = 0
    cmbCicloL.SetFocus
     
    bm_Alterado = False
    bm_novo = False
    recalcula = False
    Toolbar1.Buttons.Item(1).Enabled = False
    'st_Toolbar.Buttons.Item(3).Enabled = False
    Me.Height = 6360
    'st_Toolbar.Buttons(6).Enabled = False
    'ReDim Materiais(0)
    ReDim v_componentes(0)
    ReDim LogGeraLista(0)
    
    
    
    
    
    
    
    
    

End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(6) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
End Sub

Private Sub optPreco_Click(Index As Integer)

    If Index = 0 Then
        st_Toolbar.Buttons.Item(10).Enabled = False
    Else
        st_Toolbar.Buttons.Item(10).Enabled = True
    End If
    
End Sub

Private Function Verifica_Est_Lista(cd_tipo_est As Integer, nm_ciclo_operacional As String, cd_est_com As String) As Integer

Dim cSql  As String
Dim bResp As Boolean
Dim i     As Integer
Dim avPrecos()       As Variant

'On Error GoTo erro

cSql = "SELECT cd_lista"
cSql = cSql & " From t_pi_estrutura_lista"
cSql = cSql & " Where nm_ciclo_operacional = " & nm_ciclo_operacional
cSql = cSql & " AND cd_tipo_estrutura_comercial = " & cd_tipo_est
cSql = cSql & " AND cd_estrutura_comercial = " & cd_est_com
cSql = cSql & " AND cd_segmento_canal = 1"

'cSql = "SELECT COUNT(*) FROM t_pi_preco_cn WHERE nm_ciclo_operacional = " & nm_ciclo_operacional

ReDim avPrecos(0)
   
bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
If bResp = False Then
  MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
  End
End If

If gObjeto.nErroSQL <> 0 Then
  MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
  Verifica_Est_Lista = 0
  Exit Function
End If

Verifica_Est_Lista = avPrecos(0, i)

'Exit Function

'erro:
'  MsgBox "Erro ao obter total de pre�os gerados " & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
'  Exit Function
  
End Function

Private Sub optPreco_LostFocus(Index As Integer)
    'SE 6346 - Corrigindo erro de setfocus
    If txt_precobase.Enabled Then
        txt_precobase.SetFocus
    End If
End Sub

Private Sub st_Toolbar_ButtonClick(ByVal Button As ComctlLib.Button)
Dim i As Integer
Dim preco_componentes As Double
Dim nm_ciclo_operacional As String
Dim flag_preco As Boolean
Dim ponto_componente As Integer
Dim total As Double
Dim qtde As Integer
Dim vl_PrecoTotal As Double
Dim Pontos As Integer
Dim PontosTot As Double

   Select Case Button.Index
      Case 1  ' Novo
      
      Case 2  ' Excluir
         
            sExcluir
         
      Case 3  ' Salvar
        
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
         'If Not ChecaPermissao("frmPDO011", "M") Then
            'MsgBox "Voce n�o tem permiss�o para alterar", vbCritical + vbOKOnly, "E R R O"
            'Exit Sub
         'End If
         
          'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
         If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
            MsgBox "Voce n�o tem permiss�o para alterar", vbCritical + vbOKOnly, "E R R O"
            Exit Sub
         End If
         
         Unload frmCompKit
         
         Load frmCompKit
         
         sSalvar
            
      Case 5  ' Limpar
         cmd_componentes.Enabled = False
         
         txt_cicloano.Enabled = True
         cmbCiclo.Enabled = True
         txt_Codigo.Enabled = True
         txt_Codigo = ""
         txt_produto.Enabled = False
         txt_produto = ""
         txt_precobase.Enabled = True
         txt_precobase = ""
         txt_redutor.Enabled = True
         txt_redutor = ""
         txt_status.Enabled = True
         txt_status = ""
         txt_pontos.Enabled = True
         v_cd_material = 0
         v_cd_material_vigente = 0
         txt_pontos = ""
         cmbCiclo.SetFocus
         
         bm_Alterado = False
         bm_novo = False
         recalcula = False
         st_Toolbar.Buttons.Item(2).Enabled = False
         st_Toolbar.Buttons.Item(3).Enabled = False
         Me.Height = 6360
         st_Toolbar.Buttons(6).Enabled = False
         
         ReDim Materiais(0)
         ReDim v_componentes(0)
         ReDim LogGeraLista(0)
          
      Case 6  ' Imprimir
      
         grd_Pai_Kit.PrintAbortMsg = "Imprimindo..."
         grd_Pai_Kit.PrintHeader = "/lNatura Cosm�ticos /r" & Date & _
                                    " - " & Time & "/fb1/fz""12""/n/n/c Relat�rio de pais afetados pela altera��o de pre�o  " & " /fb0/n/n Produto - " & txt_Codigo.Text & "/fz""10""/n/n"
         'grd_Pai_Kit.PrintJobName "Relat�rio de pais afetados pela altera��o de pre�o"
         grd_Pai_Kit.Action = 32
         
         Me.Height = 6360
         st_Toolbar.Buttons(6).Enabled = False
         cmd_componentes.Enabled = False
         
         txt_cicloano.Enabled = True
         cmbCiclo.Enabled = True
         txt_Codigo.Enabled = True
         txt_Codigo = ""
         txt_produto.Enabled = False
         txt_produto = ""
         txt_precobase.Enabled = True
         txt_precobase = ""
         txt_redutor.Enabled = True
         txt_redutor = ""
         txt_status.Enabled = True
         txt_status = ""
         txt_pontos.Enabled = True
         txt_pontos = ""
         cmbCiclo.SetFocus
         
         bm_Alterado = False
         bm_novo = False
         recalcula = False
         st_Toolbar.Buttons.Item(2).Enabled = False
         st_Toolbar.Buttons.Item(3).Enabled = False
         Me.Height = 6360
         st_Toolbar.Buttons(6).Enabled = False
                    
      Case 8  ' Pesquisar
         txt_Codigo_KeyPress 13
         recalcula = False
         st_Toolbar.Buttons.Item(3).Enabled = False
         Me.Height = 6360
         st_Toolbar.Buttons(6).Enabled = False
         
      Case 10 ' reajuste individual lista de preco
            Load frmExcecaoPreco
            With frmExcecaoPreco
                .txt_Ciclo.Text = cmbCiclo.Text
                .txt_Ciclo.Tag = Left$(cmbCiclo.Text, 2) & Right$(cmbCiclo.Text, 4)
                .txt_Codigo.Text = txt_Codigo.Text
                .txt_Dc_Produto.Text = txt_produto.Text
                
                If Not Combo_Lista(.cb_Listas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                    Exit Sub
                End If

                .Show 1
            End With
         
      Case 12 ' Sair
         SetStatus ""
         If bm_Alterado Then
            If MsgBox("As Informa��es da janela " & Me.Caption & " foram alteradas. Deseja limpa-las e perder as informa��es?", vbInformation + vbYesNo, "ATEN��O") = vbYes Then
               MDIpdo001.DesbloqueiaMenu
               Unload Me
               Set frmPrecoRef = Nothing
            End If
         Else
            MDIpdo001.DesbloqueiaMenu
            Unload Me
            Set frmPrecoRef = Nothing
         End If
            
   End Select


End Sub

Private Sub TabStrip1_Click()

End Sub

Private Sub Text2_Change()

End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
    
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
            
        Case 1  ' Salvar
            Call Graba_Preco
            
        Case 3  ' Limpar
            cmdcomponentes.Enabled = False
         
            cmbCicloL.Enabled = True
            txtcodvenda.Enabled = True
            txtcodvenda = ""
            txtdescricao.Enabled = False
            txtdescricao = ""
            txtprecobase.Enabled = False
            txtprecobase = ""
            txtprecosugestao.Enabled = False
            txtprecosugestao = ""
            cmbStatusL.ListIndex = -1
            'txtstatus.Enabled = False
            'txtstatus = ""
            txtpontos.Enabled = True
            txtpontos = ""
            txtredutorlista.Text = ""
            cmbGerVenda.Text = ""
            cmbSetor.Text = ""
            cmbRegEstrat.Text = ""
            cmbLista.Text = ""
            
            v_cd_material = 0
            v_cd_material_vigente = 0
            cmbCicloL.SetFocus
             
            bm_Alterado = False
            bm_novo = False
            recalcula = False
            Toolbar1.Buttons.Item(1).Enabled = False
            'st_Toolbar.Buttons.Item(3).Enabled = False
            Me.Height = 6360
            'st_Toolbar.Buttons(6).Enabled = False
            'ReDim Materiais(0)
            ReDim v_componentes(0)
            ReDim LogGeraLista(0)
          
                   
      Case 5  ' Pesquisar
         txtCodVenda_KeyPress (13)
               
      Case 7 ' Sair
         SetStatus ""
         If bm_Alterado Then
            If MsgBox("As Informa��es da janela " & Me.Caption & " foram alteradas. Deseja limpa-las e perder as informa��es?", vbInformation + vbYesNo, "ATEN��O") = vbYes Then
               MDIpdo001.DesbloqueiaMenu
               Unload Me
               Set frmPrecoRef = Nothing
            End If
         Else
            MDIpdo001.DesbloqueiaMenu
            Unload Me
            Set frmPrecoRef = Nothing
         End If
            
   End Select

End Sub

Private Sub txt_cicloano_GotFocus()
   ' txt_cicloano.Text = Left$(txt_cicloano.Text, 2) & Right(txt_cicloano.Text, 4)
   ' txt_cicloano.MaxLength = 6
   ' SetaSelecao txt_cicloano
End Sub

Private Sub txt_cicloano_KeyPress(KeyAscii As Integer)

   If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 13) And (KeyAscii <> 8) Then
      KeyAscii = 0
      Beep
      Exit Sub
   End If
        
   If execucaoExterna Then
        Call txt_Codigo_KeyPress(13)
   Else
        If KeyAscii = 13 Then
           KeyAscii = 0
           txt_Codigo.SetFocus
        End If
   End If
    
End Sub

Private Sub txt_cicloano_LostFocus()
Dim nm_ciclo_operacional As String

If txt_cicloano.Text = "" Then
   Exit Sub
End If

'    txt_cicloano.MaxLength = 7
'    txt_cicloano.Text = Format(txt_cicloano, "0#/####")
    nm_ciclo_operacional = Right(txt_cicloano, 4) + Left(txt_cicloano, 2)
    
    MSG$ = ""
    If Trim$(txt_cicloano) <> "" Then
'        If Not Valida_Ciclo(nm_ciclo_operacional) Then
'            If MSG$ <> "" Then
'               MsgBox MSG$, vbOKOnly + vbInformation, "A T E N � � O"
'               Exit Sub
'            Else
'               MsgBox "Ciclo ainda n�o foi criado!", vbOKOnly + vbInformation, "A T E N � � O"
'               cmbCiclo.SetFocus
'               Exit Sub
'            End If
'        End If
    End If
End Sub

Private Sub txt_codigo_Change()

    If Not bm_novo Then
        If Trim$(txt_cicloano) <> "" And Trim$(txt_Codigo) <> "" Then
            st_Toolbar.Buttons.Item(8).Enabled = True
        Else
            st_Toolbar.Buttons.Item(8).Enabled = False
        End If
    End If
    
    txt_produto.Enabled = False
    txt_produto = ""
    txt_precobase.Enabled = True
    txt_precobase = ""
    txt_redutor.Enabled = True
    txt_redutor = ""
    txt_status.Enabled = True
    txt_status = ""
    txt_pontos.Enabled = True
    txt_pontos = ""
    v_cd_material = 0
    v_cd_material_vigente = 0
    ReDim Materiais(0)
    ReDim v_componentes(0)
    
End Sub

Private Sub txt_codigo_GotFocus()
    SetaSelecao txt_Codigo
End Sub

Private Sub txt_Codigo_KeyPress(KeyAscii As Integer)

    Dim nm_ciclo_operacional As String

    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) And (KeyAscii <> 13) Then
        KeyAscii = 0
        Beep
        Exit Sub
    End If
    
    execucaoExterna = False
   
    If KeyAscii = 13 Then
    
            KeyAscii = 0
            'checks
            optPreco(0).Value = True
            
            ReDim LogGeraLista(0)
             
            If Trim$(txt_cicloano) = "" Then
                MsgBox "Ciclo/Ano deve ser preenchido !", vbInformation, "A T E N � � O"
                cmbCiclo.SetFocus
                Exit Sub
            End If
             
            If Trim$(txt_Codigo) = "" Then
                MsgBox "C�digo do produto deve ser preenchido !", vbInformation, "A T E N � � O"
                txt_Codigo.SetFocus
                Exit Sub
            End If
             
            nm_ciclo_operacional = Right(txt_cicloano, 4) + Left(txt_cicloano, 2)
                         
            PonteiroMouse vbHourglass
             
            '--------------------------------------
            'verifica se o c�digo de venda existe
            '--------------------------------------
            If FU_ExisteProduto(Trim$(txt_Codigo), MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                MsgBox "C�digo de produto n�o cadastrado  !", vbInformation, "A T E N � � O"
                PonteiroMouse vbNormal
                txt_Codigo.SetFocus
                Call SelCodigo(txt_Codigo)
                Exit Sub
            End If
             
            '-------------------------------------------------
            'verifica a vig�ncia do c�digo de venda informado
            '-------------------------------------------------
            If FU_ObtemVigenciaCodVenda(nm_ciclo_operacional, Trim$(txt_Codigo), MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                MsgBox "Produto n�o dispon�vel neste ciclo: vig�ncia e abrang�ncia n�o cadastradas !", vbInformation, "A T E N � � O"
                PonteiroMouse vbNormal
                txt_Codigo.SetFocus
                Call SelCodigo(txt_Codigo)
                Exit Sub
            End If
             
            '-------------------------------------------------
            'obtem os materiais do c�digo de venda informado
            '-------------------------------------------------
            If FU_ObtemMateriais(nm_ciclo_operacional, Trim$(txt_Codigo), MSG$) = FAIL% Then
                 MsgBox MSG$, vbCritical, "A T E N � � O"
                 PonteiroMouse vbNormal
                 Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                 MsgBox "N�o h� materais cadastrados para o c�digo de venda informado !", vbInformation, "A T E N � � O"
                 PonteiroMouse vbNormal
                 txt_Codigo.SetFocus
                 Call SelCodigo(txt_Codigo)
                 Exit Sub
            End If
                      
            '---------------------------------------------------------
            'busca UM pre�o de ref�ncia para c�digo de venda digitado
            '---------------------------------------------------------
            If FU_BuscaPrecoRefProduto(nm_ciclo_operacional, Trim$(txt_Codigo), MSG$, True) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            
            '------------------------------------------------------------------
            'busca os percentuais de reajuste para o c�digo de venda informado
            '------------------------------------------------------------------
            If FU_ObtemReajusteLista(nm_ciclo_operacional, txt_Codigo.Text, MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            
            PonteiroMouse vbNormal
                                                            
            If sql_qt_lnh = 0 Then
                strOperacaoRef = "I"
                If MsgBox("Produto n�o possui pre�o cadastrado." & Chr(10) & Chr(10) & " Deseja incluir ?", vbInformation + vbYesNo, "ATEN��O") = vbYes Then
                    txt_cicloano.Enabled = False
                    cmbCiclo.Enabled = False
                    txt_Codigo.Enabled = False
                    txt_precobase.Enabled = True
                    txt_redutor.Enabled = True
                    txt_status.Enabled = True
                    txt_pontos.Enabled = True
                    optPreco(0).Value = True
                    optPreco(1).Value = False
                    txt_precobase.SetFocus
                    bm_novo = True
                Else
                    txt_cicloano.Enabled = True
                    cmbCiclo.Enabled = True
                    txt_Codigo.Enabled = True
                    txt_Codigo.Text = ""
                    txt_produto.Text = ""
                    optPreco(0).Value = True
                    optPreco(1).Value = False
                    txt_Codigo.SetFocus
                    txt_precobase.Enabled = False
                    txt_redutor.Enabled = False
                    txt_status.Enabled = False
                    txt_pontos.Enabled = False
                    bm_novo = False
                    Exit Sub
                End If
            Else
                strOperacaoRef = "A"
                txt_cicloano.Enabled = False
                cmbCiclo.Enabled = False
                txt_Codigo.Enabled = False
                txt_precobase.Enabled = True
                txt_redutor.Enabled = True
                txt_status.Enabled = True
                txt_pontos.Enabled = True
                bm_novo = False
            End If
         
         ReDim v_componentes(0)
         
         ' verificar se e pai de KIT ou estojo
         If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
         
            If bm_novo Then
                If FU_BuscaProdutoKit(nm_ciclo_operacional, Trim$(v_cd_material), MSG$) = FAIL% Then
                    MsgBox MSG$, vbCritical, "A T E N � � O"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
            Else
                If FU_BuscaPrecoRefKit(nm_ciclo_operacional, Trim$(v_cd_material), MSG$) = FAIL% Then
                    MsgBox MSG$, vbCritical, "A T E N � � O"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
            End If
            
            If sql_qt_lnh > 0 Then
                cmd_componentes.Enabled = True
            Else
                cmd_componentes.Enabled = False
            End If
                           
            'obtem os ites de kit dos outros materiais
            For contMat = 1 To UBound(Materiais)
                If Materiais(contMat).cd_material <> v_cd_material Then
                    If FU_BuscaProdutoKit(nm_ciclo_operacional, Trim$(Materiais(contMat).cd_material), MSG$) = FAIL% Then
                        MsgBox MSG$, vbCritical, "A T E N � � O"
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                End If
                If sql_qt_lnh <= 0 Then
                    Materiais(contMat).Gravar = False
                End If
            Next contMat
         Else
             cmd_componentes.Enabled = False
         End If
         
        Call ConfereComposicaoKit(v_cd_material, False, cmbCiclo.Text)
            
        bm_Alterado = False
        recalcula = False
        txt_precobase.SetFocus
        
        st_Toolbar.Buttons.Item(8).Enabled = False
        
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
        'If ChecaPermissao("frmPDO011", "M") Then
            'st_Toolbar.Buttons.Item(2).Enabled = True
        'End If
        
        'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
        If bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
            st_Toolbar.Buttons.Item(2).Enabled = True
        End If
        
    End If

End Sub

Private Sub txt_Pontos_Change()
bm_Alterado = True

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
'If ChecaPermissao("frmPDO011", "M") Then
   'st_Toolbar.Buttons.Item(3).Enabled = True
'End If

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
If bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
   st_Toolbar.Buttons.Item(3).Enabled = True
End If

End Sub

Private Sub txt_pontos_GotFocus()
    SetaSelecao txt_pontos
End Sub

Private Sub txt_Pontos_KeyPress(KeyAscii As Integer)
   If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) Then
      KeyAscii = 0
      Beep
   End If
End Sub

Private Sub txt_precobase_Change()

bm_Alterado = True

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
'If ChecaPermissao("frmPDO011", "M") Then
   'st_Toolbar.Buttons.Item(3).Enabled = True
'End If

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
If bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
   st_Toolbar.Buttons.Item(3).Enabled = True
End If

recalcula = True

If txt_precobase.Text = txt_precobase.Tag Then
    gPrecoBase = True
Else
    gPrecoBase = False
End If
End Sub


Private Sub txt_precobase_GotFocus()
    SetaSelecao txt_precobase
End Sub

Private Sub txt_precobase_KeyPress(KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 And KeyAscii <> 13 Then
       KeyAscii = 0
       Beep
    End If
   
    If KeyAscii = 13 Then
        KeyAscii = 0
'        txt_redutor_LostFocus
        txt_redutor.SetFocus
    End If
End Sub

Private Sub txt_precobase_LostFocus()
   txt_precobase = Format(txt_precobase, "#0.00")
'   txt_redutor_LostFocus
End Sub

Private Sub txt_redutor_Change()
bm_Alterado = True

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
'If ChecaPermissao("frmPDO011", "M") Then
   'st_Toolbar.Buttons.Item(3).Enabled = True
'End If

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
If bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
   st_Toolbar.Buttons.Item(3).Enabled = True
End If

recalcula = True
End Sub

Private Sub txt_redutor_GotFocus()
    SetaSelecao txt_redutor
End Sub

Private Sub txt_redutor_KeyPress(KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 And KeyAscii <> 13 Then
       KeyAscii = 0
       Beep
    End If
   
    If KeyAscii = 13 Then
        KeyAscii = 0
        cmbStatus.SetFocus
'        txt_status.SetFocus
    End If

End Sub

Private Sub txt_redutor_LostFocus()
Dim Valor As Double
Dim nm_ciclo_operacional As String



If txt_cicloano.Text = "" Then
   Exit Sub
End If
If txt_precobase.Text = "" Then
   Exit Sub
End If

'If Not recalcula Then
'   Exit Sub
'End If

recalcula = False
If Trim$(txt_Codigo) <> "" And Trim$(txt_produto) <> "" Then
    If Trim$(txt_redutor) = "" Then
        MsgBox "Informe um percentual redutor !", vbInformation, "A T E N � � O"
        txt_redutor.SetFocus
        Exit Sub
    End If
    
    txt_redutor = Format(txt_redutor, "#0.00")
    
    If CDbl(Trim$(txt_redutor)) < CDbl("0,00") Or CDbl(Trim$(txt_redutor)) > CDbl("100,00") Then
        MsgBox "Redutor de Pontos deve estar no intervalo de 0 a 100 !", vbInformation, "A T E N � � O"
        txt_redutor.SetFocus
        Exit Sub
    End If
    
    Valor = Format(CDbl(txt_precobase) * (CDbl(txt_redutor) / 100), "0.00")
    nm_ciclo_operacional = Right(txt_cicloano, 4) + Left(txt_cicloano, 2)
    
    MSG$ = ""
    txt_pontos.Text = FU_BuscaPontuacao(nm_ciclo_operacional, Str(Valor), MSG$)
    If MSG$ <> "" Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        PonteiroMouse vbNormal
        Exit Sub
    End If
End If


End Sub

Private Sub txt_Status_Change()
bm_Alterado = True

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
'If ChecaPermissao("frmPDO011", "M") Then
   'st_Toolbar.Buttons.Item(3).Enabled = True
'End If

'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
If bVerificaFuncaoPermissaoLogin("frmPDO011", "M") Then
   st_Toolbar.Buttons.Item(3).Enabled = True
End If

End Sub

Private Sub txt_status_GotFocus()
    SetaSelecao txt_status
End Sub

Private Sub txt_Status_KeyPress(KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) Then
       KeyAscii = 0
       Beep
    End If
End Sub


Public Function Parametros(pciclo As String, pproduto As String)

   ciclo = pciclo
   produto = pproduto

End Function



Private Function Delete_Produto(nm_ciclo_operacional As String, cd_vnd_produto As String, cd_produto As String, cd_tipo_produto As String) As Boolean

Dim se_Sql     As String
Dim sql_cd_ret As Integer
Dim sql_qt_lnh As Integer
Dim work       As String

   
   Delete_Produto = False
   On Error GoTo Delete_Produto
   
   PonteiroMouse vbHourglass
      
   se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
   se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
   se_Sql = se_Sql & " exec  stp_pdr_preco_d " & nm_ciclo_operacional & ", " & cd_vnd_produto & ", " & cd_produto & "," & cd_tipo_produto & ","
   se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
   
   
   If gObjeto.natPrepareQuery(hSql, se_Sql) Then
      Do While gObjeto.natFetchNext(hSql)
      Loop
   End If
   
   If gObjeto.nErroSQL <> 0 Then
      MSG$ = "Erro ao executar a Delete_Produto " & gObjeto.nServerNum & gObjeto.sServerMsg
      PonteiroMouse vbNormal
      Exit Function
   End If
    
   sql_cd_ret = CInt(gObjeto.varOutputParam(0))
   sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
   If sql_cd_ret <> 0 Then
      MSG$ = "Erro ao executar a Delete_Produto " & gObjeto.nServerNum & gObjeto.sServerMsg
      PonteiroMouse vbNormal
      Exit Function
   End If
   
   Delete_Produto = True
   PonteiroMouse vbNormal
   
   
   Exit Function
   
Delete_Produto:
   MSG$ = "Delete_Produto - Erro (" & Err & ") - " & Error
   PonteiroMouse vbNormal
   Exit Function
End Function

Private Function Delete_Produto_Kit_Divergente(nm_ciclo_operacional As String, cd_produto As String) As Boolean

Dim se_Sql     As String
Dim sql_cd_ret As Integer
Dim sql_qt_lnh As Integer
Dim work       As String

   
   Delete_Produto_Kit_Divergente = False
   On Error GoTo Delete_Produto_Kit_Divergente
   
   PonteiroMouse vbHourglass
      
   se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
   se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
   se_Sql = se_Sql & " exec  stp_pdr_preco_kit_div_d " & nm_ciclo_operacional & ", " & cd_produto & ","
   se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
   
   
   If gObjeto.natPrepareQuery(hSql, se_Sql) Then
      Do While gObjeto.natFetchNext(hSql)
      Loop
   End If
   
   If gObjeto.nErroSQL <> 0 Then
      MSG$ = "Erro ao executar a Delete_Produto_Kit_Divergente " & gObjeto.nServerNum & gObjeto.sServerMsg
      PonteiroMouse vbNormal
      Exit Function
   End If
    
   sql_cd_ret = CInt(gObjeto.varOutputParam(0))
   sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
   If sql_cd_ret <> 0 Then
      MSG$ = "Erro ao executar a Delete_Produto_Kit_Divergente " & gObjeto.nServerNum & gObjeto.sServerMsg
      PonteiroMouse vbNormal
      Exit Function
   End If
   
   Delete_Produto_Kit_Divergente = True
   PonteiroMouse vbNormal
   
   
   Exit Function
   
Delete_Produto_Kit_Divergente:
   MSG$ = "Delete_Produto_Kit_Divergente - Erro (" & Err & ") - " & Error
   PonteiroMouse vbNormal
   Exit Function
End Function




Private Sub SelCodigo(pcampo As Object)
    pcampo.SelStart = 0
    pcampo.SelLength = Len(pcampo.Text)
End Sub

Private Function FU_BuscaPrecoFuturoSug(nm_ciclo_operacional As String, cd_lista As String, MSG$) As Integer

Dim sql As String

    FU_BuscaPrecoFuturoSug = FAIL%

    On Error GoTo FU_BuscaPrecoFuturoSug

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_futuro_lista_s " & nm_ciclo_operacional & "," & cd_lista & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0
    QtdPrecoFuturo = 999

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            If gObjeto.varResultSet(hSql, 0) = cd_lista Then
                QtdPrecoFuturo = QtdPrecoFuturo + 1
            Else
                QtdPrecoFuturo = 0
                Exit Do
            End If
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_futuro_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    FU_BuscaPrecoFuturoSug = SUCCED%
    Exit Function

FU_BuscaPrecoFuturoSug:
    MSG$ = "FU_BuscaPrecoFuturoSug - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_BuscaPrecoBase(nm_ciclo_operacional As String, MSG$, Lista As Boolean) As Integer

Dim sql As String

FU_BuscaPrecoBase = FAIL%

On Error GoTo FU_BuscaPrecoBase

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_base_preco_s " & nm_ciclo_operacional & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0
QtdPrecoBase = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        QtdPrecoBase = gObjeto.varResultSet(hSql, 0)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_pdr_base_preco_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If


FU_BuscaPrecoBase = SUCCED%
Exit Function

FU_BuscaPrecoBase:
    MSG$ = "FU_BuscaPrecoBase - Erro (" & Err & ") - " & Error
    Exit Function

End Function
Private Function FU_BuscaPrecoBaseLista(nm_ciclo_operacional As String, cd_lista As String, MSG$, Lista As Boolean) As Integer

Dim sql As String

FU_BuscaPrecoBaseLista = FAIL%

On Error GoTo FU_BuscaPrecoBaseLista

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_base_preco_lista_s " & nm_ciclo_operacional & ", " & cd_lista & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0
QtdPrecoBase = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        QtdPrecoBase = gObjeto.varResultSet(hSql, 0)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_pdr_base_preco_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If


FU_BuscaPrecoBaseLista = SUCCED%
Exit Function

FU_BuscaPrecoBaseLista:
    MSG$ = "FU_BuscaPrecoBaseLista - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_ObtemMateriais(nm_ciclo_operacional As String, cd_vnd_produto As String, MSG$) As Integer
Dim sql As String

FU_ObtemMateriais = FAIL%

On Error GoTo FU_ObtemMateriais

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_materiais_venda_s " & nm_ciclo_operacional & "," & cd_vnd_produto & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0
sql_nm_tab = ""
ReDim Materiais(0)

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        sql_qt_lnh = sql_qt_lnh + 1
        ReDim Preserve Materiais(sql_qt_lnh)
        Materiais(sql_qt_lnh).dc_produto = gObjeto.varResultSet(hSql, 1)
        Materiais(sql_qt_lnh).id_tipo_produto = gObjeto.varResultSet(hSql, 0)
        Materiais(sql_qt_lnh).cd_material = gObjeto.varResultSet(hSql, 2)
        Materiais(sql_qt_lnh).vigente = IIf(Val(gObjeto.varResultSet(hSql, 4)) = 1, True, False)
        Materiais(sql_qt_lnh).Gravar = True
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_pdr_produto_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If

If sql_qt_lnh > 0 Then
    txt_produto = Materiais(1).dc_produto
    id_tipo_produto = Materiais(1).id_tipo_produto
    v_cd_material = Materiais(1).cd_material
End If

FU_ObtemMateriais = SUCCED%
Exit Function

FU_ObtemMateriais:
    MSG$ = "FU_ObtemMateriais - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_ObtemMateriaisL(nm_ciclo_operacional As String, cd_vnd_produto As String, MSG$) As Integer
    Dim sql As String

    FU_ObtemMateriaisL = FAIL%

    On Error GoTo FU_ObtemMateriaisL

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_materiais_venda_s " & nm_ciclo_operacional & "," & cd_vnd_produto & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0
    sql_nm_tab = ""
    ReDim Materiais(0)

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            sql_qt_lnh = sql_qt_lnh + 1
            ReDim Preserve Materiais(sql_qt_lnh)
            Materiais(sql_qt_lnh).dc_produto = gObjeto.varResultSet(hSql, 1)
            Materiais(sql_qt_lnh).id_tipo_produto = gObjeto.varResultSet(hSql, 0)
            Materiais(sql_qt_lnh).cd_material = gObjeto.varResultSet(hSql, 2)
            Materiais(sql_qt_lnh).vigente = IIf(Val(gObjeto.varResultSet(hSql, 4)) = 1, True, False)
            Materiais(sql_qt_lnh).Gravar = True
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_produto_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    If sql_qt_lnh > 0 Then
        txtdescricao = Materiais(1).dc_produto
        id_tipo_produto = Materiais(1).id_tipo_produto
        v_cd_material = Materiais(1).cd_material
    End If

    FU_ObtemMateriaisL = SUCCED%
    Exit Function

FU_ObtemMateriaisL:
    MSG$ = "FU_ObtemMateriaisL - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_ObtemMateriaisFuturos(nm_ciclo_operacional As String, cd_vnd_produto As String, MSG$) As Integer

    Dim sql      As String
    
    FU_ObtemMateriaisFuturos = FAIL%
    
    On Error GoTo FU_ObtemMateriaisFuturos
    
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_materiais_venda_s " & nm_ciclo_operacional & "," & cd_vnd_produto & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
    
    sql_qt_lnh = 0
    sql_nm_tab = ""
    ReDim MateriaisFuturos(0)
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            sql_qt_lnh = sql_qt_lnh + 1
            ReDim Preserve MateriaisFuturos(sql_qt_lnh)
            MateriaisFuturos(sql_qt_lnh).dc_produto = gObjeto.varResultSet(hSql, 1)
            MateriaisFuturos(sql_qt_lnh).id_tipo_produto = gObjeto.varResultSet(hSql, 0)
            MateriaisFuturos(sql_qt_lnh).cd_material = gObjeto.varResultSet(hSql, 2)
            MateriaisFuturos(sql_qt_lnh).vigente = IIf(Val(gObjeto.varResultSet(hSql, 4)) = 1, True, False)
            MateriaisFuturos(sql_qt_lnh).ciclo = nm_ciclo_operacional
            MateriaisFuturos(sql_qt_lnh).Gravar = True
        Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_materiais_venda_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    If sql_qt_lnh > 0 Then
        id_tipo_produto_futuro = MateriaisFuturos(1).id_tipo_produto
    End If

    FU_ObtemMateriaisFuturos = SUCCED%
    Exit Function

FU_ObtemMateriaisFuturos:
    MSG$ = "FU_ObtemMateriaisFuturos - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Public Function Grava_Percentual(nm_ciclo_operacional As String, _
                                 cd_lista As String, _
                                 cd_Segmento_canal As String, _
                                 cd_venda_produto As String, _
                                 pc_reajuste_lista As String, _
                                 exclui_reajuste As String) As Boolean

    Dim se_Sql      As String
    Dim sql_cd_ret  As Integer
    Dim sql_qt_lnh  As Integer
    Dim work        As String

    Grava_Percentual = False
    On Error GoTo Grava_Percentual
       
    PonteiroMouse vbHourglass
    
    If exclui_reajuste = "S" Then
        se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
        se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
        se_Sql = se_Sql & " exec st_pdr_reajuste_lista_d "
        se_Sql = se_Sql & nm_ciclo_operacional & ", "
        se_Sql = se_Sql & cd_lista & ", "
        se_Sql = se_Sql & cd_Segmento_canal & ", "
        se_Sql = se_Sql & cd_venda_produto & ", "
        se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out, @sql_nm_tab, @sql_qt_lnh out, @msg_err out"
    Else
        se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
        se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
        se_Sql = se_Sql & " exec st_pi_reajuste_lista_m "
        se_Sql = se_Sql & nm_ciclo_operacional & ", "
        se_Sql = se_Sql & cd_lista & ", "
        se_Sql = se_Sql & cd_Segmento_canal & ", "
        se_Sql = se_Sql & cd_venda_produto & ", "
        se_Sql = se_Sql & ConvertePontoDecimal(pc_reajuste_lista, Ponto) & ", "
        se_Sql = se_Sql & "'" & Le_Usuario & "', "
        se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out, @sql_nm_tab, @sql_qt_lnh out, @msg_err out"
    End If
      
    If gObjeto.natPrepareQuery(hSql, se_Sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
   
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a Grava_Percentual " & gObjeto.nServerNum & gObjeto.sServerMsg
        PonteiroMouse vbNormal
        Exit Function
    End If
    
    sql_cd_ret = Val(gObjeto.varOutputParam(0))
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))
   
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a Grava_Percentual " & gObjeto.nServerNum & gObjeto.sServerMsg
        PonteiroMouse vbNormal
        Exit Function
    End If
    Grava_Percentual = True
    PonteiroMouse vbNormal
    Exit Function
   
Grava_Percentual:
    MSG$ = "Grava_Percentual  - Erro (" & Err & ") - " & Error
    PonteiroMouse vbNormal
    Exit Function
End Function


Private Sub ExibeLog()

    Dim TelaLog As Form
    Dim cont As Integer
    
    If UBound(LogGeraLista) > 0 Then
    
        Set TelaLog = New frmLogLista
        
        Load TelaLog
        
        For cont = 1 To UBound(LogGeraLista)
            TelaLog.CriaLinha
            TelaLog.RecebeDados 1, LogGeraLista(cont).cd_material
            TelaLog.RecebeDados 2, LogGeraLista(cont).cd_venda
            TelaLog.RecebeDados 3, LogGeraLista(cont).cd_lista
            TelaLog.RecebeDados 4, LogGeraLista(cont).cd_segmento
            TelaLog.RecebeDados 5, LogGeraLista(cont).descricao
        Next cont
        
        TelaLog.Show
    End If
    
End Sub

Private Sub RegistraLog(pMaterial As String, pCodigoVenda As String, pLista As String, pSegmento As String, pMensagem As String)

    Dim cont  As Integer
    
    cont = UBound(LogGeraLista) + 1
    ReDim Preserve LogGeraLista(cont)
    
    LogGeraLista(cont).cd_material = pMaterial
    LogGeraLista(cont).cd_venda = pCodigoVenda
    LogGeraLista(cont).cd_lista = pLista
    LogGeraLista(cont).cd_segmento = pSegmento
    LogGeraLista(cont).descricao = pMensagem

End Sub

Private Function Gera_Lista(nm_ciclo_operacional As String, cd_segmento As Integer, cd_material As String, cd_venda_produto As Integer) As Boolean

    Dim se_Sql As String
    Dim sql_cd_ret As Integer
    Dim sql_qt_lnh As Integer
    Dim work As String
    Dim Cor As Long
    Dim cont  As Integer
    
    Gera_Lista = False
    
    On Error GoTo Gera_Lista
    
    se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
    se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
    se_Sql = se_Sql & " exec st_pi_cn_gera_parc_cn " & nm_ciclo_operacional & ", " & cd_material & ", 0, " & cd_segmento & " , '" & Le_Usuario & "', "
    se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
   
   If gObjeto.natPrepareQuery(hSql, se_Sql) Then
      Do While gObjeto.natFetchNext(hSql)
            
            cont = UBound(LogGeraLista) + 1
            ReDim Preserve LogGeraLista(cont)
            
            If Left(gObjeto.varResultSet(hSql, 3), 1) <> "6" Then
                LogGeraLista(cont).cd_material = gObjeto.varResultSet(hSql, 0)
                LogGeraLista(cont).cd_venda = gObjeto.varResultSet(hSql, 1)
                LogGeraLista(cont).cd_lista = gObjeto.varResultSet(hSql, 2)
                LogGeraLista(cont).cd_segmento = gObjeto.varResultSet(hSql, 3)
                LogGeraLista(cont).descricao = gObjeto.varResultSet(hSql, 5)
            End If
            
      Loop
      
   End If
   
   If gObjeto.nErroSQL <> 0 Then
   
      'cont = UBound(LogGeraLista) + 1
      'ReDim Preserve LogGeraLista(cont)
      'LogGeraLista(cont).cd_material = cd_material
      'LogGeraLista(cont).cd_segmento = cd_segmento
      'LogGeraLista(cont).descricao = "Erro ao gerar lista de preco do ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4) & " " & gObjeto.nServerNum & gObjeto.sServerMsg
      
      MSG$ = "Erro ao executar a Grava_Percentual " & gObjeto.nServerNum & gObjeto.sServerMsg
      Exit Function
   End If
    
   sql_cd_ret = Val(gObjeto.varOutputParam(0))
   sql_qt_lnh = Val(gObjeto.varOutputParam(3))
   
   If sql_cd_ret <> 0 Then
      'MSG$ = "Erro ao executar a Gera_Lista " & gObjeto.nServerNum & gObjeto.sServerMsg & " - " & gObjeto.varOutputParam(4)
      cont = UBound(LogGeraLista) + 1
      ReDim Preserve LogGeraLista(cont)
      LogGeraLista(cont).cd_material = cd_material
      LogGeraLista(cont).cd_segmento = cd_segmento
      LogGeraLista(cont).descricao = "Erro ao gerar lista de preco do ciclo " & Mid(nm_ciclo_operacional, 5, 2) & "-" & Mid(nm_ciclo_operacional, 1, 4) & " " & gObjeto.varOutputParam(4)
      
      Exit Function
   End If
   
   Gera_Lista = True
   Exit Function
   
Gera_Lista:
   MSG$ = "Gera_Lista - Erro (" & Err & ") - " & Error
   Exit Function


End Function

Private Function FU_AtualizaPrecoRefKit(nm_ciclo_operacional As String, cd_produto As String, cd_item_kit As String, vl_preco_referencia_kit As String, qt_pontos_referencia_kit As String, MSG$) As Integer

    Dim sql As String
    
    FU_AtualizaPrecoRefKit = FAIL%
    
    On Error GoTo FU_AtualizaPrecoRefKit
    
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_ref_kit_m " & nm_ciclo_operacional & ","
    sql = sql & cd_produto & "," & cd_item_kit & "," & vl_preco_referencia_kit & "," & qt_pontos_referencia_kit
    sql = sql & ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_preco_base_kit_u - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_preco_base_kit_u " & "RETURN STATUS = " & sql_cd_ret
        Exit Function
    End If
    
    FU_AtualizaPrecoRefKit = SUCCED%
    Exit Function

FU_AtualizaPrecoRefKit:
    MSG$ = "FU_AtualizaPrecoRefKit - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_AtualizaPrecoSugFutKit(cd_lista As String, nm_ciclo_operacional As String, cd_produto As String, cd_item_kit As String, vl_preco_referencia_kit As String, qt_pontos_referencia_kit As String, MSG$) As Integer

    Dim sql As String
    Dim se_Usuario   As String
    Dim ne_ret       As Integer

    
    
    FU_AtualizaPrecoSugFutKit = FAIL%
    
    On Error GoTo FU_AtualizaPrecoSugFutKit
    
    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
        
    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_sug_fut_kit_m " & nm_ciclo_operacional & ","
    sql = sql & cd_produto & "," & cd_item_kit & "," & vl_preco_referencia_kit & "," & qt_pontos_referencia_kit & ", " & cd_lista & ", " & Le_Usuario
    sql = sql & ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sug_fut_kit_m - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sug_fut_kit_m " & "RETURN STATUS = " & sql_cd_ret
        Exit Function
    End If
    
    FU_AtualizaPrecoSugFutKit = SUCCED%
    Exit Function

FU_AtualizaPrecoSugFutKit:
    MSG$ = "FU_AtualizaPrecoSugFutKit - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Function FU_AtualizaPrecoSugKit(cd_lista As String, nm_ciclo_operacional As String, cd_produto As String, cd_item_kit As String, vl_preco_referencia_kit As String, qt_pontos_referencia_kit As String, MSG$) As Integer

    Dim sql As String
    Dim se_Usuario   As String
    Dim ne_ret       As Integer

    FU_AtualizaPrecoSugKit = FAIL%
    
    On Error GoTo FU_AtualizaPrecoSugKit
    
    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_sug_kit_m " & nm_ciclo_operacional & ","
    sql = sql & cd_produto & "," & cd_item_kit & "," & vl_preco_referencia_kit & "," & qt_pontos_referencia_kit & ", " & cd_lista & ", " & Le_Usuario
    sql = sql & ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"
    
    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sug_kit_m - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_sug_kit_m " & "RETURN STATUS = " & sql_cd_ret
        Exit Function
    End If
    
    FU_AtualizaPrecoSugKit = SUCCED%
    Exit Function

FU_AtualizaPrecoSugKit:
    MSG$ = "FU_AtualizaPrecoSugKit - Erro (" & Err & ") - " & Error
    Exit Function

End Function


'---------------------------------------------------------------------------------------------------------
'Esta rotina verifica se todos os kits dos produtos tem os mesmos codigos de venda e quantidade de �tens
'---------------------------------------------------------------------------------------------------------
Private Sub ConfereComposicaoKit(p_material_eleito As String, pCicloFuturo As Boolean, pNmCiclo As String)

    Dim qtdItem                As Integer
    Dim v                      As Integer
    Dim Z                      As Integer
    Dim v_cd_venda_item_eleito As String
    Dim v_qtd_item_eleito      As String
    Dim v_kit_problema         As String
    Dim v_qtd_itens_kit_eleito As Integer
    Dim v_qtd_itens_kit        As Integer
    Dim v_produto_validar      As String
    Dim v_nm_ciclo_futuro      As String
        
    v_qtd_itens_kit = 0
    
    For qtdItem = 1 To UBound(v_componentes)
        If Trim(v_componentes(qtdItem).cd_produto_pai) = p_material_eleito Then
            v_qtd_itens_kit_eleito = v_qtd_itens_kit_eleito + 1
            v_componentes(qtdItem).QuantidadeOK = True
            v_componentes(qtdItem).KitOK = True   'este campo do array � inicializado com FALSE.
                                                  'para esta ocorrencia do array � atribu�do TRUE pq trata-se
                                                  'dos �tens de kit do produto "eleito" como base nesta tela.
        End If
    Next qtdItem
    
    v_nm_ciclo_futuro = Right(pNmCiclo, 4) + Left(pNmCiclo, 2)
    
    If Not pCicloFuturo Then
    
        '---------------------------------------------------------------------------------------------
        'confere se o N�MERO de �tens de kit corresponde ao N�MERO de �tens de kit do material eleito
        '---------------------------------------------------------------------------------------------
        For qtdItem = 1 To UBound(v_componentes)
        
            If Not v_componentes(qtdItem).QuantidadeOK Then
        
                v_produto_validar = Trim(v_componentes(qtdItem).cd_produto_pai)
                v_qtd_itens_kit = 0
                                                      
                For v = 1 To UBound(v_componentes)
        
                    If Trim(v_componentes(v).cd_produto_pai) = v_produto_validar Then
                        v_qtd_itens_kit = v_qtd_itens_kit + 1
                    End If
        
                Next v
                
                If v_qtd_itens_kit = v_qtd_itens_kit_eleito Then
                    v_componentes(qtdItem).QuantidadeOK = True
                End If
                
            End If
            
        Next qtdItem
        
        
        '------------------------------------------------------------------------------------------------------
        'confere se os c�digos de venda e quantidade dos �tens do kit correspondem ao do material eleito
        '------------------------------------------------------------------------------------------------------
        For qtdItem = 1 To UBound(v_componentes)
            
            If Trim(v_componentes(qtdItem).cd_produto_pai) = p_material_eleito Then
        
                v_cd_venda_item_eleito = Trim(v_componentes(qtdItem).cd_venda_item)
                v_qtd_item_eleito = Trim(v_componentes(qtdItem).qt_item_kit)
                                                      
                For v = 1 To UBound(v_componentes)
        
                    If Trim(v_componentes(v).cd_produto_pai) <> p_material_eleito Then

                        If v_componentes(v).QuantidadeOK And _
                            Trim(v_componentes(v).qt_item_kit) = v_qtd_item_eleito And _
                              Trim(v_componentes(v).cd_venda_item) = v_cd_venda_item_eleito Then
                            v_componentes(v).KitOK = True
                        End If
        
                    End If
        
                Next v
                
            End If
            
        Next qtdItem
                
        '------------------------------------------------------------------------------------------------------
        'marcar os materiais com kit divergente do kit do material eleito
        '------------------------------------------------------------------------------------------------------
        v_kit_problema = ""
        
        For qtdItem = 1 To UBound(v_componentes)
        
            If Not v_componentes(qtdItem).KitOK Then
                
                If InStr(1, v_kit_problema, v_componentes(qtdItem).cd_produto_pai, vbTextCompare) <= 0 Then
                
                    If Len(Trim(v_kit_problema)) = 0 Then
                        v_kit_problema = v_kit_problema & Trim(v_componentes(qtdItem).cd_produto_pai)
                    Else
                        v_kit_problema = v_kit_problema & ", " & Trim(v_componentes(qtdItem).cd_produto_pai)
                    End If
                
                    For v = 1 To UBound(Materiais)
                        If Materiais(v).cd_material = v_componentes(qtdItem).cd_produto_pai Then
                            Materiais(v).Gravar = False
                            If Materiais(v).vigente Then
                                Call RegistraLog(v_componentes(qtdItem).cd_produto_pai, txt_Codigo.Text, "", "", "Kit do produto " & v_componentes(qtdItem).cd_produto_pai & " possui composi��o divergente do produto " & v_kit_problema & " no ciclo " & pNmCiclo)
                            End If
                        End If
                    Next v
                    
                End If
                
            End If
        
        Next qtdItem
        
        'If Len(Trim(v_kit_problema)) > 0 Then
        '    Call RegistraLog("", txt_codigo.Text, "", "", "Kit do produto " & v_kit_problema & " possui composi��o diferente.")
        'End If
        
    Else
    
        '---------------------------------------------------------------------------------------------
        'confere se o N�MERO de �tens de kit corresponde ao N�MERO de �tens de kit do material eleito
        '---------------------------------------------------------------------------------------------
        For qtdItem = 1 To UBound(v_componentes_futuros)
        
            If Not v_componentes_futuros(qtdItem).QuantidadeOK Then
        
                v_produto_validar = Trim(v_componentes_futuros(qtdItem).cd_produto_pai)
                v_qtd_itens_kit = 0
                                                      
                For v = 1 To UBound(v_componentes_futuros)
        
                    If Trim(v_componentes_futuros(v).cd_produto_pai) = v_produto_validar And Trim(v_componentes_futuros(v).ciclo) = Trim(v_nm_ciclo_futuro) Then
                        v_qtd_itens_kit = v_qtd_itens_kit + 1
                    End If
        
                Next v
                
                If v_qtd_itens_kit = v_qtd_itens_kit_eleito Then
                    v_componentes_futuros(qtdItem).QuantidadeOK = True
                End If
                
            End If
            
        Next qtdItem
        
        '------------------------------------------------------------------------------------------------------
        'confere se os c�digos de venda e quantidade dos �tens do kit correspondem ao do material eleito
        '------------------------------------------------------------------------------------------------------
        For qtdItem = 1 To UBound(v_componentes)
            
            If Trim(v_componentes(qtdItem).cd_produto_pai) = p_material_eleito Then
        
                v_cd_venda_item_eleito = Trim(v_componentes(qtdItem).cd_venda_item)
                v_qtd_item_eleito = Trim(v_componentes(qtdItem).qt_item_kit)
                                                      
                For v = 1 To UBound(v_componentes_futuros)
        
                    If v_componentes_futuros(v).QuantidadeOK And _
                        Trim(v_componentes_futuros(v).qt_item_kit) = v_qtd_item_eleito And _
                          Trim(v_componentes_futuros(v).cd_venda_item) = v_cd_venda_item_eleito And _
                              Trim(v_componentes_futuros(v).ciclo) = Trim(v_nm_ciclo_futuro) Then
                        v_componentes_futuros(v).KitOK = True
                    End If
        
                Next v
                
            End If
            
        Next qtdItem
        
        '------------------------------------------------------------------------------------------------------
        'marcar os materiais com kit divergente do kit do material eleito
        '------------------------------------------------------------------------------------------------------
        v_kit_problema = ""
        
        For qtdItem = 1 To UBound(v_componentes_futuros)
        
            If Not v_componentes_futuros(qtdItem).KitOK Then
                
                If InStr(1, v_kit_problema, v_componentes_futuros(qtdItem).cd_produto_pai, vbTextCompare) <= 0 Then
                
                    If Len(Trim(v_kit_problema)) = 0 Then
                        v_kit_problema = v_kit_problema & Trim(v_componentes_futuros(qtdItem).cd_produto_pai)
                    Else
                        v_kit_problema = v_kit_problema & ", " & Trim(v_componentes_futuros(qtdItem).cd_produto_pai)
                    End If
                
                    For v = 1 To UBound(MateriaisFuturos)
                        If MateriaisFuturos(v).cd_material = v_componentes_futuros(qtdItem).cd_produto_pai And Trim(MateriaisFuturos(v).ciclo) = Trim(v_nm_ciclo_futuro) Then
                        
                            MateriaisFuturos(v).Gravar = False
                            If MateriaisFuturos(v).vigente Then
                                Call RegistraLog(v_componentes(qtdItem).cd_produto_pai, txt_Codigo.Text, "", "", "Kit do produto " & v_componentes(qtdItem).cd_produto_pai & " possui composi��o divergente do produto " & v_kit_problema & " no ciclo " & pNmCiclo)
                            End If
                        End If
                    Next v
                    
                End If
                
            End If
        
        Next qtdItem
        
        'If Len(Trim(v_kit_problema)) > 0 Then
        '    Call RegistraLog("", txt_codigo.Text, "", "", "Kit do produto " & v_kit_problema & " possui composi��o diferente.")
        'End If
        
    End If

End Sub

Private Function FU_ObtemReajusteLista(nm_ciclo_operacional, cd_venda_produto As String, MSG$) As Integer

    Dim sql      As String
    Dim Occur    As Integer
    Dim vPercent As String
    
    FU_ObtemReajusteLista = FAIL%
    
    On Error GoTo FU_ObtemReajusteLista
    
    ReDim ReajusteLista(0)
    
    sql = "declare "
    sql = sql & "@sql_cd_ret int, "
    sql = sql & "@sql_cd_opr char(1), "
    sql = sql & "@sql_nm_tab char(30), "
    sql = sql & "@sql_qt_lnh int "
    sql = sql & " exec st_pdr_reajuste_lista_s "
    sql = sql & nm_ciclo_operacional & ","
    sql = sql & cd_venda_produto & ","
    sql = sql & "@sql_cd_ret out,"
    sql = sql & "@sql_cd_opr out,"
    sql = sql & "@sql_nm_tab out,"
    sql = sql & "@sql_qt_lnh out"

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
        
            Occur = UBound(ReajusteLista) + 1
            ReDim Preserve ReajusteLista(Occur)
            
            ReajusteLista(Occur).nm_ciclo_operacional = nm_ciclo_operacional
            ReajusteLista(Occur).cd_venda_produto = cd_venda_produto
            
            ReajusteLista(Occur).cd_lista = gObjeto.varResultSet(hSql, 0)
            ReajusteLista(Occur).cd_Segmento_canal = gObjeto.varResultSet(hSql, 1)
            
            vPercent = gObjeto.varResultSet(hSql, 2)
            
            ReajusteLista(Occur).pc_reajuste_lista = ConvertePontoDecimal(vPercent, 0)
            ReajusteLista(Occur).Excluir = "N"
        
        Loop
    End If
    
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pdr_reajuste_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    'If sql_cd_ret <> 0 Then
    '    MSG$ = "Erro ao executar a st_pi_cn_produto_kit_s " & "RETURN STATUS = " & sql_cd_ret
    '    Exit Function
    'End If
    
    'If i = 0 Then ReDim v_componentes(0)
    
    FU_ObtemReajusteLista = SUCCED%
    Exit Function

FU_ObtemReajusteLista:
    MSG$ = "FU_ObtemReajusteLista - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Function FU_ObtemVigenciaCodVenda(nm_ciclo_operacional As String, cd_vnd_produto As String, MSG$) As Integer

    Dim sql As String

    FU_ObtemVigenciaCodVenda = FAIL%

    On Error GoTo FU_ObtemVigenciaCodVenda

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_ref_abrangencia_s " & nm_ciclo_operacional & "," & cd_vnd_produto & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            sql_qt_lnh = sql_qt_lnh + 1
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_produto_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))

'If sql_cd_ret <> 0 Then
'    MSG$ = "Erro ao executar a st_pi_cn_produto_s " & "RETURN STATUS = " & sql_cd_ret
'    Exit Function
'End If

FU_ObtemVigenciaCodVenda = SUCCED%
Exit Function

FU_ObtemVigenciaCodVenda:
    MSG$ = "FU_ObtemVigenciaCodVenda - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Public Sub ChamadaExterna(pCodigo As String, pDescricao As String)
    txt_Codigo.Text = pCodigo
    txt_produto.Text = pDescricao
    cmbCiclo.Text = ""
    cmbCiclo.Enabled = True
    execucaoExterna = True
    Me.Show
    cmbCiclo.SetFocus
    Me.ZOrder 0
End Sub

Private Sub txtcodvenda_Change()
    If txtcodvenda.Text <> "" And (Len(cmbLista.Text) = 0 Or cmbLista.Text = "") Then
        MsgBox "N�o h� lista de pre�os relacionada � estrutura comercial informada  !", vbInformation, "A T E N � � O"
        txtcodvenda.Text = ""
        PonteiroMouse vbNormal
        Exit Sub
    End If
        
    If Not bm_novo Then
        If Trim$(cmbCicloL) <> "" And Trim$(txtcodvenda) <> "" Then
            Toolbar1.Buttons.Item(5).Enabled = True
        Else
            Toolbar1.Buttons.Item(5).Enabled = False
        End If
    End If
    
    txtdescricao.Enabled = False
    txtdescricao = ""
    txtprecobase.Enabled = False
    txtprecobase = ""
    txtprecosugestao.Enabled = True
    txtprecosugestao = ""
    cmdcomponentes.Enabled = False
    txtredutorlista.Text = ""
    'txtstatus.Enabled = False
    'txtstatus = ""
    txtpontos.Enabled = True
    txtpontos = ""
    v_cd_material = 0
    v_cd_material_vigente = 0
    ReDim Materiais(0)
    ReDim v_componentes(0)
End Sub

Private Sub txtCodVenda_KeyPress(KeyAscii As Integer)

    Dim nm_ciclo_operacional As String

    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) And (KeyAscii <> 13) Then
        KeyAscii = 0
        Beep
        Exit Sub
    End If
    
    execucaoExterna = False
   
    If KeyAscii = 13 Then
            
            
            strOperacaoLista = "I" 'SE 6346
            
            KeyAscii = 0
            ReDim LogGeraLista(0)
             
            If Trim$(cmbCicloL) = "" Then
                MsgBox "Ciclo/Ano deve ser preenchido !", vbInformation, "A T E N � � O"
                cmbCicloL.SetFocus
                Exit Sub
            End If
             
            If Trim$(txtcodvenda) = "" Then
                MsgBox "C�digo do produto deve ser preenchido !", vbInformation, "A T E N � � O"
                txtcodvenda.SetFocus
                Exit Sub
            End If
             
            nm_ciclo_operacional = Right(cmbCicloL, 4) + Left(cmbCicloL, 2)
                         
            PonteiroMouse vbHourglass
             
            '--------------------------------------
            'verifica se o c�digo de venda existe
            '--------------------------------------
            If FU_ExisteProduto(Trim$(txtcodvenda), MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                MsgBox "C�digo de produto n�o cadastrado  !", vbInformation, "A T E N � � O"
                PonteiroMouse vbNormal
                txtcodvenda.SetFocus
                Call SelCodigo(txtcodvenda)
                Exit Sub
            End If
             
            '-------------------------------------------------
            'verifica a vig�ncia do c�digo de venda informado
            '-------------------------------------------------
            If FU_ObtemVigenciaCodVenda(nm_ciclo_operacional, Trim$(txtcodvenda), MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                MsgBox "Produto n�o dispon�vel neste ciclo: vig�ncia e abrang�ncia n�o cadastradas !", vbInformation, "A T E N � � O"
                PonteiroMouse vbNormal
                txtcodvenda.SetFocus
                Call SelCodigo(txtcodvenda)
                Exit Sub
            End If
             
            '-------------------------------------------------
            'obtem os materiais do c�digo de venda informado
            '-------------------------------------------------
            If FU_ObtemMateriaisL(nm_ciclo_operacional, Trim$(txtcodvenda), MSG$) = FAIL% Then
                 MsgBox MSG$, vbCritical, "A T E N � � O"
                 PonteiroMouse vbNormal
                 Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                 MsgBox "N�o h� materais cadastrados para o c�digo de venda informado !", vbInformation, "A T E N � � O"
                 PonteiroMouse vbNormal
                 txtcodvenda.SetFocus
                 Call SelCodigo(txtcodvenda)
                 Exit Sub
            End If
                      
            '---------------------------------------------------------
            'busca UM pre�o de ref�ncia para c�digo de venda digitado
            '---------------------------------------------------------
            If FU_BuscaPrecoRefProdutoL(nm_ciclo_operacional, Trim$(txtcodvenda), MSG$, True) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                 MsgBox "Produto n�o possui pre�o cadastrado. Realizar manuten��o de pre�o refer�ncia!", vbInformation, "A T E N � � O"
                 txtcodvenda.Text = ""
                 txtdescricao.Text = ""
                 txtprecobase.Text = ""
                 txtprecosugestao.Text = ""
                 PonteiroMouse vbNormal
                 txtcodvenda.SetFocus
                 Call SelCodigo(txtcodvenda)
                 Exit Sub
            End If
                      
            
            If FU_BuscaPrecoSugProdutoL(nm_ciclo_operacional, Trim$(txtcodvenda), MSG$, cmbLista.Text) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            If sql_qt_lnh = 0 Then
                 MsgBox "Produto n�o possui pre�o sugest�o cadastrado. !", vbInformation, "A T E N � � O"
                 PonteiroMouse vbNormal
                 txtcodvenda.SetFocus
                 Call SelCodigo(txtcodvenda)
                 Exit Sub
            End If
            
            '------------------------------------------------------------------
            'busca os percentuais de reajuste para o c�digo de venda informado
            '------------------------------------------------------------------
            'If FU_ObtemReajusteLista(nm_ciclo_operacional, txt_codigo.Text, MSG$) = FAIL% Then
            '    MsgBox MSG$, vbCritical, "A T E N � � O"
            '    PonteiroMouse vbNormal
            '    Exit Sub
            'End If
            
            PonteiroMouse vbNormal
                                                            
            'If sql_qt_lnh = 0 Then
            
            '    If MsgBox("Produto n�o possui pre�o cadastrado." & Chr(10) & Chr(10) & " Deseja incluir ?", vbInformation + vbYesNo, "ATEN��O") = vbYes Then
            '        txt_cicloano.Enabled = False
            '        cmbCiclo.Enabled = False
            '        txt_codigo.Enabled = False
            '        txt_precobase.Enabled = True
            '        txt_redutor.Enabled = True
            '        txt_status.Enabled = True
            '        txt_pontos.Enabled = True
            '        optPreco(0).Value = True
            '        optPreco(1).Value = False
            '        txt_precobase.SetFocus
            '        bm_novo = True
            '    Else
            '        txt_cicloano.Enabled = True
            '        cmbCiclo.Enabled = True
            '        txt_codigo.Enabled = True
            '        txt_codigo.Text = ""
            '        txt_produto.Text = ""
            '        optPreco(0).Value = True
            '        optPreco(1).Value = False
            '        txt_codigo.SetFocus
            '        txt_precobase.Enabled = False
            '        txt_redutor.Enabled = False
            '        txt_status.Enabled = False
            '        txt_pontos.Enabled = False
            '        bm_novo = False
            '        Exit Sub
            '    End If
            'Else
            '    txt_cicloano.Enabled = False
            '    cmbCiclo.Enabled = False
            '    txt_codigo.Enabled = False
            '    txt_precobase.Enabled = True
            '    txt_redutor.Enabled = True
            '    txt_status.Enabled = True
            '    txt_pontos.Enabled = True
            '    bm_novo = False
            'End If
         
         ReDim v_componentes(0)
         
         ' verificar se e pai de KIT ou estojo
         If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
         '   If bm_novo Then
         '       If FU_BuscaProdutoKit(nm_ciclo_operacional, Trim$(v_cd_material), MSG$) = FAIL% Then
         '           MsgBox MSG$, vbCritical, "A T E N � � O"
         '           PonteiroMouse vbNormal
         '           Exit Sub
         '       End If
         '   Else
                If FU_BuscaPrecoSugKit(nm_ciclo_operacional, Trim$(txtcodvenda.Text), cmbLista.Text, MSG$) = FAIL% Then
                    MsgBox MSG$, vbCritical, "A T E N � � O"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
         '   End If
         '
            If sql_qt_lnh > 0 Then
                cmdcomponentes.Enabled = True
            Else
                cmdcomponentes.Enabled = False
            End If
                           
         '   'obtem os ites de kit dos outros materiais
            For contMat = 1 To UBound(Materiais)
                If Materiais(contMat).cd_material <> v_cd_material Then
                    If FU_BuscaProdutoKit(nm_ciclo_operacional, Trim$(Materiais(contMat).cd_material), MSG$) = FAIL% Then
                        MsgBox MSG$, vbCritical, "A T E N � � O"
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                End If
                If sql_qt_lnh <= 0 Then
                   Materiais(contMat).Gravar = False
                End If
            Next contMat
         Else
             cmd_componentes.Enabled = False
         End If
         
         Call ConfereComposicaoKit(v_cd_material, False, cmbCicloL.Text)
            
            
        strOperacaoLista = "A" 'SE 6346
        bm_Alterado = False
        recalcula = False
        txtpontos.Enabled = True
        txtprecosugestao.Enabled = True
        txtprecosugestao.SetFocus
        
        Toolbar1.Buttons.Item(1).Enabled = True
        Toolbar1.Buttons.Item(5).Enabled = False
        
       ' If ChecaPermissao("frmPDO011", "M") Then
       '     st_Toolbar.Buttons.Item(2).Enabled = True
       ' End If
        
    End If


End Sub

Public Sub Graba_Preco()
    Dim nm_ciclo_operacional As String
    Dim Z                    As Integer
    Dim k                    As Integer
    Dim vAtualizaCicloFuturo As Boolean
    Dim v_ultima_lista As String
   
    blGravaLog = True 'SE 6346
    
    nm_ciclo_operacional = Right(cmbCicloL, 4) + Left(cmbCicloL, 2)
    
    txt_precobase = Format(txt_precobase, "#0.00")
    
    If Trim$(cmbCicloL.Text) = "" Then
        MsgBox "Ciclo / Ano deve ser preenchido !", vbInformation, "A T E N � � O"
        cmbCicloL.SetFocus
        Exit Sub
    End If
    
    If Trim$(txtcodvenda) = "" Then
        MsgBox "C�digo do produto deve ser preenchido !", vbInformation, "A T E N � � O"
        txtcodvenda.SetFocus
        Exit Sub
    End If

    ' verifica se a lista esta vazia ou se a lista = 0
    If cmbLista.Text = 0 Or cmbLista.Text = "" Then
        MsgBox "O C�digo da lista esta vazio  !", vbInformation, "A T E N � � O"
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    ' Verifica o pre�o sugest�o colocado
    If Trim$(txtprecobase) = "" Or Trim$(txtprecobase) = "0,00" Then
        MsgBox "Pre�o refer�ncia n�o pode ser zero !", vbInformation, "A T E N � � O"
        txtprecosugestao.SetFocus
        Exit Sub
    End If
    
    
    If Trim$(txtredutorlista) = "" Or (Not IsNumeric(txtredutorlista.Text)) Then
        MsgBox "Percentual redutor de Pontos deve ser preenchido !", vbInformation, "A T E N � � O"
        txtredutorlista.SetFocus
        Exit Sub
    End If
     
    If CDbl(Trim$(txtredutorlista)) < CDbl("0,00") Or CDbl(Trim$(txtredutorlista)) > CDbl("100,00") Then
        MsgBox "Redutor de Pontos deve estar no intervalo de 0 a 100 !", vbInformation, "A T E N � � O"
        txtredutorlista.SetFocus
        Exit Sub
    End If
    
    ' Verifica o pre�o sugest�o colocado
    If Mid(cmbStatusL.Text, 1, 1) = 2 And txtredutorlista.Text <> 100 Then
        MsgBox "Para Status de Venda igual a 2 o redutor deve ser obrigatoriamente igual a 100", vbInformation, "A T E N � � O"
        txtprecosugestao.SetFocus
        Exit Sub
    End If
    
    If Not ValidaKitL Then
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    vAtualizaCicloFuturo = False
    v_ultima_lista = Prox_Cod_Lista(CLng(Right(cmbCicloL, 4) + Left(cmbCicloL, 2)))
    
    'If FU_BuscaPrecoFuturoSug(Right(cmbCicloL, 4) + Left(cmbCicloL, 2), cmbLista.Text, MSG$) = FAIL% Then
    If FU_BuscaPrecoFuturoSug(Right(cmbCicloL, 4) + Left(cmbCicloL, 2), v_ultima_lista, MSG$) = FAIL% Then
         MsgBox MSG$, vbCritical, "A T E N � � O"
         PonteiroMouse vbNormal
    Else
         
         If QtdPrecoFuturo <> 999 Then
            If QtdPrecoFuturo > 0 Then
                If MsgBox("Pre�o refer�ncia gerado para ciclos futuros ! Deseja replicar as altera��es para estes ciclos ? ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
                    vAtualizaCicloFuturo = True
                End If
            Else
                MsgBox "Pre�o refer�ncia gerado para ciclos futuros. Quantidade de listas nos ciclos futuros diferente do ciclo " & Right(cmbCicloL, 4) + Left(cmbCicloL, 2) & ". As altera��es n�o poder�o ser replicadas para estes ciclos", vbInformation, "A T E N � � O"
                vAtualizaCicloFuturo = False
            End If
         End If
    End If
    
    For contMat = 1 To UBound(Materiais)
        If Materiais(contMat).Gravar Then
            '------------------------------------
            'gravando pre�o do produto pai
            '------------------------------------
            If FU_AtualizaPrecoSug(txtredutorlista, nm_ciclo_operacional, txtcodvenda.Text, id_tipo_produto, cmbLista, txtpontos, txtprecosugestao, MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                PonteiroMouse vbNormal
                Exit Sub
            End If
            
            'SE 6346 - In�cio
            If blGravaLog Then
                If Not GravaHistorico(6, strOperacaoLista, "Ciclo", nm_ciclo_operacional, "Produto", txtcodvenda.Text, "Lista", cmbLista) Then
                    If Not Runquery("ROLLBACK", hSql) Then
                        SetStatus "Erro na grava��o..."
                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                        PonteiroMouse vbNormal
                        Exit Sub
                    End If
                    MsgBox "Falha ao gravar hist�rico.", vbCritical, "Erro"
                    Exit Sub
                End If
                blGravaLog = False
            End If
            'SE 6346 - Fim
            
            '------------------------------------
            'gravando componentes de kit
            '------------------------------------
            If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
                For Z = 1 To UBound(v_componentes)
                        DoEvents
                    
                        If FU_AtualizaPrecoSugKit(cmbLista.Text, nm_ciclo_operacional, _
                                                  v_componentes(Z).cd_produto_pai, _
                                                  v_componentes(Z).cd_item_kit, _
                                                  ConvertePontoDecimal(v_componentes(Z).vl_preco_referencia_kit, 1), _
                                                  ConvertePontoDecimal(v_componentes(Z).qt_pontos_referencia_kit, 1), MSG$) = FAIL% Then
                                                  
                            MsgBox MSG$, vbCritical, "A T E N � � O"
                            If Not Runquery("ROLLBACK", hSql) Then
                                SetStatus "Erro na grava��o..."
                                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                                PonteiroMouse vbNormal
                         '       lblGravando.Visible = False
                                Exit Sub
                            End If
                            PonteiroMouse vbNormal
                            Exit Sub
                        End If
                    'End If
                Next Z
            End If
        End If
    Next contMat
    
    blGravaLog = True
    
    ' atualiza ciclos futuro
    If vAtualizaCicloFuturo Then
        For contMat = 1 To UBound(Materiais)
        If Materiais(contMat).Gravar Then
            '------------------------------------
            'gravando pre�o do produto pai
            '------------------------------------
            If FU_AtualizaPrecoFutSug(txtredutorlista, nm_ciclo_operacional, txtcodvenda.Text, id_tipo_produto, cmbLista, txtpontos, txtprecosugestao, MSG$) = FAIL% Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                If Not Runquery("ROLLBACK", hSql) Then
                    SetStatus "Erro na grava��o..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                    PonteiroMouse vbNormal
                    Exit Sub
                End If
                PonteiroMouse vbNormal
                Exit Sub
            End If
            
'            'SE 6346 - In�cio
'            If blGravaLog Then
'                If Not GravaHistorico(6, strOperacaoLista, "Ciclo", nm_ciclo_operacional, "Produto", txtcodvenda.Text, "Lista", cmbLista) Then
'                    If Not Runquery("ROLLBACK", hSql) Then
'                        SetStatus "Erro na grava��o..."
'                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
'                        PonteiroMouse vbNormal
'                        Exit Sub
'                    End If
'                    MsgBox "Falha ao gravar hist�rico.", vbCritical, "Erro"
'                    Exit Sub
'                End If
                blGravaLog = False
'            End If
'            'SE 6346 - Fim
            
            '------------------------------------
            'gravando componentes de kit
            '------------------------------------
            If Trim$(id_tipo_produto) = "2" Or Trim$(id_tipo_produto) = "3" Or Trim$(id_tipo_produto) = "4" Then
                For Z = 1 To UBound(v_componentes)
                        DoEvents
                    
                        If FU_AtualizaPrecoSugFutKit(cmbLista.Text, nm_ciclo_operacional, _
                                                  v_componentes(Z).cd_produto_pai, _
                                                  v_componentes(Z).cd_item_kit, _
                                                  ConvertePontoDecimal(v_componentes(Z).vl_preco_referencia_kit, 1), _
                                                  ConvertePontoDecimal(v_componentes(Z).qt_pontos_referencia_kit, 1), MSG$) = FAIL% Then
                                                  
                            MsgBox MSG$, vbCritical, "A T E N � � O"
                            If Not Runquery("ROLLBACK", hSql) Then
                                SetStatus "Erro na grava��o..."
                                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                                PonteiroMouse vbNormal
                         '       lblGravando.Visible = False
                                Exit Sub
                            End If
                            PonteiroMouse vbNormal
                            Exit Sub
                        End If
                    'End If
                Next Z
            End If
        End If
    Next contMat
    End If
    ' fim
    
        
    'lblGravando.Visible = False
    DoEvents
    PonteiroMouse vbNormal
    
    ' gravar o status venda
    If FU_AtualizaStatusVenda(Mid(cmbStatusL.Text, 1, 1), nm_ciclo_operacional, txtcodvenda.Text, cmbLista, MSG$) = False Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        If Not Runquery("ROLLBACK", hSql) Then
            SetStatus "Erro na grava��o..."
            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
            PonteiroMouse vbNormal
            'lblGravando.Visible = False
            Exit Sub
        End If
        PonteiroMouse vbNormal
        Exit Sub
    End If
    
    'SE 6346 - In�cio
    If UBound(Materiais) = 0 Then
        If Not GravaHistorico(6, strOperacaoLista, "Ciclo", nm_ciclo_operacional, "Produto", txtcodvenda.Text, "Lista", cmbLista) Then
            If Not Runquery("ROLLBACK", hSql) Then
                SetStatus "Erro na grava��o..."
                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                PonteiroMouse vbNormal
                Exit Sub
            End If
            MsgBox "Falha ao gravar hist�rico.", vbCritical, "Erro"
            Exit Sub
        End If
        blGravaLog = False
    End If
    'SE 6346 - Fim
    
    
    vAtualizaCicloFuturo = False
        
    ' verifica se ja existe preco base
    If FU_BuscaPrecoBaseLista(nm_ciclo_operacional, cmbLista, MSG$, False) = FAIL% Then
         MsgBox MSG$, vbCritical, "A T E N � � O"
         PonteiroMouse vbNormal
    Else
        If QtdPrecoBase > 0 Then
            MsgBox "Relat�rio Bases de Pre�os j� foi gerado !!! Necess�rio gerar novamente!! ", vbInformation, "A T E N � � O"
        End If
    End If
    
    cmdcomponentes.Enabled = False
    sProtegeTelaL
        
    If UBound(LogGeraLista) > 0 Then
        MsgBox "Atualiza��o efetuada, por�m ocorreram inconsist�ncias ao gerar lista de pre�os !", vbInformation
        Call ExibeLog
    Else
        MsgBox "Atualiza��o efetuada com sucesso !", vbInformation
    End If
    
    ReDim LogGeraLista(0)
       
End Sub

Private Sub txtprecosugestao_KeyPress(KeyAscii As Integer)
    Dim nm_ciclo As String
    If KeyAscii = 13 Then
        nm_ciclo = Right(cmbCicloL, 4) + Left(cmbCicloL, 2)
        txtprecosugestao.Text = Format(txtprecosugestao.Text, "##0.00")
        ' calcular pontos com redutor
        'txtpontos.Text = Format(FU_BuscaPontuacao(Str(nm_ciclo), Str(txtprecosugestao), MSG$), "#0")
        If Len(txtredutorlista) <> 0 Then
            txtredutorlista_LostFocus
            'txtpontos.Text = Format(txtpontos.Text * (txtredutorlista.Text / 100), "#0")
            cmbStatusL.SetFocus
        End If
        ' fim
        cmbStatusL.SetFocus
    End If
    
End Sub


Private Sub txtredutorlista_KeyPress(KeyAscii As Integer)

    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 And KeyAscii <> 13 Then
       KeyAscii = 0
       Beep
    End If
   
    If KeyAscii = 13 Then
        KeyAscii = 0
        cmbStatusL.SetFocus
    End If
    
End Sub


Private Sub txtredutorlista_LostFocus()
    Dim Valor As Double
    Dim nm_ciclo_operacional As String
    
    If cmbCicloL.Text = "" Then
        Exit Sub
    End If
    If txtprecosugestao.Text = "" Then
        Exit Sub
    End If

    If Trim$(txtcodvenda.Text) <> "" Then
        If Trim$(txtredutorlista) = "" Then
            MsgBox "Informe um percentual redutor !", vbInformation, "A T E N � � O"
            txtredutorlista.SetFocus
            Exit Sub
        End If
    
    txtredutorlista = Format(txtredutorlista, "#0.00")
    
    If CDbl(Trim$(txtredutorlista)) < CDbl("0,00") Or CDbl(Trim$(txtredutorlista)) > CDbl("100,00") Then
        MsgBox "Redutor de Pontos deve estar no intervalo de 0 a 100 !", vbInformation, "A T E N � � O"
        txtredutorlista.SetFocus
        Exit Sub
    End If
    
    Valor = Format(CDbl(txtprecosugestao) * (CDbl(txtredutorlista) / 100), "0.00")
    nm_ciclo_operacional = Right(cmbCicloL, 4) + Left(cmbCicloL, 2)
    
    MSG$ = ""
    txtpontos.Text = FU_BuscaPontuacao(nm_ciclo_operacional, Str(Valor), MSG$)
    
    If MSG$ <> "" Then
        MsgBox MSG$, vbCritical, "A T E N � � O"
        PonteiroMouse vbNormal
        Exit Sub
    End If
End If

txtpontos.SetFocus

End Sub


