VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "ss32x25.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmRelAtivo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relat�rio de Produtos Ativos no Ciclo"
   ClientHeight    =   7740
   ClientLeft      =   1860
   ClientTop       =   3285
   ClientWidth     =   14055
   Icon            =   "frmRelatAtivo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7740
   ScaleWidth      =   14055
   Begin MSComDlg.CommonDialog cD 
      Left            =   11160
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdExporta 
      Caption         =   "Exportar para Excel"
      Height          =   495
      Left            =   11760
      TabIndex        =   5
      Top             =   60
      Width           =   2115
   End
   Begin FPSpread.vaSpread Grid 
      Height          =   6795
      Left            =   120
      TabIndex        =   0
      Top             =   660
      Width           =   13755
      _Version        =   131077
      _ExtentX        =   24262
      _ExtentY        =   11986
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   12
      MaxRows         =   1
      SpreadDesigner  =   "frmRelatAtivo.frx":030A
      UserResize      =   2
      VisibleCols     =   8
      VisibleRows     =   20
   End
   Begin VB.TextBox txtCaminho 
      Height          =   360
      Left            =   8640
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1050
      Visible         =   0   'False
      Width           =   2955
   End
   Begin VB.Label Label3 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6840
      TabIndex        =   4
      Top             =   180
      Width           =   255
   End
   Begin VB.Label lblFim 
      Caption         =   "12/2006"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7260
      TabIndex        =   3
      Top             =   180
      Width           =   1035
   End
   Begin VB.Label lblInicio 
      Caption         =   "12/2006"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5640
      TabIndex        =   2
      Top             =   180
      Width           =   1035
   End
   Begin VB.Label Label1 
      Caption         =   "Ciclo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4920
      TabIndex        =   1
      Top             =   180
      Width           =   615
   End
End
Attribute VB_Name = "frmRelAtivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim EApp As Object
Dim EwkB As Object
Dim EwkS As Object
Dim objExlSht As Object
Dim cSql          As String

Private Sub cmdArquivo_Click()

On Error GoTo fim

    cD.DialogTitle = "Selecione o local e o nome do arquivo para grava��o"
    cD.Filter = "Planilha Excel  (*.xls)|*.XLS"
    cD.DefaultExt = ""
    cD.FilterIndex = 0
    cD.CancelError = True
    'cd.MaxFileSize = 512
    cD.Flags = 0
    cD.InitDir = App.Path
    'cd.filename = strNomeArquivo
    cD.Action = 2
    
    txtCaminho.Text = cD.FileName
    
    Exit Sub
    
fim:

    If Err.Number = 32755 Then
        Screen.MousePointer = vbNormal
        On Error GoTo 0
        Exit Sub
    Else
        MsgBox "Ocorreu o seguinte erro ao informar o arquivo " & Err.Number & " " & Err.Description
    End If

End Sub

Private Sub cmdExporta_Click()

'On Error GoTo fim

    cD.DialogTitle = "Selecione o local e o nome do arquivo para grava��o"
    cD.Filter = "Planilha Excel  (*.xls)|*.XLS"
    cD.DefaultExt = ""
    cD.FilterIndex = 0
    cD.CancelError = True
    'cd.MaxFileSize = 512
    cD.Flags = 0
    cD.InitDir = App.Path
    'cd.filename = strNomeArquivo
'    cD.Action = 2
    
'    txtCaminho.Text = cD.filename
    
'    exporta_texto
    exporta_colunas_excel2
    Exit Sub
    
'fim:
'
'    If Err.Number = 32755 Then
'        Screen.MousePointer = vbNormal
'        On Error GoTo 0
'        Exit Sub
'    Else
'        MsgBox "Ocorreu o seguinte erro ao informar o arquivo " & Err.Number & " " & Err.Description
'    End If
    
End Sub

Public Sub exporta_colunas_excel()
Dim aux As Variant
    If vQuebraLanc = "N" And vQuebraLin = "N" And vQuebraCat = "N" Then
            For i = 4 To Grid.MaxCols
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 3) = Grid.Text
            Next i
            For i = 1 To Grid.MaxRows
                For j = 1 To Grid.MaxCols
                    
                    Grid.Row = i
                    Grid.Col = j + 3
                    If Grid.Text <> "11" And Grid.Text <> "12" And Grid.Text <> "13" Then
                            EApp.Application.Cells(i + 1, j) = Grid.Text
                        ElseIf Grid.Text = 11 Then
                            EApp.Application.Cells(i + 1, 1) = "Lan�amento"
                        ElseIf Grid.Text = 12 Then
                            EApp.Application.Cells(i + 1, 1) = "Ativo"
                        ElseIf Grid.Text = 13 Then
                            EApp.Application.Cells(i + 1, 1) = "Descontinuado"

                    End If
                Next j
            Next i
            
       ElseIf vQuebraLanc = "S" And vQuebraLin = "S" And vQuebraCat = "S" Then
            For i = 1 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i) = Grid.Text
            Next i
            
            For i = 1 To Grid.MaxRows
                    Grid.Row = i
                    Grid.Col = 1
                    
                    While Grid.Text = ""
                    Grid.Row = i
                    Grid.Col = 1
                    i = i + 1
                    Wend
                    
                    If Val(Grid.Text) = 11 Then
                        EApp.Application.Cells(aux + 1, 1) = "Lan�amento"
                    ElseIf Val(Grid.Text) = 12 Then
                        EApp.Application.Cells(aux + 1, 1) = "Ativo"
                    ElseIf Val(Grid.Text) = 13 Then
                        EApp.Application.Cells(aux + 1, 1) = "Descontinuado"
                    End If
                    
                    Grid.Row = i
                    Grid.Col = 2
                    EApp.Application.Cells(i + 1, 2) = Grid.Text
                    
                    Grid.Row = i
                    Grid.Col = 3
                    EApp.Application.Cells(i + 1, 3) = Grid.Text
                
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 3) = Grid.Text
                Next j
            
           Next i
            
       ElseIf vQuebraLanc = "S" And vQuebraLin = "S" And vQuebraCat = "N" Then
            EApp.Application.Cells(1, 1) = "Destaque"
            EApp.Application.Cells(1, 2) = "Linha"
            
            For i = 4 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 1) = Grid.Text
                    
            Next i
            For i = 1 To Grid.MaxRows
                    Grid.Row = i
                    Grid.Col = 1
                    If Grid.Text = 11 Then
                            EApp.Application.Cells(i + 1, 1) = "Lan�amento"
                        ElseIf Grid.Text = 12 Then
                            EApp.Application.Cells(i + 1, 1) = "Ativo"
                        ElseIf Grid.Text = 13 Then
                            EApp.Application.Cells(i + 1, 1) = "Descontinuado"
                    End If
                    Grid.Row = i
                    Grid.Col = 2
                    EApp.Application.Cells(i + 1, 2) = Grid.Text
                    
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 2) = Grid.Text
                Next j
            Next i
            
       ElseIf vQuebraLanc = "N" And vQuebraLin = "N" And vQuebraCat = "S" Then
            
            For i = 3 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 2) = Grid.Text
            Next i
            For i = 1 To Grid.MaxRows
                    
                    Grid.Row = i
                    Grid.Col = 3
                    EApp.Application.Cells(i + 1, 1) = Grid.Text
                
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 1) = Grid.Text
                Next j
            Next i
       ElseIf vQuebraLanc = "S" And vQuebraLin = "N" And vQuebraCat = "N" Then
            EApp.Application.Cells(1, 1) = "Destaque"
            
            For i = 4 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 2) = Grid.Text
            Next i
            For i = 1 To Grid.MaxRows
                    
                    Grid.Row = i
                    Grid.Col = 1
                    If Grid.Text = 11 Then
                            EApp.Application.Cells(i + 1, 1) = "Lan�amento"
                        ElseIf Grid.Text = 12 Then
                            EApp.Application.Cells(i + 1, 1) = "Ativo"
                        ElseIf Grid.Text = 13 Then
                            EApp.Application.Cells(i + 1, 1) = "Descontinuado"
                    End If
                    
                
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 1) = Grid.Text
                Next j
            Next i
            
       ElseIf vQuebraLanc = "N" And vQuebraLin = "S" And vQuebraCat = "S" Then
            EApp.Application.Cells(1, 1) = "Linha"
            EApp.Application.Cells(1, 2) = "Categoria"
            For i = 4 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 1) = Grid.Text
            Next i
            For i = 1 To Grid.MaxRows
                    
                    Grid.Row = i
                    Grid.Col = 2
                    EApp.Application.Cells(i + 1, 1) = Grid.Text
                    
                    Grid.Row = i
                    Grid.Col = 3
                    EApp.Application.Cells(i + 1, 2) = Grid.Text
                
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 2) = Grid.Text
                Next j
            Next i
            
       ElseIf vQuebraLanc = "S" And vQuebraLin = "N" And vQuebraCat = "S" Then
            EApp.Application.Cells(1, 1) = "Destaque"
            
            For i = 3 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 1) = Grid.Text
            Next i
            For i = 1 To Grid.MaxRows
                    Grid.Row = i
                    Grid.Col = 1
                    If Grid.Text = 11 Then
                            EApp.Application.Cells(i + 1, 1) = "Lan�amento"
                        ElseIf Grid.Text = 12 Then
                            EApp.Application.Cells(i + 1, 1) = "Ativo"
                        ElseIf Grid.Text = 13 Then
                            EApp.Application.Cells(i + 1, 1) = "Descontinuado"
                    End If
                    
                    Grid.Row = i
                    Grid.Col = 3
                    EApp.Application.Cells(i + 1, 2) = Grid.Text
                
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 2) = Grid.Text
                Next j
            Next i
            
       ElseIf vQuebraLanc = "N" And vQuebraLin = "S" And vQuebraCat = "N" Then
            
            EApp.Application.Cells(1, 1) = "Linha"
            
            For i = 4 To Grid.MaxCols
            
                    Grid.Row = 0
                    Grid.Col = i
                    EApp.Application.Cells(1, i - 2) = Grid.Text
            Next i
            For i = 1 To Grid.MaxRows

                    Grid.Row = i
                    Grid.Col = 2
                    EApp.Application.Cells(i + 1, 1) = Grid.Text
                
                For j = 1 To Grid.MaxCols
                    Grid.Row = i
                    Grid.Col = j + 3
                    EApp.Application.Cells(i + 1, j + 1) = Grid.Text
                Next j
            Next i
        
    End If

End Sub

Public Sub exporta_colunas_excel2()

    Dim strNomeArquivo  As String
    Dim X               As Double
    Dim Y               As Double
    Dim vConteudoCelula As String
    Dim vLinha          As Double
    Dim strBody         As String
    
On Error GoTo trata_erro
    
'    If Len(Trim(txtCaminho.Text)) < 0 Then
'        MsgBox "Nome de arquivo inv�lido", vbInformation
'        cmdArquivo.SetFocus
'        Exit Sub
'    End If
    
    Screen.MousePointer = vbHourglass
    
    DoEvents
'    strNomeArquivo = txtCaminho.Text 'Tira_Espaco(Tira_Ponto(txtCaminho.Text)) & ".xls"
        
    If ExcelSheet Is Nothing Then
        Set ExcelSheet = CreateObject("Excel.Sheet")
    Else
        Set ExcelSheet = Nothing
        Set ExcelSheet = CreateObject("Excel.Sheet")
    End If
    If ExcelSheet.Application.Visible = False Then
        ExcelSheet.Application.Visible = True
    End If
    
    vLinha = 0
        
    If frmRelProdAtivo.chkLancam Then
        If frmRelProdAtivo.chkDescrCAN Then
            ExcelSheet.ActiveSheet.Columns(8).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(10).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(16).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(18).NumberFormat = "@"
        Else
            ExcelSheet.ActiveSheet.Columns(7).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(9).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(15).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(17).NumberFormat = "@"
        End If
    Else
        If frmRelProdAtivo.chkDescrCAN Then
            ExcelSheet.ActiveSheet.Columns(7).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(9).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(15).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(17).NumberFormat = "@"
        Else
            ExcelSheet.ActiveSheet.Columns(6).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(8).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(14).NumberFormat = "@"
            ExcelSheet.ActiveSheet.Columns(16).NumberFormat = "@"
        End If
    End If
        
    For X = 0 To Grid.MaxRows
                
        Grid.Row = X
        Grid.Col = 4
        vConteudoCelula = Grid.Text
        
        If Len(Trim(vConteudoCelula)) > 0 Then
        
            vLinha = vLinha + 1
    
            strBody = ""
            
            If frmRelProdAtivo.chkLancam Then
                For Y = 1 To Grid.MaxCols
        
                    Grid.Col = Y
                    vConteudoCelula = Grid.Text
                        
                    If Y = 1 Then
                        If Val(vConteudoCelula) = 11 Then
                            vConteudoCelula = "LAN�AMENTO"
                        ElseIf Val(vConteudoCelula) = 12 Then
                            vConteudoCelula = "ATIVO"
                        ElseIf Val(vConteudoCelula) = 13 Then
                            vConteudoCelula = "DESCONTINUADO"
                        End If
                    End If
                    
                    ExcelSheet.ActiveSheet.Cells(vLinha, Y).Value = vConteudoCelula
                    
                Next Y
            Else
                For Y = 2 To Grid.MaxCols
        
                    Grid.Col = Y
                    vConteudoCelula = Grid.Text
                        
                    ExcelSheet.ActiveSheet.Cells(vLinha, Y - 1).Value = vConteudoCelula
                    
                Next Y
            End If
        End If
    Next X
    
'    ExcelSheet.SaveAs strNomeArquivo
    
'    ExcelSheet.Application.Quit
'    Set ExcelSheet = Nothing
    
    Screen.MousePointer = vbNormal
            
    MsgBox "Arquivo Exportado com sucesso.", vbInformation, App.Title
        
    Exit Sub
    
trata_erro:
    
    Screen.MousePointer = vbNormal
    
    MsgBox "Ocorreu o erro " & Err.Number & " - " & Err.Description & " ao exportar para o excell!!"

End Sub


Public Sub exporta_texto()

    Dim strNomeArquivo  As String
    Dim X               As Double
    Dim Y               As Double
    Dim vConteudoCelula As String
    Dim vLinha          As Double
    Dim ExcelApp        As Object
    Dim ExcelSheet      As Object
    Dim strBody         As String
        
On Error GoTo trata_erro
    
    If Len(Trim(txtCaminho.Text)) < 0 Then
        MsgBox "Nome de arquivo inv�lido", vbInformation
        cmdArquivo.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    DoEvents
    strNomeArquivo = txtCaminho.Text
        
    Open txtCaminho.Text For Output As #1

    'ExcelSheet.Application.Visible = True
    
    vLinha = 0
        
    For X = 0 To Grid.MaxRows
                
        Grid.Row = X
        Grid.Col = 4
        vConteudoCelula = Grid.Text
        
        If Len(Trim(vConteudoCelula)) > 0 Then
        
            vLinha = vLinha + 1
            
            strBody = ""
   
            For Y = 1 To Grid.MaxCols
        
                Grid.Col = Y
                vConteudoCelula = Grid.Text
                        
                If Y = 1 Then
                    If Val(vConteudoCelula) = 11 Then
                        vConteudoCelula = "LAN�AMENTO"
                    ElseIf Val(vConteudoCelula) = 12 Then
                        vConteudoCelula = "ATIVO"
                    ElseIf Val(vConteudoCelula) = 13 Then
                        vConteudoCelula = "DESCONTINUADO"
                    End If
                End If
                
                If frmRelProdAtivo.chkDescrCAN Then
                    If Y = 8 Or Y = 10 Or Y = 16 Or Y = 18 Then
                        vConteudoCelula = "'" & vConteudoCelula
                    End If
                Else
                    If Y = 7 Or Y = 9 Or Y = 15 Or Y = 17 Then
                        vConteudoCelula = "'" & vConteudoCelula
                    End If
                End If

                strBody = strBody & vConteudoCelula & vbTab

            Next Y

            Print #1, strBody
                    
        End If
        
    Next X
    
    Close #1
            
    Screen.MousePointer = vbNormal
            
    MsgBox "Arquivo Exportado com sucesso.", vbInformation, App.Title
            
    ShellExecute 1, _
                "open", _
                strNomeArquivo, _
                vbNullString, vbNullString, _
                SW_SHOW
    
        
    Exit Sub
    
trata_erro:
    
    Screen.MousePointer = vbNormal
    
    MsgBox "Ocorreu o erro " & Err.Number & " - " & Err.Description & " ao exportar para o excell!!"

End Sub

