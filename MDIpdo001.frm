VERSION 5.00
Object = "{143734FC-7C3E-42F0-AAE6-11307D6D7F9A}#1.0#0"; "CorpSyb2.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.MDIForm MDIpdo001 
   BackColor       =   &H8000000C&
   Caption         =   "Cadastro de Produtos e Pre�os - Natura"
   ClientHeight    =   8940
   ClientLeft      =   1155
   ClientTop       =   4545
   ClientWidth     =   14370
   Icon            =   "MDIpdo001.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar barMDI 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14370
      _ExtentX        =   25347
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   20
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Abrang�ncia/Vigencia - Comercial"
            Object.Tag             =   ""
            ImageIndex      =   11
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            ImageIndex      =   12
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Produtos Ativos no Ciclo"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Abrang�ncia/vig�ncia PCL"
            Object.Tag             =   ""
            ImageIndex      =   14
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Produtos Pendentes - PCL"
            Object.Tag             =   ""
            ImageIndex      =   15
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Descri��o Capta/CAN"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   4
            Object.Width           =   500
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Carga de Pre�os"
            Object.Tag             =   ""
            ImageIndex      =   21
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button12 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Manuten��o de Pre�o de Refer�ncia"
            Object.Tag             =   ""
            ImageIndex      =   24
         EndProperty
         BeginProperty Button13 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Manuten��o de Pontua��o"
            Object.Tag             =   ""
            ImageIndex      =   19
         EndProperty
         BeginProperty Button14 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Relaciona Lista com Estrut. Comercial"
            Object.Tag             =   ""
            ImageIndex      =   20
         EndProperty
         BeginProperty Button15 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Exporta Pre�os para Excel"
            Object.Tag             =   ""
            ImageIndex      =   23
         EndProperty
         BeginProperty Button16 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Importa Pre�os de planilha Excel"
            Object.Tag             =   ""
            ImageIndex      =   22
         EndProperty
         BeginProperty Button17 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Relaciona Produtos X Meio de comunica��o"
            Object.Tag             =   ""
            ImageIndex      =   18
         EndProperty
         BeginProperty Button18 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Gera Relat�rio Base de Pre�os"
            Object.Tag             =   ""
            ImageIndex      =   25
         EndProperty
         BeginProperty Button19 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   4
            Object.Width           =   1000
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button20 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
      MouseIcon       =   "MDIpdo001.frx":2832
      Begin NaturaCorpSyb1.CorpSyb1 CorpNat 
         Height          =   600
         Left            =   13650
         TabIndex        =   1
         Top             =   150
         Visible         =   0   'False
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   1058
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   1140
      Top             =   2880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   25
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":2B4C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":2E66
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":3180
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":349A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":37B4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":3ACE
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":3DE8
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":4102
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":441C
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":4736
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":64EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":6D3E
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":7D0C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":8AEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":8E04
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":9766
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":9940
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":9B1A
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":9E34
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":A14E
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":A468
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":A782
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":AA9C
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":ADB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIpdo001.frx":B0D0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuProdutos 
      Caption         =   "&Produtos"
      Begin VB.Menu mnuManutencao 
         Caption         =   "&Manuten��es"
         Begin VB.Menu mnuVigenciaProduto 
            Caption         =   "Abrang/Vig�ncia &Comercial"
         End
         Begin VB.Menu mnuBarra1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuVigenciaMaterial 
            Caption         =   "Abrang/Vig�ncia &PCL"
         End
         Begin VB.Menu mnuBarra2 
            Caption         =   "-"
         End
         Begin VB.Menu mnuDescricaoCAN 
            Caption         =   "&Descri��o Capta/CAN"
         End
      End
      Begin VB.Menu mnuConsultas 
         Caption         =   "&Consultas"
         Begin VB.Menu mnuProdAtivo 
            Caption         =   "&Produtos Ativos no Ciclo"
         End
         Begin VB.Menu mnubarra3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPendencia 
            Caption         =   "P&end�ncias PCL"
         End
      End
   End
   Begin VB.Menu mnuCadastros 
      Caption         =   "C&adastros"
      Begin VB.Menu mnuLinha 
         Caption         =   "&Linha"
      End
      Begin VB.Menu mnuCategoria 
         Caption         =   "&Categoria"
      End
      Begin VB.Menu mnuMeioComunic 
         Caption         =   "&Meio de Comunica��o"
      End
      Begin VB.Menu mnuEmail 
         Caption         =   "&Email Pendencias"
      End
   End
   Begin VB.Menu mnuPreco 
      Caption         =   "P&re�os"
      Begin VB.Menu mnuCargaPrc 
         Caption         =   "Carga Pre�os Ciclo Anterior"
      End
      Begin VB.Menu mnucrialista 
         Caption         =   "Criar Nova Lista de Pre�os"
      End
      Begin VB.Menu mnubarra5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrcRef 
         Caption         =   "Manuten��o de Pre�o de Refer�ncia"
      End
      Begin VB.Menu mnuImpExcel 
         Caption         =   "Importar Lista do Excel"
      End
      Begin VB.Menu MnuExpExcel 
         Caption         =   "Exportar Lista para o Excel"
      End
      Begin VB.Menu mnubarra4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRelListaEstrut 
         Caption         =   "Relaciona Lista X Estrut. Comercial"
      End
      Begin VB.Menu mnuReajLista 
         Caption         =   "Reajuste individual da Lista de Pre�os"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu mnubarra6 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTabPontos 
         Caption         =   "Manuten��o Tabela de pontos"
      End
      Begin VB.Menu mnuBarra8 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRelPrdSPreco 
         Caption         =   "Rela��o de Produtos Vigentes sem pre�o"
      End
      Begin VB.Menu mnubarra7 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuBasePrc 
         Caption         =   "Gerar Base de pre�os"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuComunic 
      Caption         =   "C&omunica��o"
      Begin VB.Menu mnuRelacProdMeio 
         Caption         =   "Relaciona Produto X Meio Comunica��o"
      End
      Begin VB.Menu mnuRelatBasePrc 
         Caption         =   "Relat�rio Base de Pre�os"
      End
   End
   Begin VB.Menu mnuLogs 
      Caption         =   "Lo&gs"
      Begin VB.Menu mnuLogTransacao 
         Caption         =   "Log de Transa��o"
      End
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "MDIpdo001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub BloqueiaMenu()

    MDIpdo001.barMDI.Enabled = False
    MDIpdo001.mnuProdutos.Enabled = False
    MDIpdo001.mnuPreco.Enabled = False
    MDIpdo001.mnuComunic.Enabled = False
    MDIpdo001.mnuSair.Enabled = False

End Sub

Public Sub DesbloqueiaMenu()
    
    MDIpdo001.barMDI.Enabled = True
    MDIpdo001.mnuProdutos.Enabled = True
    MDIpdo001.mnuComunic.Enabled = True
    MDIpdo001.mnuPreco.Enabled = True
    MDIpdo001.mnuSair.Enabled = True

End Sub

Private Sub barMDI_ButtonClick(ByVal Button As ComctlLib.Button)
    
    Select Case Button.Index
        Case 1
            mnuVigenciaProduto_Click
        Case 3
            mnuProdAtivo_Click
        Case 5
            mnuVigenciaMaterial_Click
        Case 6
            mnuPendencia_Click
        'Manuten��o da descri��o CAN
        Case 8
            mnudescricaoCAN_Click
        'Carga de pre�os/listas do ciclo anterior
        Case 10
            mnuCargaPrc_Click
        'Manuten��o Pre�o de Refer�ncia
        Case 12
            mnuPrcRef_Click
        Case 13
            mnuTabPontos_Click
        Case 14
            mnuRelListaEstrut_Click
        'Exp. base de pre�os para Excel
        Case 15
            MnuExpExcel_Click
        'Imp. base de pre�os de planilha Excel
        Case 16
            mnuImpExcel_Click
        'Relaciona produtos X Meio de comunica��o
        Case 17
            mnuRelacProdMeio_Click
        'Gera base de pre�os
        Case 18
            mnuRelatBasePrc_Click
        'Sair
        Case 20
            mnuSair_Click
    End Select
  
End Sub

Private Sub MDIForm_Load()

  'Configura apresenta��o da tela -------------------------------------------
  MDIpdo001.WindowState = 2
 
  'Segmentos de canal que ter�o relacionamento de venda
  'N�o existe mais o segmento 3
   segmentos = Array(1, 2, 3, 5, 4)
  
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)

   FechaBanco

End Sub

Private Sub mnuCargaPrc_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO010", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO010", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(5) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    
    Dim CodUsuario As String
    If VerificaBloqueioFormulario(5, 0, 0, CodUsuario) Then
        ExibeMensagemBloqueio ObtemDescricaoFormulario(5), "", "", "", "", CodUsuario
        Exit Sub
    End If
    'SE 6346 - Fim
    BloqueiaMenu
    'SE 6346 - In�cio
    If Not BloqueiaRegistro(5, 0, 0) Then
        MsgBox "Falha ao tentar bloquear registro do formul�rio.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
    frmCarrPrRef.Show
    
End Sub

Private Sub mnuCategoria_Click()
    
    'ALTERACAO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO007", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERACAO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO007", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If

'    BloqueiaMenu
    frmConsultaCadCategoria.Show

End Sub

Private Sub mnucrialista_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'SE 6346 - Controle de acesso
    'Yamamoto - 28/08/2010
    'If Not ChecaPermissao("frmPDO019", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    'SE 6346 - Controle de acesso
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO019", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    'SE 6346 - Controle de acesso
    
    BloqueiaMenu
    FrmCriaLista.Show
    
End Sub

Private Sub mnudescricaoCAN_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO001", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    If Not bVerificaFuncaoPermissaoLogin("frmPDO001", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If

    BloqueiaMenu
    frmDescricaoCAN.Show

End Sub

Private Sub mnuEmail_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
     'If Not ChecaPermissao("frmPDO009", "M") Then
        'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        'Exit Sub
     'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO009", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If

'    BloqueiaMenu
    frmConsultaCadEmail.Show

End Sub

Private Sub MnuExpExcel_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO013", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO013", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmExpExcel.Show

End Sub

Private Sub mnuImpExcel_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO012", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO012", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmImpExcel.Show

End Sub

Private Sub mnuLinha_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO006", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO006", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If

'    BloqueiaMenu
    frmConsultaCadLinha.Show

End Sub

Private Sub mnuLogTransacao_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO020", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    If Not bVerificaFuncaoPermissaoLogin("frmPDO020", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    frmLogAlteracao.Show
End Sub

Private Sub mnuMeioComunic_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO008", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO008", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
'   BloqueiaMenu
    frmConsultaMeioCmc.Show


End Sub

Private Sub mnuPendencia_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO004", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO004", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmPendenciaPCL.Show

End Sub

Private Sub mnuPrcRef_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO011", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    'SE 6346 - In�cio
    'If Not DesbloqueiaRegistro(6) Then
    '    MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
    '    Exit Sub
    'End If
    
    'Dim CodUsuario As String
    'If VerificaBloqueioFormulario(6, 0, 0, CodUsuario) Then
    '    ExibeMensagemBloqueio ObtemDescricaoFormulario(6), "", "", "", "", CodUsuario
    '    Exit Sub
    'End If
    'SE 6346 - Fim

    BloqueiaMenu
    
    'SE 6346 - In�cio
    'If Not BloqueiaRegistro(6, 0, 0) Then
    '    MsgBox "Falha ao tentar bloquear registro do formul�rio.", vbCritical, "Erro"
    '    Exit Sub
    'End If
    'SE 6346 - Fim
    frmPrecoRef.Show


End Sub

Private Sub mnuProdAtivo_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
      'If Not ChecaPermissao("frmPDO005", "M") Then
         'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
         'Exit Sub
      'End If

    If Not bVerificaFuncaoPermissaoLogin("frmPDO005", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmRelProdAtivo.Show

End Sub

Private Sub mnuRelacProdMeio_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO016", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
     If Not bVerificaFuncaoPermissaoLogin("frmPDO016", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
     Exit Sub
      
    End If

    BloqueiaMenu
    frmRelPrdMeioCmc.Show

End Sub

Private Sub mnuRelatBasePrc_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
     'If Not ChecaPermissao("frmPDO017", "C") Then
        'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        'Exit Sub
     'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO017", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmParmRelBasePrc.Show
    
End Sub

Private Sub mnuRelListaEstrut_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO014", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO014", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmListaEstrut.Show
    
End Sub

Private Sub mnuRelPrdSPreco_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO018", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO018", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmProdSemLista.Show

End Sub

Private Sub mnuSair_Click()

    Call FechaBanco
    End
    
End Sub

Private Sub mnuTabPontos_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO015", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
        
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO015", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    BloqueiaMenu
    frmCNPontuacao.Show
    
End Sub

Private Sub mnuVigenciaMaterial_Click()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO003", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO003", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmVigenciaPCL.Show
    
End Sub

Private Sub mnuVigenciaProduto_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO002", "C") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       'Exit Sub
    'End If

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO002", "C") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
       Exit Sub
    End If
    
    BloqueiaMenu
    frmVigenciaComercial.Show
    
End Sub
