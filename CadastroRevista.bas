Attribute VB_Name = "CadastroRevista"
Global ListaDadosImportacaoExcel() As Variant

Option Explicit

Const COD_VENDA = 1
Const DESCRICAO_PRODUTO = 2
Const COD_MATERIAL = 3
Const tipo_estrutura_comercial = 4
Const DESCRICAO_TIPO_ESTRUTURA_COMERCIAL = 5
Const cod_estrutura_comercial = 6
Const DESCRICAO_ESTRUTURA_COMERCIAL = 7
Const CICLO_INICIO = 8
Const DATA_INICIO = 9
Const CICLO_FIM = 10
Const DATA_FIM = 11
Const PRIORIDADE_PCL = 12
Const STATUS_PCL = 13
Const STATUS_MERCADOLOGICO = 12
Const operacao = 15
Const AGRUPADOR_RELACAO = 14

'SE7107 - Yamamoto
Dim varRE                   As Integer
Dim varGV                   As Integer
Dim varSET                  As Integer
Dim vCtrlPCL                As Boolean
Dim vNomeEstruturaComercial As String
'SE7107

Dim vCiclIni   As Variant
Dim vDataIni   As Variant
Dim vCiclFin   As Variant
Dim vDataFin   As Variant


Public Function ImportarCadastroRevista(ByRef Grid As vaSpread, tipoImportacao As Integer, cod_estrutura As Variant, OrigemDados As Integer, tipo_estrutura As Variant) As Boolean
'TipoImportacao = 1 - Importacao Agrupada
'TipoImportacao = 2 - Importacao FULL

'OrigemDados = 1 - PCL
'OrigemDados = 2 - Mercadologico

On Error GoTo ErroGeral:

'variaveis de metodo
Dim linhaVazia As Boolean
Dim Linha As Double
Dim vLinhaDuplicada As Double
'fim variaveis de metodo
Linha = 1
'Grid.MaxRows = 0
linhaVazia = False

Dim indiceColunaStatus As Integer
If OrigemDados = 1 Then
    indiceColunaStatus = STATUS_PCL
ElseIf OrigemDados = 2 Then
    indiceColunaStatus = STATUS_MERCADOLOGICO
End If

Do While linhaVazia = False

    If Trim(ListaDadosImportacaoExcel(Linha, COD_VENDA)) = "" Then
        linhaVazia = True
    Else
        If Not (UCase(ListaDadosImportacaoExcel(Linha, indiceColunaStatus)) <> "ATIVA") Then
            If AbrangenciaJaAtivaCom(CStr(ListaDadosImportacaoExcel(Linha, tipo_estrutura_comercial)), CStr(ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)), vLinhaDuplicada) Then
                AlterarAbrangenciaCom vLinhaDuplicada, Linha
            Else
                AdicionarLinhaGrid Grid, Linha, OrigemDados, 17
            End If
        End If
    End If
    Linha = Linha + 1
Loop

Grid.Enabled = True
Grid.Visible = True
ImportarCadastroRevista = True
Exit Function
   
ErroConversao:
    Grid.MaxRows = 0
    MsgBox "Erro ao validar dados: Tipos Incompat�veis, vefirique os dados e tente novamente"
    ImportarCadastroRevista = False
    Exit Function

ErroGeral:
    Grid.MaxRows = 0
    MsgBox "Erro Geral: " & Err.Description & ". Source: " & Err.Source
    ImportarCadastroRevista = False
    Exit Function
End Function


Public Function ImportarCadastroRevistaPCL(ByRef Grid As vaSpread, tipoImportacao As Integer, cod_estrutura As Variant, OrigemDados As Integer, tipo_estrutura As Variant) As Boolean
    'TipoImportacao = 1 - Importacao Agrupada
    'TipoImportacao = 2 - Importacao FULL
    
    'OrigemDados = 1 - PCL
    'OrigemDados = 2 - Mercadologico
    
    On Error GoTo ErroGeral:
    
    'variaveis de metodo
    Dim linhaVazia As Boolean
    Dim Linha As Double



    'SE 7107 - Yamamoto 13/09/2011
    Dim vTip               As String
    Dim vCod               As String
    Dim vPos               As Integer
    Dim vLin               As Integer
    Dim vOper              As Variant
    Dim vStatus            As Variant
    Dim vNumLinhaDuplicada As Double

    'fim variaveis de metodo
    Linha = 1
    'Grid.MaxRows = 0
    linhaVazia = False

    Dim indiceColunaStatus As Integer
    If OrigemDados = 1 Then
        indiceColunaStatus = STATUS_PCL
    ElseIf OrigemDados = 2 Then
        indiceColunaStatus = STATUS_MERCADOLOGICO
    End If
    ImportarCadastroRevistaPCL = False
    Screen.MousePointer = vbArrowHourglass

Do While linhaVazia = False

    If Trim(ListaDadosImportacaoExcel(Linha, COD_VENDA)) = "" Then
        linhaVazia = True
    Else
        If Not (UCase(ListaDadosImportacaoExcel(Linha, indiceColunaStatus)) <> "ATIVA") Then
            If tipoImportacao = 2 Then
                vTip = ListaDadosImportacaoExcel(Linha, tipo_estrutura_comercial)
                vCod = ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
                varRE = 0
                varGV = 0
                varSET = 0
                ObterDadosEstruturaComercial vTip, vCod, varRE, varGV, varSET, vNomeEstruturaComercial
                If AbrangenciaJaAtivaPCL(vTip, vCod, Linha, vNumLinhaDuplicada) Then
                    AlterarAbrangenciaPCL vNumLinhaDuplicada, Linha
                Else
                    Grid.MaxRows = Grid.MaxRows + 1
                    Grid.Row = Grid.MaxRows
                    Grid.SetText 1, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, COD_MATERIAL)
                                 
                    Grid.SetText 2, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, DESCRICAO_PRODUTO)
                     
                    Grid.SetText 3, Grid.MaxRows, TipoEstrutComercial(vTip) & " " & ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & vNomeEstruturaComercial 'ListaDadosImportacaoExcel(Linha, DESCRICAO_ESTRUTURA_COMERCIAL)
                               
                    Grid.SetText 4, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_INICIO)
                              
                    Grid.SetText 5, Grid.MaxRows, CStr(ListaDadosImportacaoExcel(Linha, DATA_INICIO))
                             
                    Grid.SetText 6, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_FIM)
                    
                    Grid.SetText 7, Grid.MaxRows, CStr(ListaDadosImportacaoExcel(Linha, DATA_FIM))
                               
                    Grid.SetText 8, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, PRIORIDADE_PCL)
                    
                    Grid.SetText 9, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, indiceColunaStatus)
                    
                    Grid.SetText 13, Grid.MaxRows, "I" 'CStr(ListaDadosImportacaoExcel(Linha, operacao))
                    
                    'Grid.SetText 7, Grid.MaxRows, ListaDadosImportacaoExcel(linha, AGRUPADOR_RELACAO)
                    
                    If frmVigenciaPCL.chkContrLog.Value = 1 Then
                        frmVigenciaPCL.spdAVIG.MaxRows = frmVigenciaPCL.spdAVIG.MaxRows + 1
                        vLin = frmVigenciaPCL.spdAVIG.MaxRows

                       'abrangencia
                        If Val(vTip) = 0 Then
                            frmVigenciaPCL.spdAVIG.SetText 1, vLin, vCod & " - " & Trim(ListaDadosImportacaoExcel(Linha, DESCRICAO_ESTRUTURA_COMERCIAL))
                        Else
                            frmVigenciaPCL.spdAVIG.SetText 1, vLin, Trim(ListaDadosImportacaoExcel(Linha, DESCRICAO_ESTRUTURA_COMERCIAL))
                        End If
                       'ciclo inic
                       frmVigenciaPCL.spdAVIG.SetText 2, vLin, ListaDadosImportacaoExcel(Linha, CICLO_INICIO)
                       'data inicio
                       frmVigenciaPCL.spdAVIG.SetText 3, vLin, ListaDadosImportacaoExcel(Linha, DATA_INICIO)
                       'ciclo fim
                       frmVigenciaPCL.spdAVIG.SetText 4, vLin, ListaDadosImportacaoExcel(Linha, CICLO_FIM)
                       'data final
                       frmVigenciaPCL.spdAVIG.SetText 5, vLin, ListaDadosImportacaoExcel(Linha, DATA_FIM)
                       'tipo estrutura comercial
                       frmVigenciaPCL.spdAVIG.SetText 8, vLin, vTip
                       'cod estrutura comercial
                       frmVigenciaPCL.spdAVIG.SetText 9, vLin, vCod
                       'regiao estrategia
                       frmVigenciaPCL.spdAVIG.SetText 10, vLin, Val(varRE)
                       'gerencia vendas
                       frmVigenciaPCL.spdAVIG.SetText 11, vLin, Val(varGV)
                       'setor
                       frmVigenciaPCL.spdAVIG.SetText 12, vLin, Val(varSET)
                       '
                    End If

                    
                End If
            ElseIf tipoImportacao = 1 Then
                
                If (Grid.MaxRows > 0) Then
                    Dim CODIGO As Variant
                    Grid.GetText 9, Grid.MaxRows, CODIGO

                    vTip = ListaDadosImportacaoExcel(Linha, tipo_estrutura_comercial)
                    vCod = ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
                    
                    If AbrangenciaJaAtivaPCL(vTip, vCod, Linha, vNumLinhaDuplicada) Then
                        AlterarAbrangenciaPCL vNumLinhaDuplicada, Linha
                    Else
                        Grid.MaxRows = Grid.MaxRows + 1
                        Grid.Row = Grid.MaxRows
                        'AdicionarLInhaGrid Grid, Linha, OrigemDados, 13
                        Grid.SetText 1, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, COD_MATERIAL)
                                     
                        Grid.SetText 2, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, DESCRICAO_PRODUTO)
                         
                        Grid.SetText 3, Grid.MaxRows, TipoEstrutComercial(vTip) & " " & ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & ListaDadosImportacaoExcel(Linha, DESCRICAO_ESTRUTURA_COMERCIAL)
                                   
                        Grid.SetText 4, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_INICIO)
                                  
                        Grid.SetText 5, Grid.MaxRows, CStr(ListaDadosImportacaoExcel(Linha, DATA_INICIO))
                                 
                        Grid.SetText 6, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_FIM)
                        
                        Grid.SetText 7, Grid.MaxRows, CStr(ListaDadosImportacaoExcel(Linha, DATA_FIM))
                                   
                        Grid.SetText 8, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, PRIORIDADE_PCL)
                        
                        Grid.SetText 9, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, indiceColunaStatus)
                        
                        Grid.SetText 13, Grid.MaxRows, "I" 'CStr(ListaDadosImportacaoExcel(Linha, operacao))
                    End If

                Else
                    vTip = ListaDadosImportacaoExcel(Linha, tipo_estrutura_comercial)
                    vCod = ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
                    
                    If AbrangenciaJaAtivaPCL(vTip, vCod, Linha, vNumLinhaDuplicada) Then
                        AlterarAbrangenciaPCL vNumLinhaDuplicada, Linha
                    Else
                        Grid.MaxRows = Grid.MaxRows + 1
                        Grid.Row = Grid.MaxRows
                        Grid.SetText 1, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, COD_MATERIAL)
                                         
                        Grid.SetText 2, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, DESCRICAO_PRODUTO)
                             
                        Grid.SetText 3, Grid.MaxRows, TipoEstrutComercial(vTip) & " " & ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & ListaDadosImportacaoExcel(Linha, DESCRICAO_ESTRUTURA_COMERCIAL)
                                       
                        Grid.SetText 4, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_INICIO)
                                      
                        Grid.SetText 5, Grid.MaxRows, CStr(ListaDadosImportacaoExcel(Linha, DATA_INICIO))
                                     
                        Grid.SetText 6, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_FIM)
                            
                        Grid.SetText 7, Grid.MaxRows, CStr(ListaDadosImportacaoExcel(Linha, DATA_FIM))
                                       
                        Grid.SetText 8, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, PRIORIDADE_PCL)
                            
                        Grid.SetText 9, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, indiceColunaStatus)
                            
                        Grid.SetText 13, Grid.MaxRows, "I" 'CStr(ListaDadosImportacaoExcel(Linha, operacao))
                    End If

                End If
            End If
        End If
    End If
    Linha = Linha + 1
Loop

Grid.Enabled = True
Grid.Visible = True
ImportarCadastroRevistaPCL = True

     Screen.MousePointer = vbDefault
     
Exit Function
   
ErroConversao:
    Grid.MaxRows = 0
    MsgBox "Erro ao validar dados: Tipos Incompat�veis, vefirique os dados e tente novamente"
    ImportarCadastroRevistaPCL = False
    Exit Function

ErroGeral:

    Grid.MaxRows = 0
    MsgBox "Erro Geral: " & Err.Description & ". Source: " & Err.Source
    ImportarCadastroRevistaPCL = False
    Exit Function
    
    Screen.MousePointer = vbDefault
End Function


Public Sub ExportarDadosCadastroRevista(TipoExportacao As Integer, caminhoExcel As String)

Dim aplicacaoExcel As New Excel.Application
Dim workBookExcel As Excel.Workbook
Dim subtrador As Integer

If TipoExportacao = 1 Then
    subtrador = 0
ElseIf TipoExportacao = 2 Then
    subtrador = 1
End If
Dim limite As Long
limite = 1
Set aplicacaoExcel = CreateObject("Excel.Application")
Set workBookExcel = aplicacaoExcel.Workbooks.Add

workBookExcel.Sheets(1).Select


'Criando Cabe�alhos
    workBookExcel.Sheets(1).Cells(limite, COD_VENDA).Value = "C�d. Venda"
    workBookExcel.Sheets(1).Columns(COD_VENDA).AutoFit
    workBookExcel.Sheets(1).Cells(limite, tipo_estrutura_comercial - subtrador).Value = "Tipo Estrutura Comercial"
    workBookExcel.Sheets(1).Columns(tipo_estrutura_comercial - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL - subtrador).Value = "Descri��o do Tipo de Estrutura Comercial"
    workBookExcel.Sheets(1).Columns(DESCRICAO_TIPO_ESTRUTURA_COMERCIAL - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, cod_estrutura_comercial - subtrador).Value = "Estrutura Comercial"
    workBookExcel.Sheets(1).Columns(cod_estrutura_comercial - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, DESCRICAO_ESTRUTURA_COMERCIAL - subtrador).Value = "Descri��o da Estrutura Comercial"
    workBookExcel.Sheets(1).Columns(DESCRICAO_ESTRUTURA_COMERCIAL - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, CICLO_INICIO - subtrador).Value = "Ciclo Inicio"
    workBookExcel.Sheets(1).Columns(CICLO_INICIO - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, DATA_INICIO - subtrador).Value = "Data Inicio"
    workBookExcel.Sheets(1).Columns(DATA_INICIO - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, CICLO_FIM - subtrador).Value = "Ciclo Fim"
    workBookExcel.Sheets(1).Columns(CICLO_FIM - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, DATA_FIM - subtrador).Value = "Data Fim"
    workBookExcel.Sheets(1).Columns(DATA_FIM - subtrador).AutoFit
    workBookExcel.Sheets(1).Cells(limite, DESCRICAO_PRODUTO).Value = "Descri��o Produto"
    workBookExcel.Sheets(1).Columns(DESCRICAO_PRODUTO).AutoFit
    If TipoExportacao = 2 Then
        workBookExcel.Sheets(1).Cells(limite, STATUS_MERCADOLOGICO - subtrador).Value = "Status"
    ElseIf TipoExportacao = 1 Then
        workBookExcel.Sheets(1).Cells(limite, PRIORIDADE_PCL - subtrador).Value = "Prioridade"
        workBookExcel.Sheets(1).Cells(limite, STATUS_PCL - subtrador).Value = "Status"

        workBookExcel.Sheets(1).Cells(limite, COD_MATERIAL - subtrador).Value = "C�d. Material"
        workBookExcel.Sheets(1).Columns(COD_MATERIAL - subtrador).AutoFit
        workBookExcel.Sheets(1).Cells(limite, (AGRUPADOR_RELACAO) - subtrador).Value = "Agrupador"
        workBookExcel.Sheets(1).Columns(AGRUPADOR_RELACAO - subtrador).AutoFit
    End If
    
'Fim Cria��o dos Cabe�alhos

    Do While ((UBound(ListaDadosImportacaoExcel, 1)) + 1) > limite
        
        limite = limite + 1
        If ListaDadosImportacaoExcel(limite - 2, COD_VENDA) <> "" Then
            workBookExcel.Sheets(1).Cells(limite, COD_VENDA).Value = ListaDadosImportacaoExcel(limite - 2, COD_VENDA)
            
            If TipoExportacao = 2 Then
                workBookExcel.Sheets(1).Cells(limite, STATUS_MERCADOLOGICO - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, STATUS_MERCADOLOGICO)
                
            ElseIf TipoExportacao = 1 Then
                workBookExcel.Sheets(1).Cells(limite, COD_MATERIAL - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, COD_MATERIAL)
                workBookExcel.Sheets(1).Cells(limite, PRIORIDADE_PCL - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, PRIORIDADE_PCL)
                workBookExcel.Sheets(1).Cells(limite, STATUS_PCL - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, STATUS_PCL)
                workBookExcel.Sheets(1).Cells(limite, AGRUPADOR_RELACAO - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, AGRUPADOR_RELACAO)
            End If
            workBookExcel.Sheets(1).Cells(limite, DESCRICAO_PRODUTO).Value = ListaDadosImportacaoExcel(limite - 2, DESCRICAO_PRODUTO)
            workBookExcel.Sheets(1).Cells(limite, tipo_estrutura_comercial - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, tipo_estrutura_comercial)
            workBookExcel.Sheets(1).Cells(limite, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL)
            workBookExcel.Sheets(1).Cells(limite, cod_estrutura_comercial - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, cod_estrutura_comercial)
            workBookExcel.Sheets(1).Cells(limite, DESCRICAO_ESTRUTURA_COMERCIAL - subtrador).Value = ListaDadosImportacaoExcel(limite - 2, DESCRICAO_ESTRUTURA_COMERCIAL)
            workBookExcel.Sheets(1).Cells(limite, CICLO_INICIO - subtrador).Value = "'" & ListaDadosImportacaoExcel(limite - 2, CICLO_INICIO)
            workBookExcel.Sheets(1).Cells(limite, DATA_INICIO - subtrador).Value = "'" & ListaDadosImportacaoExcel(limite - 2, DATA_INICIO)
            workBookExcel.Sheets(1).Cells(limite, CICLO_FIM - subtrador).Value = "'" & ListaDadosImportacaoExcel(limite - 2, CICLO_FIM)
            workBookExcel.Sheets(1).Cells(limite, DATA_FIM - subtrador).Value = "'" & ListaDadosImportacaoExcel(limite - 2, DATA_FIM)
    
        End If
    Loop

FinalizarPlanilha workBookExcel, aplicacaoExcel, caminhoExcel, True
MsgBox "Dados exportados com sucesso!", vbInformation, "Mensagem"

Exit Sub
ErroGeral:
    Set workBookExcel = Nothing
    Set aplicacaoExcel = Nothing
    MsgBox "Erro Geral: " & Err.Description & ". Source: " & Err.Source
    Exit Sub
End Sub

Private Sub FinalizarPlanilha(workBookExcel As Excel.Workbook, aplicacaoExcel As Excel.Application, caminhoExcel As String, salvarDados As Boolean)

On Error GoTo ErroGeral

If Not workBookExcel Is Nothing Then
    If salvarDados Then
        workBookExcel.SaveAs caminhoExcel, xlNormal
    End If
    workBookExcel.Close False
    aplicacaoExcel.Visible = False
    Set workBookExcel = Nothing
    Set aplicacaoExcel = Nothing
End If

Exit Sub

ErroGeral:
    
    If Not (workBookExcel Is Nothing) Then
        Set workBookExcel = Nothing
    End If

    If Not (aplicacaoExcel Is Nothing) Then
        Set aplicacaoExcel = Nothing
    End If
    If Err.Number = 32755 Or Err.Number = 1004 Then
        Exit Sub
    End If
    MsgBox Err.Number & " - " & Err.Description

End Sub

Private Function ComparaMaiorValor(varAux1 As String, varAux2 As String, tipoDado As Integer) As String
    'TipoDado = 1 - NUmero
    'TipoDado = 2 - Data
    Dim valor1 As String
    Dim valor2 As String
    If tipoDado = 2 Then
       valor1 = Replace(varAux1, "/", "")
       valor2 = Replace(varAux2, "/", "")
    End If
    
    If valor1 = "" Then
        If valor2 = "" Then
            ComparaMaiorValor = varAux1
        Else
            ComparaMaiorValor = varAux2
        End If
    Else
        If valor2 = "" Then
            ComparaMaiorValor = varAux1
        Else
            If Val(valor1) >= Val(valor2) Then
                ComparaMaiorValor = varAux1
            Else
                ComparaMaiorValor = varAux2
            End If
        End If
    End If
    Exit Function
End Function

Private Function ComparaMenorValor(varAux1 As String, varAux2 As String, tipoDado) As String

    'TipoDado = 1 - NUmero
    'TipoDado = 2 - Data
    Dim valor1 As String
    Dim valor2 As String
    
    If tipoDado = 2 Then
       valor1 = Replace(varAux1, "/", "")
       valor2 = Replace(varAux2, "/", "")
    End If
    
    If valor1 = "" Then
        If valor2 = "" Then
            ComparaMenorValor = varAux1
        Else
            ComparaMenorValor = varAux2
        End If
    Else
        If valor2 = "" Then
            ComparaMenorValor = varAux1
        Else
            If Val(valor1) <= Val(valor2) Then
                ComparaMenorValor = varAux1
            Else
                ComparaMenorValor = varAux2
            End If
        End If
    End If
    
    Exit Function
End Function

Private Sub AdicionarLinhaGrid(Grid As vaSpread, Linha As Double, TipoOrigemLista As Integer, indiceColunaOperacao As Integer)
'1 - PCl
'2 - Mercadologico
    
    Dim vTip               As String
    Dim vCod               As String
    
    
    Grid.MaxRows = Grid.MaxRows + 1
    Grid.Row = Grid.MaxRows
                          
    Grid.SetText 2, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_INICIO)
    
    Dim dtInicio As String
    
    'dtInicio = ConverterDataDoGrid(CStr(ListaDadosImportacaoExcel(Linha, DATA_INICIO)))
    dtInicio = CStr(ListaDadosImportacaoExcel(Linha, DATA_INICIO))
    'If Trim(dtInicio) <> "" Then
    '    Grid.SetText 3, Grid.MaxRows, Format(dtInicio, "dd/mm/yyyy")
    'Else
         Grid.SetText 3, Grid.MaxRows, dtInicio
    'End If
    
    Grid.SetText 4, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, CICLO_FIM)
                        
    Dim dtFim As String
    'dtFim = ConverterDataDoGrid(CStr(ListaDadosImportacaoExcel(Linha, DATA_FIM)))
    dtFim = CStr(ListaDadosImportacaoExcel(Linha, DATA_FIM))
    'If Trim(dtFim) <> "" Then
    '    Grid.SetText 5, Grid.MaxRows, Format(dtFim, "dd/mm/yyyy")
    'Else
         Grid.SetText 5, Grid.MaxRows, dtFim
    'End If
       
    If TipoOrigemLista = 1 Then
        Grid.SetText 6, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, STATUS_PCL)
        
    Else
        Grid.SetText 6, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, STATUS_MERCADOLOGICO)
    End If

    Grid.SetText 8, Grid.MaxRows, CInt(ListaDadosImportacaoExcel(Linha, tipo_estrutura_comercial))
    Grid.SetText 9, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
                        
    vTip = ListaDadosImportacaoExcel(Linha, tipo_estrutura_comercial)
    vCod = ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
    ObterDadosEstruturaComercial vTip, vCod, varRE, varGV, varSET, vNomeEstruturaComercial
    
    If vTip = 2 Then
        Grid.SetText 10, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
        Grid.SetText 1, Grid.MaxRows, "RE " & ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & vNomeEstruturaComercial
    ElseIf vTip = 3 Then
        Grid.SetText 11, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
        Grid.SetText 1, Grid.MaxRows, "GV " & ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & vNomeEstruturaComercial
    ElseIf vTip = 4 Then
        Grid.SetText 12, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial)
        Grid.SetText 1, Grid.MaxRows, "ST " & ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & vNomeEstruturaComercial
    Else
        Grid.SetText 1, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, cod_estrutura_comercial) & " - " & vNomeEstruturaComercial
    End If
    
    Grid.SetText indiceColunaOperacao, Grid.MaxRows, ListaDadosImportacaoExcel(Linha, operacao)
    
    
      
End Sub

Public Function VerTamanhoArquivo(caminhoExcel As String) As Double
On Error GoTo erro:

Dim contador As Double
contador = 1

'Objetos do Excel
Dim aplicacaoExcel As New Excel.Application
Dim workBookExcel As Excel.Workbook
'Fim Objetos do Excel
aplicacaoExcel.Visible = False

Set workBookExcel = aplicacaoExcel.Workbooks.Open(caminhoExcel)

workBookExcel.Sheets(1).Select

'variaveis de metodo
Dim linhaVazia As Boolean

'fim variaveis de metodo
linhaVazia = False

Do While linhaVazia = False
    If VerificarLinhaVazia(workBookExcel, contador) Then
        linhaVazia = True
    Else
        contador = contador + 1
    End If
Loop
VerTamanhoArquivo = contador
FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
Exit Function

erro:
FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
MsgBox "Erro ao Contabilizar Planilha: " & Err.Description & " Souce: " & Err.Source
Exit Function

End Function

Public Function CarregarDadosParaExportar(Grid As vaSpread, TipoListaOrigem As Integer, codvenda As Double, IsControleLogistico As Boolean, DescricaoProduto As String) As Boolean

On Error GoTo erro:
    
    Dim totalLInhas As Double
    totalLInhas = Grid.MaxRows
    Dim contador As Double
    contador = 0
    Dim arrayAux() As Variant
    Dim i As Double
    Dim vTemporaria As Variant

    ReDim ListaDadosImportacaoExcel(Grid.MaxRows, 1 To 15)
    'MsgBox "redimensionou array"

    CarregarDadosParaExportar = False
    For i = 1 To Grid.MaxRows
        Grid.Row = i
        If Not Grid.RowHidden Then
            ListaDadosImportacaoExcel(contador, COD_VENDA) = codvenda
            'MsgBox "obteve codigo de venda"
            ListaDadosImportacaoExcel(contador, DESCRICAO_PRODUTO) = DescricaoProduto
            'MsgBox "obteve descri��o produto"
            Grid.GetText 8, i, vTemporaria
            ListaDadosImportacaoExcel(contador, tipo_estrutura_comercial) = vTemporaria
            'MsgBox "obteve c�digo tipo de estrutura_comercial"
            Select Case ListaDadosImportacaoExcel(contador, tipo_estrutura_comercial)
                   Case "0"
                        ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Pa�s"
                   Case "1"
                        ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Gerencia de Mercado"
                   Case "2"
                        ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Regi�o Estrat�gica"
                   Case "3"
                        ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Ger�ncia de Vendas"
                   Case "4"
                        ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Setor"
                   Case "5"
                        ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Grupo"
            End Select
            'MsgBox "obteve descri��o estrutura comercial"
            Grid.GetText 9, i, vTemporaria
            ListaDadosImportacaoExcel(contador, cod_estrutura_comercial) = vTemporaria
            'MsgBox "obteve codigo estrutura comercial"
            Grid.GetText 1, i, vTemporaria
            ListaDadosImportacaoExcel(contador, DESCRICAO_ESTRUTURA_COMERCIAL) = vTemporaria
            'MsgBox "obteve descri��o estrutura comercial"
            Grid.GetText 2, i, vTemporaria
            ListaDadosImportacaoExcel(contador, CICLO_INICIO) = vTemporaria
            'MsgBox "obteve ciclo inicio"
            Grid.GetText 3, i, vTemporaria
            ListaDadosImportacaoExcel(contador, DATA_INICIO) = vTemporaria
            'MsgBox "obteve data inicio"
            Grid.GetText 4, i, vTemporaria
            ListaDadosImportacaoExcel(contador, CICLO_FIM) = vTemporaria
            'MsgBox "obteve ciclo fim"
            Grid.GetText 5, i, vTemporaria
            ListaDadosImportacaoExcel(contador, DATA_FIM) = vTemporaria
            'MsgBox "obteve data fim"
            Grid.GetText 6, i, vTemporaria
            ListaDadosImportacaoExcel(contador, STATUS_MERCADOLOGICO) = vTemporaria
            'MsgBox "obteve status "

            contador = contador + 1
               
        End If
    Next i
    CarregarDadosParaExportar = True
Exit Function

erro:
    MsgBox "Erro ao Contabilizar Planilha: " & Err.Description & " Souce: " & Err.Source
    Exit Function

End Function



Public Function CarregarDadosParaExportarPCL(TipoListaOrigem As Integer, codvenda As Double, IsControleLogistico As Boolean, DescricaoProduto As String) As Boolean
 
 On Error GoTo erro:
    
    Dim totalLInhas As Double
    totalLInhas = frmVigenciaPCL.spdMaterial.MaxRows
    Dim contador As Double
    Dim vSequencia As Variant
    Dim vTemporaria As Variant
    contador = 0
    Dim arrayAux() As Variant
    Dim i As Double
    Dim arrayDadosEstruturaComercial() As Variant
    Dim DadosAbrangencia As Variant
    
    CarregarDadosParaExportarPCL = False
    
    ReDim ListaDadosImportacaoExcel(frmVigenciaPCL.spdMaterial.MaxRows, 1 To 15)
    
    For i = 1 To frmVigenciaPCL.spdMaterial.MaxRows
        frmVigenciaPCL.spdMaterial.Row = i
        If Not frmVigenciaPCL.spdMaterial.RowHidden Then

            'MsgBox "Entrou loop"
            'Metodo abaixo retorna um Array de 1 linha e 2 colunas, onde a 1 coluna � o c�digo e a 2 coluna a descri��o da Estrutura comercial
            frmVigenciaPCL.spdMaterial.GetText 1, i, vTemporaria
            ListaDadosImportacaoExcel(contador, COD_MATERIAL) = vTemporaria
            'MsgBox "processou uma codigo material"
            frmVigenciaPCL.spdMaterial.GetText 2, i, vTemporaria
            ListaDadosImportacaoExcel(contador, DESCRICAO_PRODUTO) = vTemporaria
            'MsgBox "processou descri��o"
            frmVigenciaPCL.spdMaterial.GetText 4, i, vTemporaria
            ListaDadosImportacaoExcel(contador, CICLO_INICIO) = vTemporaria
            'MsgBox "processou ciclo inicio"
            frmVigenciaPCL.spdMaterial.GetText 5, i, vTemporaria
            ListaDadosImportacaoExcel(contador, DATA_INICIO) = vTemporaria
            'MsgBox "processou uma datas"
            frmVigenciaPCL.spdMaterial.GetText 6, i, vTemporaria
            ListaDadosImportacaoExcel(contador, CICLO_FIM) = vTemporaria
            'MsgBox "processou ciclo fim"
            frmVigenciaPCL.spdMaterial.GetText 7, i, vTemporaria
            ListaDadosImportacaoExcel(contador, DATA_FIM) = vTemporaria
            'MsgBox "processou duas datas"
            frmVigenciaPCL.spdMaterial.GetText 8, i, vTemporaria
            ListaDadosImportacaoExcel(contador, PRIORIDADE_PCL) = vTemporaria
            'MsgBox "processou prioridade"
            frmVigenciaPCL.spdMaterial.GetText 9, i, vTemporaria
            ListaDadosImportacaoExcel(contador, STATUS_PCL) = vTemporaria
            'MsgBox "processou status"
            'Buscar Tipo Estrutura Comercial, descricao Tipo, Cod Estrutura Comercial, Descricao Codigo,
            frmVigenciaPCL.spdMaterial.GetText 3, i, DadosAbrangencia
            'MsgBox "processou dados abrangencia"
            If Left(CStr(DadosAbrangencia), 2) = "ST" Then
               ListaDadosImportacaoExcel(contador, tipo_estrutura_comercial) = "4"
               ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Setor"
            ElseIf Left(CStr(DadosAbrangencia), 2) = "GV" Then
               ListaDadosImportacaoExcel(contador, tipo_estrutura_comercial) = "3"
               ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Gerencia de Vendas"
            ElseIf Left(CStr(DadosAbrangencia), 2) = "RE" Then
               ListaDadosImportacaoExcel(contador, tipo_estrutura_comercial) = "2"
               ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Regiao Estrategica"
            Else
               ListaDadosImportacaoExcel(contador, tipo_estrutura_comercial) = "0"
               ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = "Pa�s"
            End If
            'MsgBox "atribuiu dados abrangencia pro array"
            If frmVigenciaPCL.chkContrLog.Value = vbChecked Then
                frmVigenciaPCL.spdAVIG.GetText 9, i, vTemporaria
                ListaDadosImportacaoExcel(contador, cod_estrutura_comercial) = vTemporaria
                'MsgBox "obteve codigo estrutura"
                frmVigenciaPCL.spdAVIG.GetText 1, i, vTemporaria
                ListaDadosImportacaoExcel(contador, DESCRICAO_ESTRUTURA_COMERCIAL) = vTemporaria
                'MsgBox "obteve descri��o estrutura"
            Else
                frmVigenciaPCL.spdAVIG.GetText 9, frmVigenciaPCL.spdAVIG.ActiveRow, vTemporaria
                ListaDadosImportacaoExcel(contador, cod_estrutura_comercial) = vTemporaria
                'MsgBox "obteve estrutura comercial"
                'ListaDadosImportacaoExcel(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = RetornarTipoEstruturaComercial(1)
                frmVigenciaPCL.spdAVIG.GetText 1, frmVigenciaPCL.spdAVIG.ActiveRow, vTemporaria
                ListaDadosImportacaoExcel(contador, DESCRICAO_ESTRUTURA_COMERCIAL) = vTemporaria
                'MsgBox "obteve descri��o estrutura"
                frmVigenciaPCL.spdAVIG.GetText 7, frmVigenciaPCL.spdAVIG.ActiveRow, vSequencia
                'MsgBox "obteve sequencia"
                ListaDadosImportacaoExcel(contador, AGRUPADOR_RELACAO) = vSequencia
                'MsgBox "atribuiu sequencia ao agrupador"
            End If
            ListaDadosImportacaoExcel(contador, COD_VENDA) = codvenda
            'MsgBox "obteve codigo de venda"
            contador = contador + 1
             
        End If
    Next i
    CarregarDadosParaExportarPCL = True
Exit Function

erro:
    MsgBox "Erro ao Contabilizar Planilha: " & Err.Description & " Souce: " & Err.Source
    Exit Function

End Function





Public Function CarregarListaDados(caminhoExcel As String, tamanho As Double, origem As Integer, ByRef mensagemInformativa As String, codVendaTela As String) As Boolean
 
On Error GoTo erro:
 
Dim contador As Double
contador = 1

'Objetos do Excel
Dim aplicacaoExcel As New Excel.Application
Dim workBookExcel As Excel.Workbook
'Fim Objetos do Excel
aplicacaoExcel.Visible = False

Set workBookExcel = aplicacaoExcel.Workbooks.Open(caminhoExcel)

workBookExcel.Sheets(1).Select
Dim subtrador As Integer

If origem = 1 Then
    subtrador = 0
ElseIf origem = 2 Then
    subtrador = 1
End If
'variaveis de metodo
Dim linhaVazia As Boolean
CarregarListaDados = True
'fim variaveis de metodo
linhaVazia = False
If Not ValidarLayout(origem, workBookExcel) = True Then
    mensagemInformativa = "Layout de Arquivo de Importa��o Inv�lido."
    CarregarListaDados = False
    Exit Function
End If
ReDim ListaDadosImportacaoExcel(tamanho, 1 To 15)

Do While (linhaVazia = False)
    If VerificarLinhaVazia(workBookExcel, contador) Then
        linhaVazia = True
        Exit Do
    Else
        Dim codigoErro As Integer
        codigoErro = 0
        If ValidarCodVenda(codVendaTela, workBookExcel.Sheets(1).Cells(contador + 1, COD_VENDA).Value, codigoErro) Then
            contador = contador + 1
            If Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value) <> "" Then
                'Significa que existe preenchido Ciclo
                 If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value, 1) = False Then
                    'Significa que o Ciclo In�cio � inv�lido
                      mensagemInformativa = FormatarMensagem(RetornarMensagemErro(2), contador, CICLO_INICIO - subtrador)
                      FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                      CarregarListaDados = False
                      Exit Function
                 Else
                    If Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value) <> "" Then
                        If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value, 1) = False Then
                             'Significa que o Ciclo FIM � inv�lido
                              mensagemInformativa = FormatarMensagem(RetornarMensagemErro(3), contador, CICLO_FIM - subtrador)
                              FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                              CarregarListaDados = False
                              Exit Function
                        Else
                            If verificaCiclos(Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value), Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value)) = False Then
                                'Significa que o Ciclo Fim � menor que o Ciclo In�cio
                                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(9), contador, CICLO_INICIO - subtrador)
                                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                                CarregarListaDados = False
                                Exit Function
                            End If
                        End If
                    End If
                 End If
            Else
                'Verificar se Existe data preenchida
                If Trim(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value) <> "" Then
                    If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value, 2) = False Then
                       'Significa que o Data In�cio � inv�lido
                         mensagemInformativa = FormatarMensagem(RetornarMensagemErro(4), contador, DATA_INICIO - subtrador)
                         FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                         CarregarListaDados = False
                         Exit Function
                    Else
                        If Trim(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value) <> "" Then
                            If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value, 2) = False Then
                                 'Significa que o Data FIM � inv�lido
                                  mensagemInformativa = FormatarMensagem(RetornarMensagemErro(5), contador, DATA_FIM - subtrador)
                                  FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                                  CarregarListaDados = False
                                  Exit Function
                            Else
                                If CDate(Trim(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value)) > CDate(Trim(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value)) Then
                                     'Significa que o Data FIM � maior que Data In�cio
                                     mensagemInformativa = FormatarMensagem(RetornarMensagemErro(10), contador, DATA_FIM - subtrador)
                                     FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                                     CarregarListaDados = False
                                     Exit Function
                                End If
                            End If
                        End If
                    End If
                Else
                    'n�o existe nem data nem ciclo preenchido
                    mensagemInformativa = FormatarMensagem(RetornarMensagemErro(99), contador, 0)
                    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                    CarregarListaDados = False
                    Exit Function
                End If
            End If
            If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value, 2) = True Then
                'Data In�cio e Ciclo In�cio Preenchida
                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_INICIO - subtrador)
                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                CarregarListaDados = False
                Exit Function
            Else
                If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value, 2) = True Then
                   'Data Fim e Ciclo Fim Preenchida
                    mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_FIM - subtrador)
                    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                    CarregarListaDados = False
                    Exit Function
'                Else
'                    If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value, 2) = True Then
'                       'Ciclo In�cio e Data Fim Preenchida
'                        mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_INICIO - subtrador)
'                        FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
'                        CarregarListaDados = False
'                        Exit Function
'                    Else
'                        If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value, 2) = True Then
'                            'Data In�cio e Ciclo Fim Preenchida
'                             mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_INICIO - subtrador)
'                             FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
'                             CarregarListaDados = False
'                             Exit Function
'                        End If
'                    End If
                End If
            End If
            If Not IsNumeric(workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value) Then
                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(13), contador, tipo_estrutura_comercial - subtrador)
                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                CarregarListaDados = False
                Exit Function
            End If
            If ValidarExistenciaTipoEstruturaComercial(CInt(workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value)) = False Then
                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(13), contador, tipo_estrutura_comercial - subtrador)
                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                CarregarListaDados = False
                Exit Function
            Else
                If ValidarExistenciaEstruturaComercial(CInt(workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value), CDbl(workBookExcel.Sheets(1).Cells(contador, cod_estrutura_comercial - subtrador).Value)) = False Then
                    mensagemInformativa = FormatarMensagem(RetornarMensagemErro(12), contador, cod_estrutura_comercial - subtrador)
                    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                    CarregarListaDados = False
                    Exit Function
                Else
                    If origem = 1 Then
                        If ValidarPrioridade(workBookExcel.Sheets(1).Cells(contador, PRIORIDADE_PCL - subtrador).Value) = False Then
                            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(14), contador, PRIORIDADE_PCL - subtrador)
                            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                            CarregarListaDados = False
                            Exit Function
                        End If
                        If ValidarAgrupador(workBookExcel.Sheets(1).Cells(contador, AGRUPADOR_RELACAO - subtrador).Value) = False Then
                            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(1), contador, AGRUPADOR_RELACAO - subtrador)
                            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                            CarregarListaDados = False
                            Exit Function
                        End If
                        
                        If ValidarCodMaterial(workBookExcel.Sheets(1).Cells(contador, AGRUPADOR_RELACAO - subtrador).Value, CStr(workBookExcel.Sheets(1).Cells(contador, COD_MATERIAL - subtrador).Value), Val(codVendaTela)) = False Then
                            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(7), contador, COD_MATERIAL - subtrador)
                            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                            CarregarListaDados = False
                            Exit Function
                        End If
                    End If
                    ListaDadosImportacaoExcel(contador - 1, COD_VENDA) = workBookExcel.Sheets(1).Cells(contador, COD_VENDA).Value
                    ListaDadosImportacaoExcel(contador - 1, DESCRICAO_PRODUTO) = workBookExcel.Sheets(1).Cells(contador, DESCRICAO_PRODUTO).Value
                    ListaDadosImportacaoExcel(contador - 1, tipo_estrutura_comercial) = workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = workBookExcel.Sheets(1).Cells(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, cod_estrutura_comercial) = workBookExcel.Sheets(1).Cells(contador, cod_estrutura_comercial - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, DESCRICAO_ESTRUTURA_COMERCIAL) = workBookExcel.Sheets(1).Cells(contador, DESCRICAO_ESTRUTURA_COMERCIAL - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, CICLO_INICIO) = FormatarCiclo(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value)
                    ListaDadosImportacaoExcel(contador - 1, DATA_INICIO) = CStr(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value)
                    ListaDadosImportacaoExcel(contador - 1, CICLO_FIM) = FormatarCiclo(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value)
                    ListaDadosImportacaoExcel(contador - 1, DATA_FIM) = CStr(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value)
                    If origem = 1 Then
                        ListaDadosImportacaoExcel(contador - 1, COD_MATERIAL) = workBookExcel.Sheets(1).Cells(contador, COD_MATERIAL - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, PRIORIDADE_PCL) = workBookExcel.Sheets(1).Cells(contador, PRIORIDADE_PCL - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, STATUS_PCL) = workBookExcel.Sheets(1).Cells(contador, STATUS_PCL - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, operacao) = "A"
                        ListaDadosImportacaoExcel(contador - 1, AGRUPADOR_RELACAO) = Trim(CStr(workBookExcel.Sheets(1).Cells(contador, AGRUPADOR_RELACAO - subtrador).Value))
                    ElseIf origem = 2 Then
                        ListaDadosImportacaoExcel(contador - 1, STATUS_MERCADOLOGICO) = workBookExcel.Sheets(1).Cells(contador, STATUS_MERCADOLOGICO - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, operacao) = "I"
                    End If
                   
                End If
            End If
        Else
            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(codigoErro), contador + 1, COD_VENDA)
            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
            CarregarListaDados = False
            Exit Function
        End If
    End If
Loop

FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
Exit Function

erro:
    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
    MsgBox "Erro ao Contabilizar Planilha: " & Err.Description & " Souce: " & Err.Source
    Exit Function
End Function

Public Function CarregarListaDadosPcl(caminhoExcel As String, tamanho As Double, origem As Integer, ByRef mensagemInformativa As String, codVendaTela As String) As Boolean
 
On Error GoTo erro:
 
Dim contador As Double
contador = 1

'Objetos do Excel
Dim aplicacaoExcel As New Excel.Application
Dim workBookExcel As Excel.Workbook
'Fim Objetos do Excel
aplicacaoExcel.Visible = False
Screen.MousePointer = vbHourglass

Set workBookExcel = aplicacaoExcel.Workbooks.Open(caminhoExcel)

workBookExcel.Sheets(1).Select
Dim subtrador As Integer

If origem = 1 Then
    subtrador = 0
ElseIf origem = 2 Then
    subtrador = 1
End If
'variaveis de metodo
Dim linhaVazia As Boolean
CarregarListaDadosPcl = True
'fim variaveis de metodo
linhaVazia = False
If Not ValidarLayout(origem, workBookExcel) = True Then
    CarregarListaDadosPcl = False
    mensagemInformativa = "Layout de Arquivo de Importa��o Inv�lido"
    Exit Function
End If
ReDim ListaDadosImportacaoExcel(tamanho, 1 To 15)

Do While (linhaVazia = False)
    If VerificarLinhaVazia(workBookExcel, contador) Then
        linhaVazia = True
        Exit Do
    Else
        Dim codigoErro As Integer
        codigoErro = 0
        If ValidarCodVenda(codVendaTela, workBookExcel.Sheets(1).Cells(contador + 1, COD_VENDA).Value, codigoErro) Then
            contador = contador + 1
            If Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value) <> "" Then
                'Significa que existe preenchido Ciclo
                 If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value, 1) = False Then
                    'Significa que o Ciclo In�cio � inv�lido
                      mensagemInformativa = FormatarMensagem(RetornarMensagemErro(2), contador, CICLO_INICIO - subtrador)
                      FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                      CarregarListaDadosPcl = False
                      Screen.MousePointer = vbDefault
                      Exit Function
                 Else
                    If Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value) <> "" Then
                        If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value, 1) = False Then
                             'Significa que o Ciclo FIM � inv�lido
                              mensagemInformativa = FormatarMensagem(RetornarMensagemErro(3), contador, CICLO_FIM - subtrador)
                              FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                              CarregarListaDadosPcl = False
                              Screen.MousePointer = vbDefault
                              Exit Function
                        Else
                            If verificaCiclos(Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value), Trim(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value)) = False Then
                                'Significa que o Ciclo Fim � menor que o Ciclo In�cio
                                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(9), contador, CICLO_INICIO - subtrador)
                                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                                CarregarListaDadosPcl = False
                                Screen.MousePointer = vbDefault
                                Exit Function
                            End If
                        End If
                    End If
                 End If
            Else
                'Verificar se Existe data preenchida
                If Trim(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value) <> "" Then
                    If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value, 2) = False Then
                       'Significa que o Data In�cio � inv�lido
                         mensagemInformativa = FormatarMensagem(RetornarMensagemErro(4), contador, DATA_INICIO - subtrador)
                         FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                         CarregarListaDadosPcl = False
                         Screen.MousePointer = vbDefault
                         Exit Function
                    Else
                        If Trim(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value) <> "" Then
                            If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value, 2) = False Then
                                 'Significa que o Data FIM � inv�lido
                                  mensagemInformativa = FormatarMensagem(RetornarMensagemErro(5), contador, DATA_FIM - subtrador)
                                  FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                                  CarregarListaDadosPcl = False
                                  Screen.MousePointer = vbDefault
                                  Exit Function
                            Else
                                If CDate(Trim(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value)) > CDate(Trim(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value)) Then
                                     'Significa que o Data FIM � maior que Data In�cio
                                     mensagemInformativa = FormatarMensagem(RetornarMensagemErro(10), contador, DATA_FIM - subtrador)
                                     FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                                     CarregarListaDadosPcl = False
                                     Screen.MousePointer = vbDefault
                                     Exit Function
                                End If
                            End If
                        End If
                    End If
                Else
                    'n�o existe nem data nem ciclo preenchido
                    mensagemInformativa = FormatarMensagem(RetornarMensagemErro(99), contador, 0)
                    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                    CarregarListaDadosPcl = False
                    Screen.MousePointer = vbDefault
                    Exit Function
                End If
            End If
            If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value, 2) = True Then
                'Data In�cio e Ciclo In�cio Preenchida
                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_INICIO - subtrador)
                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                CarregarListaDadosPcl = False
                Screen.MousePointer = vbDefault
                Exit Function
            Else
                If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value, 2) = True Then
                   'Data Fim e Ciclo Fim Preenchida
                    mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_FIM - subtrador)
                    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                    CarregarListaDadosPcl = False
                    Screen.MousePointer = vbDefault
                    Exit Function
                Else
                    
                    If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value, 2) = True Then
                       'Ciclo In�cio e Data Fim Preenchida
                        mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_INICIO - subtrador)
                        FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                        CarregarListaDadosPcl = False
                        Screen.MousePointer = vbDefault
                        Exit Function
                    Else
                        If ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value, 1) = True And ValidarCicloData(workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value, 2) = True Then
                            'Data In�cio e Ciclo Fim Preenchida
                             mensagemInformativa = FormatarMensagem(RetornarMensagemErro(6), contador, CICLO_INICIO - subtrador)
                             FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                             CarregarListaDadosPcl = False
                             Screen.MousePointer = vbDefault
                             Exit Function
                        End If
                    End If
                End If
            End If
            If Not IsNumeric(workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value) Then
                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(13), contador, tipo_estrutura_comercial - subtrador)
                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                CarregarListaDadosPcl = False
                Exit Function
            End If
            If ValidarExistenciaTipoEstruturaComercial(CInt(workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value)) = False Then
                mensagemInformativa = FormatarMensagem(RetornarMensagemErro(13), contador, tipo_estrutura_comercial - subtrador)
                FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                CarregarListaDadosPcl = False
                Screen.MousePointer = vbDefault
                Exit Function
            Else
                If ValidarExistenciaEstruturaComercial(CInt(workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value), CDbl(workBookExcel.Sheets(1).Cells(contador, cod_estrutura_comercial - subtrador).Value)) = False Then
                    mensagemInformativa = FormatarMensagem(RetornarMensagemErro(12), contador, cod_estrutura_comercial - subtrador)
                    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                    CarregarListaDadosPcl = False
                    Screen.MousePointer = vbDefault
                    Exit Function
                Else
                    If origem = 1 Then
                        If ValidarPrioridade(workBookExcel.Sheets(1).Cells(contador, PRIORIDADE_PCL - subtrador).Value) = False Then
                            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(14), contador, PRIORIDADE_PCL - subtrador)
                            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                            CarregarListaDadosPcl = False
                            Screen.MousePointer = vbDefault
                            Exit Function
                        End If
                        If ValidarAgrupador(workBookExcel.Sheets(1).Cells(contador, AGRUPADOR_RELACAO - subtrador).Value) = False Then
                            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(15), contador, AGRUPADOR_RELACAO - subtrador)
                            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                            CarregarListaDadosPcl = False
                            Screen.MousePointer = vbDefault
                            Exit Function
                        End If
                        
                        If ValidarCodMaterial(Val(workBookExcel.Sheets(1).Cells(contador, AGRUPADOR_RELACAO - subtrador).Value), CStr(workBookExcel.Sheets(1).Cells(contador, COD_MATERIAL - subtrador).Value), Val(codVendaTela)) = False Then
                            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(7), contador, COD_MATERIAL - subtrador)
                            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
                            CarregarListaDadosPcl = False
                            Screen.MousePointer = vbDefault
                            Exit Function
                        End If
                    End If

                    ListaDadosImportacaoExcel(contador - 1, COD_VENDA) = workBookExcel.Sheets(1).Cells(contador, COD_VENDA).Value
                    ListaDadosImportacaoExcel(contador - 1, DESCRICAO_PRODUTO) = workBookExcel.Sheets(1).Cells(contador, DESCRICAO_PRODUTO).Value
                    ListaDadosImportacaoExcel(contador - 1, tipo_estrutura_comercial) = workBookExcel.Sheets(1).Cells(contador, tipo_estrutura_comercial - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL) = workBookExcel.Sheets(1).Cells(contador, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, cod_estrutura_comercial) = workBookExcel.Sheets(1).Cells(contador, cod_estrutura_comercial - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, DESCRICAO_ESTRUTURA_COMERCIAL) = workBookExcel.Sheets(1).Cells(contador, DESCRICAO_ESTRUTURA_COMERCIAL - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, CICLO_INICIO) = FormatarCiclo(workBookExcel.Sheets(1).Cells(contador, CICLO_INICIO - subtrador).Value)
                    ListaDadosImportacaoExcel(contador - 1, DATA_INICIO) = workBookExcel.Sheets(1).Cells(contador, DATA_INICIO - subtrador).Value
                    ListaDadosImportacaoExcel(contador - 1, CICLO_FIM) = FormatarCiclo(workBookExcel.Sheets(1).Cells(contador, CICLO_FIM - subtrador).Value)
                    ListaDadosImportacaoExcel(contador - 1, DATA_FIM) = workBookExcel.Sheets(1).Cells(contador, DATA_FIM - subtrador).Value
                    If origem = 1 Then
                        ListaDadosImportacaoExcel(contador - 1, COD_MATERIAL) = workBookExcel.Sheets(1).Cells(contador, COD_MATERIAL - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, PRIORIDADE_PCL) = workBookExcel.Sheets(1).Cells(contador, PRIORIDADE_PCL - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, STATUS_PCL) = workBookExcel.Sheets(1).Cells(contador, STATUS_PCL - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, operacao) = "I"
                        ListaDadosImportacaoExcel(contador - 1, AGRUPADOR_RELACAO) = Trim(CStr(workBookExcel.Sheets(1).Cells(contador, AGRUPADOR_RELACAO - subtrador).Value))
                    ElseIf origem = 2 Then
                        ListaDadosImportacaoExcel(contador - 1, STATUS_MERCADOLOGICO) = workBookExcel.Sheets(1).Cells(contador, STATUS_MERCADOLOGICO - subtrador).Value
                        ListaDadosImportacaoExcel(contador - 1, operacao) = "I"
                    End If
                   
                End If
            End If
        Else
            mensagemInformativa = FormatarMensagem(RetornarMensagemErro(codigoErro), contador + 1, COD_VENDA)
            FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
            Screen.MousePointer = vbDefault
            CarregarListaDadosPcl = False
            Exit Function
        End If
    End If
Loop

FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
Screen.MousePointer = vbDefault
Exit Function
Resume
erro:
    CarregarListaDadosPcl = False
    Screen.MousePointer = vbDefault
    FinalizarPlanilha workBookExcel, aplicacaoExcel, "", False
    MsgBox "Erro ao Contabilizar Planilha: " & Err.Description & " Souce: " & Err.Source
    Exit Function
End Function






Private Sub RetornarEstruturaComercial(TextoAbrangencia As Variant, ByRef retorno() As Variant)

Dim PalavrasQuebradas() As Variant
ReDim retorno(1, 1 To 2)

Split Trim(TextoAbrangencia), PalavrasQuebradas, " "

If IsNumeric(PalavrasQuebradas(0)) Then
    retorno(1, 1) = PalavrasQuebradas(0)
    retorno(1, 2) = Replace(CStr(PalavrasQuebradas(2)), "-", "")
Else
    retorno(1, 1) = PalavrasQuebradas(1)
    retorno(1, 2) = Replace(CStr(PalavrasQuebradas(3)), "-", "")
End If

Exit Sub
End Sub

Private Function RetornarTipoEstruturaComercial(cdTipoEstruturaComercial As Integer) As String
    Dim bRespo As Boolean
    Dim avData() As Variant
    Dim cSql As String
    
    ReDim avData(0, 0)
    ReDim avParametros(0, 1)
    
    avParametros(0, AV_CONTEUDO) = cdTipoEstruturaComercial
    avParametros(0, AV_TIPO) = TIPO_NUMERICO
    
    cSql = " st_busca_estrutura_comercial " & FormataParametrosSQL(avParametros)
    
    bRespo = gObjeto.natExecuteQuery(hSql, cSql, avData)
    If UBound(avData, 1) > 0 Then
        RetornarTipoEstruturaComercial = Trim(avData(1, 0))
    Else
        RetornarTipoEstruturaComercial = ""
    End If
    
    Exit Function

End Function

Private Function ValidarDatas(Data1 As String, Data2 As String, ByRef codigoErro As Integer) As Boolean
    If Data1 = "" Then
        codigoErro = 4
        ValidarDatas = False
    ElseIf Data2 = "" Then
        codigoErro = 5
        ValidarDatas = False
    Else
        If Not IsDate(Data1) Then
            codigoErro = 4
            ValidarDatas = False
        ElseIf Not IsDate(Data2) Then
            codigoErro = 5
            ValidarDatas = False
        Else
            If CDate(Data1) < CDate(Data2) Then
                codigoErro = 5
                ValidarDatas = False
            Else
                ValidarDatas = True
            End If
        End If
    End If

End Function

Private Function ValidarExistenciaTipoEstruturaComercial(cdTipoEstruturaComercial As Integer) As Boolean
    If cdTipoEstruturaComercial < 0 Or cdTipoEstruturaComercial > 5 Then
    'If RetornarTipoEstruturaComercial(cdTipoEstruturaComercial) = "" Then
         ValidarExistenciaTipoEstruturaComercial = False
    Else
        ValidarExistenciaTipoEstruturaComercial = True
    End If

End Function

Private Function BuscarEstruturaComercial(cdTipoEstruturaComercial As Integer, cdEstruturaComercial As Double) As String
'st_estrut_comercial_s
    Dim bRespo As Boolean
    Dim avData() As Variant
    Dim cSql As String
    Dim nomOperacao, nomTabela As String
    Dim linhaExecucao As Double
    linhaExecucao = 1
    
    ReDim avData(0, 0)
    ReDim avParametros(5, 2)
    
    avParametros(0, AV_CONTEUDO) = cdTipoEstruturaComercial
    avParametros(0, AV_TIPO) = TIPO_NUMERICO
    
    avParametros(1, AV_CONTEUDO) = cdEstruturaComercial
    avParametros(1, AV_TIPO) = TIPO_NUMERICO

    avParametros(2, AV_CONTEUDO) = 1
    avParametros(2, AV_TIPO) = TIPO_NUMERICO

    avParametros(3, AV_CONTEUDO) = ""
    avParametros(3, AV_TIPO) = TIPO_STRING

    avParametros(4, AV_CONTEUDO) = ""
    avParametros(4, AV_TIPO) = TIPO_STRING

    avParametros(5, AV_CONTEUDO) = linhaExecucao
    avParametros(5, AV_TIPO) = TIPO_NUMERICO

    cSql = " st_estrut_comercial_s " & FormataParametrosSQL(avParametros)
    
    bRespo = gObjeto.natExecuteQuery(hSql, cSql, avData)
    If UBound(avData, 1) > 0 Then
        BuscarEstruturaComercial = Trim(avData(1, 0))
    Else
        BuscarEstruturaComercial = ""
    End If
End Function

Private Function ValidarExistenciaEstruturaComercial(cdTipoEstruturaComercial As Integer, cdEstruturaComercial As Double) As Boolean
    If BuscarEstruturaComercial(cdTipoEstruturaComercial, cdEstruturaComercial) = "" Then
        ValidarExistenciaEstruturaComercial = False
    Else
        ValidarExistenciaEstruturaComercial = True
    End If

End Function

Public Function RetornarRegistrosAgrupados(ByRef arrayDadosRetorno() As Variant, sequenciaAgrupador As Variant, codvenda As Double) As Boolean
    ReDim avParametros(2, 1)
                                    
    avParametros(0, AV_CONTEUDO) = codvenda

    avParametros(0, AV_TIPO) = TIPO_NUMERICO

    avParametros(1, AV_CONTEUDO) = sequenciaAgrupador
    avParametros(1, AV_TIPO) = TIPO_NUMERICO
    
    avParametros(2, AV_CONTEUDO) = 1
    avParametros(2, AV_TIPO) = TIPO_NUMERICO
   
    cSql = " stp_material_produto_venda_s " & FormataParametrosSQL(avParametros)
        
    RetornarRegistrosAgrupados = gObjeto.natExecuteQuery(hSql, cSql, arrayDadosRetorno)

End Function

Private Function RetornarMensagemErro(codigoErro As Integer) As String
    Dim mensagem As String
    
    If codigoErro = 1 Then
        mensagem = "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as �nconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 2 Then
        mensagem = "Ciclo In�cio inv�lido." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 3 Then
        mensagem = "Ciclo Fim inv�lido." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 4 Then
        mensagem = "Data In�cio inv�lida." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 5 Then
        mensagem = "Data Fim inv�lida." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 6 Then
        mensagem = "N�o � poss�vel importar vig�ncia com ciclo e data." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 7 Then
        mensagem = "N�o � poss�vel importar c�digo de venda sem c�digo de material associado." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 8 Then
        mensagem = "N�o � poss�vel importar mais de um c�digo de venda." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 9 Then
        mensagem = "Ciclo In�cio maior que o Ciclo Fim." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 10 Then
        mensagem = "Data In�cio maior que o Data Fim." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 11 Then
        mensagem = "C�digo de Venda Inv�lido." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 12 Then
        mensagem = "Estrutura Comercial Inv�lida." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 13 Then
        mensagem = "Tipo de Estrutura Comercial Inv�lido." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 14 Then
        mensagem = "C�digo de Prioridade Inv�lido." & vbCrLf & "Coluna {0} Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 15 Then
        mensagem = "Agrupador inv�lido." & vbCrLf & "Coluna {0} Linha {1}" & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    ElseIf codigoErro = 99 Then
        mensagem = "N�o existe Ciclo ou Data informado." & vbCrLf & "Linha {1}." & vbCrLf & "N�o ser� poss�vel concluir a importa��o da abrang�ncia enquanto as inconsist�ncias n�o forem corrigidas."
    End If
    RetornarMensagemErro = mensagem
End Function

'
Private Function ValidarCicloData(dataCiclo As String, tipoData As Integer) As Boolean
    'tipoData = 1 - Ciclos
    'tipoData = 2 - Data
    ValidarCicloData = False
    
    If tipoData = 1 Then
        Dim posicao As Integer
        posicao = InStr(dataCiclo, "/")
        If (InStr(dataCiclo, ".") > 0) Or (InStr(dataCiclo, ",") > 0) Then
            Exit Function
        End If
        If posicao > 0 Then
            If Len(dataCiclo) <> 7 Then
                Exit Function
            End If

            Dim Lista() As Variant
            Split dataCiclo, Lista, "/"
            
            If Not IsNumeric(Lista(0)) Then
                Exit Function
            Else
                If Len(Lista(0)) <> 2 Then
                    Exit Function
                End If
                If Val(Lista(0)) <= 0 Then
                    Exit Function
                Else
                    If Len(Lista(1)) <> 4 Then
                        Exit Function
                    End If
                    If Not IsNumeric(Lista(1)) Then
                        Exit Function
                    Else
                        If Val(Lista(1)) <= 0 Then
                            Exit Function
                        End If
                    End If
                End If
            End If
        Else
            If Len(CStr(dataCiclo)) <> 6 Then
                Exit Function
            End If
            If Not IsNumeric(dataCiclo) Then
                Exit Function
            Else
                If Val(dataCiclo) <= 0 Then
                    Exit Function
                End If
            End If
        End If
        
    ElseIf tipoData = 2 Then
        If Not IsDate(dataCiclo) Then
            Exit Function
        End If
    End If
    ValidarCicloData = True
End Function

Private Function VerificarLinhaVazia(workBookExcel As Excel.Workbook, contador As Double) As Boolean
    If Trim(workBookExcel.Sheets(1).Cells(contador + 1, COD_VENDA).Value) = "" And Trim(workBookExcel.Sheets(1).Cells(contador + 1, DESCRICAO_PRODUTO).Value) = "" And Trim(workBookExcel.Sheets(1).Cells(contador + 1, COD_MATERIAL).Value) = "" And Trim(workBookExcel.Sheets(1).Cells(contador + 1, tipo_estrutura_comercial).Value) = "" And Trim(workBookExcel.Sheets(1).Cells(contador + 1, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL).Value) = "" And Trim(workBookExcel.Sheets(1).Cells(contador + 1, cod_estrutura_comercial).Value) = "" And Trim(workBookExcel.Sheets(1).Cells(contador + 1, DESCRICAO_ESTRUTURA_COMERCIAL).Value) = "" Then
        VerificarLinhaVazia = True
    Else
        VerificarLinhaVazia = False
    End If
End Function

Private Function ValidarCodVenda(codVendaTela As String, codVendaImportacao As String, ByRef codErro As Integer) As Boolean
If Trim(codVendaTela) = "" Or Trim(codVendaImportacao) = "" Then
    codErro = 11
    ValidarCodVenda = False
Else
    If Not IsNumeric(codVendaImportacao) Then
        codErro = 11
        ValidarCodVenda = False
    Else
        If Val(codVendaImportacao) <= 0 Then
            codErro = 11
            ValidarCodVenda = False
        Else
            If Trim(codVendaTela) <> Trim(codVendaImportacao) Then
                codErro = 8
                ValidarCodVenda = False
            Else
                ValidarCodVenda = True
            End If
        End If
    End If
End If

End Function

Private Function ValidarPrioridade(prioridade As String) As Boolean
    If Trim(prioridade) = "" Then
        ValidarPrioridade = False
    Else
        If Not IsNumeric(prioridade) Then
            ValidarPrioridade = False
        Else
            If Val(prioridade) <= 0 Then
                ValidarPrioridade = False
            Else
                ValidarPrioridade = True
            End If
        End If
    End If

End Function

Private Function ValidarAgrupador(agrupador As String) As Boolean
    If frmVigenciaPCL.chkContrLog = vbChecked Then
        ValidarAgrupador = True
    Else
        If Trim(agrupador) = "" Then
            ValidarAgrupador = True
        Else
            If Not IsNumeric(agrupador) Then
                ValidarAgrupador = False
            Else
                If Val(agrupador) <= 0 Then
                    ValidarAgrupador = False
                Else
                    ValidarAgrupador = True
                End If
            End If
        End If
    End If
End Function

Private Function ValidarCodMaterial(vSequencia As Variant, codMaterial As String, codvenda As Double)
    Dim retorno As Boolean
    
    ValidarCodMaterial = True
    Exit Function
    
    If Trim(codMaterial) = "" Then
        retorno = False
    Else
        If Not IsNumeric(codMaterial) Then
            retorno = False
        Else
            If Val(codMaterial) <= 0 Then
                retorno = False
            Else
                Dim avData()   As Variant
                ReDim avData(0, 0)
                retorno = RetornarRegistrosAgrupados(avData, vSequencia, codvenda)
                retorno = False
                If UBound(avData, 1) > 0 Then
                    Dim i As Double
                    For i = 0 To UBound(avData, 2)
                        If Val(avData(0, i)) = Val(codMaterial) Then
                            retorno = True
                            Exit For
                        End If
                    Next i
                Else
                    retorno = False
                End If
            End If
        End If
    End If
    ValidarCodMaterial = retorno
End Function

Private Function FormatarMensagem(mensagem As String, Linha As Double, coluna As Integer) As String
    Dim mensagemRetorno As String
    mensagemRetorno = Replace(mensagem, "{0}", CStr(coluna))
    mensagemRetorno = Replace(mensagemRetorno, "{1}", CStr(Linha))
    FormatarMensagem = mensagemRetorno
End Function

Private Function ValidarLayout(OrigemDados As Integer, workBookExcel As Excel.Workbook) As Boolean
Dim retorno As Boolean
retorno = True
Dim subtrador As Integer

If OrigemDados = 1 Then
    subtrador = 0
ElseIf OrigemDados = 2 Then
    subtrador = 1
End If
    If workBookExcel.Sheets(1).Cells(1, COD_VENDA).Value <> "C�d. Venda" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, tipo_estrutura_comercial - subtrador).Value <> "Tipo Estrutura Comercial" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, DESCRICAO_TIPO_ESTRUTURA_COMERCIAL - subtrador).Value <> "Descri��o do Tipo de Estrutura Comercial" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, cod_estrutura_comercial - subtrador).Value <> "Estrutura Comercial" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, DESCRICAO_ESTRUTURA_COMERCIAL - subtrador).Value <> "Descri��o da Estrutura Comercial" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, CICLO_INICIO - subtrador).Value <> "Ciclo Inicio" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, DATA_INICIO - subtrador).Value <> "Data Inicio" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, CICLO_FIM - subtrador).Value <> "Ciclo Fim" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, DATA_FIM - subtrador).Value <> "Data Fim" Then
        retorno = False
    ElseIf workBookExcel.Sheets(1).Cells(1, DESCRICAO_PRODUTO).Value <> "Descri��o Produto" Then
        retorno = False
    End If
    
    If retorno = True Then
        If OrigemDados = 2 Then
            If workBookExcel.Sheets(1).Cells(1, STATUS_MERCADOLOGICO - subtrador).Value <> "Status" Then
                retorno = False
            End If
        ElseIf OrigemDados = 1 Then
            If workBookExcel.Sheets(1).Cells(1, PRIORIDADE_PCL - subtrador).Value <> "Prioridade" Then
                retorno = False
            ElseIf workBookExcel.Sheets(1).Cells(1, STATUS_PCL - subtrador).Value <> "Status" Then
                retorno = False
            ElseIf workBookExcel.Sheets(1).Cells(1, COD_MATERIAL - subtrador).Value <> "C�d. Material" Then
                retorno = False
            ElseIf workBookExcel.Sheets(1).Cells(1, (AGRUPADOR_RELACAO) - subtrador).Value <> "Agrupador" Then
                retorno = False
            End If
            
        End If
    End If
ValidarLayout = retorno
End Function
Private Function verificaCiclos(cicloInicio As String, cicloFim As String) As Boolean

    Dim splitCicloInicio() As Variant
    Dim splitCicloFim() As Variant
    
    If InStr(cicloInicio, "/") > 0 Then
        cicloInicio = Right(cicloInicio, 4) & Left(cicloInicio, 2)
    End If
    
    If InStr(cicloFim, "/") > 0 Then
        cicloFim = Right(cicloFim, 4) & Left(cicloFim, 2)
    End If
    
'    If InStr(cicloInicio, "/") > 0 And InStr(cicloFim, "/") > 0 Then
'        Split cicloInicio, splitCicloInicio, "/"
'        Split cicloFim, splitCicloFim, "/"
'        If Val(splitCicloFim(1)) < Val(splitCicloInicio(1)) Then
'            verificaCiclos = False
'        Else
'            If Val(splitCicloFim(0)) < Val(splitCicloInicio(0)) Then
'                verificaCiclos = False
'            Else
'                verificaCiclos = True
'            End If
'        End If
'    Else
        If Val(cicloFim) < Val(cicloInicio) Then
            verificaCiclos = False
        Else
            verificaCiclos = True
        End If
'    End If
  

End Function

Private Function ConverterDataDoGrid(data As String) As String
If Trim(data) <> "" Then
    ConverterDataDoGrid = Mid(data, 4, 2) & "/" & Left(data, 2) & "/" & Right(data, 4)
Else
    ConverterDataDoGrid = ""
End If
End Function

'
'SE 7107 - Melhorias no processo de cadastro de abrang�ncias de revistas
'Desenvolvedor: Ricardo Yamamoto - 12/09/2011
Private Sub ObterDadosEstruturaComercial(vCdTipoEstruturaComercial As String, _
                                         vCdEstruturaComercial As String, _
                                         ByRef vCdRegiaoEstrategica As Integer, _
                                         ByRef vCdGerenciaVendas As Integer, _
                                         ByRef vCdSetor As Integer, _
                                         ByRef vNoEstrutura As String)
    Dim sQuery As String
    
    
    On Error GoTo TrataErro
    
    sQuery = "declare @sql_cd_ret int ,"
    sQuery = sQuery & "@sql_cd_opr char(1),"
    sQuery = sQuery & "@sql_nm_tab char(30),"
    sQuery = sQuery & "@sql_qt_lnh int" ',"
    'sQuery = sQuery & "@msg_err Varchar(255)"
                                              
    sQuery = sQuery & " exec st_pdr_estrutura_comercial_s " & vCdTipoEstruturaComercial & ", " _
                                                            & vCdEstruturaComercial & ", " _
                                                            & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out" ', @msg_err out"
    
    
    gObjeto.natPrepareQuery hSql, sQuery
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If

    'sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    'sql_cd_opr = gObjeto.varOutputParam(1)
    'sql_nm_tab = gObjeto.varOutputParam(2)
    'sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar a st_pdr_estrutura_comercial_s " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
       Exit Sub
    End If
    
    
    Do While gObjeto.natFetchNext(hSql)
       vCdRegiaoEstrategica = Val(Trim(gObjeto.varResultSet(hSql, 5)))
       vCdGerenciaVendas = Val(Trim(gObjeto.varResultSet(hSql, 4)))
       vCdSetor = Val(Trim(gObjeto.varResultSet(hSql, 3)))
       vNoEstrutura = Trim(gObjeto.varResultSet(hSql, 2))
    Loop

    
    Exit Sub
    Resume
TrataErro:
    
    MsgBox "Erro ao realizar consulta de estrutura comercial.", vbCritical, "Mensagem"
    
    
    
End Sub

'SE 7107 - Melhorias no processo de importa��o de abrang�ncia de revistas
'Autor: Ricardo Yamamoto - 13/09/2011
Private Function AbrangenciaJaAtivaPCL(vTipoEstruturaComercial As String, _
                                       vEstruturaComercial As String, _
                                       vNumeroLinha As Double, _
                                       ByRef vLinhaDuplicada As Double) As Boolean
    Dim i As Long
    Dim vCodigoMaterial As Variant
    Dim vStatus As Variant
    Dim vEstrutComercialOld As Variant
    Dim vTipoEstrutComercialOld As Variant
    
    AbrangenciaJaAtivaPCL = False
    
    For i = 1 To frmVigenciaPCL.spdMaterial.MaxRows
        
        frmVigenciaPCL.spdMaterial.GetText 1, i, vCodigoMaterial
        frmVigenciaPCL.spdMaterial.GetText 9, i, vStatus
        If frmVigenciaPCL.chkContrLog.Value = vbChecked Then
            frmVigenciaPCL.spdAVIG.GetText 8, i, vTipoEstrutComercialOld
            frmVigenciaPCL.spdAVIG.GetText 9, i, vEstrutComercialOld
        Else
            frmVigenciaPCL.spdAVIG.GetText 8, frmVigenciaPCL.spdAVIG.ActiveRow, vTipoEstrutComercialOld
            frmVigenciaPCL.spdAVIG.GetText 9, frmVigenciaPCL.spdAVIG.ActiveRow, vEstrutComercialOld
        End If
        
        If CStr(ListaDadosImportacaoExcel(vNumeroLinha, COD_MATERIAL)) = vCodigoMaterial And _
           vTipoEstruturaComercial = vTipoEstrutComercialOld And _
           vEstruturaComercial = vEstrutComercialOld And _
           CStr(vStatus) = "ATIVA" Then
            vLinhaDuplicada = i
            AbrangenciaJaAtivaPCL = True
            Exit Function
        End If
    Next i
    
End Function


'SE 7107 - Melhoria no Cadastro de Revistas
'Autor: Ricardo Yamamoto - 13/09/2011
Private Sub AlterarAbrangenciaPCL(vNumLinhaDuplicada As Double, vNumLinha As Double)
    
    Dim vCicloInicioOld As Variant
    Dim vCicloFimOld As Variant
    Dim vDataInicioOld As Variant
    Dim vDataFimOld As Variant
    Dim vPrioridadeOld As Variant
    
    frmVigenciaPCL.spdMaterial.GetText 4, vNumLinhaDuplicada, vCicloInicioOld
    frmVigenciaPCL.spdMaterial.GetText 5, vNumLinhaDuplicada, vDataInicioOld
    frmVigenciaPCL.spdMaterial.GetText 6, vNumLinhaDuplicada, vCicloFimOld
    frmVigenciaPCL.spdMaterial.GetText 7, vNumLinhaDuplicada, vDataFimOld
    frmVigenciaPCL.spdMaterial.GetText 8, vNumLinhaDuplicada, vPrioridadeOld
    
    If (CStr(vCicloInicioOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, CICLO_INICIO))) Or _
       (CStr(vCicloFimOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, CICLO_FIM))) Or _
       (CStr(vDataInicioOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_INICIO))) Or _
       (CStr(vDataFimOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_FIM))) Or _
       (CStr(vPrioridadeOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, PRIORIDADE_PCL))) Then
        
        frmVigenciaPCL.spdMaterial.SetText 4, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, CICLO_INICIO)
        frmVigenciaPCL.spdMaterial.SetText 5, vNumLinhaDuplicada, CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_INICIO))
        frmVigenciaPCL.spdMaterial.SetText 6, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, CICLO_FIM)
        frmVigenciaPCL.spdMaterial.SetText 7, vNumLinhaDuplicada, CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_FIM))
        frmVigenciaPCL.spdMaterial.SetText 8, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, PRIORIDADE_PCL)
        
        If frmVigenciaPCL.chkContrLog.Value = 1 Then
            'ciclo inicio
            frmVigenciaPCL.spdAVIG.SetText 2, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, CICLO_INICIO)
            'data inicio
            frmVigenciaPCL.spdAVIG.SetText 3, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, DATA_INICIO)
            'ciclo fim
            frmVigenciaPCL.spdAVIG.SetText 4, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, CICLO_FIM)
            'data final
            frmVigenciaPCL.spdAVIG.SetText 5, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, DATA_FIM)
        End If
        frmVigenciaPCL.spdMaterial.SetText 13, vNumLinhaDuplicada, "A"
    End If
    
End Sub

'SE 7107 - Melhoria no Cadastro de Revistas
'Autor: Ricardo Yamamoto - 13/09/2011
Private Function FormatarCiclo(vCiclo As String) As String
    If Trim(vCiclo) = "" Then
        FormatarCiclo = ""
    Else
        If InStr(1, vCiclo, "/") = 0 Then
            FormatarCiclo = Right(vCiclo, 2) & "/" & Left(vCiclo, 4)
            
        Else
            FormatarCiclo = vCiclo
        End If
    
    End If
End Function


'SE 7107 - Melhorias no Cadastro de Revistas
'Autor: Ricardo Yamamoto
Private Function TipoEstrutComercial(vCdTipoEstruturaComercial As String) As String
    Select Case vCdTipoEstruturaComercial
           Case "0"
                TipoEstrutComercial = ""
           Case "1"
                TipoEstrutComercial = "GM"
           Case "2"
                TipoEstrutComercial = "RE"
           Case "3"
                TipoEstrutComercial = "GV"
           Case "4"
                TipoEstrutComercial = "ST"
           Case "5"
                TipoEstrutComercial = "GR"
    End Select
End Function


'SE 7107 - Melhorias no processo de importa��o de abrang�ncia de revistas
'Autor: Ricardo Yamamoto - 17/09/2011
Private Function AbrangenciaJaAtivaCom(vTipoEstruturaComercial As String, _
                                       vEstruturaComercial As String, _
                                       ByRef vLinhaDuplicada As Double) As Boolean
    Dim i As Long
    Dim vStatus As Variant
    Dim vEstrutComercialOld As Variant
    Dim vTipoEstrutComercialOld As Variant
    
    AbrangenciaJaAtivaCom = False
    
    For i = 1 To frmVigenciaComercial.spdAVIG.MaxRows
        
        frmVigenciaComercial.spdAVIG.GetText 6, i, vStatus
        frmVigenciaComercial.spdAVIG.GetText 8, i, vTipoEstrutComercialOld
        frmVigenciaComercial.spdAVIG.GetText 9, i, vEstrutComercialOld
        
        If vTipoEstruturaComercial = vTipoEstrutComercialOld And _
           vEstruturaComercial = vEstrutComercialOld And _
           CStr(vStatus) = "ATIVA" Then
            vLinhaDuplicada = i
            AbrangenciaJaAtivaCom = True
            Exit Function
        End If
    Next i
    
End Function


'SE 7107 - Melhoria no Cadastro de Revistas
'Autor: Ricardo Yamamoto - 13/09/2011
Private Sub AlterarAbrangenciaCom(vNumLinhaDuplicada As Double, vNumLinha As Double)
    
    Dim vCicloInicioOld As Variant
    Dim vCicloFimOld As Variant
    Dim vDataInicioOld As Variant
    Dim vDataFimOld As Variant
    Dim vPrioridadeOld As Variant
    
    frmVigenciaComercial.spdAVIG.GetText 2, vNumLinhaDuplicada, vCicloInicioOld
    frmVigenciaComercial.spdAVIG.GetText 3, vNumLinhaDuplicada, vDataInicioOld
    frmVigenciaComercial.spdAVIG.GetText 4, vNumLinhaDuplicada, vCicloFimOld
    frmVigenciaComercial.spdAVIG.GetText 5, vNumLinhaDuplicada, vDataFimOld
    
    If (CStr(vCicloInicioOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, CICLO_INICIO))) Or _
       (CStr(vCicloFimOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, CICLO_FIM))) Or _
       (CStr(vDataInicioOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_INICIO))) Or _
       (CStr(vDataFimOld) <> CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_FIM))) Then
        
        frmVigenciaComercial.spdAVIG.SetText 2, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, CICLO_INICIO)
        frmVigenciaComercial.spdAVIG.SetText 3, vNumLinhaDuplicada, CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_INICIO))
        frmVigenciaComercial.spdAVIG.SetText 4, vNumLinhaDuplicada, ListaDadosImportacaoExcel(vNumLinha, CICLO_FIM)
        frmVigenciaComercial.spdAVIG.SetText 5, vNumLinhaDuplicada, CStr(ListaDadosImportacaoExcel(vNumLinha, DATA_FIM))
        frmVigenciaComercial.spdAVIG.SetText 17, vNumLinhaDuplicada, "A"
    End If
    
End Sub

