VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ArquivoParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Const strNULL = ""       'constante de string nula

Private bRetorno As Boolean  ' retorno de funcoes

Private Function fPesquisaArqIni(ByRef Aplic As Object _
                              , ByRef sArquivoINI As String) As Boolean
'---
' Pesquisa arquivo INI da aplicacao corrente
' Retorna o nome do arquivo Ini
' MAR/1998 - HARUO
'---

   sArquivoINI = Aplic.Path & "\" & Aplic.EXEName & ".ini"
   If Dir(sArquivoINI) = "" Then
      MsgBox "Arquivo " & Aplic.EXEName & ".ini" & " n�o encontrado.", vbOKOnly, "ATEN��O"
      bRetorno = False
   Else
      bRetorno = True
   End If
   
   fPesquisaArqIni = bRetorno 'Seta retorno da funcao

End Function

Private Function fAliasSqlIni(ByVal sTexto As String _
                            , ByVal sAlias As String _
                            , ByRef sServer As String _
                            , ByRef sDatabase As String) As Boolean
'---
' Pesquisa alias no arquivo SQL.INI
' MAR/1998 - HARUO
'---
Dim nPosTexto As Integer
Dim sAliasServerDatabase As String

   nPosTexto = InStr(sTexto, sAlias)
   If nPosTexto > 0 Then
      nPosTexto = InStr(sTexto, "=")
      If nPosTexto > 0 Then
         sAliasServerDatabase = Mid(sTexto, nPosTexto + 1, 30)
         nPosTexto = InStr(sAliasServerDatabase, ",")
            ' nome do servidor = string do banco, posicao inicial, e posicao final
            sServer = Mid(sAliasServerDatabase, nPosTexto + 1, InStr(Mid(sAliasServerDatabase, nPosTexto + 1, 255), ",") - 1)
          '  sServer = Mid(sAliasServerDatabase, Len(Mid(sAliasServerDatabase, 14, 10)) + 2, InStr(Mid(sAliasServerDatabase, nPosTexto + 1, 255), ",") - 1)
         If sServer <> strNULL Then
            sDatabase = Mid(sAliasServerDatabase, Len(Mid(sAlias, 14, 10) & "," & sServer) + 2, 255)
         End If
         bRetorno = True
      Else
         bRetorno = False
      End If
   Else
      bRetorno = False
   End If

   fAliasSqlIni = bRetorno 'Seta retorno da funcao

End Function

Public Function GetServerDatabase(ByVal sAlias As String, ByRef sServer As String, ByRef sDatabase As String) As Integer
'---
' Retorna o Server e Banco de dados do alias informado
' Retornos:
'     0 - Ok
'     1 - Erro na leitura do arquivo
' MAR/1998 - HARUO
'---
Dim sArquivoINI As String
Dim sTextoLinha As String
Dim nRetorno As Integer

   sArquivoINI = Environ("SQLBASE") & "\sql.ini"  'Set arquivo SQL.INI
   
   nRetorno = 1
   Open sArquivoINI For Input As #1 ' Open file.
   On Error GoTo ErroLeitura
   Do While Not EOF(1) ' Loop until end of file.
       Line Input #1, sTextoLinha ' Read line into variable.
       If fAliasSqlIni(sTextoLinha, "remotedbname=" & sAlias, sServer, sDatabase) Then
          nRetorno = 0
          Exit Do
       Else
          nRetorno = 2
       End If
   Loop
   Close #1    ' Close file.
   
   GetServerDatabase = nRetorno
      
ErroLeitura:
       
End Function

Public Function GetAplicIni(ByRef aApp As Object _
                          , ByVal sSessao As String _
                          , ByRef sAlias As String _
                          , ByRef sServer As String _
                          , ByRef sDatabase As String) As Integer
'---
' Le arquivo APLIC.INI (Sessao e Alias)
' e X:\PRODUCAO\SQL.INI (Server e Database)
' Retornos
'     0 - Execu��o OK
'     1 - Arquivo APLIC.INI n�o encontrado
'     2 - Arquivo SQL.INI n�o encontrado ou corrompido
'     3 - Arquivo APLIC.INI n�o encontrado ou corrompido
'     4 - Alias n�o foi encontrado no arquivo SQL.INI
' MAR/1998 - HARUO
'---
   Dim nRetorno    As Integer
   Dim sArqIni     As String           'Arquivo INI
   Dim sAliasSaida As String * 32 'Alias banco dados
   Dim nContador   As Integer
   Dim sCaracter   As String
   Dim nLeituraIni As Integer
         
   nRetorno = 0
   
   If Not fPesquisaArqIni(aApp, sArqIni) Then
      nRetorno = 1
      GoTo SairGetAplicIni
   End If
   If GetPrivateProfileString(sSessao, "dbalias", " ", sAliasSaida, Len(sAliasSaida), sArqIni) < 0 Then
      nRetorno = 2
      GoTo SairGetAplicIni
   End If
   sAlias = ""
   For nContador = 1 To 32
       sCaracter = Mid(sAliasSaida, nContador, 1)
       If sCaracter > " " Then
          sAlias = sAlias + sCaracter
       End If
   Next
   
   nLeituraIni = GetServerDatabase(sAlias, sServer, sDatabase)
   If nLeituraIni = 1 Then
      nRetorno = 3
      GoTo SairGetAplicIni
   ElseIf nLeituraIni = 2 Then
      nRetorno = 4
      GoTo SairGetAplicIni
   End If
      
SairGetAplicIni:
   GetAplicIni = nRetorno
   
End Function

Public Function GetErroAplicIni(ByVal nErroAplicIni As Integer) As String
'---
' Retorna o erro na fun��o GetErroAplicIni
' MAR/1998 - HARUO
'---
Dim sRetorno As String
   
   If nErroAplicIni = 1 Then
      sRetorno = "Arquivo de Configura��o da Aplica��o n�o pode ser aberto"
   ElseIf nErroAplicIni = 2 Then
      sRetorno = "Arquivo X:\PRODUCAO\SQL.INI n�o encontrado ou Alias n�o encontrado"
   ElseIf nErroAplicIni = 3 Then
      sRetorno = "Erro na leitura do arquivo de Configura��o da Aplica��o"
   ElseIf nErroAplicIni = 4 Then
      sRetorno = "Alias do banco de dados n�o encontrado no arquivo SQL.INI"
   Else
      sRetorno = ""
   End If
   
   GetErroAplicIni = sRetorno

End Function


