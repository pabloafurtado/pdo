VERSION 5.00
Begin VB.Form frmCadCategoriaProduto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cadastro de Categoria de Produto"
   ClientHeight    =   2340
   ClientLeft      =   2145
   ClientTop       =   5835
   ClientWidth     =   7680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmCadCategoriaProduto.frx":0000
   ScaleHeight     =   2340
   ScaleWidth      =   7680
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1245
      Left            =   30
      TabIndex        =   4
      Top             =   0
      Width           =   7575
      Begin VB.TextBox txtCodigo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   0
         Top             =   180
         Width           =   1545
      End
      Begin VB.TextBox txtDescricao 
         Enabled         =   0   'False
         Height          =   360
         Left            =   1245
         TabIndex        =   1
         Top             =   660
         Width           =   4935
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   240
         Left            =   150
         TabIndex        =   6
         Top             =   270
         Width           =   660
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o "
         Height          =   240
         Left            =   150
         TabIndex        =   5
         Top             =   720
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1035
      Left            =   1410
      TabIndex        =   2
      Top             =   1230
      Width           =   4575
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   645
         Left            =   2460
         Picture         =   "frmCadCategoriaProduto.frx":0542
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   270
         Width           =   1425
      End
      Begin VB.CommandButton cmdSalvar 
         Caption         =   "Salvar"
         Enabled         =   0   'False
         Height          =   645
         Left            =   690
         Picture         =   "frmCadCategoriaProduto.frx":084C
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   270
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmCadCategoriaProduto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vOper    As String * 1
Dim vAlterar As Boolean
Dim vCodigo  As String
Dim vDescricao As String
Dim vOK        As Integer
Dim cSql          As String

Public Sub Incluir(maxCodigo As Long)
    txtCodigo.Text = maxCodigo + 1
    txtCodigo.Locked = True
    txtdescricao.Enabled = True
    vAlterar = True
    vOper = "I"
    Me.Show 1
End Sub

Public Sub Alterar(pCodigo As Long, pDescricao As String)
    txtdescricao.Enabled = True
    txtCodigo.Text = pCodigo
    txtdescricao.Text = pDescricao
    vAlterar = True
    vOper = "A"
    Me.Show 1
End Sub

Private Sub SelCodigo()
    txtCodigo.SelStart = 0
    txtCodigo.SelLength = Len(txtCodigo.Text)
End Sub

Private Sub Gravar()
    vCodigo = txtCodigo.Text
    vDescricao = txtdescricao.Text
    vOK = vbCancel
    
    'chama a procedure de cancelamento da Abrangencia Vigencia
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    
    If vOper = "A" Then
        cSql = cSql & " exec stp_pdr_cad_categ_u " & Val(vCodigo) & ",'" & vDescricao & "', "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    Else
        cSql = cSql & " exec stp_pdr_cad_categ_i " & vCodigo & ",'" & vDescricao & "', "
        cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
        cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    End If
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_cad_categ_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Sub
    End If
           
    MsgBox "Grava��o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
    
    vOK = vbOK
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    vOK = vbCancel
    Unload Me
End Sub

Private Sub cmdSalvar_Click()
    Call Gravar
End Sub

Private Sub Form_Load()
    vOK = vbCancel
End Sub

Private Sub txtDescricao_Change()
    If vAlterar Then
        cmdSalvar.Enabled = True
    End If
End Sub

Private Sub LimpaCampos()
    txtCodigo.Text = ""
    txtCodigo.Locked = False
    txtdescricao.Text = ""
    txtdescricao.Enabled = False
    cmdSalvar.Enabled = False
End Sub

Public Function codigoCad() As String
    codigoCad = vCodigo
End Function

Public Function descricaoCad() As String
    descricaoCad = vDescricao
End Function

Public Function OK() As Integer
    OK = vOK
End Function
