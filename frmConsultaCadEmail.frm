VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form frmConsultaCadEmail 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de emails"
   ClientHeight    =   4800
   ClientLeft      =   3300
   ClientTop       =   2850
   ClientWidth     =   7590
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4800
   ScaleWidth      =   7590
   Begin ComctlLib.Toolbar barFerramenta 
      Height          =   630
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7560
      _ExtentX        =   13335
      _ExtentY        =   1111
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   5
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "incluir"
            Object.ToolTipText     =   "Inclus�o"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "alterar"
            Object.ToolTipText     =   "Altera��o"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "Excluir"
            Object.ToolTipText     =   "Cancelamento"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "sair"
            Object.ToolTipText     =   "Sair "
            Object.Tag             =   "Sair"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin FPSpread.vaSpread grdCad 
      Height          =   4065
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   7515
      _Version        =   131077
      _ExtentX        =   13256
      _ExtentY        =   7170
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   3
      MaxRows         =   1
      OperationMode   =   5
      ScrollBarExtMode=   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmConsultaCadEmail.frx":0000
      UserResize      =   2
      VisibleCols     =   5
      VisibleRows     =   20
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":0297
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":05B1
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":08CB
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":0BE5
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":0EFF
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":1219
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":1533
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadEmail.frx":184D
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConsultaCadEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim X As Double
Dim vLinha As String
Dim vCodigo As Long
Dim vDescricao As String
Dim vEmails As String
Dim cSql          As String

Private Sub barFerramenta_ButtonClick(ByVal Button As ComctlLib.Button)

    Select Case Button.Index
'        Case 1
'            Call Incluir
        Case 2
            Call Alterar
'        Case 3
'            Call Excluir
        Case 5
             Unload Me
    End Select
    
End Sub

Private Sub Incluir()
    frmCadEmail.Incluir
    If frmCadEmail.OK Then
        grdCad.MaxRows = grdCad.MaxRows + 1
        grdCad.Row = grdCad.MaxRows
        grdCad.Col = 1
        grdCad.Text = frmCadEmail.codigoCad
        grdCad.Col = 2
        grdCad.Text = frmCadEmail.descricaoCad
    End If
    
    DesmarcaGrid grdCad

End Sub

Private Sub Alterar()

    vCodigo = 0

    For X = 1 To grdCad.MaxRows
        grdCad.Row = X
        If grdCad.SelModeSelected Then
            grdCad.Col = 1
            vCodigo = grdCad.Text
            grdCad.Col = 2
            vDescricao = grdCad.Text
            grdCad.Col = 3
            vEmails = grdCad.Text
            vLinha = X
            Exit For
        End If
    Next X
    
    If vCodigo = 0 Then
        MsgBox "Nenhum registro selecionado, favor selecionar um registro para altera��o!", vbInformation, Me.Caption
        Exit Sub
    End If

    Call frmCadEmail.Alterar(vCodigo, vDescricao, vEmails)
    If frmCadEmail.OK Then
        grdCad.Row = vLinha
        grdCad.Col = 2
        grdCad.Text = vDescricao
        grdCad.Col = 3
        grdCad.Text = vEmails
    End If

    DesmarcaGrid grdCad

End Sub

Private Sub Pesquisar()


    Dim vSequencia As Variant
    Dim avData()   As Variant
    Dim X          As Integer
    Dim vLin       As Integer
    Dim sLinha     As String
    
    ReDim avData(0, 0)
      

    grdCad.Visible = False
    
    grdCad.MaxRows = 0
       
    cSql = "stp_pdr_cons_email_s "
        
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
    If bResp = False Then
       Exit Sub
    End If
        
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Sub
    End If
   
    If Val(avData(0, 0)) <= 0 Then Exit Sub
    
   
    'Colunas do Spread
    '01 - C�d pendencia
    '02 - descri��o pendencia
    '03 - emails cadastrados
    
    grdCad.MaxRows = 0
    
    vLin = 0
    sLinha = ""
   
    For X = 0 To UBound(avData, 2)
    
        vLin = vLin + 1
   
        grdCad.MaxRows = vLin
       
        'c�d. pendencia
        grdCad.SetText 1, vLin, avData(0, X)
        'descricao email
        grdCad.SetText 2, vLin, avData(1, X)
        'emails cadastrados para receber
        grdCad.SetText 3, vLin, avData(2, X)
    Next
   
    grdCad.Visible = True

    DesmarcaGrid grdCad

End Sub

Private Sub Excluir()
    Dim vLinha As String

    vCodigo = 0
    
    For X = 1 To grdCad.MaxRows
        grdCad.Row = X
        If grdCad.SelModeSelected Then
            grdCad.Col = 1
            vCodigo = grdCad.Text
            vLinha = X
            Exit For
        End If
    Next X
    
    If vCodigo = 0 Then
        MsgBox "Nenhum registro selecionado, favor selecionar um registro para esclus�o!!", vbInformation, Me.Caption
        Exit Sub
    End If

    If MsgBox("Confirma a exclus�o do registro selecionado?", vbQuestion + vbDefaultButton2 + vbYesNo, "A T E N � � O") = vbYes Then
        Call ExcluirLinha
        MsgBox "Exclus�o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
    End If

    DesmarcaGrid grdCad

End Sub

Private Sub ExcluirLinha()
    For X = 1 To grdCad.MaxRows
        grdCad.Row = X
        If grdCad.SelModeSelected Then
            grdCad.Action = 5
            grdCad.MaxRows = grdCad.MaxRows - 1
        End If
    Next X
End Sub

Private Sub Form_Load()
    Me.Show
    Call Pesquisar

End Sub

Private Sub grdCad_DblClick(ByVal Col As Long, ByVal Row As Long)

    Call Alterar

End Sub
