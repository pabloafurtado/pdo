VERSION 5.00
Begin VB.Form FrmCriaLista 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Criar Nova Lista de Pre�os"
   ClientHeight    =   2145
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8625
   ControlBox      =   0   'False
   Icon            =   "FrmCriaLista.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2145
   ScaleWidth      =   8625
   Begin VB.CommandButton cmbCancela 
      Caption         =   "&Cancela"
      Height          =   495
      Left            =   6120
      TabIndex        =   11
      Top             =   1200
      Width           =   2295
   End
   Begin VB.CommandButton cmbGeraLista 
      Caption         =   "&Gera Lista"
      Height          =   495
      Left            =   6120
      TabIndex        =   10
      Top             =   360
      Width           =   2295
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo de Gera��o"
      Height          =   1845
      Left            =   3480
      TabIndex        =   7
      Top             =   120
      Width           =   2415
      Begin VB.OptionButton opt 
         Caption         =   "&Copiar Lista"
         Height          =   375
         Index           =   1
         Left            =   360
         TabIndex        =   9
         Top             =   1080
         Width           =   1695
      End
      Begin VB.OptionButton opt 
         Caption         =   "&Refer�ncia"
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   8
         Top             =   480
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados da Gera��o"
      Height          =   1845
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3375
      Begin VB.TextBox txtcodlista 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000000&
         Height          =   375
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   840
         Width           =   1515
      End
      Begin VB.ComboBox cmbLista 
         Height          =   315
         ItemData        =   "FrmCriaLista.frx":000C
         Left            =   1560
         List            =   "FrmCriaLista.frx":0013
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1320
         Width           =   1545
      End
      Begin VB.ComboBox cmbCiclo 
         Height          =   315
         ItemData        =   "FrmCriaLista.frx":0021
         Left            =   1560
         List            =   "FrmCriaLista.frx":0028
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   1545
      End
      Begin VB.Label lblPrecoCar 
         Caption         =   "Cd. Lista:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label Label2 
         Caption         =   "Lista a copiar:"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   1380
         Width           =   1395
      End
      Begin VB.Label Label1 
         Caption         =   "Ciclo/Ano:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   540
         Width           =   915
      End
   End
End
Attribute VB_Name = "FrmCriaLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim QtdPrecoFuturo         As Integer
Private Sub cmbCancela_Click()
    Unload Me
End Sub

Private Sub cmbCiclo_Click()
    ' busca proximo codigo de lista
    If cmbCiclo.ListIndex <> -1 Then
        txtcodlista.Text = Prox_Cod_Lista(CLng(Right(cmbCiclo, 4) + Left(cmbCiclo, 2))) + 1
    
    
    ' carrega a lista disponiveis
    Carrega_Combo_Lista
    End If
    
End Sub

Private Sub cmbGeraLista_Click()
    Dim dc_lista As String
    Dim avData()    As Variant
    Dim cSql        As String
    Dim bResp       As Boolean
    Dim cdAnt       As Long
    Dim X           As Long
    Dim Y           As Long
    Dim se_Usuario  As String
    Dim ne_ret     As Long
    Dim v_tipo      As Integer
    Dim v_lista As Integer
    Dim vAtualizaCicloFuturo As Boolean


    ' Validar campos
    If Trim$(cmbCiclo.Text) = "" Then
        MsgBox "Ciclo / Ano deve ser preenchido !", vbInformation, "A T E N � � O"
        cmbCiclo.SetFocus
        Exit Sub
    End If
    
    ' verifica se foi escolhida alguma copia
    If opt(0).Value = False And opt(1).Value = False Then
        MsgBox "Algum tipo de c�pia deve ser selecionado", vbInformation, "A T E N � � O"
        Exit Sub
    End If
    
    ' Lista somente � obrigatoria para copia de lista
    If Trim$(cmbLista.Text) = "" And opt(1).Value = True Then
        MsgBox "Lista deve ser preenchida !", vbInformation, "A T E N � � O"
        cmbLista.SetFocus
        Exit Sub
    End If
    
    
    ' Verifica se existe lista sem estrutura comercial
    dc_lista = Verifica_Lista_Est(Right(cmbCiclo, 4) + Left(cmbCiclo, 2))
    If dc_lista <> "" Then
        MsgBox "As listas " & dc_lista & " ainda n�o foram utilizadas no ciclo/ano: " & Right(cmbCiclo, 4) + Left(cmbCiclo, 2) & ". A nova lista n�o ser� criada", vbInformation, "A T E N � � O"
        'cmbLista.SetFocus
        Exit Sub
    End If
    ' fim
    
    vAtualizaCicloFuturo = False
    
    If FU_BuscaPrecoFuturoLista(Right(cmbCiclo, 4) + Left(cmbCiclo, 2), (txtcodlista - 1), MSG$) = FAIL% Then
         MsgBox MSG$, vbCritical, "A T E N � � O"
         PonteiroMouse vbNormal
    Else
         ' 999 - n�o possui nenhum ciclo apois o ciclo selecionado
         
         If QtdPrecoFuturo <> 999 Then
            If QtdPrecoFuturo > 0 Then
                If MsgBox("Pre�o refer�ncia gerado para ciclos futuros ! Deseja replicar as altera��es para estes ciclos ? ", vbQuestion + vbYesNo + vbDefaultButton2, "A T E N � � O") = vbYes Then
                    vAtualizaCicloFuturo = True
                End If
            Else
                MsgBox "Pre�o refer�ncia gerado para ciclos futuros. Quantidade de listas nos ciclos futuros diferente do ciclo " & Right(cmbCiclo, 4) + Left(cmbCiclo, 2) & ". As altera��es n�o poder�o ser replicadas para estes ciclos", vbInformation, "A T E N � � O"
                vAtualizaCicloFuturo = False
            End If
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents
    Me.Refresh
    DoEvents
    
    
    If opt(1).Value = True Then
        v_tipo = 1
        v_lista = cmbLista.Text
    Else
        v_tipo = 0
        v_lista = 0
    End If
    
    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & "exec stp_pdr_copia_lista_i " & cmbCiclo.ItemData(cmbCiclo.ListIndex) & " , "
    cSql = cSql & v_tipo & " , "
    cSql = cSql & v_lista & ", " & txtcodlista & ", "
    cSql = cSql & "'" & se_Usuario & "', "
    cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"

    If gObjeto.natPrepareQuery(hSql, cSql) Then
       Do While gObjeto.natFetchNext(hSql)
       Loop
    End If
      
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Sub
    End If
    
    sql_cd_ret = Val(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = Val(gObjeto.varOutputParam(3))
     
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar stp_pdr_copia_lista_i - RETURN STATUS = " & _
               sql_cd_ret & " - " & sql_cd_opr & " - " & sql_nm_tab
       Exit Sub
    End If
        
    If sql_qt_lnh = 0 Then
        Screen.MousePointer = vbNormal
        Refresh
        MsgBox "N�o existem produtos a relacionar para este ciclo / meio / lista!", vbCritical
        Exit Sub
    End If
    
    ' copia dados para ciclos futuros
    If vAtualizaCicloFuturo = True Then
        
        se_Usuario = Space(255)
        ne_ret = GetUserName(se_Usuario, 255)
        se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    
        cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
        cSql = cSql & "exec stp_pdr_copia_lista_fut_i " & cmbCiclo.ItemData(cmbCiclo.ListIndex) & " , "
        cSql = cSql & v_tipo & " , "
        cSql = cSql & v_lista & ", " & txtcodlista & ", "
        cSql = cSql & "'" & se_Usuario & "', "
        cSql = cSql & "@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"

        If gObjeto.natPrepareQuery(hSql, cSql) Then
            Do While gObjeto.natFetchNext(hSql)
            Loop
        End If
      
        'If gObjeto.nErroSQL <> 0 Then
        '    MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        '    Exit Sub
        'End If
    
        sql_cd_ret = Val(gObjeto.varOutputParam(0))
        sql_cd_opr = gObjeto.varOutputParam(1)
        sql_nm_tab = gObjeto.varOutputParam(2)
        sql_qt_lnh = Val(gObjeto.varOutputParam(3))
     
        If sql_cd_ret <> 0 Then
            MsgBox "Erro ao executar stp_pdr_copia_lista_fut_i - RETURN STATUS = " & _
               sql_cd_ret & " - " & sql_cd_opr & " - " & sql_nm_tab
            Exit Sub
        End If
    End If
    
    
    'If FU_BuscaPrecoBase(Right(cmbCiclo, 4) + Left(cmbCiclo, 2), MSG$, False) = FAIL% Then
    '     MsgBox MSG$, vbCritical, "A T E N � � O"
    '     PonteiroMouse vbNormal
    'Else
    '    If QtdPrecoBase > 0 Then
    '        MsgBox "Relat�rio Bases de Pre�os j� foi gerado !!! Necess�rio gerar novamente!! ", vbInformation, "A T E N � � O"
    '    End If
    'End If
               
    Screen.MousePointer = vbNormal
    DoEvents
    Me.Refresh
    DoEvents
    
    MsgBox "Processo de Cria��o da nova Lista de Pre�os " & txtcodlista.Text & " conclu�do com sucesso!", vbInformation
       
       
    ' busca proximo codigo de lista
    txtcodlista.Text = ""
    opt(0).Value = False
    opt(1).Value = False
    cmbCiclo.ListIndex = -1
    cmbLista.ListIndex = -1
    
End Sub
Private Function FU_BuscaPrecoFuturoLista(nm_ciclo_operacional As String, cd_lista As String, MSG$) As Integer

    Dim sql As String

    FU_BuscaPrecoFuturoLista = FAIL%

    On Error GoTo FU_BuscaPrecoFuturoLista

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_preco_futuro_lista_s " & nm_ciclo_operacional & "," & cd_lista & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0
    QtdPrecoFuturo = 999

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            If gObjeto.varResultSet(hSql, 0) = cd_lista Then
                QtdPrecoFuturo = QtdPrecoFuturo + 1
            Else
                QtdPrecoFuturo = 0
                Exit Do
            End If
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a stp_pdr_preco_futuro_lista_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    FU_BuscaPrecoFuturoLista = SUCCED%
    Exit Function

FU_BuscaPrecoFuturoLista:
    MSG$ = "FU_BuscaPrecoFuturoLista - Erro (" & Err & ") - " & Error
    Exit Function

End Function
Private Function Verifica_Lista_Est(nm_ciclo_operacional As String) As String
    Dim sql     As String
    Dim X       As Long
    Dim dc_lista As String

    Verifica_Lista_Est = ""
    dc_lista = ""

    On Error GoTo Verifica_Lista_Est

    sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
    sql = sql & " exec stp_pdr_ver_lista_est_s " & nm_ciclo_operacional & _
    ",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

    sql_qt_lnh = 0

    If gObjeto.natPrepareQuery(hSql, sql) Then
        Do While gObjeto.natFetchNext(hSql)
            sql_qt_lnh = sql_qt_lnh + 1
            If dc_lista = "" Then
                dc_lista = gObjeto.varResultSet(hSql, 0)
            Else
                dc_lista = dc_lista & ", " & gObjeto.varResultSet(hSql, 0)
            End If
                                      
                'txtprecobase.Text = Str(gObjeto.varResultSet(hSql, 2))
                'txtprecobase.Text = ConvertePontoDecimal(txtprecobase, 0)
                'txtprecobase.Text = Format(txtprecobase, "#0.00")
                'txtprecobase.Tag = txtprecobase.Text
               '
                'gPrecoBase = True
                'txt_redutor.Text = gObjeto.varResultSet(hSql, 3)
                'txt_redutor.Text = ConvertePontoDecimal(txt_redutor, 0)
                
                'For X = 0 To cmbStatus.ListCount - 1
                '    If cmbStatus.ItemData(X) = gObjeto.varResultSet(hSql, 1) Then
                '        cmbStatus.ListIndex = X
                '    Exit For
                'End If
                'Next X
                'txtstatus.Text = gObjeto.varResultSet(hSql, 1)
                'txtpontos.Text = gObjeto.varResultSet(hSql, 5)
        Loop
    End If

    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a st_pi_cn_preco_base_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If

    Verifica_Lista_Est = dc_lista
    Exit Function

Verifica_Lista_Est:
    MSG$ = "Verifica_Lista_Est - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Sub Form_Load()
    ' carrega combos com ciclos
    Call CarrCicloComPreco(cmbCiclo)
    

End Sub

Private Function FU_BuscaPrecoBase(nm_ciclo_operacional As String, MSG$, Lista As Boolean) As Integer

Dim sql As String

FU_BuscaPrecoBase = FAIL%

On Error GoTo FU_BuscaPrecoBase

sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int"
sql = sql & " exec stp_pdr_base_preco_s " & nm_ciclo_operacional & _
",@sql_cd_ret out,@sql_cd_opr out,@sql_nm_tab out,@sql_qt_lnh out"

sql_qt_lnh = 0
QtdPrecoBase = 0

If gObjeto.natPrepareQuery(hSql, sql) Then
    Do While gObjeto.natFetchNext(hSql)
        QtdPrecoBase = gObjeto.varResultSet(hSql, 0)
    Loop
End If

If gObjeto.nErroSQL <> 0 Then
    MSG$ = "Erro ao executar a stp_prd_base_preco_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
    Exit Function
End If


FU_BuscaPrecoBase = SUCCED%
Exit Function

FU_BuscaPrecoBase:
    MSG$ = "FU_BuscaPrecoBase - Erro (" & Err & ") - " & Error
    Exit Function

End Function


Private Sub Form_Unload(Cancel As Integer)
    MDIpdo001.DesbloqueiaMenu
End Sub


Public Sub Carrega_Combo_Lista()
    Dim se_Sql As String
    Dim sql_cd_ret As Integer
    Dim sql_qt_lnh As Integer
    Dim work As String

    cmbLista.Clear
   
    On Error GoTo Combo_Lista

    PonteiroMouse vbHourglass
   
   'se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
   'se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
   'substituido o segmento canal da tela por "1" - fixo -
   'se_Sql = se_Sql & " exec  st_pi_lista_s " & nm_ciclo_operacional & ", 1, "
   'se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
   
   
   se_Sql = " SELECT DISTINCT cd_lista   From t_pi_lista_ciclo  Where nm_ciclo_operacional = " & cmbCiclo.ItemData(cmbCiclo.ListIndex) & "  ORDER BY cd_lista asc"
   
   'se_Sql = "SELECT  DISTINCT cd_lista From t_pi_lista_ciclo WHERE   cd_segmento_canal   in  (1,2,3) ORDER BY cd_lista ASC"
   
   If gObjeto.natPrepareQuery(hSql, se_Sql) Then
      Do While gObjeto.natFetchNext(hSql)
         cmbLista.AddItem gObjeto.varResultSet(hSql, 0)
         'work = gObjeto.varResultSet(hSql, 1)
         'Controle.ItemData(Controle.NewIndex) = ConvertePontoDecimal(work, 0)
      Loop
   End If
   
   If gObjeto.nErroSQL <> 0 Then
      MSG$ = "Erro ao executar a Combo_Lista " & gObjeto.nServerNum & gObjeto.sServerMsg
      PonteiroMouse vbNormal
      Exit Sub
   End If
    
'   sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'   sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
'   If sql_cd_ret <> 0 Then
'      MSG$ = "Erro ao executar a Combo_Lista " & "RETURN STATUS = " & sql_cd_ret
'      PonteiroMouse vbNormal
'      Exit Function
'   End If
         
   PonteiroMouse vbNormal
   Exit Sub
   
Combo_Lista:
   MSG$ = "Combo_Lista - Erro (" & Err & ") - " & Error
   PonteiroMouse vbNormal
   Exit Sub

End Sub

Public Function Verifica_Ciclos_Futuros(v_ciclo As String, v_cd_lista As String) As Boolean
    
    
End Function
