VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmConsultaCadCategoria 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Categoria de Produto"
   ClientHeight    =   4800
   ClientLeft      =   2955
   ClientTop       =   2175
   ClientWidth     =   6165
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4800
   ScaleWidth      =   6165
   Begin ComctlLib.Toolbar barFerramenta 
      Height          =   630
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   7680
      _ExtentX        =   13547
      _ExtentY        =   1111
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   5
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "incluir"
            Object.ToolTipText     =   "Inclus�o"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "alterar"
            Object.ToolTipText     =   "Altera��o"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "Excluir"
            Object.ToolTipText     =   "Cancelamento"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "sair"
            Object.ToolTipText     =   "Sair "
            Object.Tag             =   "Sair"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin FPSpread.vaSpread grdCad 
      Height          =   4065
      Left            =   30
      TabIndex        =   1
      Top             =   660
      Width           =   6135
      _Version        =   131077
      _ExtentX        =   10821
      _ExtentY        =   7170
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   2
      MaxRows         =   0
      OperationMode   =   5
      ScrollBarExtMode=   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmConsultaCadCategoria.frx":0000
      UserResize      =   2
      VisibleCols     =   5
      VisibleRows     =   20
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":03BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":06D8
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":09F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":0D0C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":1026
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":1340
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":165A
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmConsultaCadCategoria.frx":1974
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConsultaCadCategoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim X           As Double
Dim vCodigo     As Long
Dim vLinha      As String
Dim avData()    As Variant
Dim cSql          As String

Private Sub barFerramenta_ButtonClick(ByVal Button As ComctlLib.Button)

    Select Case Button.Index
        Case 1
            Call Incluir
        Case 2
            Call Alterar
        Case 3
            Call Excluir
        Case 5
             Unload Me
    End Select
    
End Sub

Private Sub Incluir()
    
    Dim maxCod As Long
    maxCod = 0
    
    For X = 1 To grdCad.MaxRows
        grdCad.Row = X
        grdCad.Col = 1
        If Val(grdCad.Text) > maxCod Then
            maxCod = Val(grdCad.Text)
        End If
    Next X
    
    frmCadCategoriaProduto.Incluir (maxCod)
    If frmCadCategoriaProduto.OK = vbOK Then
        grdCad.MaxRows = grdCad.MaxRows + 1
        grdCad.Row = grdCad.MaxRows
        grdCad.Col = 1
        grdCad.Text = frmCadCategoriaProduto.codigoCad
        grdCad.Col = 2
        grdCad.Text = frmCadCategoriaProduto.descricaoCad
    End If

    DesmarcaGrid grdCad

End Sub

Private Sub Alterar()

    Dim vDescricao As String

    vCodigo = 0
    
    For X = 1 To grdCad.MaxRows
        grdCad.Row = X
        If grdCad.SelModeSelected Then
            grdCad.Col = 1
            vCodigo = grdCad.Text
            grdCad.Col = 2
            vDescricao = grdCad.Text
            vLinha = X
            Exit For
        End If
    Next X
    
    If vCodigo = 0 Then
        MsgBox "Nenhum registro selecionado, favor selecionar um registro para altera��o!!", vbInformation, Me.Caption
        Exit Sub
    End If

    Call frmCadCategoriaProduto.Alterar(vCodigo, vDescricao)
    If frmCadCategoriaProduto.OK = vbOK Then
        grdCad.Row = vLinha
        grdCad.Col = 2
        grdCad.Text = frmCadCategoriaProduto.descricaoCad
    End If

    DesmarcaGrid grdCad

End Sub

Private Sub Pesquisar()
 
    Dim vSequencia As Variant
    Dim X          As Integer
    Dim vLin       As Integer
    Dim sCateg     As String
    
    ReDim avData(0, 0)
      

    grdCad.Visible = False
    
    grdCad.MaxRows = 0
       
    cSql = "stp_pdr_cons_categoria_s "
        
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
    If bResp = False Then
       Exit Sub
    End If
        
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Sub
    End If
   
    If Val(avData(0, 0)) <= 0 Then Exit Sub
    
   
    'Colunas do Spread
    '01 - COD LINHA
    '02 - DESC LINHA
    
    grdCad.MaxRows = 0
    
    vLin = 0
    sCateg = ""
   
    For X = 0 To UBound(avData, 2)
    
        vLin = vLin + 1
   
        grdCad.MaxRows = vLin
       
        'CD LINHA
        grdCad.SetText 1, vLin, avData(0, X)
        'descricao linha
        grdCad.SetText 2, vLin, avData(1, X)
       
    Next
    
    DesmarcaGrid grdCad
   
    grdCad.Visible = True

End Sub

Private Sub Excluir()
    
    vCodigo = 0
    
    For X = 1 To grdCad.MaxRows
        grdCad.Row = X
        If grdCad.SelModeSelected Then
            grdCad.Col = 1
            vCodigo = grdCad.Text
            vLinha = X
            Exit For
        End If
    Next X
    
    If vCodigo = 0 Then
        MsgBox "Nenhum registro selecionado, favor selecionar um registro para exclus�o!!", vbInformation, Me.Caption
        Exit Sub
    End If

    cSql = "exec stp_pdr_cons_foreign_s 2, " & vCodigo
        
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
    If bResp = False Then
        GoTo Desmarca
    End If
        
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
        GoTo Desmarca
    End If
   
    If Val(avData(0, 0)) > 0 Then
        MsgBox "Exclus�o n�o pode ser efetuada - Existem produtos associados a esta categoria"
        GoTo Desmarca
    End If

    If MsgBox("Confirma a exclus�o do registro selecionado ?", vbQuestion + vbDefaultButton2 + vbYesNo, "A T E N � � O") = vbYes Then
        Call ExcluirLinha
    End If
    

Desmarca:
    DesmarcaGrid grdCad
    
End Sub

Private Sub ExcluirLinha()
    
    cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & " exec stp_pdr_cad_categ_e " & vCodigo & ","
    cSql = cSql & "'" & UCase(Environ("_U")) & "' ,@sql_cd_ret out, "
    cSql = cSql & "@sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
    
    If gObjeto.natPrepareQuery(hSql, cSql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
        MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
        Exit Sub
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_cd_opr = gObjeto.varOutputParam(1)
    sql_nm_tab = gObjeto.varOutputParam(2)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MsgBox "Erro ao executar a stp_pdr_cad_categ_e " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
        Exit Sub
    End If
    
    MsgBox "Exclus�o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
    
    grdCad.Row = vLinha
    grdCad.Col = 1
    grdCad.Action = 5
    grdCad.MaxRows = grdCad.MaxRows - 1
        
End Sub

Private Sub Form_Load()
    Me.Show
    Call Pesquisar

End Sub

Private Sub grdCad_DblClick(ByVal Col As Long, ByVal Row As Long)

    Alterar

End Sub
