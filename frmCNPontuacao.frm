VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmCNPontuacao 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o Tabela de Pontos"
   ClientHeight    =   6855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5610
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6855
   ScaleWidth      =   5610
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar st_Toolbar 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   5610
      _ExtentX        =   9895
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   11
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Description     =   "Novo"
            Object.ToolTipText     =   "Incluir faixa"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Excluir"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Gravar"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Nova pesquisa"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Imprimir"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Pesquisar"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   4
            Object.Width           =   1050
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
      EndProperty
      BorderStyle     =   1
      OLEDropMode     =   1
   End
   Begin VB.ComboBox cmbCiclo 
      Height          =   360
      ItemData        =   "frmCNPontuacao.frx":0000
      Left            =   1410
      List            =   "frmCNPontuacao.frx":0002
      TabIndex        =   0
      Top             =   780
      Width           =   1545
   End
   Begin FPSpread.vaSpread grd_pontos 
      Height          =   5280
      Left            =   390
      TabIndex        =   1
      Top             =   1320
      Width           =   4875
      _Version        =   131077
      _ExtentX        =   8599
      _ExtentY        =   9313
      _StockProps     =   64
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   1
      ScrollBars      =   2
      SpreadDesigner  =   "frmCNPontuacao.frx":0004
      VisibleCols     =   500
      VisibleRows     =   500
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Ciclo/Ano:"
      Height          =   240
      Left            =   450
      TabIndex        =   2
      Top             =   840
      Width           =   915
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   4950
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":0312
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":062C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":0946
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":0C60
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":0F7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":1294
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":15AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":18C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":1BE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCNPontuacao.frx":1EFC
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCNPontuacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Ano_ciclo   As String
Dim vCiclo      As String
Dim vOperacao   As String
Dim vExisteReg  As Boolean


Private Sub cmbCiclo_Click()

    If cmbCiclo.ListIndex > -1 Then
        st_Toolbar.Buttons.Item(8).Enabled = True
    Else
        st_Toolbar.Buttons.Item(8).Enabled = False
    End If

End Sub

Private Sub cmbCiclo_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Then
        Exit Sub
    End If
        
    If cmbCiclo.Text = "" Then
        cmbCiclo.SetFocus
        Exit Sub
    End If
    
    Call PesquisaPontos
        
    KeyAscii = 0
    grd_pontos.Col = 1
    grd_pontos.SetFocus
    grd_pontos.Action = 0
    
End Sub

Private Sub Form_Load()
    gisysmenu = GetSystemMenu(Me.hWnd, 0)
    
    iCounter = 8
    For gimenucount = iCounter To 6 Step -1
        giretval = RemoveMenu(gisysmenu, gimenucount, MF_BYPOSITION)
    Next gimenucount
    
    Call Carrega_Ciclo(cmbCiclo)
    Limpa_Grid
    
End Sub

'---------------------------------------------------------------------------------------
' Posiciona o Bot�o de Saida sempre no canto superior direito do Form Principal
'---------------------------------------------------------------------------------------
Private Sub Form_Resize()
    If Me.WindowState = 0 Then
        If Me.Width <= 3900 Then
            Me.Width = 3900
        End If
    End If
    st_Toolbar.Buttons(10).Width = Me.Width - (Me.st_Toolbar.Buttons(1).Width * 9) + _
                                    (Me.st_Toolbar.Buttons(1).Width * 0.7)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SetStatus ""
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(1) Then
        MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
    End If
    'SE 6346 - Fim
    MDIpdo001.DesbloqueiaMenu
End Sub


'-------------------------------------------------------------------------------------
' Grid para digita��o das pontua��es
'-------------------------------------------------------------------------------------
Private Sub grd_pontos_change(ByVal Col As Long, ByVal Row As Long)

    Dim Valor       As Integer
              
    Me.Refresh
    DoEvents
              
    If Col = 1 Then
        grd_pontos.Col = Col
        grd_pontos.Row = Row
        
        If grd_pontos.Text <> "" Then
            If CDbl(grd_pontos.Text) >= 0 Then
                If CDbl(grd_pontos.Text) > 0 Then
                    grd_pontos.Text = Format(grd_pontos.Text, "##0.00")
                    grd_pontos.Col = 2
                Else
                    If grd_pontos.Row = 1 Then
                        grd_pontos.Text = Format(grd_pontos.Text, "##0.00")
                        grd_pontos.Col = 2
                    Else
                        MsgBox "Valor inv�lido !", vbExclamation + vbOKOnly, "A T E N � � O"
                    End If
                End If
            Else
                MsgBox "Valor inv�lido !", vbExclamation + vbOKOnly, "A T E N � � O"
            End If
                     
            'grd_pontos.SetFocus
            grd_pontos.Action = 0
            grd_pontos.EditMode = True
            Exit Sub
        End If
    End If
     
    If Col = 2 Then
           
        grd_pontos.Col = 1
        
        If grd_pontos.Text <> "" Then
            Valor = grd_pontos.Text
        End If
        
        grd_pontos.Col = 2
        grd_pontos.Row = Row
      
        If grd_pontos.Text <> "" Then
            If CDbl(grd_pontos.Text) >= 0 Then
                If CDbl(grd_pontos.Text) > 0 Then
                    grd_pontos.Text = Format(grd_pontos.Text, "##0.00")
                    grd_pontos.Col = 3
                Else
                    If grd_pontos.Row = 1 Then
                        grd_pontos.Text = Format(grd_pontos.Text, "##0.00")
                        grd_pontos.Col = 3
                    Else
                        MsgBox "Valor deve ser maior que valor minimo !", vbExclamation + vbOKOnly, "A T E N � � O"
                    End If
                End If
            Else
                MsgBox "Valor Inv�lido !", vbExclamation + vbOKOnly, "A T E N � � O"
            End If
            
            'grd_pontos.SetFocus
            grd_pontos.Action = 0
            grd_pontos.EditMode = True
            Exit Sub
        End If
    End If
    
    If Col = 3 Then
        grd_pontos.Col = Col
        grd_pontos.Row = Row
      
        If grd_pontos.Text <> "" Then
            If Val(grd_pontos.Text) < 0 Then
                MsgBox "Pontua��o inv�lida!", vbExclamation + vbOKOnly, "A T E N � � O"
            End If
            
            'grd_pontos.SetFocus
            grd_pontos.Action = 0
            grd_pontos.EditMode = True
            Exit Sub
        End If
    End If
End Sub

Private Sub vaSpread1_Advance(ByVal AdvanceNext As Boolean)

End Sub

'-------------------------------------------------------------------------------------
' Controla a��es da barra de ferramentas
'-------------------------------------------------------------------------------------
Private Sub st_Toolbar_ButtonClick(ByVal Button As ComctlLib.Button)
   
    DoEvents
    Me.Refresh
   
On Error GoTo 0

   Select Case Button.Index
        Case 1  ' Novo
        
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
            'If Not ChecaPermissao("frmPDO015", "M") Then
               'MsgBox "Voce n�o tem permiss�o para criar", vbCritical + vbOKOnly, Me.Caption
               'Exit Sub
            'End If
            
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
            If Not bVerificaFuncaoPermissaoLogin("frmPDO015", "M") Then
               MsgBox "Voce n�o tem permiss�o para criar", vbCritical + vbOKOnly, Me.Caption
               Exit Sub
            End If
            
            grd_pontos.MaxRows = grd_pontos.MaxRows + 1
            grd_pontos.Col = 1
            grd_pontos.Row = grd_pontos.MaxRows
            'grd_pontos.SetFocus
            grd_pontos.Action = 0
            grd_pontos.EditMode = True
               
        Case 2  ' Excluir
        
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
            'If Not ChecaPermissao("frmPDO015", "M") Then
                'MsgBox "Voce n�o tem permiss�o para excluir", vbCritical + vbOKOnly, Me.Caption
                'Exit Sub
            'End If
            
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
            If Not bVerificaFuncaoPermissaoLogin("frmPDO015", "M") Then
                MsgBox "Voce n�o tem permiss�o para excluir", vbCritical + vbOKOnly, Me.Caption
                Exit Sub
            End If
            
            If MsgBox("Confirma a exclus�o das pontua��es deste ciclo?", vbQuestion + vbDefaultButton2 + vbYesNo, "A T E N � � O") = vbYes Then
                SetStatus "Excluindo pontua��es do ciclo. Aguarde ..."
                If Not vOperacao = "I" Then
                    vOperacao = "E"
                End If
                If Exclui_Pontuacao_Ciclo(Ano_ciclo) Then
                    MsgBox "Exclus�o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
                    Limpa_Grid
                    'SE 6346 - In�cio
                    If Not vOperacao = "I" Then
                        If Not GravaHistorico(1, vOperacao, "Ciclo", Ano_ciclo, "", "", "", "") Then
                            MsgBox "Erro ao gravar hist�rico da altera��o.", vbCritical, "Erro"
                            Exit Sub
                        End If
                    End If
                    If Not DesbloqueiaRegistro(1) Then
                        MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
                        Exit Sub
                    End If
                    'SE 6346 - Fim
                Else
                    SetStatus "Erro na exclus�o ..."
                    MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
                End If
            End If
                    
'            Limpa_Grid
'            cmbCiclo.Enabled = True
    '        Limpa_Grid
'            SetStatus ""
'            cmbCiclo.SetFocus
        
        Case 3  ' Salvar
        
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
            'If Not ChecaPermissao("frmPDO015", "M") Then
                'MsgBox "Voce n�o tem permiss�o para salvar", vbCritical + vbOKOnly, Me.Caption
                'Exit Sub
            'End If
            
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
            If Not bVerificaFuncaoPermissaoLogin("frmPDO015", "M") Then
                MsgBox "Voce n�o tem permiss�o para salvar", vbCritical + vbOKOnly, Me.Caption
                Exit Sub
            End If
                        
            If Not fConsGrade Then
                Exit Sub
            End If
            
            SetStatus "Gravando pontua��es. Aguarde ..."
            
            If Grava_Pontuacao_Ciclo Then
                SetStatus "Pontua��es gravadas"
                MsgBox "Grava��o efetuada com sucesso !", vbInformation + vbOKOnly, "A V I S O"
            Else
                SetStatus "Erro na grava��o ..."
                MsgBox MSG$, vbCritical + vbOKOnly, "E R R O !"
            End If
            
            Limpa_Grid
            'SE 6346 - In�cio
            If Not GravaHistorico(1, vOperacao, "Ciclo", Ano_ciclo, "", "", "", "") Then
                MsgBox "Erro ao gravar hist�rico da altera��o.", vbCritical, "Erro"
                Exit Sub
            End If
            If Not DesbloqueiaRegistro(1) Then
                MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
            End If
            'SE 6346 - Fim
            cmbCiclo.Enabled = True
            SetStatus ""
            cmbCiclo.ListIndex = -1
            cmbCiclo.SetFocus
            Exit Sub
        
        Case 5  ' Limpar
            Limpa_Grid
            'SE 6346 - In�cio
            If Not DesbloqueiaRegistro(1) Then
                MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
            End If
            vOperacao = ""
            'SE 6346 - Fim
            cmbCiclo.Enabled = True
            cmbCiclo.ListIndex = -1
            SetStatus ""
            cmbCiclo.SetFocus
        
        Case 6  ' Imprimir
        
        Case 8  ' Pesquisar
            Call PesquisaPontos
        
        Case 11 ' Sair
            Unload Me
            Set frmCNPontuacao = Nothing
            
    End Select
    
    
End Sub
Private Function fConsGrade() As Boolean

    Dim r           As Integer
    Dim vlr_min     As String
    Dim vlr_max     As String
    Dim vlr_max_ant As String
    Dim Pontos      As String
    Dim cadastrou   As Boolean
    Dim X           As Long
    
On Error GoTo ErroConsGrade

    fConsGrade = False
    ' verifica se valor minimo e menor que valor maximo na
    ' linha do gride e se vlr maximo e pontuacao e maior
    ' que o valor maximo e pontuacao da linha do grid anterior
    
    For r = 1 To grd_pontos.MaxRows
        grd_pontos.Row = r
        grd_pontos.Col = 1
        vlr_min = grd_pontos.Text
        grd_pontos.Col = 2
        vlr_max = grd_pontos.Text
        grd_pontos.Col = 3
        Pontos = grd_pontos.Text
        cadastrou = True
                
        If vlr_min = "" Or _
            vlr_max = "" Or _
            Pontos = "" Then
            cadastrou = False
        Else
            If r > 1 Then
                For X = r - 1 To 1 Step -1
                    grd_pontos.Col = 2
                    grd_pontos.Row = X
                    If Trim(grd_pontos.Text) <> "" Then
                        vlr_max_ant = grd_pontos.Text
                        Exit For
                    End If
                Next X
                grd_pontos.Row = r
            Else
                '#
                If CDbl(vlr_min) <> 0# Then
                    MsgBox "O valor m�nimo inicial n�o pode ser diferente de 0,00!", vbInformation, "A T E N � � O"
                    grd_pontos.SetFocus
                    grd_pontos.Action = 0
                    grd_pontos.EditMode = True
                    SetStatus ""
                    Exit Function
                End If
            End If
            
            If CDbl(vlr_min) <> 0 Then
                If CDbl(vlr_max) <> 0 Then
                    If CDbl(vlr_min) > CDbl(vlr_max) Then
                        MsgBox "Na linha " & r & " da grade o Vlr. minimo est� maior que o vlr. m�ximo!", vbInformation, "A T E N � � O"
                        grd_pontos.SetFocus
                        grd_pontos.Action = 0
                        grd_pontos.EditMode = True
                        SetStatus ""
                        Exit Function
                    Else
                        If vlr_min <= vlr_max_ant Then
                            MsgBox "O valor m�nimo da linha " & r & " deve ser maior que o Vlr. m�ximo da linha anterior!", vbInformation, "A T E N � � O"
                            grd_pontos.SetFocus
                            grd_pontos.Action = 0
                            grd_pontos.EditMode = True
                            SetStatus ""
                            Exit Function
                        Else
                            If CDbl(Format(vlr_min - vlr_max_ant, "##.00")) > 0.01 Then
                                MsgBox "O valor m�nimo da linha " & r & " � maior que o Vlr. m�ximo da linha anterior + 0,01!", vbInformation, "A T E N � � O"
                                grd_pontos.SetFocus
                                grd_pontos.Action = 0
                                grd_pontos.EditMode = True
                                SetStatus ""
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End If
        End If
    Next r

    If Not cadastrou Then
        If Not MsgBox("Existem linha(s) com campo(s) em branco!.Estas linhas n�o ser�o gravadas !. Deseja gravar mesmo assim ?", vbQuestion + vbDefaultButton2 + vbYesNo, "A T E N � � O") = vbYes Then
            grd_pontos.SetFocus
            grd_pontos.Action = 0
            grd_pontos.EditMode = True
            SetStatus ""
            Exit Function
        End If
    End If



    fConsGrade = True
    Exit Function
    
ErroConsGrade:
    If Err.Number = 5 Then
        Resume Next
    End If
    
    Resume Next
    

End Function
'-------------------------------------------------------------------------------------
' Limpa grid da tela
'-------------------------------------------------------------------------------------
Sub Limpa_Grid()
Dim ne_Conta As Integer

    st_Toolbar.Buttons.Item(1).Enabled = False
    st_Toolbar.Buttons.Item(2).Enabled = False
    st_Toolbar.Buttons.Item(3).Enabled = False


    grd_pontos.MaxRows = 1

    For ne_Conta = 1 To grd_pontos.MaxRows
        grd_pontos.Row = ne_Conta
        grd_pontos.Col = 1
        grd_pontos.Text = ""
        grd_pontos.CellType = SS_CELL_TYPE_FLOAT
        grd_pontos.Col = 2
        grd_pontos.Text = ""
        grd_pontos.CellType = SS_CELL_TYPE_FLOAT
        grd_pontos.Col = 3
        grd_pontos.Text = ""
        grd_pontos.CellType = SS_CELL_TYPE_INTEGER
    Next

End Sub

Private Sub PesquisaPontos()

Dim AnoCiclo As String
Dim lAnterior As Long
Dim sAntForm As String

    AnoCiclo = Right$(cmbCiclo.Text, 4) + Left$(cmbCiclo.Text, 2)
            
    'SE 6346 - In�cio
    Dim CodUsuario As String
    If Not DesbloqueiaRegistro(1) Then
        MsgBox "Falha ao desbloquear registros.", vbCritical, "Erro"
        Exit Sub
    End If
    If AnoCiclo = "" Then
        Exit Sub
    Else
        If VerificaBloqueioFormulario(1, CDbl(AnoCiclo), 0, CodUsuario) Then
            ExibeMensagemBloqueio ObtemDescricaoFormulario(1), "Ciclo", AnoCiclo, Empty, Empty, CodUsuario
            Exit Sub
        End If
    End If
    'SE 6346 - Fim
            
    SetStatus "Pesquisando pontua��es para este Ciclo/Ano. Aguarde ..."
    MSG$ = ""
    If Lista_Pontuacao_Ciclo(AnoCiclo, lAnterior) Then
        'SE 6346
        vOperacao = "A"
    Else
        If MSG$ <> "" Then
            MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
            Exit Sub
        Else
            sAntForm = Trim(Str(lAnterior))
            sAntForm = Right(sAntForm, 2) & "/" & Left(sAntForm, 4)
            'SE 6346
            vOperacao = "I"
            If MsgBox("N�o h� pontua��es para este ciclo!. Deseja carregar pontua��es do ciclo " & sAntForm & "?", vbQuestion + vbDefaultButton2 + vbYesNo, "A T E N � � O") = vbYes Then
'                lAnterior = 1
                'SE 6346 - In�cio
                If VerificaBloqueioFormulario(1, CDbl(AnoCiclo), 0, CodUsuario) Then
                    ExibeMensagemBloqueio ObtemDescricaoFormulario(1), "Ciclo", AnoCiclo, Empty, Empty, CodUsuario
                    Exit Sub
                End If
                'SE 6346 - Fim
                If Not Lista_Pontuacao_Ciclo(AnoCiclo, lAnterior) Then
                    If MSG$ <> "" Then
                        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                        Exit Sub
                    Else
                        MsgBox "Nenhum ciclo foi gravado anteriormente!. Cadastre o ciclo e suas pontua��es", vbInformation + vbOKOnly, "A V I S O"
                        Exit Sub
                    End If
                End If
            Else
                'SE 6346 - In�cio
                If VerificaBloqueioFormulario(1, CDbl(AnoCiclo), 0, CodUsuario) Then
                    ExibeMensagemBloqueio ObtemDescricaoFormulario(1), "Ciclo", AnoCiclo, Empty, Empty, CodUsuario
                    Exit Sub
                End If
                If Not BloqueiaRegistro(1, CDbl(AnoCiclo), 0) Then
                    MsgBox "Erro ao tentar bloquear registros.", vbCritical, "Desbloquear registros"
                    Exit Sub
                End If
                'SE 6346 - Fim
            End If
        End If
    End If
    
    SetStatus ""
    grd_pontos.Col = 1
    grd_pontos.Row = 1
    grd_pontos.SetFocus
    grd_pontos.Action = 0
    grd_pontos.EditMode = True
    
    st_Toolbar.Buttons.Item(1).Enabled = True
    st_Toolbar.Buttons.Item(2).Enabled = True
    st_Toolbar.Buttons.Item(3).Enabled = True
    st_Toolbar.Buttons.Item(8).Enabled = False
    cmbCiclo.Enabled = False

End Sub

'---------------------------------------------------------------------------------------
' Checa a existencia de pontua��s para o Ciclo/Ano digitado e preenche os campos na tela
'---------------------------------------------------------------------------------------
Function Lista_Pontuacao_Ciclo(ByRef Ano_ciclo As String, ByRef Anterior As Long)

    Dim se_Sql      As String
    Dim sql_cd_ret  As Integer
    Dim sql_qt_lnh  As Integer
    Dim avData()    As Variant
    Dim bResp       As Boolean
    Dim X           As Long
    
    Lista_Pontuacao_Ciclo = False
    On Error GoTo Lista_Pontuacao_Ciclo
    
    If Anterior = 0 Then
        se_Sql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), "
        se_Sql = se_Sql & "@sql_qt_lnh int, @anterior int, @msg_err Varchar(255) "
        se_Sql = se_Sql & " exec st_pi_verifica_pontos_s " & Ano_ciclo & ", "
        se_Sql = se_Sql & "@anterior out, @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"

        If gObjeto.natPrepareQuery(hSql, se_Sql) Then
           Do While gObjeto.natFetchNext(hSql)
           Loop
        End If

        If gObjeto.nErroSQL <> 0 Then
           MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
           Exit Function
        End If

        Anterior = CLng(gObjeto.varOutputParam(0))
        sql_cd_ret = CInt(gObjeto.varOutputParam(1))
        sql_cd_opr = gObjeto.varOutputParam(2)
        sql_nm_tab = gObjeto.varOutputParam(3)
        sql_qt_lnh = CLng(gObjeto.varOutputParam(4))

        If sql_cd_ret <> 0 Then
           MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
           Exit Function
        End If
        If Anterior <> Ano_ciclo Then
            Exit Function
        End If
    End If
    
    PonteiroMouse vbHourglass
    
    If Anterior = 0 Then
        se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30),"
        se_Sql = se_Sql & " @sql_qt_lnh int, @msg_err Varchar(255) "
    '    se_Sql = se_Sql & " exec st_pi_pontos_s " & Ano_ciclo & ", " & Anterior & ", "
        se_Sql = se_Sql & " exec st_pi_pontos_s " & Ano_ciclo & ", 0, "
        se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
    Else
        se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30),"
        se_Sql = se_Sql & " @sql_qt_lnh int, @msg_err Varchar(255) "
    '    se_Sql = se_Sql & " exec st_pi_pontos_s " & Ano_ciclo & ", " & Anterior & ", "
        se_Sql = se_Sql & " exec st_pi_pontos_s " & Anterior & ", 0, "
        se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
    End If
    
    grd_pontos.MaxRows = 0

    ReDim avData(0, 0)

    bResp = gObjeto.natExecuteQuery(hSql, se_Sql, avData)

    If bResp = False Then
        Me.MousePointer = vbNormal
        Refresh
        Exit Function
    End If

    If gObjeto.nErroSQL <> 0 Then
        Me.MousePointer = vbNormal
        Refresh
        MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
        Exit Function
    End If
   
    For X = 0 To UBound(avData, 2)
        grd_pontos.MaxRows = grd_pontos.MaxRows + 1
        grd_pontos.Row = grd_pontos.MaxRows
                        
        grd_pontos.Col = 1
        grd_pontos.Text = avData(0, X)
            grd_pontos.Text = Format(ConvertePontoDecimal(grd_pontos.Text, 0), "###,##0.00")
        
        grd_pontos.Col = 2
        grd_pontos.Text = avData(1, X)
        grd_pontos.Text = Format(ConvertePontoDecimal(grd_pontos.Text, 0), "###,##0.00")
        
        grd_pontos.Col = 3
        grd_pontos.Text = avData(2, X)
        
        grd_pontos.Col = 4
        grd_pontos.Text = avData(0, X)
            grd_pontos.Text = Format(ConvertePontoDecimal(grd_pontos.Text, 0), "###,##0.00")
        
        grd_pontos.Col = 5
        grd_pontos.Text = avData(1, X)
        grd_pontos.Text = Format(ConvertePontoDecimal(grd_pontos.Text, 0), "###,##0.00")
        
    Next X

    If UBound(avData, 2) > 0 Then
        'SE 6346 - In�cio
        If Anterior = 0 Then
            If Not BloqueiaRegistro(1, CDbl(Ano_ciclo), 0) Then
                MsgBox "Erro ao tentar bloquear registros.", vbCritical, "Desbloquear registros"
                Exit Function
            End If
        Else
            If Not BloqueiaRegistro(1, CDbl(Ano_ciclo), 0) Then
                MsgBox "Erro ao tentar bloquear registros.", vbCritical, "Desbloquear registros"
                Exit Function
            End If
        End If
        'SE 6346 - Fim
    
        Lista_Pontuacao_Ciclo = True
    End If
    
    
    

'    If gObjeto.natPrepareQuery(hSql, se_Sql) Then
'        Do While gObjeto.natFetchNext(hSql)
'            If gObjeto.varResultSet(hSql, 0) <> "" Then
'                grd_pontos.MaxRows = grd_pontos.MaxRows + 1
'                grd_pontos.Row = grd_pontos.MaxRows
'
'                grd_pontos.Col = 1
'                grd_pontos.Text = gObjeto.varResultSet(hSql, 0)
'                grd_pontos.Text = Format(ConvertePontoDecimal(grd_pontos.Text, 0), "###,##0.00")
'
'                grd_pontos.Col = 2
'                grd_pontos.Text = gObjeto.varResultSet(hSql, 1)
'                grd_pontos.Text = Format(ConvertePontoDecimal(grd_pontos.Text, 0), "###,##0.00")
'
'                grd_pontos.Col = 3
'                grd_pontos.Text = gObjeto.varResultSet(hSql, 2)
'
'                Lista_Pontuacao_Ciclo = True
'
'           End If
'        Loop
'    End If
'
'    If gObjeto.nErroSQL <> 0 Then
'        MSG$ = "Erro ao executar a Lista_Pontuacao_Ciclo " & gObjeto.nServerNum & gObjeto.sServerMsg
'        PonteiroMouse vbNormal
'        Exit Function
'    End If
'
'    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
'    If sql_cd_ret <> 0 Then
'        MSG$ = "Erro ao executar a Lista_Pontuacao_Ciclo " & "RETURN STATUS = " & sql_cd_ret
'        PonteiroMouse vbNormal
'        Exit Function
'    End If
       
    PonteiroMouse vbNormal
    Exit Function

Lista_Pontuacao_Ciclo:
    MSG$ = "Lista_Pontuacao_Ciclo - Erro (" & Err & ") - " & Error
    PonteiroMouse vbNormal
    Exit Function
End Function

'----------------------------------------------------------------------------------------
' Grava pontua��es digitadas no Ciclo/Ano digitado
'----------------------------------------------------------------------------------------
Public Function Grava_Pontuacao_Ciclo() As Boolean
    Dim se_Sql          As String
    Dim se_Usuario      As String
    Dim ne_ret          As Long
    Dim sql_cd_ret      As Integer
    Dim sql_qt_lnh      As Integer
    Dim vlr_minimo_par  As String
    Dim vlr_maximo_par  As String
    Dim vlr_minimo_ant  As String
    Dim vlr_maximo_ant  As String
    Dim pontos_par      As Integer
    Dim cadastrou       As Boolean
    Dim r               As Long
    
    On Error GoTo 0

    Grava_Pontuacao_Ciclo = False

    PonteiroMouse vbHourglass
    
    Ano_ciclo = Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)
    
    For r = 1 To grd_pontos.MaxRows
        grd_pontos.Row = r
        grd_pontos.Col = 1
        If grd_pontos.Text <> "" Then
            grd_pontos.Col = 1
            vlr_minimo_par = grd_pontos.Text
        
            grd_pontos.Col = 2
            vlr_maximo_par = grd_pontos.Text
        
            grd_pontos.Col = 3
            pontos_par = grd_pontos.Text
        
            grd_pontos.Col = 4
            If Trim(grd_pontos.Text) = "" Then
                vlr_minimo_ant = "0.00"
            Else
                vlr_minimo_ant = grd_pontos.Text
            End If
            grd_pontos.Col = 5
            If Trim(grd_pontos.Text) = "" Then
                vlr_maximo_ant = "0.00"
            Else
                vlr_maximo_ant = grd_pontos.Text
            End If
            ' verifica se toda linha do grid
            ' esta preechida para poder gravar (vlr. min/vlr. max/ptos)
                
            cadastrou = False
            If vlr_minimo_par <> "" Then
                If vlr_maximo_par <> "" Then
                    If CStr(pontos_par) <> "" Then
                        cadastrou = True
                    End If
                End If
            End If
            
            If cadastrou Then
                se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30),"
                se_Sql = se_Sql & " @sql_qt_lnh int, @msg_err Varchar(255) "
                se_Sql = se_Sql & " exec st_pi_pontos_m " & Ano_ciclo & ", "
                se_Sql = se_Sql & ConvertePontoDecimal(vlr_minimo_par, 1) & ","
                se_Sql = se_Sql & ConvertePontoDecimal(vlr_maximo_par, 1) & ","
                se_Sql = se_Sql & ConvertePontoDecimal(vlr_minimo_ant, 1) & ","
                se_Sql = se_Sql & ConvertePontoDecimal(vlr_maximo_ant, 1) & ","
                se_Sql = se_Sql & pontos_par & ","
                se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
            
                If gObjeto.natPrepareQuery(hSql, se_Sql) Then
                    Do While gObjeto.natFetchNext(hSql)
                    Loop
                End If
            
                If gObjeto.nErroSQL <> 0 Then
                    MSG$ = "Erro ao executar a Grava_Pontuacao_Ciclo - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
                    PonteiroMouse vbNormal
                    Exit Function
                End If
                
                sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                If sql_cd_ret <> 0 Then
                    MSG$ = "Erro ao executar a Grava_Pontuacao_Ciclo " & "RETURN STATUS = " & sql_cd_ret
                    PonteiroMouse vbNormal
                    Exit Function
                End If
            End If
        Else
            grd_pontos.Col = 1
            vlr_minimo_par = grd_pontos.Text
        
            grd_pontos.Col = 2
            vlr_maximo_par = grd_pontos.Text
        
            grd_pontos.Col = 3
            pontos_par = Val(grd_pontos.Text)
        
            grd_pontos.Col = 4
            vlr_minimo_ant = grd_pontos.Text
        
            grd_pontos.Col = 5
            vlr_maximo_ant = grd_pontos.Text
        
            ' verifica se toda linha do grid
            ' esta preechida para poder gravar (vlr. min/vlr. max/ptos)
                
            cadastrou = False
            If Trim(vlr_minimo_par) = "" Then
                If Trim(vlr_maximo_par) = "" Then
                    If Trim(CStr(pontos_par)) = "0" Then
                        cadastrou = True
                    End If
                End If
            End If
            
            If cadastrou Then
                se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30),"
                se_Sql = se_Sql & " @sql_qt_lnh int, @msg_err Varchar(255) "
                se_Sql = se_Sql & " exec st_pi_pontos_d " & Ano_ciclo & ", "
                se_Sql = se_Sql & ConvertePontoDecimal(vlr_minimo_ant, 1) & ","
                se_Sql = se_Sql & ConvertePontoDecimal(vlr_maximo_ant, 1) & ","
                se_Sql = se_Sql & pontos_par & ","
                se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
            
                If gObjeto.natPrepareQuery(hSql, se_Sql) Then
                    Do While gObjeto.natFetchNext(hSql)
                    Loop
                End If
            
                If gObjeto.nErroSQL <> 0 Then
                    MSG$ = "Erro ao executar a Grava_Pontuacao_Ciclo - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
                    PonteiroMouse vbNormal
                    Exit Function
                End If
                
                sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                If sql_cd_ret <> 0 Then
                    MSG$ = "Erro ao executar a Grava_Pontuacao_Ciclo " & "RETURN STATUS = " & sql_cd_ret
                    PonteiroMouse vbNormal
                    Exit Function
                End If
            End If
        End If
    Next r
    
    Grava_Pontuacao_Ciclo = True
    PonteiroMouse vbNormal
       
End Function

'----------------------------------------------------------------------------------------
' Exclui todas as pontua��es do Ciclo/Ano digitado
'----------------------------------------------------------------------------------------
Function Exclui_Pontuacao_Ciclo(Ano_ciclo As String)
    
    Dim se_Sql      As String
    Dim sql_cd_ret  As Integer
    Dim sql_qt_lnh  As Integer
 
    Exclui_Pontuacao_Ciclo = False
    On Error GoTo Exclui_Pontuacao_Ciclo
    
    Ano_ciclo = Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)
    
    se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30),"
    se_Sql = se_Sql & " @sql_qt_lnh int, @msg_err Varchar(255) "
    se_Sql = se_Sql & " exec st_pi_exclui_pontos_ciclo " & Ano_ciclo & ", "
    se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
          
    If gObjeto.natPrepareQuery(hSql, se_Sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
                
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a Exclui_Pontuacao_Ciclo - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a st_pi_exclui_pontos_ciclo " & "RETURN STATUS = " & sql_cd_ret
        Exit Function
    End If
    
    Exclui_Pontuacao_Ciclo = True
    Exit Function
    
Exclui_Pontuacao_Ciclo:
    MSG$ = "Exclui_Pontuacao_Ciclo - Erro (" & Err & ") - " & Error
    Exit Function
End Function


