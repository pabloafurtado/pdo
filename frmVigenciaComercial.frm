VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmVigenciaComercial 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Manuten��o de Abrang�ncias / Vig�ncia - Mercadol�gico"
   ClientHeight    =   7770
   ClientLeft      =   1725
   ClientTop       =   3090
   ClientWidth     =   9990
   ControlBox      =   0   'False
   Icon            =   "frmVigenciaComercial.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   9990
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9990
      _ExtentX        =   17621
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   21
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Nova pesquisa"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Inclui abrang�ncia"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Altera abrang�ncia escolhida"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "exportar"
            Object.ToolTipText     =   "Exportar Abrang�ncia"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "importar"
            Object.ToolTipText     =   "Importar Abrang�ncia"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Cancela abrang�ncia escolhida"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Encerrar "
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            Object.Width           =   1e-4
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button12 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button13 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button14 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "ContrLog"
            Object.ToolTipText     =   "Controle Geral Log�stico"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button15 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button16 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button17 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button18 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button19 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "DivBoleto"
            Object.ToolTipText     =   "Manuten��o do Indicador de divis�o de Boleto"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button20 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button21 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "PrcRef"
            Object.ToolTipText     =   "Manuten��o de Pre�o de Refer�ncia"
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4800
      Top             =   3600
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   2760
      TabIndex        =   14
      Top             =   6630
      Width           =   4575
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   765
         Left            =   2760
         Picture         =   "frmVigenciaComercial.frx":2072
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   1125
      End
      Begin VB.CommandButton cmdSalvar 
         Caption         =   "Salvar"
         Height          =   765
         Left            =   660
         Picture         =   "frmVigenciaComercial.frx":237C
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   1185
      End
   End
   Begin VB.Frame Frame5 
      Enabled         =   0   'False
      Height          =   705
      Left            =   60
      TabIndex        =   12
      Top             =   2580
      Width           =   9855
      Begin VB.CheckBox chkDivisao 
         Caption         =   "Permite divis�o de Boleto"
         Height          =   375
         Left            =   6300
         TabIndex        =   5
         Top             =   180
         Width           =   2655
      End
      Begin VB.CheckBox chkCGL 
         Caption         =   "Controle Geral log�stico"
         Height          =   375
         Left            =   840
         TabIndex        =   4
         Top             =   180
         Width           =   2475
      End
   End
   Begin VB.Frame Frame4 
      Height          =   3345
      Left            =   60
      TabIndex        =   13
      Top             =   3270
      Width           =   9855
      Begin VB.CheckBox chkMostraCanc 
         Caption         =   "Mostrar abrang�ncias canceladas e encerradas"
         Height          =   255
         Left            =   6000
         TabIndex        =   6
         Top             =   240
         Width           =   3735
      End
      Begin FPSpread.vaSpread spdAVIG 
         Height          =   2685
         Left            =   60
         TabIndex        =   7
         Top             =   540
         Width           =   9645
         _Version        =   131077
         _ExtentX        =   17013
         _ExtentY        =   4736
         _StockProps     =   64
         AllowCellOverflow=   -1  'True
         ColHeaderDisplay=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   17
         MaxRows         =   0
         ScrollBars      =   2
         SpreadDesigner  =   "frmVigenciaComercial.frx":2686
         UserResize      =   0
         VisibleCols     =   500
         VisibleRows     =   500
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Abrang�ncias e Vig�ncias"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   180
         TabIndex        =   17
         Top             =   180
         Width           =   2760
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1755
      Left            =   60
      TabIndex        =   9
      Top             =   840
      Width           =   9855
      Begin VB.ComboBox cmbCategoria 
         Height          =   315
         Left            =   5760
         TabIndex        =   3
         Top             =   1290
         Width           =   3915
      End
      Begin VB.ComboBox cmbLinha 
         Height          =   315
         Left            =   180
         TabIndex        =   2
         Top             =   1290
         Width           =   4155
      End
      Begin VB.TextBox txtDescricao 
         Height          =   615
         Left            =   1560
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   390
         Width           =   8145
      End
      Begin VB.TextBox txtCodVenda 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   180
         MaxLength       =   7
         TabIndex        =   0
         Top             =   390
         Width           =   1245
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Categoria"
         Height          =   240
         Left            =   5760
         TabIndex        =   19
         Top             =   1050
         Width           =   885
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Linha"
         Height          =   240
         Left            =   180
         TabIndex        =   18
         Top             =   1050
         Width           =   480
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         Height          =   240
         Left            =   1560
         TabIndex        =   11
         Top             =   150
         Width           =   1050
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Venda"
         Height          =   240
         Left            =   180
         TabIndex        =   10
         Top             =   150
         Width           =   1065
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   7920
      Top             =   7260
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":2BCE
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":2EE8
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":3202
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":351C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":3836
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":3B50
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":3E6A
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmVigenciaComercial.frx":4E38
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSalvar 
         Caption         =   "&Salvar"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "S&air"
      End
   End
   Begin VB.Menu mnuOpcoes 
      Caption         =   "&Op��es"
      Begin VB.Menu mnuIncluir 
         Caption         =   "&Incluir"
      End
      Begin VB.Menu mnuAlterar 
         Caption         =   "A&lterar"
      End
      Begin VB.Menu mnuExcluir 
         Caption         =   "&Cancelar"
      End
   End
End
Attribute VB_Name = "frmVigenciaComercial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vGeraPend         As Boolean
Dim vProdNovo         As Boolean
Dim vExecuta          As Boolean
Dim vDescricao        As String
Dim vDescr            As String
Dim vOperador         As Integer
Dim cont              As Integer
Dim vLinha            As Integer
Dim vCateg            As Integer
Dim vQtdMat           As Integer
Dim caminhoExcel      As String
Dim X                 As Integer
Dim Y                 As Integer
Dim fonteDados        As Integer
Dim vCiclIni          As Variant
Dim vDataIni          As Variant
Dim vCiclFin          As Variant
Dim vDataFin          As Variant
Dim vSequencia        As Variant
Dim vTipEstr          As Variant
Dim vCodEstr          As Variant
Dim vCiclIniAnt       As Variant
Dim vDataIniAnt       As Variant
Dim vCiclFinAnt       As Variant
Dim vDataFinAnt       As Variant
Dim vOperacao         As Variant
Dim vStatus           As Variant
Dim cSql              As String

Dim strOperacao       As String 'SE 6346
Sub sLiberaSalvar()
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If ChecaPermissao("frmPDO002", "M") Then
        'cmdSalvar.Enabled = True
        'cmdCancelar.Enabled = True
        'mnuSalvar.Enabled = True
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
     If bVerificaFuncaoPermissaoLogin("frmPDO002", "M") Then
        cmdSalvar.Enabled = True
        cmdCancelar.Enabled = True
        mnuSalvar.Enabled = True
    End If
   
End Sub


Sub ProtegeTela()
    
    txtCodVenda.Enabled = True
    txtDescricao.Enabled = False
    chkCGL.Enabled = False
    cmbLinha.Enabled = False
    cmbCategoria.Enabled = False
    spdAVIG.Enabled = False
    spdAVIG.MaxRows = 0
    cmdSalvar.Enabled = False
    cmdCancelar.Enabled = False
    
    txtCodVenda.SetFocus
    
    mnuOpcoes.Enabled = False
    mnuSalvar.Enabled = False
    
    Toolbar1.Buttons(1).Enabled = False
    Toolbar1.Buttons(3).Enabled = False
    Toolbar1.Buttons(4).Enabled = False
    Toolbar1.Buttons(5).Enabled = False
    Toolbar1.Buttons(6).Enabled = False
    Toolbar1.Buttons(12).Enabled = False
    Toolbar1.Buttons(17).Enabled = False
    Toolbar1.Buttons(19).Enabled = False
    
End Sub

Sub DesprotegeTela()
    
    txtCodVenda.Enabled = False
    txtDescricao.Enabled = True
    chkCGL.Enabled = True
    
    cmbLinha.Enabled = True
    cmbCategoria.Enabled = True
    
    If chkCGL.Value = 0 And Not indicaKit Then
        Toolbar1.Buttons(3).Enabled = True
        Toolbar1.Buttons(4).Enabled = True
        Toolbar1.Buttons(5).Enabled = True
        Toolbar1.Buttons(6).Enabled = True
        mnuOpcoes.Enabled = True
    End If
    
    spdAVIG.Enabled = True
    Toolbar1.Buttons(1).Enabled = True
    Toolbar1.Buttons(12).Enabled = True
    Toolbar1.Buttons(17).Enabled = True
    Toolbar1.Buttons(19).Enabled = True
    
    txtDescricao.SetFocus
    
End Sub

Private Sub chkMostraCanc_Click()

    Dim vStat As Variant
    

   spdAVIG.Visible = False
   Me.MousePointer = vbHourglass
    
    For X = 1 To spdAVIG.MaxRows
     
        spdAVIG.GetText 6, X, vStat
    
        If vStat <> "ATIVA" Then
           spdAVIG.BlockMode = True
           spdAVIG.Row = X
           spdAVIG.Row2 = X
           spdAVIG.Col = -1
           spdAVIG.Col2 = spdAVIG.MaxCols
           spdAVIG.RowHidden = IIf(chkMostraCanc.Value = 0, True, False)
           spdAVIG.BlockMode = False
        End If
        
    Next
   
   spdAVIG.Visible = True
   Me.MousePointer = 1
    
    
End Sub

Private Sub cmbCategoria_Click()

    If cmbCategoria.ListIndex <> -1 Then
       If cmbCategoria.ItemData(cmbCategoria.ListIndex) <> vCateg Then
          sLiberaSalvar
       End If
    End If
    
End Sub

Private Sub cmbLinha_Click()

    If cmbLinha.ListIndex <> -1 Then
        If cmbLinha.ItemData(cmbLinha.ListIndex) <> vLinha Then
            sLiberaSalvar
        End If
    End If

End Sub

Private Sub cmdSalvar_Click()
   
    Dim sLinha       As String
    Dim sCateg       As String
    Dim sCodVendaAf  As String
    Dim sDescVendaAf As String
   
   'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO002", "M") Then
       'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "E R R O"
       'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO002", "M") Then
       MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "E R R O"
       Exit Sub
    End If
    
    If Trim(txtDescricao.Text) = "" Then
       MsgBox "� obrigat�rio a digita��o de uma Descri��o para o produto!", vbCritical + vbOKOnly, "E R R O"
       txtDescricao.SetFocus
       Exit Sub
    End If
    
    If cmbLinha.ListIndex = -1 Then
       MsgBox "� obrigat�rio a escolha de uma Linha para o produto!", vbCritical + vbOKOnly, "E R R O"
       cmbLinha.SetFocus
       Exit Sub
    End If
    
    If cmbCategoria.ListIndex = -1 Then
       MsgBox "� obrigat�rio a escolha de uma Categoria para o produto!", vbCritical + vbOKOnly, "E R R O"
       cmbCategoria.SetFocus
       Exit Sub
    End If
   
    'consiste conflitos de abrangencias vigencias
    If Not ConsisteAbrangenciaVigencia(spdAVIG) Then Exit Sub
    
    If MsgBox("Confirma a Altera��o das Informa��es do Produto?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
    
    If Trim(txtDescricao.Text) <> Trim(vDescr) Or _
       vLinha <> cmbLinha.ItemData(cmbLinha.ListIndex) Or _
       vCateg <> cmbCategoria.ItemData(cmbCategoria.ListIndex) Then
    
       'atualizar os dados do produto de venda
       cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
       cSql = cSql & " exec stp_pdr_produto_venda_u " & txtCodVenda.Text & " ,'" & Trim(txtDescricao.Text) & "',"
       cSql = cSql & cmbLinha.ItemData(cmbLinha.ListIndex) & "," & cmbCategoria.ItemData(cmbCategoria.ListIndex)
       cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
         
       If gObjeto.natPrepareQuery(hSql, cSql) Then
          Do While gObjeto.natFetchNext(hSql)
          Loop
       End If
         
       If gObjeto.nErroSQL <> 0 Then
          MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
          Exit Sub
       End If
    
       sql_cd_ret = CInt(gObjeto.varOutputParam(0))
       sql_cd_opr = gObjeto.varOutputParam(1)
       sql_nm_tab = gObjeto.varOutputParam(2)
       sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
        
       If sql_cd_ret <> 0 Then
          MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
          Exit Sub
       End If
    End If
    
    'inclusao de alteracao das abrangencias e vigencias
    If ManutencaoAbrangencia Then
        MsgBox "Atualiza��o Realizada com Sucesso.", vbInformation, "Aviso"
    Else
        Exit Sub
    End If
        
    'Obtem os ciclos ativos para o produto, ap�s a altera��o (apenas p/informar ao usu�rio)
    Call ObtemCiclosProduto(txtCodVenda.Text, 2)
    'Call ComparaCiclosProduto
    sCodVendaAf = txtCodVenda.Text
    sDescVendaAf = txtDescricao.Text
    
    'SE 6346 - In�cio
    If Not GravaHistorico(3, strOperacao, "Produto", txtCodVenda.Text, "", "", "", "") Then
        MsgBox "Erro ao gravar hist�rico da altera��o.", vbCritical, "Erro"
        Exit Sub
    End If
    
    If Not DesbloqueiaRegistro(3) Then
        MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim
    
    LimparCampos

    ProtegeTela
    
    Call ObtemInfomacoesAfetadas(sCodVendaAf, sDescVendaAf)
    MsgBox "Atualiza��o Realizada com Sucesso.", vbInformation, "Aviso"
End Sub

Private Sub cmdCancelar_Click()
    
    If cmdSalvar.Enabled = True Then
        If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
            Exit Sub
        End If
    End If
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(3) Then
        MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
    End If
    'SE 6346 - Fim
    LimparCampos
    
    ProtegeTela

End Sub

Private Sub Form_Activate()
 
    If txtCodVenda.Enabled = True Then
       txtCodVenda.SetFocus
    End If
    

    For X = 7 To 17
        spdAVIG.Col = X
        spdAVIG.ColHidden = True
    Next

End Sub

Private Sub Form_Load()
    
    Screen.MousePointer = 11
    
    Me.Top = 10
    Me.Left = 10
    
    'Carrega listbox de Categoria
    Call Carrega_Categoria_Produto(cmbCategoria)
    
    'Carrega listbox de Linha
    Call Carrega_Linha_Produto(cmbLinha)
    
    Screen.MousePointer = 1
    fonteDados = 1
    
    ImageList2.ListImages.Add ImageList2.ListImages.Count + 1, "", MDIpdo001.ImageList1.ListImages(23).Picture
    ImageList2.ListImages.Add ImageList2.ListImages.Count + 1, "", MDIpdo001.ImageList1.ListImages(22).Picture
    Toolbar1.ImageList = ImageList2
    Toolbar1.Buttons(1).Image = 6
    Toolbar1.Buttons(3).Image = 1
    Toolbar1.Buttons(4).Image = 2
    Toolbar1.Buttons(5).Image = 9
    Toolbar1.Buttons(6).Image = 10
    Toolbar1.Buttons(7).Image = 3
    Toolbar1.Buttons(9).Image = 4
    Toolbar1.Buttons(14).Image = 5
    Toolbar1.Buttons(19).Image = 7
    Toolbar1.Buttons(21).Image = 8
    Me.Show
    
    ProtegeTela
    
    If chkCGL.Value = 1 Then
       Toolbar1.Buttons(1).Enabled = False
       Toolbar1.Buttons(2).Enabled = False
       Toolbar1.Buttons(3).Enabled = False
    End If
    
    'conexao com a base oracle
    'Anderson
    nRet = DbOra.NatConnect(App, "TMOracle")
    If Not nRet Then
       MsgBox "Erro ao Conectar com o Banco Oracle !", vbInformation
       Set DbOra = Nothing
       End
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If cmdSalvar.Enabled = True Then
       If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
          Cancel = -1
          Exit Sub
       End If
    End If
    
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(3) Then
        MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
    End If
    'SE 6346 - Fim

    MDIpdo001.DesbloqueiaMenu
    
    Unload frmVigenciaComercialAux

    'Anderson
    If DbOra.Connected = True Then
       DbOra.Disconnect
       Set DbOra = Nothing
    End If
    
End Sub

Private Sub mnuAlterar_Click()

    If Val(txtCodVenda.Text) <= 0 Then
       MsgBox "Informar o Produto.", vbCritical, "Aten��o"
       Exit Sub
    End If

    If spdAVIG.ActiveRow <= 0 Then
       MsgBox "Informar a Abrang�ncia / Vig�ncia a ser Alterada.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    spdAVIG.GetText 6, spdAVIG.ActiveRow, vStatus
    
    If vStatus = "CANCELADA" Then
       MsgBox "Abrang�ncias Canceladas N�o Podem Ser Alteradas.", vbCritical, "Aten��o"
       Exit Sub
    End If

    indicaAltera = 1
    
    frmVigenciaComercialAux.Show (1)
    
End Sub

Private Sub mnuExcluir_Click()
   
    If Val(txtCodVenda.Text) <= 0 Then
       MsgBox "Informar o Produto.", vbCritical, "Aten��o"
       Exit Sub
    End If

    If spdAVIG.ActiveRow <= 0 Then
       MsgBox "Informar a Abrang�ncia / Vig�ncia a ser Alterada.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    spdAVIG.GetText 6, spdAVIG.ActiveRow, vStatus
    
    If vStatus = "CANCELADA" Then
       MsgBox "Abrang�ncia J� Est� Cancelada.", vbCritical, "Aten��o"
       Exit Sub
    End If
    
    If Not CancelarAbrangenciaVigencia Then Exit Sub

End Sub

Private Sub mnuImportar_Click()

On Error GoTo TrataErro

If txtCodVenda.Text <> "" Then
    If Not IsNumeric(txtCodVenda.Text) Then
        MsgBox "C�digo de Venda inv�lido", vbExclamation, "Mensagem"
    Else
        CommonDialog1.Filter = "Arquivo do Excel (*.xls)|*.xls"
        CommonDialog1.FilterIndex = 2
        CommonDialog1.InitDir = App.Path
        CommonDialog1.Flags = cdlOFNHideReadOnly
        CommonDialog1.CancelError = True
        CommonDialog1.ShowOpen
        If CommonDialog1.FileName <> "" Then
            caminhoExcel = CommonDialog1.FileName
            If Dir(caminhoExcel) = "" Then
                MsgBox "Arquivo n�o encontrado." & vbCrLf & "Informe um arquivo v�lido.", vbExclamation, "Mensagem"
                Exit Sub
            End If
            fonteDados = 2
            Dim tamanho As Double
            tamanho = VerTamanhoArquivo(caminhoExcel)
            Dim carregamentoValido As Boolean
            Dim mensagem As String
            carregamentoValido = CarregarListaDados(caminhoExcel, tamanho, 2, mensagem, txtCodVenda.Text)
            If carregamentoValido Then
                Dim retorno As Boolean
                fonteDados = 1
                spdAVIG.MaxRows = 0
                BuscarDadosTela
                fonteDados = 2
                retorno = ImportarCadastroRevista(spdAVIG, 1, 0, 2, 0)
                If retorno = True Then
                    txtCodVenda.Text = ListaDadosImportacaoExcel(1, 1)
                    txtCodVenda.Enabled = False
                    'busca informacoes pelo produto
                    If SelecionarDadosProduto(txtCodVenda.Text) = True Then DesprotegeTela
                    
                    chkMostraCanc.Enabled = False
                    cmdSalvar.Enabled = True
                    cmdCancelar.Enabled = True
                    
                    BuscarDadosTela
                    MsgBox "Importa��o Conlu�da com Sucesso.", vbInformation, "Mensagem"
                End If
                
            Else
                MsgBox mensagem, vbExclamation, "Mensagem"
            End If
        End If
    End If
Else
    MsgBox "C�digo de Venda Obrigat�rio", vbExclamation, "Mensagem"
End If

Exit Sub

TrataErro:
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    If Err.Number = 32755 Or Err.Number = 1004 Then
        Exit Sub
    End If

    MsgBox Err.Number & " - " & Err.Description
End Sub

Private Sub mnuExportar_Click()

On Error GoTo TrataErro

If txtCodVenda.Text = "" Then
    MsgBox "Selecione o C�digo de Venda", vbExclamation, "Mensagem"
Else
    If spdAVIG.MaxRows = 0 Then
        MsgBox "N�o existem dados a serem exportados"
        
    Else
        If MsgBox("Confirma a Exporta��o dos Dados? ", vbQuestion + vbYesNo) = vbYes Then
            CommonDialog1.Filter = "Arquivo do Excel (*.xls)|*.xls"
            CommonDialog1.FilterIndex = 2
            CommonDialog1.InitDir = App.Path
            CommonDialog1.Flags = cdlOFNHideReadOnly
            
            CommonDialog1.CancelError = True
            CommonDialog1.ShowSave
            If CarregarDadosParaExportar(spdAVIG, 2, CDbl(txtCodVenda.Text), chkCGL.Value, txtDescricao.Text) Then
                ExportarDadosCadastroRevista 2, CommonDialog1.FileName
            End If
        End If
    End If
End If


Exit Sub


TrataErro:
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    If Err.Number = 32755 Or Err.Number = 1004 Then
        Exit Sub
    End If
    MsgBox Err.Number & " - " & Err.Description, vbCritical, "Erro"
End Sub
Private Sub mnuIncluir_Click()

    If Val(txtCodVenda.Text) <= 0 Then
       MsgBox "Informar o Produto.", vbCritical, "Aten��o"
       Exit Sub
    End If
   
    indicaAltera = 0
    
    frmVigenciaComercialAux.Show (1)
    
End Sub

Private Sub mnuSair_Click()
    Unload Me
End Sub

Private Sub mnuSalvar_Click()
    cmdSalvar_Click
End Sub

Private Sub spdAVIG_DblClick(ByVal Col As Long, ByVal Row As Long)

    If mnuOpcoes.Enabled Then
        mnuAlterar_Click
    End If

End Sub

Private Sub txtCodVenda_KeyPress(KeyAscii As Integer)
     If KeyAscii <> 13 Then Exit Sub
        fonteDados = 1
        BuscarDadosTela
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
 
    Select Case Button.Index
    
        Case 1
            If cmdSalvar.Enabled = True Then
               If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
                  Exit Sub
               End If
            End If
            
            'SE 6346 - In�cio
            If Not DesbloqueiaRegistro(3) Then
                MsgBox "Erro ao tentar desbloquear registro.", vbCritical, "Erro"
            End If
            'SE 6346 - Fim
            
            'limpar campos
            LimparCampos
            
            'protege os controles
            ProtegeTela
            
        Case 3
            mnuIncluir_Click
            
        Case 4
            mnuAlterar_Click
        Case 5
            mnuExportar_Click
        Case 6
            mnuImportar_Click
        Case 7
        
            mnuExcluir_Click
            
        Case 9
            Unload Me
            
        Case 14
            frmContrLog.Show (1)
            
        Case 19
            frmDivisaoBoleto.Show (1)
            
        Case 21
        
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
             'If Not ChecaPermissao("frmPDO011", "C") Then
                'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
                'Exit Sub
             'End If
            
            'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
             If Not bVerificaFuncaoPermissaoLogin("frmPDO011", "C") Then
               MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
               Exit Sub
             End If
            
            If Len(Trim(txtCodVenda.Text)) <= 0 Then
               MsgBox "Informe o c�digo de venda", vbCritical + vbOKOnly, Me.Caption
               If txtCodVenda.Enabled Then
                   txtCodVenda.SetFocus
               End If
               Exit Sub
            End If
        
            frmPrecoRef.ChamadaExterna txtCodVenda.Text, Trim(txtDescricao.Text)
            
    End Select

End Sub

Private Function SelecionarDadosProduto(codvenda As String) As Boolean

   SelecionarDadosProduto = False

   Dim avData() As Variant

   ReDim avData(0, 0)
   ReDim avParametros(0, 1)
      
   avParametros(0, AV_CONTEUDO) = codvenda
   avParametros(0, AV_TIPO) = TIPO_NUMERICO
        
   cSql = " stp_produto_venda_dados_s " & FormataParametrosSQL(avParametros)
        
   bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
   If bResp = False Then
      Exit Function
   End If
        
   If gObjeto.nErroSQL <> 0 Then
      MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
      Exit Function
   End If
    
   If Val(avData(0, 0)) <= 0 Then
      MsgBox "Produto N�o Cadastrado ou Inativo.", vbCritical
        If txtCodVenda.Enabled Then
            txtCodVenda.Text = ""
            txtCodVenda.SetFocus
        End If
      Exit Function
   End If

    'verifica se � kit
    Call CarregarMateriaisProduto
    
    'Consiste lista tecnica para kits
    If indicaKit = True Then
        If Not ExisteListaTecnica(Val(codvenda)) Then
            MsgBox "Este Kit n�o tem nenhuma lista t�cnica cadastrada, por isto n�o pode ser disponibilizado!!", vbCritical
'            Exit Function
        Else
            indicaKit = False
        End If
    End If
    
   'Descricao
   txtDescricao.Text = avData(1, 0)
   'Linha
   If Val(avData(2, 0)) > 0 Then
        vLinha = Val(avData(2, 0))
        For X = 0 To cmbLinha.ListCount - 1
            If cmbLinha.ItemData(X) = Val(avData(2, 0)) Then
                cmbLinha.ListIndex = X
                cmbLinha.Tag = cmbLinha.Text
                Exit For
            End If
        Next
   End If
   'Categoria
   If Val(avData(3, 0)) > 0 Then
        For X = 0 To cmbCategoria.ListCount - 1
            vCateg = Val(avData(3, 0))
            If cmbCategoria.ItemData(X) = Val(avData(3, 0)) Then
                cmbCategoria.ListIndex = X
                cmbCategoria.Tag = cmbCategoria.Text
                Exit For
            End If
        Next
   End If
   'Controle do PCL
   If Val(avData(4, 0)) = 1 Then
      chkCGL.Value = 1
   ElseIf Val(avData(4, 0)) = 2 Then
          chkCGL.Value = 0
   End If
   
   'variaveis para controle de alteracao
   vDescr = Trim(codvenda)
   vLinha = Val(avData(2, 0))
   vCateg = Val(avData(3, 0))
   
   SelecionarDadosProduto = True
   
End Function

Private Sub CarregarMateriaisProduto()

    Dim X As Integer

    ReDim avData(0, 0)
    ReDim avParametros(1, 1)
      
    avParametros(0, AV_CONTEUDO) = txtCodVenda.Text
    avParametros(0, AV_TIPO) = TIPO_NUMERICO
    
    cSql = " stp_lista_material_produto_s " & FormataParametrosSQL(avParametros)
        
    bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)
        
    If bResp = False Then
       Exit Sub
    End If
        
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
       Exit Sub
    End If
   
    If Val(avData(0, 0)) <= 0 Then Exit Sub
   
    indicaKit = False
   
    For X = 0 To UBound(avData, 2)
        'indica se e kit ou nao
        If Val(avData(2, X)) = 2 Or Val(avData(2, X)) = 3 Or Val(avData(2, X)) = 4 Then
           indicaKit = True
           Exit Sub
        End If
    Next

End Sub

Private Function ExisteListaTecnica(vCodKit1) As Boolean

    ExisteListaTecnica = False
    
    'no caso do ciclo deve buscar a data final do ciclo
    cSql = "declare @kit_existe char(1), @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
    cSql = cSql & " exec stp_pdr_existe_kit_s " & vCodKit1 & ", 'P'"
    cSql = cSql & ",@kit_existe out, @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
    If gObjeto.natPrepareQuery(hSql, cSql) Then
       Do While gObjeto.natFetchNext(hSql)
       Loop
    End If
    If gObjeto.nErroSQL <> 0 Then
       MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
       Exit Function
    End If
          
    sql_cd_ret = CInt(gObjeto.varOutputParam(1))
    sql_cd_opr = gObjeto.varOutputParam(2)
    sql_nm_tab = gObjeto.varOutputParam(3)
    sql_qt_lnh = CLng(gObjeto.varOutputParam(4))
          
    If sql_cd_ret <> 0 Then
       MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
       Exit Function
    End If
    
    If gObjeto.varOutputParam(0) = "S" Then
       ExisteListaTecnica = True
    Else
       ExisteListaTecnica = False
    End If
          
End Function

Private Sub SelecionarAbrangenciaVigencia()

'On Error GoTo erro
   Dim avData() As Variant
   Dim vTipoEst As String
   
   ReDim avData(0, 0)
   ReDim avParametros(0, 1)
   
   spdAVIG.MaxRows = 0
      
   avParametros(0, AV_CONTEUDO) = txtCodVenda.Text
   avParametros(0, AV_TIPO) = TIPO_NUMERICO
    
   cSql = " stp_abrangencia_prod_venda_s " & FormataParametrosSQL(avParametros)
   bResp = gObjeto.natExecuteQuery(hSql, cSql, avData)

   If bResp = False Then
      Exit Sub
   End If
        
   If gObjeto.nErroSQL <> 0 Then
      MsgBox gObjeto.nServerNum & " - " & gObjeto.sServerMsg
      Exit Sub
   End If

   vGeraPend = True
      
    If Val(avData(1, 0)) <= 0 Then
        strOperacao = "I"
        vProdNovo = True
        Exit Sub
    Else
        strOperacao = "A"
        vProdNovo = False
    End If
    
   'Colunas do Spread
   '01 - ABRANG
   '02 - CICLO INI
   '03 - DATA INI
   '04 - CICLO FIN
   '05 - DATA FIN
   '06 - STATUS
   '07 - SEQUENCIA - CPO TABELA
   '08 - TIPO DE ESTRUTURA
   '09 - COD ESTRUTURA
   '10 - RE
   '11 - GV
   '12 - SET
   '13 - CICLO INI ANT
   '14 - DATA INI ANT
   '15 - CICLO FIN ANT
   '16 - DATA FIN ANT
   '17 - OPERACAO - (I)NCLUIR, (A)LTERAR

   spdAVIG.Visible = False
   Me.MousePointer = vbHourglass
   
   
   For X = 0 To UBound(avData, 2)
       spdAVIG.MaxRows = spdAVIG.MaxRows + 1
       
       vTipoEst = ""
       'tipo estrutura comercial
       If avData(9, X) = "2" Then
          vTipoEst = "RE "
       ElseIf avData(9, X) = "3" Then
              vTipoEst = "GV "
       ElseIf avData(9, X) = "4" Then
              vTipoEst = "ST "
       End If
       'abrangencia
       spdAVIG.SetText 1, spdAVIG.MaxRows, vTipoEst & avData(1, X) & " - " & avData(2, X)
       'ciclo inic
       If avData(3, X) <> "" Then
          spdAVIG.SetText 2, spdAVIG.MaxRows, Right(avData(3, X), 2) & "/" & Left(avData(3, X), 4)
       End If
       'data inicio
       spdAVIG.SetText 3, spdAVIG.MaxRows, avData(4, X)
       'ciclo fim
       If avData(5, X) <> "" Then
          spdAVIG.SetText 4, spdAVIG.MaxRows, Right(avData(5, X), 2) & "/" & Left(avData(5, X), 4)
       End If
       'data final
       spdAVIG.SetText 5, spdAVIG.MaxRows, avData(6, X)
       'sequencia
       spdAVIG.SetText 7, spdAVIG.MaxRows, avData(8, X)
       'tipo estrutura comercial
       spdAVIG.SetText 8, spdAVIG.MaxRows, avData(9, X)
       'cod estrutura comercial
       spdAVIG.SetText 9, spdAVIG.MaxRows, avData(1, X)
       'regiao estrategia
       spdAVIG.SetText 10, spdAVIG.MaxRows, avData(10, X)
       'gerencia vendas
       spdAVIG.SetText 11, spdAVIG.MaxRows, avData(11, X)
       'setor
       spdAVIG.SetText 12, spdAVIG.MaxRows, avData(12, X)
       'ciclo inicio ant
       If avData(3, X) <> "" Then
          spdAVIG.SetText 13, spdAVIG.MaxRows, Right(avData(3, X), 2) & "/" & Left(avData(3, X), 4)
       End If
       'data inicio ant
       spdAVIG.SetText 14, spdAVIG.MaxRows, avData(4, X)
       'ciclo fim ant
       If avData(5, X) <> "" Then
          spdAVIG.SetText 15, spdAVIG.MaxRows, Right(avData(5, X), 2) & "/" & Left(avData(5, X), 4)
       End If
       'data final ant
       spdAVIG.SetText 16, spdAVIG.MaxRows, avData(6, X)
       'status
       If Val(avData(7, X)) = 1 Then
          vStatus = "CANCELADA"
       Else
          vStatus = VerificarStatusCiclo(spdAVIG.MaxRows)
       End If

       spdAVIG.SetText 6, spdAVIG.MaxRows, vStatus
       If vStatus = "CANCELADA" Then
          spdAVIG.BlockMode = True
          spdAVIG.Row = spdAVIG.MaxRows
          spdAVIG.Row2 = spdAVIG.MaxRows
          spdAVIG.Col = -1
          spdAVIG.Col2 = spdAVIG.MaxCols
          spdAVIG.BackColor = &H80FFFF
          spdAVIG.RowHidden = True
          spdAVIG.BlockMode = False
        Else
            If vStatus <> "ATIVA" Then
                 spdAVIG.BlockMode = True
                 spdAVIG.Row = spdAVIG.MaxRows
                 spdAVIG.Row2 = spdAVIG.MaxRows
                 spdAVIG.Col = -1
                 spdAVIG.Col2 = spdAVIG.MaxCols
                 spdAVIG.BackColor = &HFFFF80
                 spdAVIG.RowHidden = True
                 spdAVIG.BlockMode = False
            End If
        End If
       
       'variavel para indicar se ira gerar pendencia
       vGeraPend = False
       
   Next
   spdAVIG.Visible = True
   Me.MousePointer = 1

   'Erase avData
   
   Exit Sub

erro:
'   If Err.Number = 10 Then
'       Exit Sub
'   End If
   MsgBox Err.Number & " - " & Err.Description, vbCritical, "Aviso"
End Sub

Private Sub LimparCampos()

    spdAVIG.MaxRows = 0
    chkCGL.Value = 0
    chkDivisao.Value = 0
    chkMostraCanc.Value = 0
    txtCodVenda.Text = ""
    txtDescricao.Text = ""
    cmbLinha.ListIndex = -1
    cmbCategoria.ListIndex = -1
    
    cmbLinha.Tag = ""
    cmbCategoria.Tag = ""
    
    vGeraPend = False
        
    vDescr = ""
    vLinha = 0
    vCateg = 0
            
End Sub

Private Function CancelarAbrangenciaVigencia() As Boolean
 
   CancelarAbrangenciaVigencia = False
  
   spdAVIG.GetText 1, spdAVIG.ActiveRow, vCodEstr
   spdAVIG.GetText 6, spdAVIG.ActiveRow, vStatus
   spdAVIG.GetText 7, spdAVIG.ActiveRow, vSequencia
   spdAVIG.GetText 17, spdAVIG.ActiveRow, vOperacao
   
   If vStatus = "CANCELADA" Then
      MsgBox "Abrang�ncia / Vig�ncia J� Cancelada.", vbCritical, "Aten��o"
      Exit Function
   End If
   
   If MsgBox("Confirma o Cancelamento da Abrang�ncia / Vig�ncia --> " & vCodEstr, vbQuestion + vbYesNo, "Aviso") = vbNo Then
      CancelarAbrangenciaVigencia = False
      Exit Function
   End If
   
   'indica que esta cancelando uma abrangencia que ainda nao incluir
   If vOperacao = "I" Then
   
      spdAVIG.BlockMode = True
      spdAVIG.Col = -1
      spdAVIG.Row = spdAVIG.ActiveRow
      spdAVIG.Col2 = -1
      spdAVIG.Row2 = spdAVIG.ActiveRow
      'Delete a row in the spreadsheet
      spdAVIG.Action = 5
      spdAVIG.BlockMode = False
      spdAVIG.MaxRows = spdAVIG.MaxRows - 1
      
   Else
   
      'Cancelada ou Encerrada
      spdAVIG.SetText 6, spdAVIG.ActiveRow, "CANCELADA"
      spdAVIG.SetText 17, spdAVIG.ActiveRow, "E"
      spdAVIG.BlockMode = True
      spdAVIG.Row = spdAVIG.ActiveRow
      spdAVIG.Row2 = spdAVIG.ActiveRow
      spdAVIG.Col = -1
      spdAVIG.Col2 = spdAVIG.MaxCols
      spdAVIG.BackColor = vbYellow
      spdAVIG.RowHidden = True
      spdAVIG.BlockMode = False
      
    End If
      
    MsgBox "Abrang�ncia / Vig�ncia Cancelada.", vbInformation, "Aviso"
   
    CancelarAbrangenciaVigencia = True
   
    sLiberaSalvar

End Function

Private Function ManutencaoAbrangencia() As Boolean

   '01 - ABRANG
   '02 - CICLO INI
   '03 - DATA INI
   '04 - CICLO FIN
   '05 - DATA FIN
   '06 - STATUS
   '07 - SEQUENCIA - CPO TABELA
   '08 - TIPO DE ESTRUTURA
   '09 - COD ESTRUTURA
   '10 - RE
   '11 - GV
   '12 - SET
   '13 - CICLO INI ANT
   '14 - DATA INI ANT
   '15 - CICLO FIN ANT
   '16 - DATA FIN ANT
   '17 - OPERACAO - (I)NCLUIR, (A)LTERAR
 
   ManutencaoAbrangencia = False
   
On Error GoTo ErroManut
   
   vQtdMat = 0
   
   bResp = gObjeto.natPrepareQuery(hSql, "Begin Transaction")
   
   
    vGeraPend = False
   
   For X = 1 To spdAVIG.MaxRows
   
       spdAVIG.GetText 2, X, vCiclIni
       spdAVIG.GetText 3, X, vDataIni
       spdAVIG.GetText 4, X, vCiclFin
       spdAVIG.GetText 5, X, vDataFin
       spdAVIG.GetText 7, X, vSequencia
       spdAVIG.GetText 8, X, vTipEstr
       spdAVIG.GetText 9, X, vCodEstr
       spdAVIG.GetText 13, X, vCiclIniAnt
       spdAVIG.GetText 14, X, vDataIniAnt
       spdAVIG.GetText 15, X, vCiclFinAnt
       spdAVIG.GetText 16, X, vDataFinAnt
       spdAVIG.GetText 17, X, vOperacao
       
       If Trim(vCiclIni) <> "" Then
          vCiclIni = Right(vCiclIni, 4) & Left(vCiclIni, 2)
       Else
          vCiclIni = 0
       End If
       If Trim(vCiclFin) <> "" Then
          vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
       Else
          vCiclFin = 0
       End If
       
       If Trim(vCiclIniAnt) <> "" Then
          vCiclIniAnt = Right(vCiclIniAnt, 4) & Left(vCiclIniAnt, 2)
       Else
          vCiclIniAnt = 0
       End If
       If Trim(vCiclFinAnt) <> "" Then
          vCiclFinAnt = Right(vCiclFinAnt, 4) & Left(vCiclFinAnt, 2)
       Else
          vCiclFinAnt = 0
       End If
       
       'indica se executa ou nao a proc
       vExecuta = False
       
       'Inclusao
       If vOperacao = "I" Then
       
          vExecuta = True
          
          'chama a procedure de insercao de uma nova Abrangencia Vigencia
          cSql = "declare @qt_lin_mat int, @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
          cSql = cSql & " exec stp_pdr_abrangencia_i " & txtCodVenda.Text & "," & vTipEstr & "," & vCodEstr & ","
          cSql = cSql & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "','" & UCase(Environ("_U")) & "'"
          cSql = cSql & " ,@qt_lin_mat out, @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
          
       ElseIf vOperacao = "E" Then
           
              vExecuta = True
       
              'chama a procedure de cancelamento da Abrangencia Vigencia
              cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
              cSql = cSql & " exec stp_pdr_canc_abrangencia_u " & txtCodVenda.Text & "," & vSequencia & ",'" & UCase(Environ("_U")) & "'"
              cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
       ElseIf vOperacao = "A" Then
          
              'indica que houve alteracao
              If vCiclIni <> vCiclIniAnt Or _
                 vDataIni <> vDataIniAnt Or _
                 vCiclFin <> vCiclFinAnt Or _
                 vDataFin <> vDataFinAnt Then
               
                 vExecuta = True
               
                 'chama a procedure de alteracao de uma Abrangencia Vigencia
                 cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
                 cSql = cSql & " exec stp_pdr_abrangencia_u " & txtCodVenda.Text & "," & vSequencia & "," & vTipEstr & ","
                 cSql = cSql & vCodEstr & "," & vCiclIni & ",'" & vDataIni & "'," & vCiclFin & ",'" & vDataFin & "',"
                 cSql = cSql & vCiclIniAnt & ",'" & vDataIniAnt & "'," & vCiclFinAnt & ",'" & vDataFinAnt & "','" & UCase(Environ("_U")) & "'"
                 cSql = cSql & " , @sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
               
              End If
       
       End If
       
       'executa a proc de insert ou de update
       If vExecuta = True Then
       
          If gObjeto.natPrepareQuery(hSql, cSql) Then
             Do While gObjeto.natFetchNext(hSql)
             Loop
          End If
          If gObjeto.nErroSQL <> 0 Then
             MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
            GoTo Rollback
          End If
          
          If vOperacao = "I" Then
             vGeraPend = True
             vQtdMat = CInt(gObjeto.varOutputParam(0))
             sql_cd_ret = CInt(gObjeto.varOutputParam(1))
             sql_cd_opr = gObjeto.varOutputParam(2)
             sql_nm_tab = gObjeto.varOutputParam(3)
             sql_qt_lnh = CLng(gObjeto.varOutputParam(4))
          ElseIf vOperacao = "A" Or vOperacao = "E" Then
                 sql_cd_ret = CInt(gObjeto.varOutputParam(0))
                 sql_cd_opr = gObjeto.varOutputParam(1)
                 sql_nm_tab = gObjeto.varOutputParam(2)
                 sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
          End If
              
          If sql_cd_ret <> 0 Then
             MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
            GoTo Rollback
          End If
          
       End If
      
   Next
   
   If vGeraPend = True And vQtdMat = 0 Then
      MsgBox "Nenhuma abrang�ncia foi criada porque o produto n�o tem nenhum material Ativo!!"
      ManutencaoAbrangencia = True
      GoTo Rollback
   End If
   
   'verifica a necessidade de se criar a pendencia
   If vGeraPend = True And vQtdMat > 1 Then
   
      'chama a procedure de insercao de pendencia
      cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
      cSql = cSql & " exec stp_produto_venda_pendencia_i " & txtCodVenda.Text & "," & 1 & "," & 1 & "," & 0
      cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
      If gObjeto.natPrepareQuery(hSql, cSql) Then
         Do While gObjeto.natFetchNext(hSql)
         Loop
      End If
      If gObjeto.nErroSQL <> 0 Then
         MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
         GoTo Rollback
      End If
          
      If sql_cd_ret <> 0 Then
         MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
         GoTo Rollback
      End If
      
      MsgBox "Pend�ncia de Prioridade criada para esta Abrang�ncia/Vig�ncia.", vbInformation, "Aviso"
      
   End If
   
    If vProdNovo And vQtdMat > 0 Then
      cSql = "declare @sql_cd_ret int, @sql_cd_opr char(1), @sql_nm_tab char(30), @sql_qt_lnh int "
      cSql = cSql & " exec stp_produto_venda_pendencia_i " & txtCodVenda.Text & "," & 3 & "," & 1 & "," & 0
      cSql = cSql & " ,@sql_cd_ret out, @sql_cd_opr out, @sql_nm_tab out, @sql_qt_lnh out"
       
      If gObjeto.natPrepareQuery(hSql, cSql) Then
         Do While gObjeto.natFetchNext(hSql)
         Loop
      End If
      If gObjeto.nErroSQL <> 0 Then
         MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
         GoTo Rollback
      End If
          
      If sql_cd_ret <> 0 Then
         MsgBox "Erro ao executar a stp_pdr_produto_venda_u " & "RETURN STATUS = " & sql_cd_ret & " " & gObjeto.sServerMsg
         GoTo Rollback
      End If
      
      MsgBox "Criada Pend�ncia de Descri��o CAN para este produto novo.", vbInformation, "Aviso"
      
    End If
    
   bResp = gObjeto.natPrepareQuery(hSql, "Commit Transaction")
   
   'MsgBox "Atualiza��o Realizada com Sucesso.", vbInformation, "Aviso"
   
   ManutencaoAbrangencia = True
   
   Exit Function
   
ErroManut:

   MsgBox "Erro. " & Err.Number & " - " & Err.Description, vbCritical, "Erro."

Rollback:
   bResp = gObjeto.natPrepareQuery(hSql, "Rollback Transaction")

End Function

Private Sub VerificarDivisaoBoleto()

   Dim nQtd As Integer
   
   If Trim(txtCodVenda.Text) = "" Then Exit Sub
   
   'Anderson
   nRet = DbOra.DbCall("P_SELECIONA_DIVISAO_BOLETO", txtCodVenda.Text, nQtd)

   If nRet <> "" Then
      MsgBox " Falha na Execu��o da Procedure "
      Exit Sub
   End If
   
   chkDivisao.Value = 0
   If Val(nQtd) > 0 Then
      chkDivisao.Value = 1
   End If
      
End Sub

Private Function VerificarStatusCiclo(vLin As Integer) As String
 
    vDataInicioCiclo = ""
    vDataFinalCiclo = ""
 
    VerificarStatusCiclo = "ATIVA"
    
    spdAVIG.GetText 4, vLin, vCiclFin
    spdAVIG.GetText 5, vLin, vDataFin
    spdAVIG.GetText 6, vLin, vStatus
    spdAVIG.GetText 8, vLin, vTipEstr
    spdAVIG.GetText 9, vLin, vCodEstr
    
    'formata ano/dia
    vCiclFin = Right(vCiclFin, 4) & Left(vCiclFin, 2)
    
    'determinar data final
    If Trim(vCiclFin) = "" And Trim(vDataFin) = "" Then
       vDataFin = "31/12/9999"
    End If
    
    'qdo e por data, so compara com a data de hoje
    If Trim(vDataFin) <> "" Then
       If DateValue(Now) > DateValue(vDataFin) Then
          VerificarStatusCiclo = "ENCERRADA"
          Exit Function
       End If
    End If
    
    'no caso do ciclo deve buscar a data final do ciclo
    If Trim(vCiclFin) <> "" Then
       If Not SelecionarVigenciaCiclo(vCiclFin, vTipEstr, vCodEstr) Then Exit Function
    End If
    
    'qdo e por data, so compara com a data de hoje
    If Trim(vDataFinalCiclo) <> "" Then
       If DateValue(Now) > DateValue(vDataFinalCiclo) Then
          VerificarStatusCiclo = "ENCERRADA"
          Exit Function
       End If
    End If

End Function

Private Sub txtDescricao_KeyPress(KeyAscii As Integer)

    If Trim(txtDescricao.Text) = "" Then
       Exit Sub
    Else
        sLiberaSalvar
    End If

End Sub

'Private Sub ObtemInfomacoesAfetadas(p_cod_venda As String, p_desc_venda As String)
'
'    Dim sql       As String
'    Dim Occur     As Integer
'    Dim X         As Double
'    Dim TelaLog   As Form
'    Dim vSituacao As String
'
'    If UBound(CiclosProdutosDEPOIS) <= 0 Then
'        Exit Sub
'    End If
'
'    On Error GoTo trata_erro
'
'    Set TelaLog = New frmLogCiclosAfetados
'
'    For X = 1 To UBound(CiclosProdutosDEPOIS)
'
'        sql = "declare "
'        sql = sql & "@sql_cd_ret int, "
'        sql = sql & "@sql_cd_opr char(1), "
'        sql = sql & "@sql_nm_tab char(30), "
'        sql = sql & "@sql_qt_lnh int "
'        sql = sql & " exec st_pdr_info_afetadas_s "
'        sql = sql & CiclosProdutosDEPOIS(X) & ","
'        sql = sql & p_cod_venda & ","
'        sql = sql & "@sql_cd_ret out,"
'        sql = sql & "@sql_cd_opr out,"
'        sql = sql & "@sql_nm_tab out,"
'        sql = sql & "@sql_qt_lnh out"
'
'        If gObjeto.natPrepareQuery(hSql, sql) Then
'            Do While gObjeto.natFetchNext(hSql)
'                vSituacao = gObjeto.varResultSet(hSql, 0)
'                TelaLog.CriaLinha
'                TelaLog.RecebeDados 1, Mid(CiclosProdutosDEPOIS(X), 5, 2) & "/" & Mid(CiclosProdutosDEPOIS(X), 1, 4)
'                TelaLog.RecebeDados 2, p_cod_venda
'                TelaLog.RecebeDados 3, p_desc_venda
'                TelaLog.RecebeDados 4, vSituacao
'                DoEvents
'            Loop
'        End If
'
'        If gObjeto.nErroSQL <> 0 Then
'            MsgBox "Erro ao executar a st_pdr_produto_ciclos_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg, vbCritical, "Aten��o"
'            Exit Sub
'        End If
'
'    Next X
'
'    TelaLog.Show
'
'    Exit Sub
'
'trata_erro:
'    MsgBox "ObtemInfomacoesAfetadas - Erro (" & Err & ") - " & Error, vbCritical, "Aten��o"
'    Exit Sub
'
'End Sub

Private Sub ObtemInfomacoesAfetadas(p_cod_venda As String, p_desc_venda As String)

    Dim sql       As String
    Dim Occur     As Integer
    Dim X         As Double
    Dim TelaLog   As Form
    Dim vSituacao As String
    Dim qtdMsg    As Double
    
    If UBound(CiclosProdutosDEPOIS) <= 0 Then
        Exit Sub
    End If
        
    On Error GoTo trata_erro
    
    Set TelaLog = New frmLogCiclosAfetados
    qtdMsg = 0
    
    For X = 1 To UBound(CiclosProdutosDEPOIS)
    
        sql = "declare "
        sql = sql & "@sql_cd_ret int, "
        sql = sql & "@sql_cd_opr char(1), "
        sql = sql & "@sql_nm_tab char(30), "
        sql = sql & "@sql_qt_lnh int "
        sql = sql & " exec st_pdr_info_afetadas_s "
        sql = sql & CiclosProdutosDEPOIS(X) & ","
        sql = sql & p_cod_venda & ","
        sql = sql & "@sql_cd_ret out,"
        sql = sql & "@sql_cd_opr out,"
        sql = sql & "@sql_nm_tab out,"
        sql = sql & "@sql_qt_lnh out"
    
        If gObjeto.natPrepareQuery(hSql, sql) Then
            Do While gObjeto.natFetchNext(hSql)
                vSituacao = gObjeto.varResultSet(hSql, 0)
                
                If Len(Trim(vSituacao)) > 0 Then
                    TelaLog.CriaLinha
                    TelaLog.RecebeDados 1, Mid(CiclosProdutosDEPOIS(X), 5, 2) & "/" & Mid(CiclosProdutosDEPOIS(X), 1, 4)
                    TelaLog.RecebeDados 2, p_cod_venda
                    TelaLog.RecebeDados 3, p_desc_venda
                    TelaLog.RecebeDados 4, vSituacao
                    qtdMsg = qtdMsg + 1
                    DoEvents
                End If
                
            Loop
        End If
        
        If gObjeto.nErroSQL <> 0 Then
            MsgBox "Erro ao executar a st_pdr_produto_ciclos_s - nServerNum = " & gObjeto.nServerNum & " sServerMsg = " & gObjeto.sServerMsg, vbCritical, "Aten��o"
            Exit Sub
        End If
        
    Next X
    
    If qtdMsg > 0 Then
        TelaLog.Show
    Else
        Set TelaLog = Nothing
    End If
    
    Exit Sub

trata_erro:
    MsgBox "ObtemInfomacoesAfetadas - Erro (" & Err & ") - " & Error, vbCritical, "Aten��o"
    Exit Sub
    
End Sub

Private Sub BuscarDadosTela()

On Error GoTo erro
    If Trim(txtCodVenda.Text) = "" Then
        MsgBox "Obrigat�rio preencher o c�digo de venda para pesquisa!!", vbCritical
        Exit Sub
    End If
    If Not IsNumeric(txtCodVenda.Text) Then
        MsgBox "O c�digo de venda deve ser num�rico!!", vbCritical
        Exit Sub
    End If
    

    If ChecaCodigo(Val(txtCodVenda.Text)) Then
        'SE 6346 - In�cio
        Dim CodUsuario As String
        If Not DesbloqueiaRegistro(3) Then
            MsgBox "Falha ao desbloquear registros.", vbCritical, "Erro"
            Exit Sub
        End If
        If VerificaBloqueioFormulario(3, CDbl(txtCodVenda.Text), 0, CodUsuario) Then
            ExibeMensagemBloqueio ObtemDescricaoFormulario(3), "Produto", txtCodVenda.Text, Empty, Empty, CodUsuario
            Exit Sub
        End If
        'SE 6346 - Fim
        'Seleciona os Dados do Produto na t_produto_venda e t_abrangencia_produto_venda
        If SelecionarDadosProduto(txtCodVenda.Text) = True Then
            'SE 6346 - In�cio
            If Not BloqueiaRegistro(3, Val(txtCodVenda.Text), 0) Then
                MsgBox "Falha ao tentar realizar bloqueio de registro.", vbCritical, "Erro"
                Exit Sub
            End If
            'SE 6346 - Fim
            'Seleciona as Abrangencias/Vigencias na t_abrangencia_produto
            If fonteDados = 1 Then
                SelecionarAbrangenciaVigencia
            End If
            'MsgBox "6"
            'Seleciona a Informacao de Divisao
            VerificarDivisaoBoleto
            'Obtem os ciclos ativos para o produto.
            'Informa��o usada somente ap�s a atualiza��o dos dados (apenas p/informar ao usu�rios)
            'Call ObtemCiclosProduto(txtCodVenda.Text, 1)
            'Desproteje os controles
            
            DesprotegeTela
        End If
    End If
    Exit Sub
    
    
erro:
'    If Err.Number = 10 Then
'        Resume Next
'    End If
    MsgBox Err.Number & " - " & Err.Description, vbCritical, "Aviso"
End Sub
