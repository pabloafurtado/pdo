VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmListaEstrut 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relaciona Lista Pre�o X Estrutura Comercial"
   ClientHeight    =   7020
   ClientLeft      =   600
   ClientTop       =   2130
   ClientWidth     =   11160
   Icon            =   "frmListaEstrut.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7020
   ScaleWidth      =   11160
   Begin ComctlLib.Toolbar st_Toolbar 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11160
      _ExtentX        =   19685
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   11
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Description     =   "Novo"
            Object.ToolTipText     =   "Nova Pesquisa"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Carrega Lista"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Excluir"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Gravar"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Visible         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Imprimir"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   ""
            Object.ToolTipText     =   "Pesquisar"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   4
            Object.Width           =   2000
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Sair"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Frame Frame7 
      Height          =   735
      Left            =   300
      TabIndex        =   1
      Top             =   720
      Width           =   10755
      Begin VB.TextBox txtReajuste 
         Enabled         =   0   'False
         Height          =   360
         Left            =   8520
         TabIndex        =   7
         Top             =   240
         Width           =   1110
      End
      Begin VB.ComboBox cmbListas 
         Enabled         =   0   'False
         Height          =   360
         ItemData        =   "frmListaEstrut.frx":27A2
         Left            =   4740
         List            =   "frmListaEstrut.frx":27A4
         TabIndex        =   5
         Top             =   240
         Width           =   870
      End
      Begin VB.ComboBox cmbCiclo 
         Height          =   360
         ItemData        =   "frmListaEstrut.frx":27A6
         Left            =   1860
         List            =   "frmListaEstrut.frx":27D1
         TabIndex        =   3
         Top             =   240
         Width           =   1545
      End
      Begin VB.Label Label3 
         Caption         =   "% Reajuste:"
         Height          =   255
         Left            =   7200
         TabIndex        =   6
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Lista:"
         Height          =   255
         Left            =   3900
         TabIndex        =   4
         Top             =   300
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Ciclo/ano:"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   300
         Width           =   915
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Ger�ncia de Venda"
      Height          =   735
      Left            =   3780
      TabIndex        =   10
      Top             =   1560
      Width           =   3585
      Begin VB.ComboBox cmbGerVenda 
         Enabled         =   0   'False
         Height          =   360
         Left            =   90
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   270
         Width           =   3420
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Regi�o Estrat�gica"
      Height          =   735
      Left            =   30
      TabIndex        =   8
      Top             =   1560
      Width           =   3720
      Begin VB.ComboBox cmbRegEstrat 
         Enabled         =   0   'False
         Height          =   360
         Left            =   120
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   270
         Width           =   3555
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Setor"
      Height          =   735
      Left            =   7365
      TabIndex        =   12
      Top             =   1545
      Width           =   3690
      Begin VB.ComboBox cmbSetor 
         Enabled         =   0   'False
         Height          =   360
         Left            =   135
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   270
         Width           =   3480
      End
   End
   Begin VB.CommandButton cmdRelaciona 
      Caption         =   "&Relacionar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   4905
      Picture         =   "frmListaEstrut.frx":284A
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   2355
      Width           =   1440
   End
   Begin FPSpread.vaSpread grdVisao 
      Height          =   3615
      Left            =   180
      TabIndex        =   15
      Top             =   3240
      Width           =   10845
      _Version        =   131077
      _ExtentX        =   19129
      _ExtentY        =   6376
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   4
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      ScrollBars      =   2
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmListaEstrut.frx":2B54
      UserResize      =   1
      VisibleCols     =   500
      VisibleRows     =   500
   End
   Begin VB.Label lblTipoProcesso 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   210
      TabIndex        =   16
      Top             =   2640
      Visible         =   0   'False
      Width           =   60
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   6960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":2F84
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":329E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":35B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":38D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":3BEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":3F06
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":4220
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":453A
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":4854
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":4B6E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   870
      Top             =   14490
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":4E88
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":51A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":54BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":57D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":5AF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":5E0A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":6124
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmListaEstrut.frx":643E
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmListaEstrut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vRelacionouLista As Boolean

Private Sub cmbCiclo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If cmbCiclo.ListIndex <> -1 Then
            cmbRegEstrat.Enabled = True
            cmbGerVenda.Enabled = True
            cmbSetor.Enabled = True
            cmbListas.Enabled = True
            txtReajuste.Enabled = True
'            cmbRegEstrat.SetFocus
            cmbListas.SetFocus
        Else
            cmbRegEstrat.Enabled = False
            cmbGerVenda.Enabled = False
            cmbSetor.Enabled = False
            cmbListas.Enabled = False
            txtReajuste.Enabled = False
        End If
        KeyAscii = 0
        Exit Sub
    End If


End Sub

Private Sub cmbGerVenda_Click()

   If cmbGerVenda.ListIndex = -1 Then
      Exit Sub
   End If
   
   cmbSetor.Clear
   cmbSetor.ListIndex = -1
'   cmbSetor.Text = ""

   If Not Lista_Estrutura(cmbSetor, 4, cmbRegEstrat.ItemData(cmbRegEstrat.ListIndex), _
                                       cmbGerVenda.ItemData(cmbGerVenda.ListIndex)) Then
        SetStatus "Erro na leitura..."
        MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
        PonteiroMouse vbNormal
   End If
   
   cmdRelaciona.Enabled = Libera_Gravacao
   

End Sub

Private Sub cmbListas_Change()

Dim r As Long

   If cmbListas.ListIndex <> -1 Then
      txtReajuste.Text = cmbListas.ItemData(cmbListas.ListIndex)
      txtReajuste.Text = Format(txtReajuste.Text, "#0.00")
      
   Else
      For r = 0 To cmbListas.ListCount - 1
         If cmbListas.Text = cmbListas.List(r) Then
            txtReajuste.Text = cmbListas.ItemData(r)
            txtReajuste.Text = Format(txtReajuste.Text, "#0.00")
            cmdRelaciona.Enabled = Libera_Gravacao
            Exit Sub
         Else
            txtReajuste.Text = ""
         End If
      Next
   End If
   
   txtReajuste.Tag = txtReajuste.Text
   
   cmdRelaciona.Enabled = Libera_Gravacao
   
End Sub

Private Sub cmbListas_Click()

Dim r As Long

   If cmbListas.ListIndex <> -1 Then
      txtReajuste.Text = cmbListas.ItemData(cmbListas.ListIndex)
      txtReajuste.Text = Format(txtReajuste.Text, "#0.00")
   Else
      For r = 1 To cmbListas.ListCount - 1
         If cmbListas.Text = cmbListas.List(r) Then
            txtReajuste.Text = cmbListas.ItemData(r)
            txtReajuste.Text = Format(txtReajuste.Text, "#0.00")
            Exit Sub
         End If
      Next
   End If
   
   txtReajuste.Tag = txtReajuste.Text
   
   cmdRelaciona.Enabled = Libera_Gravacao
   
End Sub

Private Sub cmbListas_GotFocus()
   cmdRelaciona.Enabled = Libera_Gravacao
End Sub

Private Sub cmbListas_KeyPress(KeyAscii As Integer)
   cmdRelaciona.Enabled = Libera_Gravacao
End Sub



Private Sub cmbRegEstrat_Click()
   If cmbRegEstrat.ListIndex = -1 Then
      Exit Sub
   End If
   
   cmbGerVenda.Clear
   cmbGerVenda.ListIndex = -1
   
   cmbSetor.Clear
   cmbSetor.ListIndex = -1
   
   DoEvents
   
   If Not Lista_Estrutura(Me.cmbGerVenda, 3, cmbRegEstrat.ItemData(cmbRegEstrat.ListIndex), 0) Then
      SetStatus "Erro na leitura..."
      MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
   End If
   
   cmdRelaciona.Enabled = Libera_Gravacao


End Sub

Private Sub cmbSetor_LostFocus()
   cmdRelaciona.Enabled = Libera_Gravacao
End Sub

Private Sub cmbCiclo_Click()
   
'    cmbCiclo.Text = Left$(cmbCiclo.Text, 2) & Right$(cmbCiclo.Text, 4)
    
    cmdRelaciona.Enabled = Libera_Gravacao
    cmbRegEstrat.Enabled = True
    cmbGerVenda.Enabled = True
    cmbSetor.Enabled = True
    cmbListas.Enabled = True
    txtReajuste.Enabled = True
'    cmbRegEstrat.SetFocus
    cmbListas.SetFocus
    
    Exit Sub
  
    grdVisao.MaxRows = 0



End Sub

Private Sub cmbCiclo_LostFocus()
    
    If cmbCiclo.Text = "" Then
        Exit Sub
    End If
    
    'SE 6346 - In�cio
    If Not DesbloqueiaRegistro(8) Then
        MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    Dim CodUsuario As String
    If VerificaBloqueioFormulario(8, CDbl(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)), 0, CodUsuario) Then
        ExibeMensagemBloqueio ObtemDescricaoFormulario(8), "Ciclo", Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), "", "", CodUsuario
        sLimpaTela
        Exit Sub
    End If
    'SE 6346 - Fim
    
    If Not PreencheGrid(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
       SetStatus "Erro na leitura..."
       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
    End If
    
    'SE 6346 - In�cio
    If Not BloqueiaRegistro(8, CDbl(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)), 0) Then
        MsgBox "Falha ao tentar bloquear registro.", vbCritical, "Erro"
        Exit Sub
    End If
    'SE 6346 - Fim

    If Not Combo_Lista(Me.cmbListas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
       SetStatus "Erro na leitura..."
       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
    End If
      
    If cmbListas.Enabled Then
        cmbListas.SetFocus
    End If
'    If cmbRegEstrat.Enabled Then
'        cmbRegEstrat.SetFocus
'    End If

End Sub

Private Sub cmdRelaciona_Click()

    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    'If Not ChecaPermissao("frmPDO014", "M") Then
        'MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        'Exit Sub
    'End If
    
    'ALTERADO - VALIDAR PERMISSAO NA BASE O02PR
    If Not bVerificaFuncaoPermissaoLogin("frmPDO014", "M") Then
        MsgBox "Opera��o n�o autorizada para o usu�rio", vbCritical + vbOKOnly, "Controle de Acesso"
        Exit Sub
    End If
        
    If Libera_Gravacao Then
    
         vRelacionouLista = True
    
       ' Checa se � setor
        If cmbSetor.ListIndex <> -1 Then
        
           If Not Relacao_Lista(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), 4, cmbSetor.ItemData(cmbSetor.ListIndex), cmbListas.Text, _
                                txtReajuste, "I") Then
              SetStatus "Erro na leitura..."
              MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
              vRelacionouLista = False
           End If
           
        Else
        
          ' Checa se � gerencia de venda
            If cmbGerVenda.ListIndex <> -1 Then
               If Not Relacao_Lista(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), 3, cmbGerVenda.ItemData(cmbGerVenda.ListIndex), cmbListas.Text, _
                                    txtReajuste, "I") Then
                  SetStatus "Erro na leitura..."
                  MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                  vRelacionouLista = False
               End If
            Else
          ' Checa se � regi�o estrat�gica
                If cmbRegEstrat.ListIndex <> -1 Then
                   If Not Relacao_Lista(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), 2, cmbRegEstrat.ItemData(cmbRegEstrat.ListIndex), cmbListas.Text, _
                                        txtReajuste, "I") Then
                      SetStatus "Erro na leitura..."
                      MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
                      vRelacionouLista = False
                   End If
                End If
            End If
        End If
             
        If Not PreencheGrid(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
             SetStatus "Erro na leitura..."
             MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
             vRelacionouLista = False
        End If
             
        Call LiberaLista
        
        
        'SE 6346 - In�cio
        If vRelacionouLista Then
            If Not GravaHistorico(8, "A", "Ciclo", Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), "", "", "", "") Then
                 MsgBox "Falha ao tentar gravar hist�rico.", vbCritical
                 Exit Sub
            End If
        End If
        'SE 6346 - Fim
        
    '    txtReajuste.Text = ""
             
    '    If Not Combo_Lista(Me.cmbListas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
    '        SetStatus "Erro na leitura..."
    '        MsgBox MSG$, vbCritical + vbvRelacionouListaOnly, "E R R O "
    '    End If
    
    End If

End Sub

Private Sub LiberaLista()

    If vRelacionouLista And (txtReajuste.Tag <> txtReajuste.Text) Then
    
        ReDim LogGeraLista(0)
        
        If Val(TotalPrecos(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2))) > 0 Then
        
            If Not LiberarListas(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), MSG$) Then
                MsgBox MSG$, vbCritical, "A T E N � � O"
                lblTipoProcesso.Caption = ""
                PonteiroMouse vbNormal
            End If
            txtReajuste.Tag = txtReajuste.Text
            
            lblTipoProcesso.Caption = ""
                
            If UBound(LogGeraLista) > 0 Then
                Call ExibeLog
            Else
                MsgBox "Gera��o de lista de pre�o conclu�do com sucesso!!", vbInformation, Me.Caption
            End If
        End If
        
        vRelacionouLista = False
        
    End If

End Sub

Private Sub Form_Load()

'    Call Carrega_Ciclo(cmbCiclo)
    
    Call CarrCicloComPreco(cmbCiclo)

    If Not Lista_Estrutura(cmbRegEstrat, 2, 0, 0) Then
       SetStatus "Erro na leitura..."
       MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
    End If
   
End Sub

Private Sub Form_Resize()
   st_Toolbar.Buttons(10).Width = Me.Width - (Me.st_Toolbar.Buttons(1).Width * 9) + _
                                    (Me.st_Toolbar.Buttons(1).Width * 0.7)
End Sub

Private Sub Form_Unload(Cancel As Integer)

'    If cmdSalvar.Enabled = True Then
'       If MsgBox("Confirma o Cancelamento da Manuten��o.", vbQuestion + vbYesNo) = vbNo Then
'          Cancel = -1
'          Exit Sub
'       End If
'    End If


    MDIpdo001.DesbloqueiaMenu
   'SE 6346 - In�cio
   If Not DesbloqueiaRegistro(8) Then
       MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
       Exit Sub
   End If
   'SE 6346 - Fim
    Unload frmListaEstrut

End Sub

Private Sub grdVisao_DblClick(ByVal Col As Long, ByVal Row As Long)
Dim cd_estrutura As String
Dim Lista As String

   If Row = 0 Then Exit Sub
   
   If MsgBox("Deseja Realmente apagar a rela��o?", vbQuestion + vbYesNo, "A T E N � � O") = vbNo Then
      Exit Sub
   End If
   
   grdVisao.Row = Row
   
   grdVisao.Col = 4
   Lista = grdVisao.Text
   
   ' Setor
   grdVisao.Col = 3
   If Trim(grdVisao.Text) <> "" Then
   
      cd_estrutura = Trim(Left$(grdVisao.Text, InStr(grdVisao.Text, "-") - 1))
      
      If Not Relacao_Lista(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), 4, cd_estrutura, Lista, 0, "D") Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
      If Not Combo_Lista(Me.cmbListas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
      If Not PreencheGrid(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      Exit Sub
   End If
   
   ' Gerencia
   grdVisao.Col = 2
   If Trim(grdVisao.Text) <> "" Then
   
      cd_estrutura = Trim(Left$(grdVisao.Text, InStr(grdVisao.Text, "-") - 1))
      
      If Not Relacao_Lista(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), 3, cd_estrutura, Lista, 0, "D") Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
      If Not Combo_Lista(Me.cmbListas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
      If Not PreencheGrid(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      Exit Sub
   End If
   
   ' Regiao
   grdVisao.Col = 1
   If Trim(grdVisao.Text) <> "" Then
   
      cd_estrutura = Trim(Left$(grdVisao.Text, InStr(grdVisao.Text, "-") - 1))
      
      If Not Relacao_Lista(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), 2, cd_estrutura, Lista, 0, "D") Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
      If Not Combo_Lista(Me.cmbListas, Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
      If Not PreencheGrid(Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2)) Then
         SetStatus "Erro na leitura..."
         MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
      End If
      
   End If
   
   'SE 6346 - In�cio
   If Not GravaHistorico(8, "A", "Ciclo", Right$(cmbCiclo.Text, 4) & Left$(cmbCiclo.Text, 2), "", "", "", "") Then
       MsgBox "Falha ao tentar gravar hist�rico.", vbCritical, "Erro"
       Exit Sub
   End If
   'SE 6346 - Fim
End Sub

Private Sub st_Toolbar_ButtonClick(ByVal Button As ComctlLib.Button)

    Select Case Button.Index
        Case 1  ' Limpar
            sLimpaTela
        Case 3
            'frm_CN_ciclo_de_para.Show
            
        Case 5  ' Salvar
            MsgBox "Salvo", vbInformation, ""
        Case 6  ' imprimir
            grdVisao.Action = 32
        
        Case 8  ' Pesquisar
        Case 11 ' Sair
            SetStatus ""
            Unload Me
            Set frmListaEstrut = Nothing
    End Select
End Sub
Private Sub txtReajuste_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
      txtReajuste.Text = Format(txtReajuste.Text, "#0.00")
      cmdRelaciona.Enabled = Libera_Gravacao
      KeyAscii = 0
   End If
   
  If (KeyAscii = 45 And (Len(txtReajuste.Text) = 0 Or txtReajuste.SelLength = Len(txtReajuste.Text))) Then
      Exit Sub
   End If
   
   If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
      KeyAscii = 0
      Beep
   End If
   
End Sub

Private Sub txtReajuste_LostFocus()

   txtReajuste.Text = Format(txtReajuste.Text, "#0.00")
   cmdRelaciona.Enabled = Libera_Gravacao
   
End Sub

Private Function PreencheGrid(nm_ciclo_operacional As String)
Dim se_Sql As String
Dim sql_cd_ret As Integer
Dim sql_qt_lnh As Integer
Dim work As String

   grdVisao.Visible = False
   
   PreencheGrid = False
   On Error GoTo PreencheGrid
   
   PonteiroMouse vbHourglass
   
   grdVisao.MaxRows = 0
   
   se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
   se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
    'trocado o cd_segmento por 1 - fixo
   se_Sql = se_Sql & " exec  st_pi_estrutura_lista_s " & nm_ciclo_operacional & ", 1, "
   se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
   
   
   If gObjeto.natPrepareQuery(hSql, se_Sql) Then
      Do While gObjeto.natFetchNext(hSql)
      
         grdVisao.MaxRows = grdVisao.MaxRows + 1
         grdVisao.Row = grdVisao.MaxRows
         
         grdVisao.Col = 1
         grdVisao.Text = gObjeto.varResultSet(hSql, 0)
         grdVisao.Lock = True
         
         grdVisao.Col = 2
         grdVisao.Text = gObjeto.varResultSet(hSql, 1)
         grdVisao.Lock = True
         
         grdVisao.Col = 3
         grdVisao.Text = gObjeto.varResultSet(hSql, 2)
         grdVisao.Lock = True
         
         grdVisao.Col = 4
         grdVisao.Text = gObjeto.varResultSet(hSql, 3)
         grdVisao.Lock = True
         
      Loop
   End If
   
   If gObjeto.nErroSQL <> 0 Then
      MSG$ = "Erro ao executar a PreencheGrid " & gObjeto.nServerNum & gObjeto.sServerMsg
      grdVisao.Visible = True
      PonteiroMouse vbNormal
      Exit Function
   End If
    
'   sql_cd_ret = CInt(gObjeto.varOutputParam(0))
'   sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
'   If sql_cd_ret <> 0 Then
'      MSG$ = "Erro ao executar a PreencheGrid " & "RETURN STATUS = " & sql_cd_ret
'      PonteiroMouse vbNormal
'      grdVisao.Visible = True
'      Exit Function
'   End If
   
   grdVisao.Row = 1
   grdVisao.Col = 1
   grdVisao.Row2 = grdVisao.MaxRows
   grdVisao.Col2 = 4
   grdVisao.SortBy = 0
   grdVisao.SortKey(1) = 1
   grdVisao.SortKey(2) = 2
   grdVisao.SortKey(3) = 3
'   grdVisao.SortKey(4) = 4
   grdVisao.SortKeyOrder(1) = 1
   grdVisao.SortKeyOrder(2) = 1
   grdVisao.SortKeyOrder(3) = 2
'   grdVisao.SortKeyOrder(4) = 1
   grdVisao.Action = SS_ACTION_SORT
   
   PreencheGrid = True
   PonteiroMouse vbNormal
   grdVisao.Visible = True
   
   Exit Function
   
PreencheGrid:
   MSG$ = "PreencheGrid - Erro (" & Err & ") - " & Error
   PonteiroMouse vbNormal
   grdVisao.Visible = True
   Exit Function

End Function

Public Function Libera_Gravacao() As Boolean

   If cmbCiclo.Text <> "" And _
      cmbRegEstrat.ListIndex <> -1 And _
      cmbListas.Text <> "" And _
      txtReajuste.Text <> "" Then
      Libera_Gravacao = True
   Else
      Libera_Gravacao = False
   End If
   
End Function

Private Function Relacao_Lista(nm_ciclo_operacional As String, _
                                cd_tipo_estrutura_comercial As String, _
                                cd_estrutura_comercial As String, _
                                cd_lista As String, _
                                pc_reajuste_lista As String, _
                                operacao As String) As Boolean
Dim se_Sql      As String
Dim sql_cd_ret  As Integer
Dim sql_qt_lnh  As Integer
Dim work        As String
Dim se_Usuario  As String
Dim ne_ReajCF   As Double
Dim ne_gravacf  As Boolean
Dim ne_ret      As Long
    ne_gravacf = True
    Relacao_Lista = False
    
    If operacao = "I" Then
        'AGUARDANDO PARA VER SE VOU CONTINUAR VENDO O PERC. PARA CF
        'Calcula percentual de reajuste do CF
        'work = Le_pi_parametro(11, 5)
        'ne_ReajCF = CDbl(ConvertePontoDecimal(work, Virgula))
        'ne_ReajCF = CDbl(pc_reajuste_lista) * (ne_ReajCF / 100)
        
        ' Grava CF
        'If Not Cabecalho_Lista(nm_ciclo_operacional, cd_Lista, 0, CStr(ne_ReajCF), 2) Then
        If Not Cabecalho_Lista(nm_ciclo_operacional, cd_lista, 0, pc_reajuste_lista, 2) Then
            Exit Function
        End If
                    
        If Not Cabecalho_Lista(nm_ciclo_operacional, cd_lista, 0, pc_reajuste_lista, 1) Then
            Exit Function
        End If
    End If
    
   
    On Error GoTo Relacao_Lista
    
    PonteiroMouse vbHourglass
    
    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    
    
    se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
    se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
    se_Sql = se_Sql & " exec st_pi_relac_lista_cn_m " & nm_ciclo_operacional & ", "
    se_Sql = se_Sql & cd_tipo_estrutura_comercial & ", "
    se_Sql = se_Sql & cd_estrutura_comercial & ", "
    se_Sql = se_Sql & cd_lista & ", "
    'TROCADO O CODIGO DO SEGMENTO POR 1 - FIXO
    se_Sql = se_Sql & "1, '"
    se_Sql = se_Sql & operacao & "', '"
    se_Sql = se_Sql & se_Usuario & "', "
    se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
    
    If gObjeto.natPrepareQuery(hSql, se_Sql) Then
        Do While gObjeto.natFetchNext(hSql)
        Loop
    End If
   
    If gObjeto.nErroSQL <> 0 Then
        MSG$ = "Erro ao executar a Relacao_Lista " & gObjeto.nServerNum & gObjeto.sServerMsg
        PonteiroMouse vbNormal
        Exit Function
    End If
    
    sql_cd_ret = CInt(gObjeto.varOutputParam(0))
    sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
    
    If sql_cd_ret <> 0 Then
        MSG$ = "Erro ao executar a Relacao_Lista " & gObjeto.varOutputParam(4)
        PonteiroMouse vbNormal
        Exit Function
    End If
   
    ' Grava estrutura para CF
    If ne_gravacf Then
        se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
        se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
        se_Sql = se_Sql & " exec st_pi_relac_lista_cn_m " & nm_ciclo_operacional & ", "
        se_Sql = se_Sql & cd_tipo_estrutura_comercial & ", "
        se_Sql = se_Sql & cd_estrutura_comercial & ", "
        se_Sql = se_Sql & cd_lista & ", "
        se_Sql = se_Sql & 2 & ", '"
        se_Sql = se_Sql & operacao & "', '"
        se_Sql = se_Sql & se_Usuario & "', "
        se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
        
        If gObjeto.natPrepareQuery(hSql, se_Sql) Then
           Do While gObjeto.natFetchNext(hSql)
           Loop
        End If
        
        If gObjeto.nErroSQL <> 0 Then
           MSG$ = "Erro ao executar a Relacao_Lista " & gObjeto.nServerNum & gObjeto.sServerMsg
           PonteiroMouse vbNormal
           Exit Function
        End If
         
        sql_cd_ret = CInt(gObjeto.varOutputParam(0))
        sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
        
        If sql_cd_ret <> 0 Then
           MSG$ = "Erro ao executar a Relacao_Lista " & gObjeto.varOutputParam(4)
           PonteiroMouse vbNormal
           Exit Function
        End If
    End If
   
    Relacao_Lista = True
    PonteiroMouse vbNormal
    Exit Function
   
Relacao_Lista:
    MSG$ = "Relacao_Lista - Erro (" & Err & ") - " & Error
    PonteiroMouse vbNormal
    Exit Function

End Function

Private Sub sLimpaTela()

   If Not Lista_Estrutura(cmbRegEstrat, 2, 0, 0) Then
      SetStatus "Erro na leitura..."
      MsgBox MSG$, vbCritical + vbOKOnly, "E R R O "
   End If
   
    cmbCiclo.ListIndex = -1
    cmbCiclo.SetFocus
    cmbRegEstrat.ListIndex = -1
    cmbGerVenda.Clear
    cmbGerVenda.ListIndex = -1
    cmbSetor.Clear
    cmbSetor.ListIndex = -1
    cmbListas.Clear
    cmbListas.ListIndex = -1
    txtReajuste.Text = ""
    cmbRegEstrat.Enabled = False
    cmbGerVenda.Enabled = False
    cmbSetor.Enabled = False
    cmbListas.Enabled = False
    cmdRelaciona.Enabled = False
    txtReajuste.Tag = txtReajuste.Text
    
    grdVisao.MaxRows = 0
    'SE 6346
    If Not DesbloqueiaRegistro(8) Then
         MsgBox "Falha ao tentar desbloquear registro.", vbCritical, "Erro"
         Exit Sub
    End If

End Sub

Private Function Cabecalho_Lista(nm_ciclo_operacional As String, _
                                 cd_lista As String, _
                                 id_liberacao_lista As Integer, _
                                 pc_reajuste_lista As String, _
                                 cd_Segmento_canal As Integer) As Boolean
                                 
Dim se_Sql      As String
Dim sql_cd_ret  As Integer
Dim sql_qt_lnh  As Integer
Dim work        As String
Dim se_Usuario  As String
Dim ne_ret      As Long
   
   Cabecalho_Lista = False
   On Error GoTo Cabecalho_Lista

   PonteiroMouse vbHourglass
   
   se_Usuario = Space(255)
   ne_ret = GetUserName(se_Usuario, 255)
   se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
   
   
   se_Sql = "Declare @sql_cd_ret int, @sql_cd_opr char(1), "
   se_Sql = se_Sql & " @sql_nm_tab char(30), @sql_qt_lnh int, @msg_err Varchar(255) "
   se_Sql = se_Sql & " exec st_pi_cabec_lista_cn_m " & nm_ciclo_operacional & ", "
   se_Sql = se_Sql & cd_lista & ", "
   se_Sql = se_Sql & ConvertePontoDecimal(pc_reajuste_lista, 1) & ", "
   se_Sql = se_Sql & cd_Segmento_canal & ", "
   se_Sql = se_Sql & "'" & se_Usuario & "', "
   se_Sql = se_Sql & " @sql_cd_ret out , @sql_cd_opr out , @sql_nm_tab out, @sql_qt_lnh out, @msg_err out"
   
   If gObjeto.natPrepareQuery(hSql, se_Sql) Then
      Do While gObjeto.natFetchNext(hSql)
      Loop
   End If
   
   If gObjeto.nErroSQL <> 0 Then
      MSG$ = "Erro ao executar a Cabecalho_Lista " & gObjeto.nServerNum & gObjeto.sServerMsg
      PonteiroMouse vbNormal
      Exit Function
   End If
    
   sql_cd_ret = CInt(gObjeto.varOutputParam(0))
   sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
   
   If sql_cd_ret <> 0 Then
      MSG$ = "Erro " & sql_cd_ret & " ao executar a Cabecalho_Lista :" & gObjeto.varOutputParam(4)
      PonteiroMouse vbNormal
      Exit Function
   End If
   
   Cabecalho_Lista = True
   PonteiroMouse vbNormal
   Exit Function
   
Cabecalho_Lista:
   MSG$ = "Cabecalho_Lista - Erro (" & Err & ") - " & Error
   PonteiroMouse vbNormal
   Exit Function

End Function

Private Function LiberarListas(nm_ciclo_operacional As String, MSG$) As Boolean
       
Dim sql        As String
Dim msg_err    As String
Dim se_Usuario As String
Dim ne_ret     As Long
Dim segmento   As Integer
Dim cont       As Integer
ReDim LogGeraLista(0)

LiberarListas = False

On Error GoTo ErroLiberarListas

    MSG$ = ""
    
    PonteiroMouse vbHourglass
    lblTipoProcesso.Caption = "Aguarde, Liberando Listas de Pre�os..."
    lblTipoProcesso.Visible = True
    Refresh

    se_Usuario = Space(255)
    ne_ret = GetUserName(se_Usuario, 255)
    se_Usuario = Left(Left(se_Usuario, InStr(se_Usuario, Chr(0)) - 1), 8)
    
    For segmento = 1 To 4
    
        msg_err = ""
                
        sql = "DECLARE @sql_cd_ret int, "
        sql = sql & "@sql_cd_opr char(1), "
        sql = sql & "@sql_nm_tab char(30), "
        sql = sql & "@sql_qt_lnh int, "
        sql = sql & "@msg_err varchar(255)"
        sql = sql & " exec st_pi_cn_gera_lista_cn "
        sql = sql & nm_ciclo_operacional & ","
        sql = sql & segmento & ","
        sql = sql & "'" & se_Usuario & "', "
        sql = sql & "@sql_cd_ret out, "
        sql = sql & "@sql_cd_opr out, "
        sql = sql & "@sql_nm_tab out, "
        sql = sql & "@sql_qt_lnh out, "
        sql = sql & "@msg_err out"
        
        If gObjeto.natPrepareQuery(hSql, sql) Then
           Do While gObjeto.natFetchNext(hSql)
                 
                cont = UBound(LogGeraLista) + 1
                ReDim Preserve LogGeraLista(cont)
                
                If Left(gObjeto.varResultSet(hSql, 3), 1) <> "6" Then
                    LogGeraLista(cont).cd_material = gObjeto.varResultSet(hSql, 0)
                    LogGeraLista(cont).cd_venda = gObjeto.varResultSet(hSql, 1)
                    LogGeraLista(cont).cd_lista = gObjeto.varResultSet(hSql, 2)
                    LogGeraLista(cont).cd_segmento = gObjeto.varResultSet(hSql, 3)
                    LogGeraLista(cont).descricao = gObjeto.varResultSet(hSql, 5)
                End If
                 
           Loop
           
        End If
        
        msg_err = gObjeto.varOutputParam(4)
        
        If gObjeto.nErroSQL <> 0 Then
            MSG$ = "Erro ao executar a st_pi_cn_gera_lista_cn " & gObjeto.nServerNum & " = " & gObjeto.sServerMsg & " -  " & msg_err
            Exit Function
        End If
        
        sql_cd_ret = CInt(gObjeto.varOutputParam(0))
        
        If sql_cd_ret <> 0 Then
            MSG$ = "Erro ao executar a st_pi_cn_gera_lista_cn = " & gObjeto.nServerNum & " - " & gObjeto.sServerMsg & "  " & msg_err
            Exit Function
        End If
        
        sql_qt_lnh = CLng(gObjeto.varOutputParam(3))
        
    Next segmento
        
    PonteiroMouse vbNormal
    

    LiberarListas = True
    
    Exit Function

ErroLiberarListas:
    MSG$ = "LiberarListas - Erro (" & Err & ") - " & Error
    Exit Function

End Function

Private Sub ExibeLog()

    Dim TelaLog As Form
    Dim cont As Integer
    
    If UBound(LogGeraLista) > 0 Then
    
        MsgBox "Ocorreram inconsist�ncias ao liberar lista de pre�os", vbInformation, "Aten��o"
    
        PonteiroMouse vbHourglass
    
        Set TelaLog = New frmLogLista
        
        Load TelaLog
        DoEvents
        
        For cont = 1 To UBound(LogGeraLista)
            TelaLog.CriaLinha
            
            TelaLog.RecebeDados 1, LogGeraLista(cont).cd_material
            TelaLog.RecebeDados 2, LogGeraLista(cont).cd_venda
            TelaLog.RecebeDados 3, LogGeraLista(cont).cd_lista
            TelaLog.RecebeDados 4, LogGeraLista(cont).cd_segmento
            TelaLog.RecebeDados 5, LogGeraLista(cont).descricao
        Next cont
        
        PonteiroMouse vbNormal
        
        TelaLog.Show
        
    End If
    
End Sub

Private Function TotalPrecos(nm_ciclo_operacional As String) As String

Dim cSql  As String
Dim bResp As Boolean
Dim i     As Integer
Dim avPrecos()       As Variant

On Error GoTo erro

' Carrega Ciclo
cSql = "SELECT COUNT(*) FROM t_pi_preco_cn WHERE nm_ciclo_operacional = " & nm_ciclo_operacional

ReDim avPrecos(0)
   
bResp = gObjeto.natExecuteQuery(hSql, cSql, avPrecos())
If bResp = False Then
  MsgBox gObjeto.nErroSQL & " " & gObjeto.sServerMsg, vbCritical
  End
End If

If gObjeto.nErroSQL <> 0 Then
  MsgBox gObjeto.sClientMsg & " " & gObjeto.sServerMsg, vbCritical
  Exit Function
End If

TotalPrecos = avPrecos(0, i)

Exit Function

erro:
  MsgBox "Erro ao obter total de pre�os gerados " & Err.Number & "-" & Err.Description, vbCritical, Me.Caption
  Exit Function
  
End Function

