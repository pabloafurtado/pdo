VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{04EACA3F-7311-11D2-874B-00104B24CEE0}#1.1#0"; "CORPSYB1.OCX"
Begin VB.MDIForm mdiPIO001 
   BackColor       =   &H8000000C&
   Caption         =   "Cadastro de Produtos e Pre�os - Natura"
   ClientHeight    =   5910
   ClientLeft      =   2985
   ClientTop       =   3225
   ClientWidth     =   9810
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar barMDI 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9810
      _ExtentX        =   17304
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   11
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Abrang�ncia/Vigencia - Comercial"
            Object.Tag             =   ""
            ImageIndex      =   11
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            ImageIndex      =   12
            Style           =   3
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Produtos Ativos no Ciclo"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Abrang�ncia/vig�ncia PCL"
            Object.Tag             =   ""
            ImageIndex      =   14
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Produtos Pendentes - PCL"
            Object.Tag             =   ""
            ImageIndex      =   15
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Descri��o Capta/CAN"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
      MouseIcon       =   "MDIcdpp.frx":0000
      Begin NaturaCorpSyb1.CorpSyb1 CorpNat 
         Height          =   600
         Left            =   5580
         TabIndex        =   1
         Top             =   150
         Visible         =   0   'False
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   1058
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   1140
      Top             =   2880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   15
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":0F82
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":129C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":15B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":18D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":1BEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":1F04
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":3CBA
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":450C
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":54DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":62B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIcdpp.frx":65D2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuProdutos 
      Caption         =   "&Produtos"
      Begin VB.Menu mnuManutencao 
         Caption         =   "&Manuten��es"
         Begin VB.Menu mnuVigenciaProduto 
            Caption         =   "Abrang/Vig�ncia &Comercial"
         End
         Begin VB.Menu mnuDivisaoBoleto 
            Caption         =   "&Divis�o de Boletos"
         End
         Begin VB.Menu mnuBarra1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuVigenciaMaterial 
            Caption         =   "Abrang/Vig�ncia &PCL"
         End
         Begin VB.Menu mnuBarra2 
            Caption         =   "-"
         End
         Begin VB.Menu mnuDescricaoCapta 
            Caption         =   "&Descri��o Capta/CAN"
         End
      End
      Begin VB.Menu mnuConsultas 
         Caption         =   "&Consultas"
         Begin VB.Menu mnuProdAtivo 
            Caption         =   "&Produtos Ativos no Ciclo"
         End
         Begin VB.Menu mnuBarra3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPendencia 
            Caption         =   "P&end�ncias PCL"
         End
      End
   End
   Begin VB.Menu mnuPreco 
      Caption         =   "P&re�os"
      Enabled         =   0   'False
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "mdiPIO001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub BloqueiaMenu()

    MDIcdpp.barMDI.Enabled = False
    MDIcdpp.mnuProdutos.Enabled = False
    MDIcdpp.mnuPreco.Enabled = False
    MDIcdpp.mnuSair.Enabled = False

End Sub

Public Sub DesbloqueiaMenu()

    MDIcdpp.barMDI.Enabled = True
    MDIcdpp.mnuProdutos.Enabled = True
'    MDIcdpp.mnuPreco.Enabled = True
    MDIcdpp.mnuSair.Enabled = True

End Sub

Private Sub barMDI_ButtonClick(ByVal Button As ComctlLib.Button)

    MDIcdpp.barMDI.Enabled = False
    MDIcdpp.mnuProdutos.Enabled = False
    MDIcdpp.mnuPreco.Enabled = False
    MDIcdpp.mnuSair.Enabled = False
    
    Select Case Button.Index
        Case 1
            mnuVigenciaProduto_Click
        Case 2
            mnuDivisaoBoleto_Click
        Case 3
            mnuProdAtivo_Click
        Case 5
            mnuVigenciaMaterial_Click
        Case 6
            mnuPendencia_Click
        Case 8
            mnuDescricaoCapta_Click
        Case 11
            mnuSair_Click
    End Select
  
End Sub

Private Sub MDIForm_Load()

  'Configura apresenta��o da tela -------------------------------------------
  MDIcdpp.WindowState = 2
 
  'Segmentos de canal que ter�o relacionamento de venda
  'N�o existe mais o segmento 3
   segmentos = Array(1, 2, 3, 5, 4)
   
   gCarregaCiclo = False
  
End Sub

Private Sub mnuDescricaoCapta_Click()

    BloqueiaMenu
    frmDescricaoCapta.Show

End Sub

Private Sub mnuDivisaoBoleto_Click()

    BloqueiaMenu
    frmDivisaoBoleto.Show

End Sub

Private Sub mnuPendencia_Click()

    BloqueiaMenu
    frmPendenciaPCL.Show

End Sub

Private Sub mnuProdAtivo_Click()

    BloqueiaMenu
    frmRelProdAtivo.Show

End Sub

Private Sub mnuSair_Click()

    Unload Me

End Sub

Private Sub mnuVigenciaMaterial_Click()
    
    BloqueiaMenu
    
    frmVigenciaPCL.Show
    
End Sub

Private Sub mnuVigenciaProduto_Click()

    BloqueiaMenu
    frmVigenciaComercial.Show
    
End Sub
