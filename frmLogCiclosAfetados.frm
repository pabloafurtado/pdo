VERSION 5.00
Object = "{B02F3647-766B-11CE-AF28-C3A2FBE76A13}#2.5#0"; "SS32X25.OCX"
Begin VB.Form frmLogCiclosAfetados 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ciclos afetados ap�s altera��o de vig�ncia comercial"
   ClientHeight    =   5760
   ClientLeft      =   555
   ClientTop       =   4140
   ClientWidth     =   11940
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   11940
   ShowInTaskbar   =   0   'False
   Begin FPSpread.vaSpread grd_inconsistencia 
      Height          =   4575
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   11805
      _Version        =   131077
      _ExtentX        =   20823
      _ExtentY        =   8070
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8,25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   4
      MaxRows         =   0
      OperationMode   =   2
      RetainSelBlock  =   0   'False
      ScrollBarExtMode=   -1  'True
      ScrollBarMaxAlign=   0   'False
      ScrollBarShowMax=   0   'False
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmLogCiclosAfetados.frx":0000
      UserResize      =   1
      VisibleCols     =   500
      VisibleRows     =   500
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   60
      TabIndex        =   2
      Top             =   4950
      Width           =   11775
      Begin VB.CommandButton cmdOK 
         Caption         =   "&Fechar"
         Default         =   -1  'True
         Height          =   435
         Left            =   9930
         TabIndex        =   3
         Top             =   210
         Width           =   1725
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "As informa��es listadas abaixo devem ser verificadas devido a altera��o efetuada."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   90
      TabIndex        =   1
      Top             =   90
      Width           =   7065
   End
End
Attribute VB_Name = "frmLogCiclosAfetados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nRow As Long

Public Sub CriaLinha()
    nRow = grd_inconsistencia.MaxRows + 1
    grd_inconsistencia.MaxRows = nRow
    grd_inconsistencia.Row = nRow
End Sub

Public Sub RecebeDados(pCol As Integer, pDados As String)

    With grd_inconsistencia
        nRow = .MaxRows
        .Row = nRow
        .Col = pCol
        .Text = pDados
    End With
        
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub


